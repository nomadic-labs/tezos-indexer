# Open Source License
# Copyright (c) 2019-2022 Nomadic Labs <contact@nomadic-labs.com>
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
# DEALINGS IN THE SOFTWARE.

ARG BASE_IMAGE=registry.gitlab.com/tezos/opam-repository:runtime-build-dependencies--f84b4412bf92f81fa4d554aed624fa0a6a72ec96 as build
# cf. grep 'export opam_repository_tag=' tezos/scripts/version.sh | sed -e 's/.*=//'
FROM $BASE_IMAGE as build

USER root
RUN apk add opam alpine-sdk m4 gmp-dev postgresql-dev perl libev-dev libffi libffi-dev postgresql-client coreutils && apk add --no-cache -X http://dl-cdn.alpinelinux.org/alpine/edge/community hidapi-dev

COPY .git /home/tezos/.git
RUN chown tezos -R /home/tezos/.git

USER tezos
WORKDIR /home/tezos
ENV OPAMYES=true

RUN opam remote add official https://opam.ocaml.org
RUN git checkout .
RUN make tezos -f tezos.Makefile
RUN eval $(opam env) && make install-minimal-opam-deps -f opam.Makefile
RUN eval $(opam env) && opam list && make protos && make build && ln -f ./_build/default/src/bin_indexer/main_indexer.exe tezos-indexer && /home/tezos/tezos-indexer --version && opam clean

RUN ./tezos-indexer --help

ENTRYPOINT ["/home/tezos/utils/run-tezos-indexer-multicore.bash"]
