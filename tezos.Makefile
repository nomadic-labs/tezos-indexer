# Open Source License
# Copyright (c) 2021-2022 Nomadic Labs <contact@nomadic-labs.com>
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
# DEALINGS IN THE SOFTWARE.


SHELL=/bin/bash

OCTEZ_COMMIT = 10b119feab01be49e1c38ff61469d3e1f8480a6e

tezos:
#	git clone https://gitlab.com/tezos/tezos.git -b v12-release --single-branch
#	git clone https://gitlab.com/tezos/tezos.git -b master --single-branch && cd tezos && git checkout ${OCTEZ_COMMIT}
	git clone https://gitlab.com/tezos/tezos.git && cd tezos && git checkout ${OCTEZ_COMMIT}
#	git clone https://gitlab.com/tezos/tezos.git -b master --single-branch
#	find ./tezos/src/proto_*/lib_protocol/ -name alpha_context.mli -exec sed -i.back -e 's/type balance_update_item = private/type balance_update_item =/' {} \;
	find ./tezos/src/proto_*/lib_protocol/ -name receipt_repr.ml -exec sed -i.back -e 's/\\xEA\\x9C\\xA9//' {} \;
	find ./tezos/src/proto_*/lib_protocol/ -name voting_period_repr.ml -exec sed -i.bak -e 's/@ ,/,@ /g' -e 's/kind:%a@,/kind:%a,@ /g' -e 's/%ld@, /%ld,@ /g' -e 's/,@  /,@ /g' {} \;
	find ./tezos/src/proto_*/lib_protocol/ -name voting_period_repr.ml.bak -delete

update-tezos:
	(cd tezos ; git pull ; git show)
	@sed -ie "s/^OCTEZ_COMMIT.*/$$(cd tezos && git show | head -n 1 | sed -e 's/commit/OCTEZ_COMMIT ='/)/" tezos.Makefile
	@grep -q $$(grep 'ARG BASE_IMAGE=registry.gitlab.com/tezos/opam-repository:runtime-build-dependencies' Dockerfile|sed -e 's|ARG BASE_IMAGE=registry.gitlab.com/tezos/opam-repository:runtime-build-dependencies--\(.*\) as build.*|\1|') tezos/scripts/version.sh || (echo 'docker base has changed, press enter to run `sudo make update-opam-list`' ; read && sudo make update-opam-list)
	@if (( $$(git diff tezos.Makefile|wc -l) > 0 )) ; then echo 'press enter to run `make alpha`, or Ctrl-c to abort' && read && make alpha ; else echo "octez has had no new commit"; fi
