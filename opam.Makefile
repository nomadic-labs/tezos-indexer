# Open Source License
# Copyright (c) 2021-2022 Nomadic Labs <contact@nomadic-labs.com>
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
# DEALINGS IN THE SOFTWARE.


SHELL=/bin/bash

# TEZOS_INDEXER_OPAM_DEPS := caqti.1.8.0 caqti-driver-postgresql caqti-dynload caqti-lwt cmdliner conf-postgresql.1 mpp.0.3.5 uucd
TEZOS_INDEXER_OPAM_DEPS := mpp.0.3.5 uucd lwt.5.7.0

install-opam-deps:
	opam install $$(cat opam-list) ${TEZOS_INDEXER_OPAM_DEPS}

install-minimal-opam-deps:
	opam install ${TEZOS_INDEXER_OPAM_DEPS}

install-opam-extra:
	opam install merlin tuareg ocp-indent

.PHONY: install-opam-deps install-opam-deps

update-opam-list:
	eval $$(grep 'opam_repository_tag=' tezos/scripts/version.sh) && \
	(docker run -t --entrypoint=/bin/bash registry.gitlab.com/tezos/opam-repository:runtime-build-dependencies--$${opam_repository_tag}  -c "opam list | grep -v '#' | awk '{print \$$1\".\"\$$2}'" | tr -d '\r') > opam-list.tmp ; ( (( "$$(wc -l < opam-list.tmp)" > 0 )) && mv opam-list.tmp opam-list)
