(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2022 Nomadic Labs, <contact@nomadic-labs.com>               *)
(* Copyright (c) 2022 Philippe Wang, <philippe.wang@nomadic-labs.com>        *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

(**
   - stdin = base job
   - argv.(1) = number of parallel jobs
   - argv.(2) = logs directory
   - env(top) = target block
   - env(bottom) = resuming if set to resume (otherwise may crash)
   - env(TEZOS_NODES) = list of nodes
*)

module Print = struct
  let escape = Char.chr 0o33

  let print kind s = Printf.printf "%c[%dm%s%c[0m\n%!" escape kind s escape
  let printf kind s = Format.printf "%c[%dm%a%c[0m\n%!" escape kind Format.fprintf s escape
  let sprint kind s = Printf.sprintf "%c[%dm%s%c[0m" escape kind s escape

  let bold = print 1
  let fail = print 91
  let header = print 95
  let blue = print 94
  let cyan = print 96
  let green = print 92
  let underline = print 4
  let warning = print 93

  module Test () = struct
    let _ =  bold "plop"
    let _ =  fail "plop"
    let _ =  header "plop"
    let _ =  blue "plop"
    let _ =  cyan "plop"
    let _ =  green "plop"
    let _ =  underline "plop"
    let _ =  warning "plop"
  end

end

let int_of_filecontents file =
  let ic = open_in file in
  let f = input_line ic in
  close_in ic;
  int_of_string f

let extract_urls ns =
  String.split_on_char ',' ns
  |> List.map (String.split_on_char ' ')
  |> List.flatten
  |> List.map (String.split_on_char ',')
  |> List.flatten
  |> List.map (String.split_on_char ';')
  |> List.flatten
  |> List.map (String.split_on_char '\n')
  |> List.flatten
  |> List.map (String.split_on_char '\t')
  |> List.flatten
  |> List.map (String.split_on_char '\r')
  |> List.flatten
  |> List.filter ((<>) "")

module M = Map.Make (struct type t = int * int let compare = compare end)

let logs_dir = Sys.argv.(2)

type balancing = Balanced | Predetermined | Default

let balancing = match Sys.getenv_opt "BALANCING" with
  | Some "balanced" -> Balanced
  | Some "predetermined" -> Predetermined
  | _ -> Default

let nodes =
  let l =
    match Sys.getenv_opt "TEZOS_NODES" with
    | None -> prerr_endline "TEZOS_NODES is not set"; exit 42
    | Some ns -> extract_urls ns
  in
  let counter = ref 0 in
  ref
    (List.fold_left
       (fun r e -> incr counter; M.add (0, !counter) e r)
       M.empty
       l)

let nodes_array = Array.of_list (M.fold (fun _k v r -> v::r)  !nodes [])

let nodes_mutex = Mutex.create ()

let psql = match Sys.getenv_opt "PSQL" with
  | None -> ""
  | Some psql -> psql


let bottom = match Sys.getenv_opt "bottom" with
  | None
  | Some "" -> 0
  | Some l ->
    try int_of_string l with
    | _ ->
      match
        Sys.command
        @@ Printf.sprintf "%s -tc 'select level from c.block order by level desc limit 1;' > bottom.tmp" psql
      with
      | 0 -> int_of_filecontents "bottom.tmp"
      | _ -> 0


module Mutex = struct
  include Mutex
  let lock ~__LOC__ m =
    if false then Printf.eprintf "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Mutex.lock @ %s\n%!" __LOC__;
    lock m
end

let pick_node = ref (fun () ->
    Mutex.lock ~__LOC__ nodes_mutex;
    let ((n, id) as k), url = M.min_binding !nodes in
    nodes := M.add (succ n, id) url (M.remove k !nodes);
    let done_with_node () =
      Mutex.lock ~__LOC__ nodes_mutex;
      let new_counter = ref 0 in
      let filtered =
        (M.filter
           (fun (n, c_id) _ ->
              if c_id = id then
                new_counter := max 0 (pred n);
              c_id <> id)
           !nodes)
      in
      nodes := M.add (!new_counter, id) url filtered;
      Mutex.unlock nodes_mutex
    in
    Mutex.unlock nodes_mutex;
    (fun () -> url), done_with_node
  )

let jobs = Event.new_channel ()

let no_more_jobs = ref false

let number_of_workers =
  try int_of_string Sys.argv.(1)
  with _ -> failwith "Cannot parse number of jobs from Sys.argv.(1)"

let number_of_jobs = ref 0

let get_job_base () =
  let r = Buffer.create 1024 in
  try
    while true do
      read_line () |> Buffer.add_string r;
      Buffer.add_char r ' '
    done;
    assert false
  with End_of_file ->
    Buffer.contents r

let all_events_sent = Mutex.create ()

let get_jobs () =
  let () = Mutex.lock ~__LOC__ all_events_sent in
  let base = get_job_base () in
  let i = number_of_jobs in
  let top = try Sys.getenv "top" |> int_of_string with _ -> assert false in
  let assign_chunk base target chunk =
    incr i;
    let (w, id, from, upto) as j =
      base, (if target = 2 then 0 else (max 2 (target - chunk))), target, !i
    in
    (* Printf.eprintf "# %d-----%S\n%!" id w; *)
    j
  in
  let rec make top =
    if top mod 1000 <> 0 then
      assign_chunk base top (top mod 1000)
      :: make (top / 1000 * 1000)
    else if top <= 0 then
      []
    else
      let chunk = 2_000 in
      assign_chunk base top chunk
      :: make (top - chunk)
  in
  (* let rec make top =
   *   if top mod 1000 <> 0 then
   *     assign_chunk base top (top mod 1000)
   *     :: make (top / 1000 * 1000)
   *   else if top > 1200000 then
   *     let chunk = 1000 in
   *     assign_chunk base top chunk
   *     :: make (top - chunk)
   *   else if top > 200_000 then
   *     let chunk = 2_000 in
   *     assign_chunk base top chunk
   *     :: make (top - chunk)
   *   else if top <= 0 then
   *     []
   *   else
   *     let chunk = 2_000 in
   *     assign_chunk base top chunk
   *     :: make (top - chunk)
   * in *)
  let assignments = assign_chunk base 2 2 :: make top in
  List.iter (fun a -> Event.send jobs a |> Event.sync) assignments;
  Mutex.unlock all_events_sent


let () = print_endline "read jobs"
let get_jobs_thread = Thread.create get_jobs ()
let () = print_endline "jobs read"

let () = print_endline "create worker array"

let command cmd =
  (* This is to replace Sys.command, which doesn't kill its child process automatically when the main process exits  *)
  match Unix.fork () with
  | 0 ->
    Unix.execv "/bin/bash" [| "/bin/bash" ; "-c" ; cmd |]
  | child ->
    match Unix.waitpid [] child with
    | pid, Unix.WEXITED 0 when pid = child -> 0
    | pid, Unix.WEXITED n when pid = child -> Unix._exit n
    | _ -> Unix._exit 43

(* those unicode emojis cause trouble in Emacs when inlined *)
let emoji_start = "🎬"
let emoji_ok = "✅"
let emoji_error = "😨️"
let emoji_worker = "👷"
let emoji_thread = "🧶"

let rec one_thread i =
  match Event.receive jobs |> Event.poll with
  | None ->
    if Mutex.try_lock all_events_sent then
      Mutex.unlock all_events_sent
    else
      let () = Unix.sleepf 0.1 in
      one_thread i
  | Some (j, from, upto, line) ->
    let node, done_with_node =
      match balancing with
      | Default
      | Predetermined ->
        (fun () -> (nodes_array.(line mod Array.length nodes_array))), (fun () -> ())
      | Balanced -> !pick_node () in
    let cmd = Printf.sprintf "%s --force-from=%d --bootstrap-upto=%d --tezos-url %s > %s/indexer-%d.log 2> %s/indexer-%d.error.log" j from upto (node ()) logs_dir line logs_dir line in
    let start = Unix.gettimeofday () in
    match
      Print.bold @@ Printf.sprintf "%s[%s%d(%s%d)] %S" emoji_start emoji_worker line emoji_thread i cmd;
      command cmd
    with
    | 0 ->
      Print.blue @@ Printf.sprintf "%s[%s%d/%d(%s%d)] %S terminated successfully in %f seconds" emoji_ok emoji_worker line !number_of_jobs emoji_thread i cmd (Unix.gettimeofday () -. start);
      done_with_node ();
      one_thread i
    | n ->
      Print.fail @@
      Printf.sprintf "%s[%s%d/%d(%s%d)] %S terminated with error code %d after %f seconds%s" emoji_error emoji_worker line !number_of_jobs emoji_thread i cmd n (Unix.gettimeofday () -. start)
        (if psql <> "" then
           ""
         else
           " -- will try to recover!"
        );
      if psql <> ""
      && Sys.command (Printf.sprintf "%s -tc 'select level+1 from c.block where level > %d order by level asc limit 1' > %s/indexer-%d.recover_from" psql from logs_dir from) <> 0
      && Sys.command (Printf.sprintf "%s -tc 'select level-1 from c.block where level < %d order by level desc limit 1' > %s/indexer-%d.recover_upto" psql upto logs_dir upto) <> 0
      then
        let from =
          int_of_filecontents @@ Printf.sprintf "%s/indexer-%d.recover_from" logs_dir from
        and upto =
          int_of_filecontents @@ Printf.sprintf "%s/indexer-%d.recover_upto" logs_dir upto
        in
        let cmd = Printf.sprintf "%s --force-from=%d --bootstrap-upto=%d --tezos-url %s > %s/indexer-%d.log 2> %s/indexer-%d.error.log" j from upto (node ()) logs_dir line logs_dir line in
        begin match
            Print.bold @@ Printf.sprintf "%s[%s%d(%s%d)] %S" emoji_start emoji_worker line emoji_thread i cmd;
            command cmd
          with
          | 0 ->
            Print.blue @@ Printf.sprintf "%s[%s%d/%d(%s%d)] %S terminated successfully in %f seconds" emoji_ok emoji_worker line !number_of_jobs emoji_thread i cmd (Unix.gettimeofday () -. start);
            done_with_node ();
            one_thread i
          | n ->
            Print.fail @@
            Printf.sprintf "%s[%s%d/%d(%s%d)] %S failed again, with error code %d after %f seconds" emoji_error emoji_worker line !number_of_jobs emoji_thread i cmd n (Unix.gettimeofday () -. start);
            pick_node := (fun () -> exit n);
            exit n
        end;
        done_with_node ();
        one_thread i
      else
        (pick_node := (fun () -> exit n);
         exit n)

let _ =
  print_endline "create worker poll";
  Array.iter Thread.join (Array.init number_of_workers (fun i -> Thread.create one_thread i));
  Thread.join get_jobs_thread;
  exit 0
