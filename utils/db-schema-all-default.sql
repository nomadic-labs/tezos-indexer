-- src/db-schema/versions.sql
-- Open Source License
-- Copyright (c) 2019-2020 Nomadic Labs <contact@nomadic-labs.com>
--
-- Permission is hereby granted, free of charge, to any person obtaining a
-- copy of this software and associated documentation files (the "Software"),
-- to deal in the Software without restriction, including without limitation
-- the rights to use, copy, modify, merge, publish, distribute, sublicense,
-- and/or sell copies of the Software, and to permit persons to whom the
-- Software is furnished to do so, subject to the following conditions:
--
-- The above copyright notice and this permission notice shall be included
-- in all copies or substantial portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
-- IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
-- FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
-- THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
-- LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
-- FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
-- DEALINGS IN THE SOFTWARE.

\set ON_ERROR_STOP on

SELECT 'versions.sql' as file;


DO $$
BEGIN
  IF EXISTS (SELECT 1 FROM pg_tables where tablename = 'block')
  AND NOT EXISTS (SELECT 1 FROM pg_tables where tablename = 'indexer_version') THEN
    raise 'You seem to be running a non compatible version of the indexer';
  END IF;
END
$$;

----------------------------------------------------------------------
-- VERSION HISTORY
----------------------------------------------------------------------

CREATE TABLE IF NOT EXISTS indexer_version (
     version text not null -- the current version
   , new_tables text not null -- the most recent version where new tables were introduced
   , new_columns text not null -- the most recent version where new columns were introduced
   , alter_types text not null -- the most recent version where some types were altered
   , build text -- result of `git describe --tags`
   , dev bool not null -- should be set to true, except for released versions
   , multicore bool not null
   , conversion_in_progress bool not null default false
   , autoid SERIAL UNIQUE
);

DO $$
BEGIN
 ALTER TABLE indexer_version DROP CONSTRAINT indexer_version_pkey;
 EXCEPTION WHEN OTHERS THEN RETURN;
END; $$;

UPDATE indexer_version SET build = concat('v', version) where build = '';

DO $$
BEGIN
 CREATE UNIQUE INDEX IF NOT EXISTS indexer_version_build on indexer_version using btree (build);
 EXCEPTION WHEN OTHERS THEN RETURN;
END; $$;

DO $$
BEGIN
 CREATE INDEX IF NOT EXISTS indexer_version_version on indexer_version using btree (version);
 EXCEPTION WHEN OTHERS THEN RETURN;
END; $$;

CREATE OR REPLACE FUNCTION update_indexer_version ()
RETURNS VOID
AS $$
BEGIN
  INSERT INTO indexer_version (version,new_tables,new_columns,alter_types,build,dev,multicore) VALUES (
     '10.6.4' -- version
   , '10.6.4' -- new_tables
   , '10.6.4' -- new_columns
   , '10.6.4' -- alter_types
   , 'v10.6.3-2-g2271a0c' -- build
   , false -- dev
   , true -- multicore
   ) ON CONFLICT (build) DO NOTHING;
END
$$ LANGUAGE PLPGSQL;

DO $$
BEGIN
  IF (SELECT count(*) FROM indexer_version WHERE version < '10.0.0') > 0
  THEN
    RAISE 'You already have a non-compatible schema.';
  END IF;

  IF (SELECT count(*) FROM indexer_version WHERE conversion_in_progress) > 0
  THEN
    RAISE 'You have an unreliable DB: it was partially created or it was broken during a conversion from multicore mode to default mode';
  END IF;
  UPDATE indexer_version SET conversion_in_progress = true WHERE multicore; --SEQONLY
END $$;
-- src/db-schema/schemas.sql
-- Open Source License
-- Copyright (c) 2021-2022 Nomadic Labs <contact@nomadic-labs.com>
--
-- Permission is hereby granted, free of charge, to any person obtaining a
-- copy of this software and associated documentation files (the "Software"),
-- to deal in the Software without restriction, including without limitation
-- the rights to use, copy, modify, merge, publish, distribute, sublicense,
-- and/or sell copies of the Software, and to permit persons to whom the
-- Software is furnished to do so, subject to the following conditions:
--
-- The above copyright notice and this permission notice shall be included
-- in all copies or substantial portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
-- IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
-- FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
-- THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
-- LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
-- FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
-- DEALINGS IN THE SOFTWARE.

SELECT 'schemas.sql' as file;

-- Note that although we use upper case for schema names in our code,
-- Postgres doesn't see them any differently than if they were in lower case.
-- Moreover, if you are using psql and want autocompletion, upper case may not work
-- and you may have to use lower case notation.
-- The upper case is used to facilitate refactoring when need be and reading.


-- Blockchain's core data
create schema if not exists C;

-- Insertion functions
create schema if not exists I;

-- Pre-Insertion functions (insertion of incomplete rows)
create schema if not exists H;

-- Update functions
create schema if not exists U;

-- Get functions
create schema if not exists G;

-- Mempool
create schema if not exists M;

-- Tokens
create schema if not exists T;

-- Bigmaps
create schema if not exists B;

-- Features
create schema if not exists F;
-- src/db-schema/config.sql
-- Open Source License
-- Copyright (c) 2021 Nomadic Labs <contact@nomadic-labs.com>
--
-- Permission is hereby granted, free of charge, to any person obtaining a
-- copy of this software and associated documentation files (the "Software"),
-- to deal in the Software without restriction, including without limitation
-- the rights to use, copy, modify, merge, publish, distribute, sublicense,
-- and/or sell copies of the Software, and to permit persons to whom the
-- Software is furnished to do so, subject to the following conditions:
--
-- The above copyright notice and this permission notice shall be included
-- in all copies or substantial portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
-- IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
-- FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
-- THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
-- LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
-- FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
-- DEALINGS IN THE SOFTWARE.

CREATE TABLE IF NOT EXISTS F.config (
  feature varchar(80) primary key,
  inactive bool not null
);

-- default values:
INSERT INTO F.config (feature, inactive) VALUES ('postmulticore_address_extraction', true) ON CONFLICT DO NOTHING;
INSERT INTO F.config (feature, inactive) VALUES ('postmulticore_contract_balance', true) ON CONFLICT DO NOTHING;
INSERT INTO F.config (feature, inactive) VALUES ('postmulticore_bigmap_copy', true) ON CONFLICT DO NOTHING;


CREATE OR REPLACE FUNCTION F.enable (feature_ varchar)
RETURNS VOID
AS $$
  INSERT INTO F.config VALUES (feature_, false) ON CONFLICT (feature) DO UPDATE SET inactive = false;
$$ LANGUAGE SQL;


CREATE OR REPLACE FUNCTION F.disable (feature_ varchar)
RETURNS VOID
AS $$
  INSERT INTO F.config VALUES (feature_, true) ON CONFLICT (feature) DO UPDATE SET inactive = true;
$$ LANGUAGE SQL;


CREATE OR REPLACE FUNCTION F.is_enabled (feature_ varchar)
RETURNS BOOL
AS $$
   SELECT COALESCE((select NOT inactive FROM F.config WHERE feature = feature_), true);
$$ LANGUAGE SQL STABLE;
-- src/db-schema/addresses.sql
-- Open Source License
-- Copyright (c) 2020-2022 Nomadic Labs <contact@nomadic-labs.com>
--
-- Permission is hereby granted, free of charge, to any person obtaining a
-- copy of this software and associated documentation files (the "Software"),
-- to deal in the Software without restriction, including without limitation
-- the rights to use, copy, modify, merge, publish, distribute, sublicense,
-- and/or sell copies of the Software, and to permit persons to whom the
-- Software is furnished to do so, subject to the following conditions:
--
-- The above copyright notice and this permission notice shall be included
-- in all copies or substantial portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
-- IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
-- FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
-- THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
-- LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
-- FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
-- DEALINGS IN THE SOFTWARE.

-- Lines starting with --OPT may be automatically activated
-- Lines ending with --OPT may be automatically deactivated

SELECT 'addresses.sql' as file;

-- table of all existing addresses, so their storage can be factorized here

CREATE TABLE IF NOT EXISTS C.address ( --NL
 address text,
 address_id bigint not null unique,
 alias bigint,
 primary key(address)
);
-- No need to create index on address because it's the primary key.
-- The following index is not useful during multicore mode. To be confirmed.

CREATE OR REPLACE VIEW C.addresses AS SELECT * FROM C.address;

-- insert into C.address values ('tz1UzKDcDyvK9fq7nfXvXr85v5CqXnhU8r37', -1) on conflict do nothing;
-- insert into C.address values ('tz1bVjLh33Q7dJ7YEgr4pEbqGCxNMmg2QZvu', -2) on conflict do nothing;


-- If you need to know if an address is for an implicit contract (tz...) or an originated contract (KT...), the fastest way is likely to use < and > comparisons.
-- select * from C.address where address < 'L' ; -- returns all originated contracts
-- select * from C.address where address > 't' ; -- returns all implicit contracts
-- select * from C.address where address > 'r' and address < 't' ; -- upcoming rollups (address format not set in stone yet)
-- That should perform a lot faster than something like
-- select * from C.address where address like 'tz%' ; -- returns all implicit contracts
-- because the contents of the `address` column are sorted in an (btree) index.

DROP FUNCTION IF EXISTS address_id(char); -- since v9.9.8
CREATE OR REPLACE FUNCTION address_id(a text) -- since v9.9.8
returns bigint
as $$
select address_id from C.address where address = a;
$$ language sql stable;

DROP FUNCTION IF EXISTS I.address_aux(char,bigint); -- since v9.9.8
CREATE OR REPLACE FUNCTION I.address_aux(a text, id bigint) -- since v9.9.8
returns bigint
as $$
insert into C.address (address, address_id) values(a, id) on conflict do nothing returning address_id;
$$ language SQL;

DROP FUNCTION IF EXISTS I.address(char,bigint); -- since v9.9.8
CREATE OR REPLACE FUNCTION I.address(a text, id bigint) -- since v9.9.8
returns bigint
as $$
DECLARE r bigint := null; tmp bigint := null;
BEGIN
r := (select address_id from C.address where address = a);
if r is not null
then
  return r;
else
  r := (select I.address_aux(a, id));
  if r is null
  then
    r := (select address_id from C.address where address = a);
    if r is null then
      r := (select I.address_aux(a, -id));
    end if;
  else
    if a < 't' then insert into C.contract_script(address_id)values(r) on conflict do nothing; end if; --SEQONLY
    return r;
  end if;
  if r is null
  then
    r := (select address_id from C.address where address = a);
    if r is null then
      tmp := (select address_id from c.address where address_id < 20000000 order by address_id desc limit 1);
      if tmp is null then
        tmp := coalesce((select address_id from c.address order by address_id desc limit 1), 0);
      end if;
    end if;
    loop
      if r is null then
        tmp := tmp + 1;
        r := (select I.address_aux(a, tmp));
        if r is not null then
          return r;
        else
          r := (select address_id from C.address where address = a);
        end if;
      else
        return r;
      end if;
    end loop;
  else
    if a < 't' then insert into C.contract_script(address_id)values(r) on conflict do nothing; end if; --SEQONLY
    return r;
  end if;
  if r is null
  then
    raise 'Failed to record address % % % %', a, r, (select address from c.address where address = a), (select address_id::text from c.address where address = a);
  else
    if a < 't' then insert into C.contract_script(address_id)values(r) on conflict do nothing; end if; --SEQONLY
    return r;
  end if;
end if;
END
$$ language plpgsql;


DROP FUNCTION IF EXISTS address(bigint); -- since v9.9.8
CREATE OR REPLACE FUNCTION address(id bigint)
returns text
as $$
select address from C.address where address_id = id;
$$ language sql stable;

DROP FUNCTION IF EXISTS address(bigint[]); -- since v9.9.8
CREATE OR REPLACE FUNCTION address(id bigint[])
returns text[]
as $$
select array_agg(address) from C.address where address_id in (select unnest(id));
$$ language sql stable;

CREATE TABLE IF NOT EXISTS C.uri ( --NL
 address text,
 address_id int not null unique,
 primary key(address)
);

CREATE OR REPLACE FUNCTION uri(id bigint)
returns text
as $$
select address from C.uri where address_id = id;
$$ language sql stable;

CREATE OR REPLACE FUNCTION uri(id int[])
returns text[]
as $$
select array_agg(address) from C.uri where address_id in (select unnest(id));
$$ language sql stable;

CREATE OR REPLACE FUNCTION uri_id(i text)
returns int
as $$
select address_id from C.uri where address = i;
$$ language sql stable;



CREATE OR REPLACE FUNCTION I.uri_aux(a text, id integer)
returns integer
as $$
insert into C.uri values(a, id) on conflict do nothing returning address_id;
$$ language SQL;

CREATE OR REPLACE FUNCTION I.uri(a text)
returns integer
as $$
DECLARE r integer := null;
BEGIN
r := (select address_id from C.uri where address = a);
if r is not null
then
  return r;
else
  r := (select I.uri_aux(a, coalesce((select address_id+1 from c.uri order by address_id desc limit 1), 0)));
  if r is null
  then
    r := (select address_id from C.uri where address = a);
    if r is null then
      r := (select I.uri_aux(a, coalesce((select address_id-1 from c.uri order by address_id asc limit 1), 0)));
    end if;
  else
    return r;
  end if;
  if r is null
  then
    r := (select address_id from C.uri where address = a);
    if r is null then
      raise 'Failed to record URI address % % % %', a, r, (select address from C.uri where address = a), (select address_id::text from C.uri where address = a);
    end if;
  else
    return r;
  end if;
  if r is null
  then
    raise 'Failed to record URI address % % % %', a, r, (select address from C.uri where address = a), (select address_id::text from C.uri where address = a);
  else
    return r;
  end if;
end if;
END
$$ language plpgsql;
-- src/db-schema/chain.sql
-- Open Source License
-- Copyright (c) 2019 Vincent Bernardoff <vb@luminar.eu.org>
-- Copyright (c) 2019-2022 Nomadic Labs <contact@nomadic-labs.com>
--
-- Permission is hereby granted, free of charge, to any person obtaining a
-- copy of this software and associated documentation files (the "Software"),
-- to deal in the Software without restriction, including without limitation
-- the rights to use, copy, modify, merge, publish, distribute, sublicense,
-- and/or sell copies of the Software, and to permit persons to whom the
-- Software is furnished to do so, subject to the following conditions:
--
-- The above copyright notice and this permission notice shall be included
-- in all copies or substantial portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
-- IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
-- FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
-- THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
-- LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
-- FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
-- DEALINGS IN THE SOFTWARE.

-----------------------------------------------------------------------------
-----------------------------------------------------------------------------
-- Lines starting with --OPT may be automatically activated
-- Lines ending with --OPT may be automatically deactivated
-----------------------------------------------------------------------------
-----------------------------------------------------------------------------
-- Naming conventions:
-- - for tables:
--   * use singular for names, use plural for column names that are arrays
--   * table 'proposals' is plural because what it contains is plural (it contains 'proposals' on each row)
--   * exceptions: C.address (probably should've been named C.address)
-----------------------------------------------------------------------------
-----------------------------------------------------------------------------
-- Foreign keys:
-- They are declared in special comments starting with `--FKEY`, and must start the line.
--  --FKEY name_of_foreign_key ; name_of_table ; set, of, columns ; foreign_table_name(column_name) ; action
-- where action can be CASCADE or SET NULL
-- Refer to existing one for examples.
-----------------------------------------------------------------------------
-----------------------------------------------------------------------------

SELECT 'chain.sql' as file;

-----------------------------------------------------------------------------
-- Some logs about what happens while running the indexer

CREATE TABLE IF NOT EXISTS indexer_log (
   timestamp timestamp DEFAULT CURRENT_TIMESTAMP,
   version text not null default '',
   argv text not null default '',
   action text not null default '',
   primary key (timestamp, version, argv, action)
);

-----------------------------------------------------------------------------
-- storing the chain id

CREATE TABLE IF NOT EXISTS C.chain ( --L
  hash char(15) primary key
);


-----------------------------------------------------------------------------
-- this table inlines blocks and block headers
-- see lib_base/block_header.mli

CREATE TABLE IF NOT EXISTS C.block ( --NL
  hash char(51) not null,
  -- Block hash.
  -- 51 = 32 bytes hashes encoded in b58check + length of prefix "B"
  -- see lib_crypto/base58.ml
  level int not null,
  -- Height of the block, from the genesis block.
  proto smallint not null,
  -- Number of protocol changes since genesis modulo 256.
  predecessor int not null,
  -- preceding block -- useful mostly for cascade deletion!
  timestamp timestamp not null,
  -- Timestamp at which the block is claimed to have been created.
  validation_passes smallint not null,
  -- Number of validation passes (also number of lists of operations).
  merkle_root char(53) not null,
  -- see [operations_hash]
  -- Hash of the list of lists (actually root hashes of merkle trees)
  -- of operations included in the block. There is one list of
  -- operations per validation pass.
  -- 53 = 32 bytes hashes encoded in b58 check + "LLo" prefix
  fitness text,
  -- A sequence of sequences of unsigned bytes, ordered by length and
  -- then lexicographically. It represents the claimed fitness of the
  -- chain ending in this block.
  context_hash char(52) not null
  -- Hash of the state of the context after application of this block.
);
--PKEY block_pkey; C.block; hash --SEQONLY
--FKEY block_predecessor_fkey ; C.block ; predecessor ; C.block(level) ; CASCADE --SEQONLY

-----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS C.operation_kind ( -- since 10.3.0
  kind text not null primary key
, id smallint not null
);
-- this table is populated when DB is created or each time the DB is updated
-- do not populate this table manually or you may make your DB incompatible with the indexer

CREATE OR REPLACE FUNCTION operation_kind (k smallint)
RETURNS	char
AS
$$
SELECT kind FROM C.operation_kind WHERE id = k;
$$ LANGUAGE SQL STABLE;

CREATE OR REPLACE FUNCTION operation_kind (k int)
RETURNS char
AS
$$
SELECT operation_kind(k::smallint)
$$ LANGUAGE SQL STABLE;

CREATE OR REPLACE FUNCTION declare_operation (op text, n int) -- since 10.3.0
RETURNS VOID
AS $$
DECLARE x text := (SELECT operation_kind(n));
BEGIN
  IF x = op
  THEN
    RETURN;
  ELSIF x IS NULL
  THEN
    INSERT INTO C.operation_kind(kind, id) VALUES (op, n::smallint);
  -- ELSE
  --   RAISE 'Your database has incompatible data in table C.operation_kind';
  -- --> we do nothing if the id is already taken
  END IF;
END;
$$ LANGUAGE PLPGSQL;


-----------------------------------------------------------------------------
-- implicit operations results (since protocol PtGRANAD) -- since v9.5.0
CREATE TABLE IF NOT EXISTS C.implicit_operations_results ( --NL
  block_level int not null
, operation_kind smallint not null
, consumed_milligas numeric not null
, storage jsonb
, originated_contracts bigint[]
, storage_size numeric
, paid_storage_size_diff numeric
, allocated_destination_contract bool
, strings text[]
, id int not null
, originated_tx_rollup_id bigint  -- since v9.9.3
, tx_rollup_level int -- since v10.0.0rc1
, ticket_hash jsonb -- since v10.0.0rc1
, sc_rollup_address_id bigint -- since v9.9.3 -- unused until at least proto J
, sc_rollup_size numeric -- since v9.8.2 -- unused until at least proto J
, sc_rollup_inbox_after jsonb -- since v9.9 -- unused until at least proto J
, ticket_receipt jsonb -- since v10.3.0
, genesis_commitment_hash char(54)  -- since v10.4.0
-- balance updates are detailed in C.balance_updates
);
--FKEY implicit_operations_results_tx_roll; C.implicit_operations_results; originated_tx_rollup_id; C.address(address_id); CASCADE --SEQONLY
--FKEY implicit_operations_results_op_kind; C.implicit_operations_results; operation_kind; C.operation_kind(id); CASCADE --SEQONLY -- since v10.3.0

DO $$
BEGIN
ALTER TABLE C.implicit_operations_results ADD COLUMN tx_rollup_level int;
EXCEPTION WHEN OTHERS THEN RETURN;
END $$;
DO $$
BEGIN
ALTER TABLE C.implicit_operations_results ADD COLUMN ticket_hash jsonb;
EXCEPTION WHEN OTHERS THEN RETURN;
END $$;
DO $$
BEGIN
ALTER TABLE C.implicit_operations_results ADD COLUMN ticket_receipt jsonb;
EXCEPTION WHEN OTHERS THEN RETURN;
END $$;
DO $$
BEGIN
ALTER TABLE C.implicit_operations_results ADD COLUMN genesis_commitment_hash char(54);
EXCEPTION WHEN OTHERS THEN RETURN;
END $$;
DO $$
BEGIN
ALTER TABLE C.implicit_operations_results ALTER COLUMN genesis_commitment_hash TYPE char(54);
EXCEPTION WHEN OTHERS THEN RETURN;
END $$;



-----------------------------------------------------------------------------
-- operations seen from block-level, therefore non-protocol-specific information

CREATE TABLE IF NOT EXISTS C.operation ( --NL
  -- Note: an operation may point to a rejected block only if the
  -- operation itself was deleted from the chain.
  -- If the operation was included in a rejected block but then
  -- reinjected into another block, then this table contains the
  -- latest block_hash associated to that operation.
  -- Hypothesis: the latest write is always right.
  hash char(51) not null, -- operation hash
  block_level int not null, -- char(51) not null, -- block hash
  hash_id bigint not null
);
--FKEY operation_block_level_fkey; C.operation; block_level; C.block(level) ; CASCADE --SEQONLY
--PKEY operation_pkey; C.operation; hash_id --SEQONLY


CREATE OR REPLACE FUNCTION operation_hash(id bigint) returns char as $$ select hash from C.operation where hash_id = id $$ language sql stable;

CREATE OR REPLACE FUNCTION operation_hash_id(h char) returns bigint as $$ select hash_id from C.operation where hash = h $$ language sql stable;

-----------------------------------------------------------------------------
-- Index of protocol-specific contents of an operation
-- An "operation" at the "shell" level is a "set of operations" at the "protocol" level.
-- In the following table, "hash_id" refers to an operation at the shell level.
-- At the protocol level, a shell-level operation is a list of operations (the word "operation" has different meanings).
-- Inside protocol-level operations, there can be some additional "internal operations".
-- "Internal operations" (a.k.a. "internal manager operation") are operations that are at manager-operation-level.
-- An "internal operation", so far, for protocols 1 to 11, are only manager operations (inside manager operations).
-- There are differences between "manager operations" and "internal manager operations".
-- For instance, internal ones don't have data in manager_numbers, but are the only ones that have a "nonce" (which are integers).

CREATE TABLE IF NOT EXISTS C.operation_alpha ( --NL
    block_level int not null
  -- block hash id
  , hash_id bigint not null
  -- operation hash id
  , id smallint not null
  -- index of op in contents_list
  , operation_kind smallint not null -- cf. table C.operation_kind
  , internal smallint not null
  -- block hash
  , autoid bigint not null -- counter id
);
--PKEY operation_alpha_pkey; C.operation_alpha; hash_id, id, internal, block_level --SEQONLY
--FKEY operation_alpha_hash_fkey; C.operation_alpha; hash_id; C.operation(hash_id) ; CASCADE --SEQONLY
--FKEY operation_alpha_block_hash_fkey; C.operation_alpha; block_level; C.block(level) ; CASCADE --SEQONLY
--FKEY operation_alpha_op_kind; C.operation_alpha; operation_kind; C.operation_kind(id); CASCADE --SEQONLY -- since v10.3.0


CREATE OR REPLACE FUNCTION operation_alpha_autoid(ophid bigint, opid smallint, i smallint, block_level bigint)
RETURNS bigint
AS $$
SELECT autoid
FROM C.operation_alpha
WHERE (hash_id, id, internal, block_level) = (ophid, opid, i, block_level);
$$ LANGUAGE SQL STABLE;

DROP FUNCTION IF EXISTS operation_hash_alpha; --change of signature in v9.7.7
CREATE OR REPLACE FUNCTION operation_hash_alpha(opaid bigint)
RETURNS char as $$
select o.hash from c.operation o, c.operation_alpha a where a.autoid = opaid and a.hash_id = o.hash_id
$$ LANGUAGE SQL STABLE;

CREATE OR REPLACE FUNCTION operation_hash_id_alpha(opaid bigint)
returns bigint as $$ select hash_id from C.operation_alpha where autoid = opaid $$ language sql stable;

CREATE OR REPLACE FUNCTION operation_id_alpha(opaid bigint)
returns smallint as $$ select id from C.operation_alpha where autoid = opaid $$ language sql stable;

CREATE OR REPLACE FUNCTION operation_alpha_ids(h char)
returns table (id bigint) as $$ select autoid from C.operation_alpha where hash_id = operation_hash_id(h) $$ language sql stable;

CREATE OR REPLACE FUNCTION operation_id (h char, i smallint, inter smallint)
returns bigint
as $$ select autoid from c.operation_alpha where hash_id = operation_hash_id(h) and id = i and internal = inter $$ language SQL stable;
CREATE OR REPLACE FUNCTION operation_id (h char, i int, inter int)
returns bigint
as $$ select autoid from c.operation_alpha where hash_id = operation_hash_id(h) and id = i::smallint and internal = inter::smallint $$ language SQL stable;


CREATE OR REPLACE FUNCTION block_hash(id int) returns char as $$ select hash from C.block where level = id $$ language sql stable;
CREATE OR REPLACE FUNCTION op_shift() returns bigint as $$ select 10000000::bigint $$ language sql immutable;
CREATE OR REPLACE FUNCTION block_level_from_opid(opid bigint) returns int as $$ select (opid / op_shift())::int where opid > 0 $$ language sql immutable;
CREATE OR REPLACE FUNCTION block_hash(opid bigint) returns char as $$ select hash from C.block where level = block_level_from_opid(opid) $$ language sql stable;
CREATE OR REPLACE FUNCTION block_level(h char) returns int as $$ select level from C.block where hash = h $$ language sql stable;
CREATE OR REPLACE FUNCTION block_level_of_block_level(block_level int) returns int as $$ select block_level $$ language sql stable;
CREATE OR REPLACE FUNCTION block_level_of_opaid(opaid bigint) returns int as $$ select block_level_from_opid(opaid) $$ language sql stable;
CREATE OR REPLACE FUNCTION block_level_of_operation_hash_id(ophid bigint) returns int as $$ select block_level_from_opid(ophid) $$ language sql stable;

-----------------------------------------------------------------------------
-- Convenience table to rapidly get a list of operations linked to an address

CREATE TABLE IF NOT EXISTS C.operation_sender_and_receiver ( --L
  operation_id bigint not null,
  sender_id bigint not null,
  receiver_id bigint
);
--PKEY operation_sender_and_receiver_pkey; C.operation_sender_and_receiver; operation_id --SEQONLY
--FKEY operation_sender_and_receiver_operation_id_fkey; C.operation_sender_and_receiver; operation_id; C.operation_alpha(autoid) ; CASCADE --SEQONLY
--FKEY operation_sender_and_receiver_sender_id_fkey; C.operation_sender_and_receiver; sender_id; C.address(address_id) ; CASCADE
--FKEY operation_sender_and_receiver_receiver_id_fkey; C.operation_sender_and_receiver; receiver_id; C.address(address_id) ; CASCADE

-- since v9.9.3, the following 2 indexes replace indexes on solely sender_id and receiver_id

-----------------------------------------------------------------------------
-- Protocol amendment proposals

CREATE TABLE IF NOT EXISTS C.proposals ( --NL
  proposal char(51) not null,
  proposal_id bigint not null
  -- about proposal_id: the important factor is to have a unique
  -- value. That value could be smaller and probably using a smallint
  -- would be enough to represent all proposals happening on tezos for
  -- many years to come. However here we use a bigint, to "simply" use
  -- the first operation's ophid that needs to access `proposal_id`.
  -- The first operation that needs that access will write the
  -- proposal's hash value into the `proposal` column, and give its
  -- `ophid` as `proposal_id`. Further operations will attempt to do
  -- the same, and will fail the writing (since `proposal` is a pkey)
  -- but will be able to access `proposal_id`.
  -- All that is to avoid using Postgresql's SERIAL because those
  -- generate values that cannot be accessed within an SQL transaction.
);
--PKEY proposals_pkey; C.proposals; proposal

-- pre-filling c.proposals for multicore mode
-- note that the proposal_id is (generally) NOT the protocol number


CREATE OR REPLACE FUNCTION proposal(id bigint) returns char as $$ select proposal from C.proposals where proposal_id = id $$ language sql stable;

CREATE OR REPLACE FUNCTION proposal_id(p char) returns bigint as $$ select proposal_id from C.proposals where proposal = p $$ language sql stable;

CREATE TABLE IF NOT EXISTS C.proposal ( --L
    operation_id bigint not null
  , source_id bigint not null
  , period int not null
  , proposal_id bigint not null
);
--PKEY proposal_pkey; C.proposal; operation_id, proposal_id
--FKEY proposal_operation_id_fkey; C.proposal; operation_id; C.operation_alpha(autoid); CASCADE --SEQONLY
--FKEY proposal_source_fkey; C.proposal; source_id; C.address(address_id); CASCADE --SEQONLY
--FKEY proposal_proposal_id_fkey; C.proposal; proposal_id; C.proposals(proposal_id); CASCADE --SEQONLY

-----------------------------------------------------------------------------
-- Ballots for proposal amendment proposals
DO $$
BEGIN
  IF NOT EXISTS (SELECT 1 FROM pg_type WHERE typname = 'one_ballot') THEN
    CREATE TYPE one_ballot AS ENUM ('nay', 'yay', 'pass');
  END IF;
END
$$;

CREATE TABLE IF NOT EXISTS C.ballot ( --L
    operation_id bigint not null
  , source_id bigint not null
  , period int not null
  , proposal_id bigint not null
  , ballot one_ballot not null
);
--PKEY ballot_pkey; C.ballot; operation_id --SEQONLY
--FKEY ballot_operation_id_fkey; C.ballot; operation_id; C.operation_alpha(autoid); CASCADE --SEQONLY
--FKEY ballot_proposal_id_fkey; C.ballot; proposal_id; C.proposals(proposal_id); CASCADE --SEQONLY


-----------------------------------------------------------------------------
-- Double endorsement evidence

CREATE TABLE IF NOT EXISTS C.double_endorsement_evidence ( --L
    operation_id bigint not null
  , op1 jsonb
  , op2 jsonb
  , op1_ json
  , op2_ json
);
--PKEY double_endorsement_evidence_pkey; C.double_endorsement_evidence; operation_id --SEQONLY
--FKEY double_endorsement_evidence_operation_id_fkey; C.double_endorsement_evidence; operation_id; C.operation_alpha(autoid); CASCADE --SEQONLY

-----------------------------------------------------------------------------
-- Double preendorsement evidence

CREATE TABLE IF NOT EXISTS C.double_preendorsement_evidence ( --L
    operation_id bigint not null
  , op1 jsonb
  , op2 jsonb
  , op1_ json
  , op2_ json
);
--PKEY double_preendorsement_evidence_pkey; C.double_preendorsement_evidence; operation_id --SEQONLY
--FKEY double_preendorsement_evidence_operation_id_fkey; C.double_preendorsement_evidence; operation_id; C.operation_alpha(autoid); CASCADE --SEQONLY


-----------------------------------------------------------------------------
-- Double baking evidence

CREATE TABLE IF NOT EXISTS C.double_baking_evidence ( --L
    operation_id bigint not null
  , bh1 jsonb -- block header 1
  , bh2 jsonb -- block header 2
  , bh1_ json
  , bh2_ json
);
--PKEY double_baking_evidence_pkey; C.double_baking_evidence; operation_id --SEQONLY
--FKEY double_baking_evidence_operation_id_fkey; C.double_baking_evidence;operation_id; C.operation_alpha(autoid); CASCADE --SEQONLY


-----------------------------------------------------------------------------
-- Common data for manager operations

CREATE TABLE IF NOT EXISTS C.manager_numbers ( --NL
    operation_id bigint not null
  , counter numeric not null
  -- counter
  , gas_limit numeric not null
  -- gas limit
  , storage_limit numeric not null
  -- storage limit
);
--PKEY manager_numbers_pkey; C.manager_numbers; operation_id --SEQONLY
--FKEY manager_numbers_operation_id_fkey; C.manager_numbers; operation_id; C.operation_alpha(autoid); CASCADE --SEQONLY
-- Not sure the following indexes are relevant:


-----------------------------------------------------------------------------
-- Account Activations

CREATE TABLE IF NOT EXISTS C.activation ( --L
   operation_id bigint not null
 , pkh_id bigint not null
 , activation_code text not null
);
--PKEY activation_pkey; C.activation; operation_id --SEQONLY
--FKEY activation_operation_id_fkey; C.activation; operation_id; C.operation_alpha(autoid) ; CASCADE --SEQONLY
--FKEY activation_pkh_fkey; C.activation; pkh_id; C.address(address_id) ; CASCADE --SEQONLY


-----------------------------------------------------------------------------
-- Endorsements

CREATE TABLE IF NOT EXISTS C.endorsement ( --L
   operation_id bigint not null
 , level int
 , delegate_id bigint
 , slots smallint[]
 , round int
 , block_payload_hash char(52)
 , endorsement_power int -- nullable because not present until proto 12
);
--PKEY endorsement_pkey; C.endorsement; operation_id --SEQONLY
--FKEY endorsement_operation_id_fkey; C.endorsement; operation_id; C.operation_alpha(autoid) ; CASCADE --SEQONLY
--FKEY endorsement_delegate_fkey; C.endorsement; delegate_id; C.address(address_id) ; CASCADE --SEQONLY


-----------------------------------------------------------------------------
-- Preendorsements

CREATE TABLE IF NOT EXISTS C.preendorsement ( --L
   operation_id bigint not null
 , level int
 , delegate_id bigint
 , slots smallint[]
 , round int
 , block_payload_hash char(52)
 , preendorsement_power int -- nullable because not present until proto 12
);
--PKEY preendorsement_pkey; C.preendorsement; operation_id --SEQONLY
--FKEY preendorsement_operation_id_fkey; C.preendorsement; operation_id; C.operation_alpha(autoid) ; CASCADE --SEQONLY
--FKEY preendorsement_delegate_fkey; C.preendorsement; delegate_id; C.address(address_id) ; CASCADE --SEQONLY


-----------------------------------------------------------------------------
-- Seed nonce revelation

CREATE TABLE IF NOT EXISTS C.seed_nonce_revelation ( --L
    operation_id bigint not null
  -- index of the operation in the block's list of operations
 , level int not null
 , nonce char(66) not null
);
--PKEY seed_nonce_revelation_pkey; C.seed_nonce_revelation; operation_id --SEQONLY
--FKEY seed_nonce_revelation_operation_id_fkey; C.seed_nonce_revelation; operation_id; C.operation_alpha(autoid); CASCADE --SEQONLY


-----------------------------------------------------------------------------
-- Blocks at alpha level.
-- "level_position = cycle * blocks_per_cycle + cycle_position"

CREATE TABLE IF NOT EXISTS C.block_alpha ( --L
  level int not null
  -- block hash id
  , baker_id bigint not null -- baker_id or consensus_pkh_id
  -- pkh of baker
  , level_position int not null
  /* Verbatim from lib_protocol/level_repr:
     The level of the block relative to the block that
     starts protocol alpha. This is specific to the
     protocol alpha. Other protocols might or might not
     include a similar notion.
  */
  , cycle int not null
  -- cycle
  , cycle_position int not null
  /* Verbatim from lib_protocol/level_repr:
     The current level of the block relative to the first
     block of the current cycle.
  */
  , voting_period jsonb not null
  /* increasing integer.
     from proto_alpha/level_repr:
     voting_period = level_position / blocks_per_voting_period */
  , voting_period_position int not null
  -- voting_period_position = remainder(level_position / blocks_per_voting_period)
  , voting_period_kind smallint not null
  /* Proposal = 0
     Testing_vote = 1
     Testing = 2
     Promotion_vote = 3
     Adoption = 4
   */
  , consumed_milligas numeric not null
  /* total milligas consumed by block. Arbitrary-precision integer. */
  , delegate_id bigint -- if not null, then baker_id contains consensus_pkh_id (i.e. the pkh used for baking by the baker), introduced by proto L
);
--PKEY block_alpha_pkey; C.block_alpha; level --SEQONLY
--FKEY block_alpha_hash_fkey; C.block_alpha; level; C.block(level); CASCADE --SEQONLY
--FKEY block_alpha_baker_fkey; C.block_alpha; baker_id; C.address(address_id); CASCADE --SEQONLY

DO $$
BEGIN
ALTER TABLE C.block_alpha ADD COLUMN delegate_id bigint;
EXCEPTION WHEN OTHERS THEN RETURN;
END $$;
--FKEY block_alpha_delegate; C.block_alpha; delegate_id; C.address(address_id); CASCADE --SEQONLY

-----------------------------------------------------------------------------
-- Deactivated accounts

CREATE TABLE IF NOT EXISTS C.deactivated ( --L
  pkh_id bigint not null,
  -- pkh of the deactivated account(tz1...)
  block_level int not null
  -- block hash at which deactivation occured
);
--PKEY deactivated_pkey; C.deactivated; pkh_id, block_level  --SEQONLY
--FKEY deactivated_pkh_fkey; C.deactivated; pkh_id; C.address(address_id); CASCADE --SEQONLY
--FKEY deactivated_block_hash_fkey; C.deactivated; block_level; C.block(level); CASCADE --SEQONLY


-----------------------------------------------------------------------------
-- Table of contract balance by block level: each time a contract has its balance updated, we write it here

CREATE TABLE IF NOT EXISTS C.contract_balance ( --L
  address_id bigint not null,
  block_level int not null,
  balance bigint -- make it nullable so that it can be filled asynchronously
  -- N.B. it would be bad to have "address_id" as a primary key,
  -- because if you update a contract's balance using a
  -- rejected block(uncle block) and then the new balance is not updated
  -- once the rejected block is discovered,
  -- you end up with wrong information
);
--PKEY contract_balance_pkey; C.contract_balance; address_id, block_level  --SEQONLY
--FKEY contract_balance_address_fkey; C.contract_balance; address_id; C.address(address_id); CASCADE --SEQONLY
--FKEY contract_balance_block_hash_fkey; C.contract_balance; block_level; C.block(level); CASCADE --SEQONLY

-- the following index is for insertion performance

-----------------------------------------------------------------------------
-- Table of contract balance by block level: each time a contract has its balance updated, we write it here

CREATE TABLE IF NOT EXISTS C.contract_script ( -- since v9.1.0 --L
  address_id bigint primary key
, script jsonb -- make it nullable so that it can be filled asynchronously
, block_level int -- if null then value of `script` is unreliable and should be updated, if not null, then it's origin of the script
, strings text[] -- since v9.3.0
, uri int[]
, contracts bigint[]
, script_ json
, missing_script smallint -- not null means the indexer failed to get the script -- since v9.5.0
);
--FKEY contract_script_address_fkey; C.contract_script; address_id; C.address(address_id); CASCADE --SEQONLY
--FKEY contract_script_block_level_fkey; C.contract_script; block_level; C.block(level); SET NULL --SEQONLY

-- the following index is for speeding up insertions
-- the following index is for speeding up insertions

-- BEGIN keep up to date with conversion_from_multicore.sql
-- END keep up to date with conversion_from_multicore.sql

INSERT INTO C.contract_script (address_id) select address_id from C.address where address < 't' on conflict do nothing; --SEQONLY



-----------------------------------------------------------------------------
-- Transactions -- manop

CREATE TABLE IF NOT EXISTS C.tx ( --L
  operation_id bigint not null
  -- operation id from operation_alpha
, source_id bigint not null
  -- source address
, destination_id bigint not null
  -- dest address
, fee bigint -- null if internal operation -- fix, since v10.5.0
  -- fees
, amount bigint not null
  -- amount
, originated_contracts bigint[]
  -- since v9.5.0
, parameters jsonb -- jsonb since v9.3 -- invalid Unicode sequences are replaced by an error message
  -- optional parameters to contract in json-encoded Micheline
, storage jsonb
  -- optional parameter for storage update
, consumed_milligas numeric
  -- consumed milligas
, storage_size numeric
  -- storage size
, paid_storage_size_diff numeric
  -- paid storage size diff
, entrypoint text
  -- entrypoint
, nonce int -- non null for internal operations
, status smallint not null default 0 -- 0: applied, 1: backtracked, 2: failed, 3: skipped
, error_trace jsonb
, strings text[] -- since v9.3.0
, uri int[]
, contracts bigint[]
, parameters_ json
, storage_ json
, ticket_hash jsonb -- since v10
, ticket_receipt jsonb -- since v10.3.0
);
--PKEY tx_pkey; C.tx; operation_id --SEQONLY
--FKEY tx_operation_id_fkey; C.tx; operation_id; C.operation_alpha(autoid); CASCADE --SEQONLY
--FKEY tx_source_fkey; C.tx; source_id; C.address(address_id); CASCADE --SEQONLY
--FKEY tx_destination_fkey; C.tx; destination_id; C.address(address_id); CASCADE --SEQONLY

ALTER TABLE C.tx ALTER COLUMN fee DROP NOT NULL ; -- null if internal operation -- fix, since v10.5.0
DO $$
BEGIN
IF NOT((SELECT operation_id FROM C.tx WHERE fee IS NULL LIMIT 1) IS NOT NULL)
THEN
  UPDATE C.tx SET fee = NULL WHERE nonce IS NOT NULL;
END IF;
END $$;

-- BEGIN keep up to date with conversion_from_multicore.sql
-- END keep up to date with conversion_from_multicore.sql

DO $$
BEGIN
ALTER TABLE C.tx ADD COLUMN ticket_hash jsonb;
EXCEPTION WHEN OTHERS THEN RETURN;
END $$;

DO $$
BEGIN
ALTER TABLE C.tx ADD COLUMN ticket_receipt jsonb;
EXCEPTION WHEN OTHERS THEN RETURN;
END $$;


-----------------------------------------------------------------------------
-- Origination table -- manop

CREATE TABLE IF NOT EXISTS C.origination ( --L
  operation_id bigint not null
  -- operation id from operation_alpha
, source_id bigint not null
  -- source of origination op
, k_id bigint
  -- address of originated contract
, consumed_milligas numeric
  -- consumed milligas
, storage_size numeric
  -- storage size
, paid_storage_size_diff numeric
  -- paid storage size diff
, fee bigint -- null when internal -- fix, since v10.5.0
  -- fees
, nonce int -- non null for internal operations
, preorigination_id bigint -- optional for protos 1 to 8
, delegate_id bigint -- optional for protos 1,2,3,4,5,6,7,8
, credit bigint not null
, status smallint not null default 0 -- 0: applied, 1: backtracked, 2: failed, 3: skipped
, error_trace jsonb
, error_trace_ json
-- for script, cf. C.contract_script
-- for manager, cf. C.manager -- protos 1 to 4 included only
);
--PKEY origination_pkey; C.origination; operation_id --SEQONLY
--FKEY origination_operation_id_fkey; C.origination; operation_id; C.operation_alpha(autoid); CASCADE --SEQONLY
--FKEY origination_operation_source_fkey; C.origination; source_id; C.address(address_id); CASCADE --SEQONLY
--FKEY origination_k_fkey; C.origination; k_id; C.address(address_id); CASCADE --SEQONLY
ALTER TABLE C.origination ALTER COLUMN fee DROP NOT NULL ; -- null if internal operation -- fix, since v10.5.0
DO $$
BEGIN
IF NOT((SELECT operation_id FROM C.origination WHERE fee IS NULL LIMIT 1) IS NOT NULL)
THEN
  UPDATE c.origination SET fee = NULL WHERE nonce IS NOT NULL;
END IF;
END $$;



-----------------------------------------------------------------------------
-- Manager table for old protocols (1 to 4)
-- This would be part of C.origination if it was not dropped by protocol 5 and replaced by a non-null script (which was optional before protocol 5).
-- You might want to choose not to fill this table at all. In that case, modify I.origination in chain_functions.sql to simply remove the insertion.

CREATE TABLE IF NOT EXISTS C.manager ( --L
  operation_id bigint not null
, manager_id bigint not null
);
--PKEY manager_pkey; C.manager; operation_id --SEQONLY
--FKEY manager_operation_id_fkey; C.manager; operation_id; C.operation_alpha(autoid); CASCADE --SEQONLY
--FKEY manager_manager_id_fkey; C.manager; manager_id; C.address(address_id); CASCADE --SEQONLY


-----------------------------------------------------------------------------
-- Delegation -- manop

CREATE TABLE IF NOT EXISTS C.delegation ( --L
  operation_id bigint not null
  -- operation id from operation_alpha
  , source_id bigint not null
  -- source of the delegation op
  , pkh_id bigint
  -- optional delegate
  , consumed_milligas numeric
  -- consumed milligas
  , fee bigint -- null when internal -- fix, since v10.5.0
  -- fees
  , nonce int -- non null for internal operations
  , status smallint not null default 0 -- 0: applied, 1: backtracked, 2: failed, 3: skipped
  , error_trace jsonb
  , error_trace_ json
);
--PKEY delegation_pkey; C.delegation; operation_id --SEQONLY
--FKEY delegation_operation_id_fkey; C.delegation; operation_id; C.operation_alpha(autoid); CASCADE --SEQONLY
--FKEY delegation_source_fkey; C.delegation; source_id; C.address(address_id); CASCADE --SEQONLY
--FKEY delegation_pkh_fkey; C.delegation; pkh_id; C.address(address_id); CASCADE --SEQONLY
ALTER TABLE C.delegation ALTER COLUMN fee DROP NOT NULL ; -- null if internal operation -- fix, since v10.5.0
DO $$
BEGIN
IF NOT((SELECT operation_id FROM C.delegation WHERE fee IS NULL LIMIT 1) IS NOT NULL)
THEN
  UPDATE C.delegation SET fee = NULL WHERE nonce IS NOT NULL;
END IF;
END $$;


-----------------------------------------------------------------------------
-- Reveals -- manop

CREATE TABLE IF NOT EXISTS C.reveal ( --L
    operation_id bigint not null
  -- operation id from operation_alpha
  , source_id bigint not null
  -- source
  , pk text not null
  -- revealed pk
  , consumed_milligas numeric
  -- consumed milligas
  , fee bigint -- null when internal -- fix, since v10.5.0
  -- fees
  , nonce int -- non null for internal operations
  , status smallint not null default 0 -- 0: applied, 1: backtracked, 2: failed, 3: skipped
  , error_trace jsonb
  , error_trace_ json
);
--PKEY reveal_pkey; C.reveal; operation_id --SEQONLY
--FKEY reveal_operation_hash_op_id_fkey; C.reveal; operation_id; C.operation_alpha(autoid); CASCADE --SEQONLY
--FKEY reveal_source_fkey; C.reveal; source_id; C.address(address_id); CASCADE --SEQONLY
DO $$
BEGIN
ALTER TABLE C.reveal ALTER COLUMN pk TYPE text;
EXCEPTION WHEN OTHERS THEN RETURN;
END $$;
ALTER TABLE C.reveal ALTER COLUMN fee DROP NOT NULL ; -- null if internal operation -- fix, since v10.5.0
DO $$
BEGIN
IF NOT((SELECT operation_id FROM C.reveal WHERE fee IS NULL LIMIT 1) IS NOT NULL)
THEN
  UPDATE c.reveal SET fee = NULL WHERE nonce IS NOT NULL;
END IF;
END $$;


-----------------------------------------------------------------------------
-- global constants -- since v9.7.0
CREATE TABLE IF NOT EXISTS C.global_constants ( --NL
  global_address char(54) not null
, "value" jsonb not null
, global_address_id bigint not null
, size int not null
-- , strings text[]
);
DROP INDEX IF EXISTS c.global_constants_value; --since v9.7.b and v9.8.3
--FKEY global_constants_global_address_id_fkey; C.global_constants; global_address_id; C.operation_alpha(autoid); CASCADE --SEQONLY

CREATE OR REPLACE FUNCTION global_address_id(ga char)
RETURNS bigint
AS $$
SELECT global_address_id FROM C.global_constants WHERE global_address = ga;
$$ LANGUAGE SQL STABLE;

-----------------------------------------------------------------------------
-- Register_Global_Constant -- manop -- since v9.7.0

CREATE TABLE IF NOT EXISTS C.register_global_constant ( -- introduced by proto 11 --L
  operation_id bigint not null
  -- operation id from operation_alpha
  , source_id bigint not null
  -- source of the register_global_constant op
  , global_address_id bigint -- this is equal to operation_id if all goes well, otherwise it's equal to another operation's operation_id: the operation that is indexed first will give its operation_id
  -- id of hash of the "value"
  , consumed_milligas numeric
  -- consumed milligas
  , fee bigint not null
  -- fees
  , status smallint not null default 0 -- 0: applied, 1: backtracked, 2: failed, 3: skipped
  , error_trace jsonb
  , error_trace_ json
);
--PKEY register_global_constant_pkey; C.register_global_constant; operation_id --SEQONLY
--FKEY register_global_constant_operation_id_fkey; C.register_global_constant; operation_id; C.operation_alpha(autoid); CASCADE --SEQONLY
--FKEY register_global_constant_source_fkey; C.register_global_constant; source_id; C.address(address_id); CASCADE --SEQONLY
DO $$
BEGIN
ALTER TABLE C.register_global_constant DROP COLUMN nonce;
EXCEPTION WHEN OTHERS THEN RETURN;
END $$;


--The following key is not really necessary because `register_global_constant_operation_id_fkey` should be enough.
--fkey register_global_constant_global_address_id_fkey; C.register_global_constant; global_address_id; C.global_constants(global_address_id); CASCADE --SEQONLY


-----------------------------------------------------------------------------
-- Set_deposits_limit -- manop -- since v9.7.a

CREATE TABLE IF NOT EXISTS C.set_deposits_limit ( -- introduced by proto 12 --L
  operation_id bigint not null
  -- operation id from operation_alpha
  , source_id bigint not null
  -- source
  , value numeric
  -- value of the limit
  , consumed_milligas numeric
  -- consumed milligas
  , fee bigint not null
  -- fees
  , status smallint not null default 0 -- 0: applied, 1: backtracked, 2: failed, 3: skipped
  , error_trace jsonb
  , error_trace_ json
);
--PKEY set_deposits_limit_pkey; C.set_deposits_limit; operation_id --SEQONLY
--FKEY set_deposits_limit_operation_id_fkey; C.set_deposits_limit; operation_id; C.operation_alpha(autoid); CASCADE --SEQONLY
--FKEY set_deposits_limit_source_fkey; C.set_deposits_limit; source_id; C.address(address_id); CASCADE --SEQONLY
DO $$
BEGIN
ALTER TABLE C.set_deposits_limit DROP COLUMN nonce;
EXCEPTION WHEN OTHERS THEN RETURN;
END $$;


-----------------------------------------------------------------------------
-- Tx_rollup_origination -- manop -- since v9.8.2
CREATE TABLE IF NOT EXISTS C.tx_rollup_origination ( -- introduced by proto 13 candidate --L
  operation_id bigint not null
  -- operation id from operation_alpha
  , source_id bigint not null
  -- source
  , tx_rollup_origination bigint -- since v9.9.3
  , consumed_milligas numeric
  -- consumed milligas
  , fee bigint not null
  -- fees
  , status smallint not null default 0 -- 0: applied, 1: backtracked, 2: failed, 3: skipped
  , error_trace jsonb
  , error_trace_ json
);
--PKEY tx_rollup_origination_pkey; C.tx_rollup_origination; operation_id --SEQONLY
--FKEY tx_rollup_origination_operation_id_fkey; C.tx_rollup_origination; operation_id; C.operation_alpha(autoid); CASCADE --SEQONLY
--FKEY tx_rollup_origination_source_fkey; C.tx_rollup_origination; source_id; C.address(address_id); CASCADE --SEQONLY
--FKEY tx_rollup_origination_tx_roll; C.tx_rollup_origination; tx_rollup_origination; C.address(address_id); CASCADE --SEQONLY


-----------------------------------------------------------------------------
-- tx_rollup_submit_batch -- manop -- since v10
CREATE TABLE IF NOT EXISTS C.tx_rollup_submit_batch ( -- introduced by proto 13 --L
  operation_id bigint not null
  -- operation id from operation_alpha
  , source_id bigint not null
  -- source
  , tx_rollup bigint not null
  , consumed_milligas numeric
  -- consumed milligas
  , fee bigint not null
  -- fees
  , content bytea not null
  , burn_limit numeric
  , paid_storage_size_diff numeric
  , status smallint not null default 0 -- 0: applied, 1: backtracked, 2: failed, 3: skipped
  , error_trace jsonb
  , error_trace_ json
);
DO $$
BEGIN
ALTER TABLE C.tx_rollup_submit_batch ALTER COLUMN content TYPE bytea;
EXCEPTION WHEN OTHERS THEN RETURN;
END $$;
DO $$
BEGIN
ALTER TABLE C.tx_rollup_submit_batch ADD COLUMN paid_storage_size_diff numeric;
EXCEPTION WHEN OTHERS THEN RETURN;
END $$;
DO $$
BEGIN
ALTER TABLE C.tx_rollup_submit_batch DROP COLUMN nonce;
EXCEPTION WHEN OTHERS THEN RETURN;
END $$;


--PKEY tx_rollup_submit_batch_pkey; C.tx_rollup_submit_batch; operation_id --SEQONLY
--FKEY tx_rollup_submit_batch_operation_id_fkey; C.tx_rollup_submit_batch; operation_id; C.operation_alpha(autoid); CASCADE --SEQONLY
--FKEY tx_rollup_submit_batch_source_fkey; C.tx_rollup_submit_batch; source_id; C.address(address_id); CASCADE --SEQONLY
--FKEY tx_rollup_submit_batch_tx_roll; C.tx_rollup_submit_batch; tx_rollup; C.address(address_id); CASCADE --SEQONLY


-----------------------------------------------------------------------------
-- tx_rollup_commit -- manop -- since v10
CREATE TABLE IF NOT EXISTS C.tx_rollup_commit ( -- introduced by proto 13 --L
  operation_id bigint not null
  -- operation id from operation_alpha
  , source_id bigint not null
  -- source
  , tx_rollup bigint not null
  , consumed_milligas numeric
  -- consumed milligas
  , fee bigint not null
  -- fees
  , commitment jsonb not null
  , status smallint not null default 0 -- 0: applied, 1: backtracked, 2: failed, 3: skipped
  , error_trace jsonb
  , error_trace_ json
);
--PKEY tx_rollup_commit_pkey; C.tx_rollup_commit; operation_id --SEQONLY
--FKEY tx_rollup_commit_operation_id_fkey; C.tx_rollup_commit; operation_id; C.operation_alpha(autoid); CASCADE --SEQONLY
--FKEY tx_rollup_commit_source_fkey; C.tx_rollup_commit; source_id; C.address(address_id); CASCADE --SEQONLY
--FKEY tx_rollup_commit_tx_roll; C.tx_rollup_commit; tx_rollup; C.address(address_id); CASCADE --SEQONLY
DO $$
BEGIN
ALTER TABLE C.tx_rollup_commit DROP COLUMN nonce;
EXCEPTION WHEN OTHERS THEN RETURN;
END $$;


-----------------------------------------------------------------------------
-- tx_rollup_finalize_commitment -- manop -- since v10
CREATE TABLE IF NOT EXISTS C.tx_rollup_finalize_commitment ( -- introduced by proto 13 --L
  operation_id bigint not null
  -- operation id from operation_alpha
  , source_id bigint not null
  -- source
  , tx_rollup bigint not null
  , consumed_milligas numeric
  -- consumed milligas
  , fee bigint not null
  -- fees
  , level int not null
  , status smallint not null default 0 -- 0: applied, 1: backtracked, 2: failed, 3: skipped
  , error_trace jsonb
  , error_trace_ json
);
--PKEY tx_rollup_finalize_commitment_pkey; C.tx_rollup_finalize_commitment; operation_id --SEQONLY
--FKEY tx_rollup_finalize_commitment_operation_id_fkey; C.tx_rollup_finalize_commitment; operation_id; C.operation_alpha(autoid); CASCADE --SEQONLY
--FKEY tx_rollup_finalize_commitment_source_fkey; C.tx_rollup_finalize_commitment; source_id; C.address(address_id); CASCADE --SEQONLY
--FKEY tx_rollup_finalize_commitment_tx_roll; C.tx_rollup_finalize_commitment; tx_rollup; C.address(address_id); CASCADE --SEQONLY
DO $$
BEGIN
ALTER TABLE C.tx_rollup_finalize_commitment DROP COLUMN nonce;
EXCEPTION WHEN OTHERS THEN RETURN;
END $$;


-----------------------------------------------------------------------------
-- tx_rollup_rejection -- manop -- since v10
CREATE TABLE IF NOT EXISTS C.tx_rollup_rejection ( -- introduced by proto 13 --L
  operation_id bigint not null
  -- operation id from operation_alpha
  , source_id bigint not null
  -- source
  , tx_rollup bigint not null
  , consumed_milligas numeric
  -- consumed milligas
  , fee bigint not null
  -- fees
  , level int not null
  , message jsonb not null
  , message_position int not null
  , message_path jsonb not null
  , message_result_hash jsonb not null
  , message_result_path jsonb not null
  , previous_message_result jsonb not null
  , previous_message_result_path jsonb not null
  , proof jsonb not null
  , status smallint not null default 0 -- 0: applied, 1: backtracked, 2: failed, 3: skipped
  , error_trace jsonb
  , error_trace_ json
);
--PKEY tx_rollup_rejection_pkey; C.tx_rollup_rejection; operation_id --SEQONLY
--FKEY tx_rollup_rejection_operation_id_fkey; C.tx_rollup_rejection; operation_id; C.operation_alpha(autoid); CASCADE --SEQONLY
--FKEY tx_rollup_rejection_source_fkey; C.tx_rollup_rejection; source_id; C.address(address_id); CASCADE --SEQONLY
--FKEY tx_rollup_rejection_tx_roll; C.tx_rollup_rejection; tx_rollup; C.address(address_id); CASCADE --SEQONLY
DO $$
BEGIN
ALTER TABLE C.tx_rollup_rejection DROP COLUMN nonce;
EXCEPTION WHEN OTHERS THEN RETURN;
END $$;


DO $$
BEGIN
ALTER TABLE C.tx_rollup_rejection ADD COLUMN message jsonb not null;
EXCEPTION WHEN OTHERS THEN RETURN;
END $$;
DO $$
BEGIN
ALTER TABLE C.tx_rollup_rejection ADD COLUMN message_position int not null;
EXCEPTION WHEN OTHERS THEN RETURN;
END $$;
DO $$
BEGIN
ALTER TABLE C.tx_rollup_rejection ADD COLUMN message_path jsonb not null;
EXCEPTION WHEN OTHERS THEN RETURN;
END $$;
DO $$
BEGIN
ALTER TABLE C.tx_rollup_rejection ADD COLUMN message_result_hash jsonb not null;
EXCEPTION WHEN OTHERS THEN RETURN;
END $$;
DO $$
BEGIN
ALTER TABLE C.tx_rollup_rejection ADD COLUMN message_result_path jsonb not null;
EXCEPTION WHEN OTHERS THEN RETURN;
END $$;
DO $$
BEGIN
ALTER TABLE C.tx_rollup_rejection ADD COLUMN previous_message_result jsonb not null;
EXCEPTION WHEN OTHERS THEN RETURN;
END $$;
DO $$
BEGIN
ALTER TABLE C.tx_rollup_rejection ADD COLUMN previous_message_result_path jsonb not null;
EXCEPTION WHEN OTHERS THEN RETURN;
END $$;
DO $$
BEGIN
ALTER TABLE C.tx_rollup_rejection ADD COLUMN proof jsonb not null;
EXCEPTION WHEN OTHERS THEN RETURN;
END $$;


-----------------------------------------------------------------------------
-- tx_rollup_dispatch_tickets -- manop -- since v10
CREATE TABLE IF NOT EXISTS C.tx_rollup_dispatch_tickets ( -- introduced by proto 13 --L
  operation_id bigint not null
  -- operation id from operation_alpha
  , source_id bigint not null
  -- source
  , tx_rollup bigint not null
  , consumed_milligas numeric
  -- consumed milligas
  , fee bigint not null
  -- fees
  , level int not null
  , context_hash jsonb not null
  , message_index int not null
  , message_result_path jsonb not null
  , tickets_info jsonb not null
  , paid_storage_size_diff numeric
  , status smallint not null default 0 -- 0: applied, 1: backtracked, 2: failed, 3: skipped
  , error_trace jsonb
  , error_trace_ json
);
--PKEY tx_rollup_dispatch_tickets_pkey; C.tx_rollup_dispatch_tickets; operation_id --SEQONLY
--FKEY tx_rollup_dispatch_tickets_operation_id_fkey; C.tx_rollup_dispatch_tickets; operation_id; C.operation_alpha(autoid); CASCADE --SEQONLY
--FKEY tx_rollup_dispatch_tickets_source_fkey; C.tx_rollup_dispatch_tickets; source_id; C.address(address_id); CASCADE --SEQONLY
--FKEY tx_rollup_dispatch_tickets_tx_roll; C.tx_rollup_dispatch_tickets; tx_rollup; C.address(address_id); CASCADE --SEQONLY
DO $$
BEGIN
ALTER TABLE C.tx_rollup_dispatch_tickets DROP COLUMN nonce;
EXCEPTION WHEN OTHERS THEN RETURN;
END $$;

DO $$
BEGIN
ALTER TABLE C.tx_rollup_dispatch_tickets ADD COLUMN context_hash jsonb not null;
EXCEPTION WHEN OTHERS THEN RETURN;
END $$;
DO $$
BEGIN
ALTER TABLE C.tx_rollup_dispatch_tickets ADD COLUMN message_index int not null;
EXCEPTION WHEN OTHERS THEN RETURN;
END $$;
DO $$
BEGIN
ALTER TABLE C.tx_rollup_dispatch_tickets ALTER COLUMN message_index TYPE int USING message_index::int;
EXCEPTION WHEN OTHERS THEN RETURN;
END $$;
DO $$
BEGIN
ALTER TABLE C.tx_rollup_dispatch_tickets ADD COLUMN message_result_path jsonb not null;
EXCEPTION WHEN OTHERS THEN RETURN;
END $$;
DO $$
BEGIN
ALTER TABLE C.tx_rollup_dispatch_tickets ADD COLUMN tickets_info jsonb not null;
EXCEPTION WHEN OTHERS THEN RETURN;
END $$;
DO $$
BEGIN
ALTER TABLE C.tx_rollup_dispatch_tickets ADD COLUMN paid_storage_size_diff numeric;
EXCEPTION WHEN OTHERS THEN RETURN;
END $$;


-----------------------------------------------------------------------------
-- tx_rollup_remove_commitment -- manop -- since v10
CREATE TABLE IF NOT EXISTS C.tx_rollup_remove_commitment ( -- introduced by proto 13 --L
  operation_id bigint not null
  -- operation id from operation_alpha
  , source_id bigint not null
  -- source
  , tx_rollup bigint not null
  , consumed_milligas numeric
  -- consumed milligas
  , fee bigint not null
  -- fees
  , level int not null
  , status smallint not null default 0 -- 0: applied, 1: backtracked, 2: failed, 3: skipped
  , error_trace jsonb
  , error_trace_ json
);
--PKEY tx_rollup_remove_commitment_pkey; C.tx_rollup_remove_commitment; operation_id --SEQONLY
--FKEY tx_rollup_remove_commitment_operation_id_fkey; C.tx_rollup_remove_commitment; operation_id; C.operation_alpha(autoid); CASCADE --SEQONLY
--FKEY tx_rollup_remove_commitment_source_fkey; C.tx_rollup_remove_commitment; source_id; C.address(address_id); CASCADE --SEQONLY
--FKEY tx_rollup_remove_commitment_tx_roll; C.tx_rollup_remove_commitment; tx_rollup; C.address(address_id); CASCADE --SEQONLY
DO $$
BEGIN
ALTER TABLE C.tx_rollup_remove_commitment DROP COLUMN nonce;
EXCEPTION WHEN OTHERS THEN RETURN;
END $$;


-----------------------------------------------------------------------------
-- tx_rollup_return_bond -- manop -- since v10
CREATE TABLE IF NOT EXISTS C.tx_rollup_return_bond ( -- introduced by proto 13 --L
  operation_id bigint not null
  -- operation id from operation_alpha
  , source_id bigint not null
  -- source
  , tx_rollup bigint not null
  , consumed_milligas numeric
  -- consumed milligas
  , fee bigint not null
  -- fees
  , status smallint not null default 0 -- 0: applied, 1: backtracked, 2: failed, 3: skipped
  , error_trace jsonb
  , error_trace_ json
);
--PKEY tx_rollup_return_bond_pkey; C.tx_rollup_return_bond; operation_id --SEQONLY
--FKEY tx_rollup_return_bond_operation_id_fkey; C.tx_rollup_return_bond; operation_id; C.operation_alpha(autoid); CASCADE --SEQONLY
--FKEY tx_rollup_return_bond_source_fkey; C.tx_rollup_return_bond; source_id; C.address(address_id); CASCADE --SEQONLY
--FKEY tx_rollup_return_bond_tx_roll; C.tx_rollup_return_bond; tx_rollup; C.address(address_id); CASCADE --SEQONLY
DO $$
BEGIN
ALTER TABLE C.tx_rollup_return_bond DROP COLUMN nonce;
EXCEPTION WHEN OTHERS THEN RETURN;
END $$;


-----------------------------------------------------------------------------
-- transfer_ticket -- manop -- since v10
CREATE TABLE IF NOT EXISTS C.transfer_ticket ( -- introduced by proto 13 --L
  operation_id bigint not null
  -- operation id from operation_alpha
  , source_id bigint not null
  -- source
  , consumed_milligas numeric
  -- consumed milligas
  , fee bigint not null
  -- fees
  , contents jsonb not null
  , ty jsonb not null
  , ticketer bigint not null
  , amount numeric not null
  , destination_id bigint not null
  , entrypoint jsonb not null
  , paid_storage_size_diff numeric
  , ticket_receipt jsonb -- since v10.4.0
  , status smallint not null default 0 -- 0: applied, 1: backtracked, 2: failed, 3: skipped
  , error_trace jsonb
  , error_trace_ json
);
--PKEY transfer_ticket_pkey; C.transfer_ticket; operation_id --SEQONLY
--FKEY transfer_ticket_operation_id_fkey; C.transfer_ticket; operation_id; C.operation_alpha(autoid); CASCADE --SEQONLY
--FKEY transfer_ticket_source_fkey; C.transfer_ticket; source_id; C.address(address_id); CASCADE --SEQONLY

DO $$
BEGIN
ALTER TABLE C.transfer_ticket DROP COLUMN tx_rollup; -- since v10.2.2 (was created by mistake)
EXCEPTION WHEN OTHERS THEN RETURN;
END $$;

DO $$
BEGIN
ALTER TABLE C.transfer_ticket ADD COLUMN ticket_receipt jsonb; -- necessary since v10.4.0 for DB upgraded from a prior version, alteration added in v10.6.3
EXCEPTION WHEN OTHERS THEN RETURN;
END $$;

-----------------------------------------------------------------------------
-- Drain delegate -- since v10.3.0
CREATE TABLE IF NOT EXISTS C.drain_delegate ( --L
  operation_id bigint not null
, consensus_key_id bigint not null
, delegate_id bigint not null
, destination_id bigint not null
, allocated_destination_contract bool not null
);
--PKEY drain_delegate_pkey; C.drain_delegate; operation_id --SEQONLY
--FKEY drain_delegate_operation_id_fkey; C.drain_delegate; operation_id; C.operation_alpha(autoid); CASCADE --SEQONLY
--FKEY drain_delegate_consensus_key_id_fkey; C.drain_delegate; consensus_key_id; C.address(address_id); CASCADE --SEQONLY
--FKEY drain_delegate_delegate_id_fkey; C.drain_delegate; delegate_id; C.address(address_id); CASCADE --SEQONLY
--FKEY drain_delegate_destination_id_fkey; C.drain_delegate; destination_id; C.address(address_id); CASCADE --SEQONLY

-----------------------------------------------------------------------------
-- Sc_rollup_originate -- manop -- since v9.8.2

CREATE TABLE IF NOT EXISTS C.sc_rollup_originate ( --L
  operation_id bigint not null
  -- operation id from operation_alpha
  , source_id bigint not null
  -- source
  , address_id bigint
  -- address
  , size numeric
  -- size
  , kind text
  -- kind
  , boot_sector_hash char(32) not null -- since v10.4.0
  -- boot sector
  , consumed_milligas numeric
  -- consumed milligas
  , fee bigint not null
  -- fees
  , parameters_ty jsonb not null
  , origination_proof text
  , genesis_commitment_hash char(54)
  , status smallint not null default 0 -- 0: applied, 1: backtracked, 2: failed, 3: skipped
  , error_trace jsonb
  , error_trace_ json
);
--PKEY sc_rollup_originate_pkey; C.sc_rollup_originate; operation_id --SEQONLY
--FKEY sc_rollup_originate_operation_id_fkey; C.sc_rollup_originate; operation_id; C.operation_alpha(autoid); CASCADE --SEQONLY
--FKEY sc_rollup_originate_source_fkey; C.sc_rollup_originate; source_id; C.address(address_id); CASCADE --SEQONLY
--FKEY sc_rollup_originate_address_fkey; C.sc_rollup_originate; address_id; C.address(address_id); CASCADE --SEQONLY
DO $$
BEGIN
ALTER TABLE C.sc_rollup_originate DROP COLUMN nonce;
EXCEPTION WHEN OTHERS THEN RETURN;
END $$;


DO $$
BEGIN
ALTER TABLE C.sc_rollup_originate ADD COLUMN boot_sector_hash char(32) not null;
EXCEPTION WHEN OTHERS THEN RETURN;
END $$;
DO $$
BEGIN
ALTER TABLE C.sc_rollup_originate ADD COLUMN genesis_commitment_hash char(54);
EXCEPTION WHEN OTHERS THEN RETURN;
END $$;
DO $$
BEGIN
ALTER TABLE C.sc_rollup_originate ADD COLUMN parameters_ty jsonb not null;
EXCEPTION WHEN OTHERS THEN RETURN;
END $$;
DO $$
BEGIN
ALTER TABLE C.sc_rollup_originate ADD COLUMN origination_proof text not null;
EXCEPTION WHEN OTHERS THEN RETURN;
END $$;

DO $$
BEGIN
ALTER TABLE C.sc_rollup_originate DROP COLUMN boot_sector;
EXCEPTION WHEN OTHERS THEN RETURN;
END $$;
DO $$
BEGIN
ALTER TABLE C.sc_rollup_originate ALTER COLUMN genesis_commitment_hash TYPE char(54);
EXCEPTION WHEN OTHERS THEN RETURN;
END $$;
DO $$
BEGIN
ALTER TABLE C.sc_rollup_originate ALTER COLUMN origination_proof TYPE text;
EXCEPTION WHEN OTHERS THEN RETURN;
END $$;
DO $$
BEGIN
ALTER TABLE C.sc_rollup_originate ALTER COLUMN origination_proof DROP NOT NULL;
EXCEPTION WHEN OTHERS THEN RETURN;
END $$;

CREATE TABLE IF NOT EXISTS C.sc_rollup_boot_sector ( -- since v10.4.0 --L
  hash char(32) primary key -- md5sum
, txt text -- more human readable
, bin bytea -- fallback when it doesn't fit boot_sector_txt
);

-----------------------------------------------------------------------------
-- sc_rollup_publish -- manop -- since v10.3.0
CREATE TABLE IF NOT EXISTS C.sc_rollup_publish ( -- introduced by proto 13 --L
  operation_id bigint not null
  -- operation id from operation_alpha
  , source_id bigint not null
  -- source
  , rollup_id bigint not null
  , consumed_milligas numeric
  -- consumed milligas
  , fee bigint not null
  -- fees
  , commitment jsonb not null
  , staked_hash char(54)
  , published_at_level int
  , status smallint not null default 0 -- 0: applied, 1: backtracked, 2: failed, 3: skipped
  , error_trace jsonb
  , error_trace_ json
);
--PKEY sc_rollup_publish_pkey; C.sc_rollup_publish; operation_id --SEQONLY
--FKEY sc_rollup_publish_operation_id_fkey; C.sc_rollup_publish; operation_id; C.operation_alpha(autoid); CASCADE --SEQONLY
--FKEY sc_rollup_publish_source_fkey; C.sc_rollup_publish; source_id; C.address(address_id); CASCADE --SEQONLY
--FKEY sc_rollup_publish_sc_rollup_fkey; C.sc_rollup_publish; rollup_id; C.address(address_id); CASCADE --SEQONLY
DO $$
BEGIN
ALTER TABLE C.sc_rollup_publish DROP COLUMN nonce;
EXCEPTION WHEN OTHERS THEN RETURN;
END $$;


DO $$ -- since v10.4.0
BEGIN
ALTER TABLE C.sc_rollup_publish ALTER COLUMN staked_hash TYPE char(54);
EXCEPTION WHEN OTHERS THEN RETURN;
END $$;


-----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS C.sc_rollup_cement ( --L
  operation_id bigint not null
  -- operation id from operation_alpha
  , source_id bigint not null
  -- source
  , fee bigint not null
  -- fees
  , consumed_milligas numeric
  -- consumed milligas
  , status smallint not null default 0 -- 0: applied, 1: backtracked, 2: failed, 3: skipped
  , error_trace jsonb
  , inbox_level int
  , rollup_id bigint
  , commitment_hash char(54)
  , error_trace_ json
);
--PKEY sc_rollup_cement_pkey; C.sc_rollup_cement; operation_id --SEQONLY
--FKEY sc_rollup_cement_operation_id_fkey; C.sc_rollup_cement; operation_id; C.operation_alpha(autoid); CASCADE --SEQONLY
--FKEY sc_rollup_cement_source_fkey; C.sc_rollup_cement; source_id; C.address(address_id); CASCADE --SEQONLY
--FKEY sc_rollup_cement_rollup_fkey; C.sc_rollup_cement; rollup_id; C.address(address_id); CASCADE --SEQONLY


-----------------------------------------------------------------------------
-- sc_rollup_timeout -- manop -- since v10.4.0
CREATE TABLE IF NOT EXISTS C.sc_rollup_timeout ( --L
  operation_id bigint not null
  -- operation id from operation_alpha
  , source_id bigint not null
  -- source
  , rollup_id bigint not null
  , consumed_milligas numeric
  -- consumed milligas
  , fee bigint not null
  -- fees
  , stakers jsonb not null
  , game_status jsonb
  , status smallint not null default 0 -- 0: applied, 1: backtracked, 2: failed, 3: skipped
  , error_trace jsonb
  , error_trace_ json
);
--PKEY sc_rollup_timeout_pkey; C.sc_rollup_timeout; operation_id --SEQONLY
--FKEY sc_rollup_timeout_operation_id_fkey; C.sc_rollup_timeout; operation_id; C.operation_alpha(autoid); CASCADE --SEQONLY
--FKEY sc_rollup_timeout_source_fkey; C.sc_rollup_timeout; source_id; C.address(address_id); CASCADE --SEQONLY
--FKEY sc_rollup_timeout_sc_rollup_fkey; C.sc_rollup_timeout; rollup_id; C.address(address_id); CASCADE --SEQONLY


-----------------------------------------------------------------------------
-- sc_rollup_refute -- manop -- since v10.4.0
CREATE TABLE IF NOT EXISTS C.sc_rollup_refute ( --L
  operation_id bigint not null
  -- operation id from operation_alpha
  , source_id bigint not null
  -- source
  , rollup_id bigint not null
  , consumed_milligas numeric
  -- consumed milligas
  , fee bigint not null
  -- fees
  , refutation jsonb not null
  , opponent_id bigint not null
  , game_status jsonb
  , status smallint not null default 0 -- 0: applied, 1: backtracked, 2: failed, 3: skipped
  , error_trace jsonb
  , error_trace_ json
);
--PKEY sc_rollup_refute_pkey; C.sc_rollup_refute; operation_id --SEQONLY
--FKEY sc_rollup_refute_operation_id_fkey; C.sc_rollup_refute; operation_id; C.operation_alpha(autoid); CASCADE --SEQONLY
--FKEY sc_rollup_refute_source_fkey; C.sc_rollup_refute; source_id; C.address(address_id); CASCADE --SEQONLY
--FKEY sc_rollup_refute_opponent_fkey; C.sc_rollup_refute; opponent_id; C.address(address_id); CASCADE --SEQONLY
--FKEY sc_rollup_refute_sc_rollup_fkey; C.sc_rollup_refute; rollup_id; C.address(address_id); CASCADE --SEQONLY


-----------------------------------------------------------------------------
-- zk_rollup_publish -- manop -- since v10.3.0
CREATE TABLE IF NOT EXISTS C.zk_rollup_publish ( -- introduced by proto 13 --L
  operation_id bigint not null
  , source_id bigint not null
  , fee bigint not null
  , status smallint not null default 0 -- 0: applied, 1: backtracked, 2: failed, 3: skipped
  , consumed_milligas numeric
  , error_trace jsonb
  , error_trace_ json
  , zk_rollup_id bigint not null
  , ops jsonb not null
  , paid_storage_size_diff numeric
);
--PKEY zk_rollup_publish_pkey; C.zk_rollup_publish; operation_id --SEQONLY
--FKEY zk_rollup_publish_operation_id_fkey; C.zk_rollup_publish; operation_id; C.operation_alpha(autoid); CASCADE --SEQONLY
--FKEY zk_rollup_publish_source_fkey; C.zk_rollup_publish; source_id; C.address(address_id); CASCADE --SEQONLY
--FKEY zk_rollup_publish_zk_rollup_fkey; C.zk_rollup_publish; zk_rollup_id; C.address(address_id); CASCADE --SEQONLY

-----------------------------------------------------------------------------
-- Sc_rollup_add_messages -- manop -- since v9.9.9

CREATE TABLE IF NOT EXISTS C.sc_rollup_add_messages ( -- introduced by proto 13 candidate --L
  operation_id bigint not null
  -- operation id from operation_alpha
  , source_id bigint not null
  -- source
  , messages text[]
  , consumed_milligas numeric
  -- consumed milligas
  , fee bigint not null
  -- fees
  , status smallint not null default 0 -- 0: applied, 1: backtracked, 2: failed, 3: skipped
  , error_trace jsonb
  , error_trace_ json
);
--PKEY sc_rollup_add_messages_pkey; C.sc_rollup_add_messages; operation_id --SEQONLY
--FKEY sc_rollup_add_messages_operation_id_fkey; C.sc_rollup_add_messages; operation_id; C.operation_alpha(autoid); CASCADE --SEQONLY
--FKEY sc_rollup_add_messages_source_fkey; C.sc_rollup_add_messages; source_id; C.address(address_id); CASCADE --SEQONLY
DO $$
BEGIN
ALTER TABLE C.sc_rollup_add_messages DROP COLUMN nonce;
EXCEPTION WHEN OTHERS THEN RETURN;
END $$;


DO $$ -- since v10.4.0
BEGIN
ALTER TABLE C.sc_rollup_add_messages DROP COLUMN rollup_id;
EXCEPTION WHEN OTHERS THEN RETURN;
END $$;
DO $$
BEGIN
ALTER TABLE C.sc_rollup_add_messages DROP COLUMN inbox_after;
EXCEPTION WHEN OTHERS THEN RETURN;
END $$;



-----------------------------------------------------------------------------
-- event -- manop -- since v10.2.0

CREATE TABLE IF NOT EXISTS C.event ( -- introduced by proto 14 --L
  operation_id bigint not null
  -- operation id from operation_alpha
  , source_id bigint not null
  -- source
  , nonce int not null
  , status smallint not null default 0 -- 0: applied, 1: backtracked, 2: failed, 3: skipped
  , ty jsonb not null
  , tag jsonb not null
  , payload jsonb not null
  , consumed_milligas numeric -- nullable
  -- consumed milligas
  , error_trace jsonb -- nullable
  , error_trace_ json
);
--PKEY event_pkey; C.event; operation_id --SEQONLY
--FKEY event_operation_id_fkey; C.event; operation_id; C.operation_alpha(autoid); CASCADE --SEQONLY
--FKEY event_source_fkey; C.event; source_id; C.address(address_id); CASCADE --SEQONLY
ALTER TABLE C.event ALTER COLUMN nonce SET NOT NULL;
DO $$
BEGIN
ALTER TABLE C.event DROP COLUMN fee;
EXCEPTION WHEN OTHERS THEN RETURN;
END $$;


-----------------------------------------------------------------------------
-- increase_paid_storage -- manop -- since v10.2.0

CREATE TABLE IF NOT EXISTS C.increase_paid_storage ( -- introduced by proto 14 --L
  operation_id bigint not null
  -- operation id from operation_alpha
  , source_id bigint not null
  , fee bigint not null
  , status smallint not null default 0 -- 0: applied, 1: backtracked, 2: failed, 3: skipped
  , amount_in_bytes numeric not null
  , destination_id bigint not null
  , consumed_milligas numeric -- nullable
  , error_trace jsonb -- nullable
  , error_trace_ json
);
--PKEY increase_paid_storage_pkey; C.increase_paid_storage; operation_id --SEQONLY
--FKEY increase_paid_storage_operation_id_fkey; C.increase_paid_storage; operation_id; C.operation_alpha(autoid); CASCADE --SEQONLY
--FKEY increase_paid_storage_source_fkey; C.increase_paid_storage; source_id; C.address(address_id); CASCADE --SEQONLY
--FKEY increase_paid_storage_destination_fkey; C.increase_paid_storage; destination_id; C.address(address_id); CASCADE --SEQONLY
DO $$
BEGIN
  ALTER TABLE C.increase_paid_storage DROP COLUMN nonce;
EXCEPTION WHEN OTHERS THEN RETURN;
END $$;

-----------------------------------------------------------------------------
-- dal_publish_slot_header -- manop -- since v10.2.1

CREATE TABLE IF NOT EXISTS C.dal_publish_slot_header ( -- introduced by proto 14 but implemented in proto 15 --L
  operation_id bigint not null
  -- operation id from operation_alpha
  , source_id bigint not null
  -- source
  , fee bigint not null
  -- fees
  , published_level int -- nullable since proto O
  , index jsonb not null
  , commitment jsonb not null
  , proof jsonb not null
  , status smallint not null default 0 -- 0: applied, 1: backtracked, 2: failed, 3: skipped
  , consumed_milligas numeric -- nullable
  , error_trace jsonb -- nullable
  , error_trace_ json
);
--PKEY dal_publish_slot_header_pkey; C.dal_publish_slot_header; operation_id --SEQONLY
--FKEY dal_publish_slot_header_operation_id_fkey; C.dal_publish_slot_header; operation_id; C.operation_alpha(autoid); CASCADE --SEQONLY
--FKEY dal_publish_slot_header_source_fkey; C.dal_publish_slot_header; source_id; C.address(address_id); CASCADE --SEQONLY

DO $$ -- since v10.4.0
BEGIN
ALTER TABLE C.dal_publish_slot_header DROP COLUMN nonce;
EXCEPTION WHEN OTHERS THEN RETURN;
END $$;
DO $$
BEGIN
ALTER TABLE C.dal_publish_slot_header DROP COLUMN dal_slot;
EXCEPTION WHEN OTHERS THEN RETURN;
END $$;
DO $$ -- since v10.4.0
BEGIN
ALTER TABLE C.dal_publish_slot_header ADD COLUMN published_level int;
EXCEPTION WHEN OTHERS THEN RETURN;
END $$;
DO $$
BEGIN
ALTER TABLE C.dal_publish_slot_header ALTER COLUMN published_level DROP NOT NULL;
EXCEPTION WHEN OTHERS THEN RETURN;
END $$;
DO $$
BEGIN
ALTER TABLE C.dal_publish_slot_header ADD COLUMN index jsonb not null;
EXCEPTION WHEN OTHERS THEN RETURN;
END $$;
DO $$
BEGIN
ALTER TABLE C.dal_publish_slot_header ADD COLUMN commitment jsonb not null;
EXCEPTION WHEN OTHERS THEN RETURN;
END $$;
DO $$
BEGIN
ALTER TABLE C.dal_publish_slot_header ADD COLUMN proof jsonb not null;
EXCEPTION WHEN OTHERS THEN RETURN;
END $$;

-----------------------------------------------------------------------------
-- dal_attestation -- since v10.4.0

CREATE TABLE IF NOT EXISTS C.dal_attestation ( --L
  operation_id bigint not null
  , attestor_id bigint -- nullable since proto 18
  , attestation jsonb not null
  , level int not null
  , delegate_id bigint not null
);
--PKEY dal_attestation_pkey; C.dal_attestation; operation_id --SEQONLY
--FKEY dal_attestation_operation_id_fkey; C.dal_attestation; operation_id; C.operation_alpha(autoid); CASCADE --SEQONLY
--FKEY dal_attestation_attestor_fkey; C.dal_attestation; attestor_id; C.address(address_id); CASCADE --SEQONLY
--FKEY dal_attestation_delegate_fkey; C.dal_attestation; delegate_id; C.address(address_id); CASCADE --SEQONLY

DO $$
BEGIN
ALTER TABLE C.dal_attestation ALTER COLUMN attestor_id DROP NOT NULL;
EXCEPTION WHEN OTHERS THEN RETURN;
END $$;

-----------------------------------------------------------------------------
-- sc_rollup_execute_outbox_message -- manop -- since v10.3.0

CREATE TABLE IF NOT EXISTS C.sc_rollup_execute_outbox_message ( -- introduced by proto 14 but implemented in proto 15 --L
  operation_id bigint not null
  -- operation id from operation_alpha
  , source_id bigint not null
  -- source
  , fee bigint not null
  -- fees
  , status smallint not null default 0 -- 0: applied, 1: backtracked, 2: failed, 3: skipped
  , consumed_milligas numeric -- nullable
  , error_trace jsonb -- nullable
  , error_trace_ json
  , paid_storage_size_diff numeric -- nullable
  , rollup_id bigint not null
  , cemented_commitment char(54) not null
  , output_proof text not null
  , ticket_receipt jsonb not null
);
--PKEY sc_rollup_execute_outbox_message_pkey; C.sc_rollup_execute_outbox_message; operation_id --SEQONLY
--FKEY sc_rollup_execute_outbox_message_operation_id_fkey; C.sc_rollup_execute_outbox_message; operation_id; C.operation_alpha(autoid); CASCADE --SEQONLY
--FKEY sc_rollup_execute_outbox_message_source_fkey; C.sc_rollup_execute_outbox_message; source_id; C.address(address_id); CASCADE --SEQONLY
--FKEY sc_rollup_execute_outbox_message_rollup_fkey; C.sc_rollup_execute_outbox_message; rollup_id; C.address(address_id); CASCADE --SEQONLY

DO $$ -- since v10.4.0
BEGIN
ALTER TABLE C.sc_rollup_execute_outbox_message ADD COLUMN ticket_receipt jsonb not null;
EXCEPTION WHEN OTHERS THEN RETURN;
END $$;
DO $$
BEGIN
ALTER TABLE C.sc_rollup_execute_outbox_message ALTER COLUMN cemented_commitment TYPE char(54);
EXCEPTION WHEN OTHERS THEN RETURN;
END $$;


-----------------------------------------------------------------------------
-- update_consensus_key -- manop -- since v10.3.0

CREATE TABLE IF NOT EXISTS C.update_consensus_key ( -- introduced by proto 15 --L
  operation_id bigint not null
  -- operation id from operation_alpha
  , source_id bigint not null
  -- source
  , fee bigint not null
  -- fees
  , status smallint not null default 0 -- 0: applied, 1: backtracked, 2: failed, 3: skipped
  , consumed_milligas numeric -- nullable
  , error_trace jsonb -- nullable
  , error_trace_ json
  , public_key text
);
--PKEY update_consensus_key_pkey; C.update_consensus_key; operation_id --SEQONLY
--FKEY update_consensus_key_operation_id_fkey; C.update_consensus_key; operation_id; C.operation_alpha(autoid); CASCADE --SEQONLY
--FKEY update_consensus_key_source_fkey; C.update_consensus_key; source_id; C.address(address_id); CASCADE --SEQONLY
DO $$
BEGIN
ALTER TABLE C.update_consensus_key DROP COLUMN nonce;
EXCEPTION WHEN OTHERS THEN RETURN;
END $$;

----------------------------------------------------------------------------
-- sc_rollup_recover_bond -- manop -- since v10.3.0

CREATE TABLE IF NOT EXISTS C.sc_rollup_recover_bond ( -- introduced by proto 14 but implemented in proto 15 --L
  operation_id bigint not null
  -- operation id from operation_alpha
  , source_id bigint not null
  -- source
  , fee bigint not null
  -- fees
  , status smallint not null default 0 -- 0: applied, 1: backtracked, 2: failed, 3: skipped
  , consumed_milligas numeric -- nullable
  , error_trace jsonb -- nullable
  , error_trace_ json
  , rollup_id bigint not null
);
--PKEY sc_rollup_recover_bond_pkey; C.sc_rollup_recover_bond; operation_id --SEQONLY
--FKEY sc_rollup_recover_bond_operation_id_fkey; C.sc_rollup_recover_bond; operation_id; C.operation_alpha(autoid); CASCADE --SEQONLY
--FKEY sc_rollup_recover_bond_source_fkey; C.sc_rollup_recover_bond; source_id; C.address(address_id); CASCADE --SEQONLY
--FKEY sc_rollup_recover_bond_rollup_fkey; C.sc_rollup_recover_bond; rollup_id; C.address(address_id); CASCADE --SEQONLY
DO $$
BEGIN
ALTER TABLE C.sc_rollup_recover_bond DROP COLUMN nonce;
EXCEPTION WHEN OTHERS THEN RETURN;
END $$;


---------------------------------------------------------------------------
-- sc_rollup_dal_slot_subscribe
DROP TABLE IF EXISTS C.sc_rollup_dal_slot_subscribe CASCADE; -- this operation no longer exists, and has never been used on mainnet

-----------------------------------------------------------------------------
-- vdf_revelation -- since v10.2.1

CREATE TABLE IF NOT EXISTS C.vdf_revelation ( -- introduced by proto 14 --L
  operation_id bigint not null
  -- operation id from operation_alpha
  , vdf_solution jsonb
  , error_trace jsonb -- nullable
  , error_trace_ json
);
--PKEY vdf_revelation_pkey; C.vdf_revelation; operation_id --SEQONLY
--FKEY vdf_revelation_operation_id_fkey; C.vdf_revelation; operation_id; C.operation_alpha(autoid); CASCADE --SEQONLY

-----------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION status (s smallint)
RETURNS char
AS $$
begin
case
when s = 0 then return 'applied';
when s = 1 then return 'backtracked';
when s = 2 then return 'failed';
when s = 3 then return 'skipped';
end case;
end;
$$ LANGUAGE PLPGSQL STABLE;


-----------------------------------------------------------------------------
-- Balance: record balance diffs

-- v9.5 merged C.balance_updates_block and C.balance_updates_op into a single table, and adds column implicit_operations_results_id
CREATE TABLE IF NOT EXISTS C.balance_updates ( --L
  block_level int not null,
  -- block hash
  operation_id bigint,
  -- references C.operation_alpha.autoid if any
  implicit_operations_results_id int,
  -- references C.implicit_operations_results.id if any
  balance_kind smallint not null,
  -- balance kind: -- TODO/FIXME: make this list automatically generated (so that clashes are guaranteed to never exist)
  --  0 : Contract
  --  1 : Rewards, or legacy rewards
  --  2 : Fees, or legacy fees
  --  3 : Deposits, or legacy deposits
  --  4 : Block_fees (since proto 12)
  --  5 : Nonce_revelation_rewards (since proto 12)
  --  6 : Double_signing_evidence_rewards (since proto 12)
  --  7 : Endorsing_rewards (since proto 12)
  --  8 : Baking_rewards (since proto 12)
  --  9 : Baking_bonuses (since proto 12)
  -- 10 : Storage_fees (since proto 12)
  -- 11 : Double_signing_punishments (since proto 12)
  -- 12 : Lost_endorsing_rewards (since proto 12)
  -- 13 : Liquidity_baking_subsidies (since proto 12)
  -- 14 : Burned (since proto 12)
  -- 15 : Commitments (since proto 12)
  -- 16 : Bootstrap (since proto 12)
  -- 17 : Invoice (since proto 12)
  -- 18 : Initial_commitments (since proto 12)
  -- 19 : Minted (since proto 12)
  contract_address_id bigint, -- nullable since protocol I -- since v9.9.5
  blinded_public_key_hash char(37), -- since proto 12
  -- b58check encoded address of contract(either implicit or originated)
  cycle int, -- only balance_kind 1,2,3 have cycle
  -- cycle
  diff bigint not null,
  -- balance update
  -- credited if positve
  -- debited if negative
  id int not null -- unique position within the block to allow rightful duplicates and reject wrong duplicates -- this number is ≥ 0
  , origin smallint -- 0: block application, 1: protocol migration, 2: inflationary subsidy, 3: simulation of an operation -- since v10.3.0
    -- 0 | Block_application
    -- 1 | Protocol_migration
    -- 2 | Subsidy
    -- 3 | Simulation
    -- 4 | Delayed_operation : since proto Oxford2
  , delayed_operation char(51) -- (non null) <=> (if origin = 4)
  , primary key (block_level, id)
);
DO $$
BEGIN
ALTER TABLE C.balance_updates ADD COLUMN origin int; -- since v10.3.0
EXCEPTION WHEN OTHERS THEN RETURN;
END $$;
DO $$
BEGIN
ALTER TABLE C.balance_updates ADD COLUMN delayed_operation char(51); -- since v10.6.4
EXCEPTION WHEN OTHERS THEN RETURN;
END $$;
--FKEY balance_updates_block_hash_fkey; C.balance_updates; block_level; C.block(level); CASCADE --SEQONLY
--FKEY balance_updates_contract_address_fkey; C.balance_updates; contract_address_id; C.address(address_id); CASCADE --SEQONLY
--FKEY balance_updates_iorid_fkey; C.balance_updates; block_level, implicit_operations_results_id; C.implicit_operations_results(block_level, id); CASCADE --SEQONLY



CREATE OR REPLACE FUNCTION balance_kind (bk smallint) -- TODO: generate this function
RETURNS char
AS
$$
SELECT coalesce ((select 'contract' where bk = 0),
       coalesce ((select 'rewards' where bk = 1),
       coalesce ((select 'fees' where bk = 2),
       coalesce ((select 'deposits' where bk = 3),
       coalesce ((select 'block_fees' where bk = 4),
       coalesce ((select 'nonce_revelation_rewards' where bk = 5),
       coalesce ((select 'double_signing_evidence_rewards' where bk = 6),
       coalesce ((select 'endorsing_rewards' where bk = 7),
       coalesce ((select 'baking_rewards' where bk = 8),
       coalesce ((select 'baking_bonuses' where bk = 9),
       coalesce ((select 'storage_fees' where bk = 10),
       coalesce ((select 'double_signing_punishments' where bk = 11),
       coalesce ((select 'lost_endorsing_rewards' where bk = 12),
       coalesce ((select 'liquidity_baking_subsidies' where bk = 13),
       coalesce ((select 'burned' where bk = 14),
       coalesce ((select 'commitments' where bk = 15),
       coalesce ((select 'bootstrap' where bk = 16),
       coalesce ((select 'invoice' where bk = 17),
       coalesce ((select 'initial_commitments' where bk = 18),
       coalesce ((select 'minted' where bk = 19),
       coalesce ((select 'frozen_bonds' where bk = 20),
       coalesce ((select 'tx_rollup_rejection_punishments' where bk = 21),
       coalesce ((select 'tx_rollup_rejection_rewards' where bk = 22),
       coalesce ((select 'sc_rollup_refutation_punishments' where bk = 23),
       coalesce ((select 'sc_rollup_refutation_rewards' where bk = 24))))))))))))))))))))))))))
$$ LANGUAGE SQL STABLE;

CREATE OR REPLACE FUNCTION balance_kind (bk int)
RETURNS char
AS $$ SELECT balance_kind (bk::smallint) $$ LANGUAGE SQL STABLE;



-----------------------------------------------------------------------------
-- Snapshot blocks
-- The snapshot block for a given cycle is obtained as follows
-- at the last block of cycle n, the snapshot block for cycle n+6 is selected
-- Use [Storage.Roll.Snapshot_for_cycle.get C.txt cycle] in proto_alpha to
-- obtain this value.
-- RPC: /chains/main/blocks/${block}/context/raw/json/cycle/${cycle}
-- where:
-- ${block} denotes a block(either by hash or level)
-- ${cycle} denotes a cycle which must be in [cycle_of(level)-5,cycle_of(level)+7]

CREATE TABLE IF NOT EXISTS C.snapshot ( --L
  cycle int,
  level int,
  primary key (cycle, level)
);

-----------------------------------------------------------------------------
-- Could be useful for baking.
-- CREATE TABLE IF NOT EXISTS delegate (
--   cycle int not null,
--   level int not null,
--   pkh char(36) not null,
--   balance bigint not null,
--   frozen_balance bigint not null,
--   staking_balance bigint not null,
--   delegated_balance bigint not null,
--   deactivated bool not null,
--   grace smallint not null,
--   primary key (cycle, pkh),
--   , foreign key (cycle, level) references snapshot(cycle, level)
--   , foreign key (pkh) references implicit(pkh)
-- );

-----------------------------------------------------------------------------
-- Delegated contract table -- NOT FILLED

CREATE TABLE IF NOT EXISTS C.delegated_contract ( --L
  delegate_id bigint,
  -- tz1 of the delegate
  delegator_id bigint,
  -- address of the delegator (for now, KT1 but this could change)
  cycle int,
  level int
  , primary key (delegate_id, delegator_id, cycle, level)
);
--PKEY delegated_contract_pkey; C.delegated_contract; delegate_id, delegator_id, cycle, level --SEQONLY
--FKEY delegated_contract_delegate_fkey; C.delegated_contract; delegate_id; C.address(address_id); CASCADE --SEQONLY
--FKEY delegated_contract_delegator_fkey; C.delegated_contract; delegator_id; C.address(address_id); CASCADE --SEQONLY
--FKEY delegated_contract_cycle_level_fkey; C.delegated_contract; cycle, level; C.snapshot(cycle, level); CASCADE --SEQONLY

-----------------------------------------------------------------------------
-- Could be useful for baking.
-- CREATE TABLE IF NOT EXISTS stake (
--   delegate char(36) not null,
--   level int not null,
--   k char(36) not null,
--   kind smallint not null,
--   diff bigint not null,
--   primary key (delegate, level, k, kind, diff),
--   , foreign key (delegate) references implicit(pkh)
--   , foreign key (k) references C.address(address_id)
-- );
-- list of operations and their unique IDs
select declare_operation('Event', 43);
select declare_operation('Transaction', 8);
select declare_operation('Origination', 9);
select declare_operation('Delegation', 10);
select declare_operation('Reveal', 7);
select declare_operation('Tx_rollup_origination', 17);
select declare_operation('Tx_rollup_submit_batch', 18);
select declare_operation('Tx_rollup_commit', 19);
select declare_operation('Tx_rollup_return_bond', 20);
select declare_operation('Tx_rollup_finalize_commitment', 21);
select declare_operation('Tx_rollup_remove_commitment', 22);
select declare_operation('Tx_rollup_rejection', 23);
select declare_operation('Tx_rollup_dispatch_tickets', 24);
select declare_operation('Transfer_ticket', 25);
select declare_operation('Sc_rollup_originate', 26);
select declare_operation('Sc_rollup_add_messages', 27);
select declare_operation('Sc_rollup_cement', 28);
select declare_operation('Sc_rollup_publish', 29);
select declare_operation('Sc_rollup_refute', 30);
select declare_operation('Sc_rollup_timeout', 31);
select declare_operation('Dal_publish_slot_header', 32);
select declare_operation('Sc_rollup_execute_outbox_message', 33);
select declare_operation('Sc_rollup_recover_bond', 34);
select declare_operation('Sc_rollup_dal_slot_subscribe', 35);
select declare_operation('Increase_paid_storage', 38);
select declare_operation('Register_global_constant', 13);
select declare_operation('Set_deposits_limit', 14);
select declare_operation('Zk_rollup_origination', 39);
select declare_operation('Update_consensus_key', 40);
select declare_operation('Zk_rollup_publish', 42);
select declare_operation('Zk_rollup_update', 44);
select declare_operation('Endorsement', 0);
select declare_operation('Attestation', 0);
select declare_operation('Seed_nonce_revelation', 1);
select declare_operation('Double_endorsement_evidence', 2);
select declare_operation('Double_attestation_evidence', 2);
select declare_operation('Double_baking_evidence', 3);
select declare_operation('Activate_account', 4);
select declare_operation('Proposals', 5);
select declare_operation('Ballot', 6);
select declare_operation('Endorsement_with_slot', 11);
select declare_operation('Endorsement_with_slot', 11);
select declare_operation('Preendorsement', 15);
select declare_operation('Preattestation', 15);
select declare_operation('Double_preendorsement_evidence', 16);
select declare_operation('Double_preattestation_evidence', 16);
select declare_operation('Failing_noop', 12);
select declare_operation('Dal_slot_availability', 36);
select declare_operation('Vdf_revelation', 37);
select declare_operation('Drain_delegate', 41);
select declare_operation('Dal_attestation', 45);
-- src/db-schema/chain_functions.sql
-- Open Source License
-- Copyright (c) 2019-2022 Nomadic Labs <contact@nomadic-labs.com>
--
-- Permission is hereby granted, free of charge, to any person obtaining a
-- copy of this software and associated documentation files (the "Software"),
-- to deal in the Software without restriction, including without limitation
-- the rights to use, copy, modify, merge, publish, distribute, sublicense,
-- and/or sell copies of the Software, and to permit persons to whom the
-- Software is furnished to do so, subject to the following conditions:
--
-- The above copyright notice and this permission notice shall be included
-- in all copies or substantial portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
-- IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
-- FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
-- THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
-- LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
-- FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
-- DEALINGS IN THE SOFTWARE.

-----------------------------------------------------------------------------
-- Naming conventions:
-- - for functions:
--   * I.table -> insert into table
--   * U_table -> update table
--   * IU_table -> insert or update table (aka upsert)
--   * u_concept -> update more than one table
--   * B_action -> action on bigmaps
--   * BEWARE: upper/lower cases for prefixes are only for aesthetic purposes!
--     Function names are case-insensitive!
-----------------------------------------------------------------------------

SELECT 'chain_functions.sql' as file;

CREATE OR REPLACE FUNCTION record_log (msg text)
RETURNS void
AS $$
insert into indexer_log values (CURRENT_TIMESTAMP, '', '', msg) on conflict do nothing;
$$ LANGUAGE SQL;

CREATE OR REPLACE FUNCTION I.chain(c char)
RETURNS void
AS $$
BEGIN
  INSERT INTO C.chain(hash) VALUES (c)
  ON CONFLICT DO NOTHING;
  IF (SELECT COUNT(*) FROM C.chain) <> 1
  THEN
    RAISE 'You are trying to index a chain on a other chain‘s database.';
  END IF;
END;
$$ LANGUAGE PLPGSQL;


CREATE OR REPLACE FUNCTION I.block(bh char, l int, p smallint, pr char, t timestamp with time zone, vp smallint, m char, f char, c char)
RETURNS int
AS $$
DECLARE bl int := (SELECT level FROM C.block WHERE hash = bh);
BEGIN
  IF bl IS NOT NULL
  THEN
--EXTRA UPDATE indexer_measurements SET last_indexed = CURRENT_TIMESTAMP;
   RETURN bl;
  ELSE
    IF block_level(pr) IS NULL --SEQONLY
    THEN --SEQONLY
      RAISE 'block % cannot be inserted, predecessor % is absent', bh, pr; --SEQONLY
    END IF; --SEQONLY
    INSERT INTO C.block VALUES (bh, l, p, block_level(pr), t, vp, m, f, c); --SEQONLY
    bl := (SELECT level FROM C.block WHERE hash = bh); --SEQONLY
    IF bl IS NOT NULL --SEQONLY
    THEN --SEQONLY
--EXTRA INSERT INTO indexer_measurements (block_hash, level, timestamp, first_indexed) VALUES (bh, l, t, CURRENT_TIMESTAMP) ON CONFLICT (level, timestamp) DO UPDATE SET last_indexed = CURRENT_TIMESTAMP; --SEQONLY
      RETURN bl; --SEQONLY
    ELSE --SEQONLY
      RETURN -1; --SEQONLY
    END IF; --SEQONLY
  END IF;
END;
$$ LANGUAGE PLPGSQL;



CREATE OR REPLACE FUNCTION I.block0(bh char, l int, p smallint, pr char, t timestamp with time zone, vp smallint, m char, f char, c char)
RETURNS int
AS $$
DECLARE result int;
BEGIN
  IF bh = pr
  THEN
    insert into C.block values (bh, l, p, l, t, vp, m, f, c) on conflict do nothing;
  ELSE
    insert into C.block values (bh, l, p, block_level(pr), t, vp, m, f, c) on conflict do nothing;
  END IF;
  result := (SELECT block_level(bh));
  IF result IS NULL
  THEN RAISE 'block % failed to be inserted', bh;
  END IF;
--EXTRA INSERT INTO indexer_measurements (block_hash, level, timestamp, first_indexed) VALUES (bh, l, t, CURRENT_TIMESTAMP) ON CONFLICT (level, timestamp) DO UPDATE SET last_indexed = CURRENT_TIMESTAMP;
  -- result := l;
  RETURN result;
END;
$$ LANGUAGE PLPGSQL;


-- CREATE OR REPLACE FUNCTION confirm_block(block_level int, depth smallint)
-- RETURNS void
-- AS $$
-- update C.block set indexing_depth = depth where hash_id = block_level; --SEQONLY
-- $$ LANGUAGE SQL;


CREATE OR REPLACE FUNCTION max_level()
RETURNS int
AS $$
select level from C.block
order by level desc limit 1;
$$ LANGUAGE SQL stable;

CREATE OR REPLACE FUNCTION max_level_bh()
RETURNS table (level int, block_hash char)
AS $$
select (level, hash) from C.block
order by level desc limit 1;
$$ LANGUAGE SQL stable;

CREATE OR REPLACE FUNCTION I.operation_aux(h char, b int, hi bigint)
RETURNS bigint
AS $$
insert into C.operation (hash, block_level, hash_id)
values (h, b, hi)
on conflict do nothing
returning hash_id;
$$ LANGUAGE SQL;


CREATE OR REPLACE FUNCTION I.operation(h char, b int, hi bigint)
RETURNS bigint
AS $$
DECLARE r bigint := null;
BEGIN
  r := I.operation_aux(h, b, hi);
  if r is not null
  then
    return r;
  else
    r := (select hash_id from C.operation where hash = h and block_level = b);
    return r;
  end if;
END;
$$ LANGUAGE PLPGSQL;


DROP FUNCTION IF EXISTS I.block_alpha (block_level int, baker bigint, level_position int, cycle int, cycle_position int, voting_period jsonb, voting_period_position int, voting_period_kind smallint, consumed_milligas numeric);
CREATE OR REPLACE FUNCTION I.block_alpha (block_level int, baker bigint, level_position int, cycle int, cycle_position int, voting_period jsonb, voting_period_position int, voting_period_kind smallint, consumed_milligas numeric, delegate_id_ bigint)
RETURNS int
AS $$
BEGIN
  INSERT INTO C.block_alpha
  VALUES (block_level, baker, level_position, cycle, cycle_position, voting_period, voting_period_position, voting_period_kind, consumed_milligas, delegate_id_)
  ON CONFLICT DO NOTHING --SEQONLY
  ;
  RETURN (select level from c.block_alpha where level = block_level);
END;
$$ LANGUAGE PLPGSQL;


CREATE OR REPLACE FUNCTION I.opalpha (ophid bigint, opid smallint, opkind smallint, block_level int, i smallint, a bigint)
returns void
as $$
insert into C.operation_alpha(hash_id, id, operation_kind, block_level, internal, autoid)
values (ophid, opid, opkind, block_level, i, a)
on conflict do nothing --SEQONLY
$$ language sql;


CREATE OR REPLACE FUNCTION I.deactivated (pkhid bigint, block_level int)
returns void
as $$
insert into C.deactivated (pkh_id, block_level) values (pkhid, block_level)
on conflict do nothing  --SEQONLY
$$ language sql;


CREATE OR REPLACE FUNCTION I.activate (opaid bigint, pkhid bigint, ac char)
returns void
as $$
insert into C.activation(operation_id, pkh_id, activation_code)
values (opaid, pkhid, ac)
on conflict do nothing; --SEQONLY
$$ language sql;


CREATE OR REPLACE FUNCTION I.proposal (opaid bigint, i bigint, s bigint, period int, proposal char)
RETURNS VOID
AS $$
insert into C.proposals values (proposal, i) on conflict do nothing;
insert into C.proposal values (opaid, s, period, proposal_id(proposal))
on conflict do nothing --SEQONLY
;
insert into C.operation_sender_and_receiver values (opaid, s, null)
on conflict do nothing; --SEQONLY
$$ LANGUAGE SQL;

CREATE OR REPLACE FUNCTION I.proposal2 (opaid bigint, i bigint, s bigint, period int, proposal char)
RETURNS VOID
AS $$
-- the only difference with I.proposal is that this one does not create an entry in C.operation_sender_and_receiver because we know there already is one
insert into C.proposals values (proposal, i) on conflict do nothing;
insert into C.proposal values (opaid, s, period, proposal_id(proposal)) on conflict do nothing;
$$ LANGUAGE SQL;


CREATE OR REPLACE FUNCTION I.ballot (opaid bigint, i bigint, s bigint, period int, proposal char, ballot one_ballot)
RETURNS VOID
AS $$
-- in non-multicore mode, there's no vote for proposals that are unknown
-- in multicore mode, we can be recording ballots for proposals that haven't been recorded yet
insert into C.ballot
values (opaid, s, period, proposal_id(proposal), ballot)
on conflict do nothing --SEQONLY
;
insert into C.operation_sender_and_receiver values (opaid, s, null)
on conflict do nothing; --SEQONLY
$$ LANGUAGE SQL;

CREATE OR REPLACE FUNCTION I.vdf_revelation (opaid bigint, vdf_sol json)
RETURNS VOID
AS $$
insert into C.vdf_revelation (operation_id, vdf_solution) values (opaid, vdf_sol) on conflict do nothing;
$$ LANGUAGE SQL;

CREATE OR REPLACE FUNCTION I.double_endorsement(opaid bigint, baker_id bigint, offender_id bigint, xop1 text, xop2 text)
RETURNS void
AS $$
DECLARE err text;
BEGIN
  BEGIN
    INSERT INTO C.double_endorsement_evidence (operation_id, op1, op2) VALUES (opaid, xop1::jsonb, xop2::jsonb)
    ON CONFLICT DO NOTHING --SEQONLY
    ;
    EXCEPTION WHEN OTHERS THEN
      GET STACKED DIAGNOSTICS err = MESSAGE_TEXT;
      IF err = 'unsupported Unicode escape sequence'
      THEN
        INSERT INTO C.double_endorsement_evidence (operation_id, op1_, op2_) VALUES (opaid, xop1::json, xop2::json)
        ON CONFLICT DO NOTHING --SEQONLY
        ;
      ELSE
        RAISE EXCEPTION 'Error: %', err;
      END IF;
  END;
  INSERT INTO C.operation_sender_and_receiver VALUES (opaid, baker_id, offender_id)
  ON CONFLICT DO NOTHING  --SEQONLY
  ;
END;
$$ LANGUAGE PLPGSQL;


CREATE OR REPLACE FUNCTION I.double_preendorsement(opaid bigint, baker_id bigint, offender_id bigint, xop1 text, xop2 text)
RETURNS void
AS $$
DECLARE err text;
BEGIN
  BEGIN
    INSERT INTO C.double_preendorsement_evidence (operation_id, op1, op2) VALUES (opaid, xop1::jsonb, xop2::jsonb)
    ON CONFLICT DO NOTHING --SEQONLY
    ;
    EXCEPTION WHEN OTHERS THEN
      GET STACKED DIAGNOSTICS err = MESSAGE_TEXT;
      IF err = 'unsupported Unicode escape sequence'
      THEN
        INSERT INTO C.double_preendorsement_evidence (operation_id, op1_, op2_) VALUES (opaid, xop1::json, xop2::json)
        ON CONFLICT DO NOTHING --SEQONLY
        ;
      ELSE
        RAISE EXCEPTION 'Error: %', err;
      END IF;
  END;
  INSERT INTO C.operation_sender_and_receiver VALUES (opaid, baker_id, offender_id)
  ON CONFLICT DO NOTHING  --SEQONLY
  ;
END;
$$ LANGUAGE PLPGSQL;



CREATE OR REPLACE FUNCTION I.double_baking(opaid bigint, xbh1 text, xbh2 text, baker_id bigint, offender_id bigint)
RETURNS void
AS $$
DECLARE err text;
BEGIN
  BEGIN
    INSERT INTO C.double_baking_evidence (operation_id, bh1, bh2) VALUES (opaid, xbh1::jsonb, xbh2::jsonb)
    ON CONFLICT DO NOTHING --SEQONLY
    ;
    EXCEPTION WHEN OTHERS THEN
      GET STACKED DIAGNOSTICS err = MESSAGE_TEXT;
      IF err = 'unsupported Unicode escape sequence'
      THEN
        INSERT INTO C.double_baking_evidence (operation_id, bh1_, bh2_) VALUES (opaid, xbh1::json, xbh2::json)
        ON CONFLICT DO NOTHING --SEQONLY
        ;
      ELSE
        RAISE EXCEPTION 'Error: %', err;
      END IF;
  END;
  INSERT INTO C.operation_sender_and_receiver VALUES (opaid, baker_id, offender_id)
  ON CONFLICT DO NOTHING --SEQONLY
  ;
END;
$$ LANGUAGE PLPGSQL;



CREATE OR REPLACE FUNCTION I.manager_numbers (opaid bigint, counter numeric, gas_limit numeric, storage_limit numeric)
returns void
as $$
insert into C.manager_numbers
values (opaid, counter, gas_limit, storage_limit)
on conflict do nothing --SEQONLY
;
$$ language sql;


CREATE OR REPLACE FUNCTION I.endorsement(opaid bigint, level int, del bigint, sl smallint[], power integer, round integer, block_payload_hash char)
returns void
as $$
insert into C.endorsement values (opaid, level, del, sl, round, block_payload_hash, power)
on conflict (operation_id) do nothing --SEQONLY
;
insert into C.operation_sender_and_receiver values (opaid, del, null)
on conflict do nothing --SEQONLY
;
$$ language sql;

CREATE OR REPLACE FUNCTION I.preendorsement(opaid bigint, level int, del bigint, sl smallint[], power integer, round integer, block_payload_hash char)
returns void
as $$
insert into C.preendorsement values (opaid, level, del, sl, round, block_payload_hash, power)
on conflict (operation_id) do nothing --SEQONLY
;
insert into C.operation_sender_and_receiver values (opaid, del, null)
on conflict do nothing --SEQONLY
;
$$ language sql;

CREATE OR REPLACE FUNCTION I.endoslot(opaid bigint, del bigint, sl smallint[], level int, slot smallint)
returns void
as $$
insert into C.endorsement values (opaid, level, del, sl, slot)
on conflict (operation_id) do nothing --SEQONLY
;
insert into C.operation_sender_and_receiver values (opaid, del, null)
on conflict do nothing --SEQONLY
;
$$ language sql;


CREATE OR REPLACE FUNCTION I.seed_nonce (opaid bigint, sender_id bigint, baker_id bigint, l int, n char)
returns void
as $$
insert into C.seed_nonce_revelation (operation_id, level, nonce)
values (opaid, l, n)
on conflict do nothing --SEQONLY
;
insert into C.operation_sender_and_receiver values (
  opaid
, sender_id
, baker_id
)
on conflict do nothing --SEQONLY
;
$$ language sql;


CREATE OR REPLACE FUNCTION I.snapshot (c int, l int)
returns void
as $$
insert into C.snapshot
values (c, l)
on conflict do nothing;
$$ language sql;


CREATE OR REPLACE FUNCTION extract_uris (xstrings char[], max_length smallint)
RETURNS int[]
AS $$
  select array_agg(I.uri(element)) from unnest(xstrings) as element where char_length(element) <= max_length AND element ~ '^([a-zA-Z][a-zA-Z]*://..*|tezos-storage:.*)'
$$ LANGUAGE SQL STABLE;


CREATE OR REPLACE FUNCTION extract_contracts (xstrings char[], opaid bigint)
RETURNS bigint[]
AS $$
(select array_agg(I.address(element::char(36),opaid)) from unnest(xstrings) as element where (element like 'tz%' or element like 'KT%') and octet_length(element) >= 36)
$$ LANGUAGE SQL STABLE;


-- This is only for updating the script on mainnet after transitioning to Babylon
DROP FUNCTION IF EXISTS U.script (kid bigint, xscript text, block_level int, xstrings text[], max_length smallint);
CREATE OR REPLACE FUNCTION U.script (kid bigint, xscript text, block_level_ int, xstrings text[], max_length smallint)
RETURNS void
AS $$
DECLARE err text;
BEGIN
  IF (select level from c.block where level = block_level_) is null
  THEN
    RAISE NOTICE 'U.script: block of hash_id = % does not exist', (block_level_::text);
    RETURN;
  END IF;
  UPDATE C.contract_script set script = xscript::jsonb, block_level = block_level_, strings = xstrings
  , uri = (select extract_uris(xstrings, max_length))
  , contracts = (select extract_contracts(xstrings, 0::bigint))
  where address_id = kid and (script is null AND script_ is null);
  EXCEPTION WHEN OTHERS THEN
    GET STACKED DIAGNOSTICS err = MESSAGE_TEXT;
    IF err = 'unsupported Unicode escape sequence'
    THEN
      UPDATE C.contract_script set script_ = xscript::json, block_level = block_level_, strings = xstrings where address_id = kid and script is null AND script_ is null;
    ELSE
      RAISE EXCEPTION 'Error: %', err;
    END IF;
END;
$$ LANGUAGE PLPGSQL;


CREATE OR REPLACE FUNCTION G.scriptless_contracts (lowest bigint)
-- RELEVANT ONLY FOR MAINNET
returns table (address char, address_id bigint)
as $$
select address(address_id), address_id
from C.contract_script s
where address_id > lowest and s.script is null and s.script_ is null
and missing_script is null
order by address_id asc
limit 100;
$$ language sql stable;


-- when the block level is too close to head, U.c_bs2 should be used instead!
CREATE OR REPLACE FUNCTION U.c_bs (xaddress bigint, bl int, xbalance bigint, xscript text, xstrings text[], max_length smallint)
RETURNS void
AS $$
DECLARE err text;
BEGIN
  IF (select level from c.block where level = bl) is null
  THEN
    RAISE NOTICE 'U.c_bs: block of level = % does not exist', bl::text;
    RETURN;
  END IF;
  UPDATE C.contract_balance SET balance = xbalance WHERE (address_id, block_level) = (xaddress, bl);
  IF xscript IS NOT NULL
  THEN
    IF (SELECT block_level FROM C.contract_script WHERE address_id = xaddress AND (script IS NOT NULL OR script_ IS NOT NULL) LIMIT 1) IS NULL
    THEN
      BEGIN
        INSERT INTO C.contract_script
        VALUES (xaddress, xscript::jsonb, bl, xstrings, (select extract_uris(xstrings, max_length)), (select extract_contracts(xstrings, 0::bigint)))
        ON CONFLICT DO NOTHING;
        EXCEPTION WHEN OTHERS THEN
          GET STACKED DIAGNOSTICS err = MESSAGE_TEXT;
          IF err = 'unsupported Unicode escape sequence'
          THEN
            INSERT INTO C.contract_script (address, script_, block_level, strings, uri, contracts)
            VALUES (xaddress, xscript::json, bl, xstrings, (select extract_uris(xstrings, max_length)), (select extract_contracts(xstrings, 0::bigint)))
            ON CONFLICT DO NOTHING;
          ELSE
            RAISE EXCEPTION 'Error: %', err;
          END IF;
      END;
    END IF;
  END IF;
  EXCEPTION WHEN OTHERS THEN return; --FAILSAFE: if update doesn't work, then it means the block was likely deleted
END;
$$ LANGUAGE PLPGSQL;


CREATE OR REPLACE FUNCTION U.c_bs2 (xaddress bigint, bh char, xbalance bigint, xscript jsonb, xstrings text[], max_length smallint)
returns void
as $$
declare bl int;
begin
bl := (select block_level(bh));
if bl is null
then
  RAISE NOTICE 'U.c_bs2: block of hash = % does not exist', bh;
  return;
end if;
update C.contract_balance set balance = xbalance where (address_id, block_level) = (xaddress, bl) ;

if xscript is not null
then
  if (select block_level from C.contract_script where address_id = xaddress and script = xscript limit 1) is null
  then
    insert into C.contract_script values(xaddress, xscript, bl, xstrings, (select extract_uris(xstrings, max_length)), (select extract_contracts(xstrings, 0::bigint)))
    on conflict do update set script = xscript, strings = xstrings, uri = (select extract_uris(xstrings, max_length)), contracts = (select extract_contracts(xstrings, 0::bigint));
  end if;
end if;
EXCEPTION WHEN OTHERS THEN RETURN; --FAILSAFE FOR WHEN THE BLOCK HAS BEEN DELETED IN THE MEANTIME BECAUSE OF A REORG
end;
$$ language plpgsql;
-- this alternative version of U.c_bs serves to insert balances that are close to head's level, such that in case of a reorganization happening, it'll safely update balances: if the block was deleted because of a reorganization, the update will do nothing because the function takes "block hashes" instead of "block hash ids". This version is not systematically used because it's much slower to deal with those hashes: additional data (char vs int) and id lookup from hash.


CREATE OR REPLACE FUNCTION I.c_bs (xaddress bigint, xbalance bigint, xblock_level int, xscript jsonb, xstrings text[], max_length smallint)
returns void
as $$
begin
  IF (select level from c.block where level = xblock_level) is null
  THEN
    RAISE NOTICE 'I.c_bs: block at level = % does not exist', xblock_level::text;
    RETURN;
  END IF;
insert into C.contract_balance (address_id, block_level, balance)
values (xaddress, xblock_level, xbalance)
on conflict do nothing --SEQONLY
;
if xscript is not null
then
  if (select block_level from C.contract_script where address_id = xaddress and script = xscript limit 1) is null
  then
    insert into C.contract_script values(xaddress, xscript, xblock_level, xstrings, (select extract_uris(xstrings, max_length)), (select extract_contracts(xstrings, 0::bigint)))
    on conflict (address_id) do update set script = xscript, strings = xstrings, uri = (select extract_uris(xstrings, max_length)), contracts = (select extract_contracts(xstrings, 0::bigint));
  end if;
end if;
end;
$$ language plpgsql;


CREATE OR REPLACE FUNCTION H.c_bs (xaddress bigint, xblock_level int)
returns void
as $$
begin
insert into C.contract_balance (address_id, block_level)
values (xaddress, xblock_level)
on conflict do nothing --SEQONLY
;
end;
$$ language plpgsql;


CREATE OR REPLACE FUNCTION G.balanceless_contracts (lim int)
returns table(address char, address_id bigint, block_hash char, block_level int) -- block_level is for logs
as $$
select address(address_id) as address, address_id, block_hash(block_level) as block_hash, block_level
from C.contract_balance
where balance is null
order by block_level desc
limit lim;
$$ language sql stable;

CREATE OR REPLACE FUNCTION G.balanceless_contracts_rev (p int, lim int)
returns table(address char, address_id bigint, block_hash char, block_level int) -- block_level is for logs
as $$
select address(address_id) as address, address_id, block_hash(block_level) as block_hash, block_level
from C.contract_balance
where balance is null
and block_level >= (select level
                      from c.block
                      where proto = p::smallint
                      order by level asc
                      limit 1)
and block_level <= (select level
                      from c.block
                      where proto = p::smallint
                      order by level desc
                      limit 1)
order by block_level asc
limit lim;
$$ language sql stable;



DROP FUNCTION IF EXISTS I.tx (opaid bigint, xsource_id bigint, xdestination_id bigint, xfee bigint, xamount bigint, xorig bigint[], xparameters text, xstorage text, xconsumed_milligas numeric, xstorage_size numeric, xpaid_storage_size_diff numeric,  xentrypoint char, xnonce int, xstatus smallint, errors jsonb, xstrings text[], max_length smallint, ticket_hash_ jsonb);
-- `g text` and not `g jsonb` because sometimes g receives PG-unsupported json
CREATE OR REPLACE FUNCTION I.tx (opaid bigint, xsource_id bigint, xdestination_id bigint, xfee bigint, xamount bigint, xorig bigint[], xparameters text, xstorage text, xconsumed_milligas numeric, xstorage_size numeric, xpaid_storage_size_diff numeric,  xentrypoint char, xnonce int, xstatus smallint, errors jsonb, xstrings text[], max_length smallint, ticket_hash_ jsonb, ticket_receipt_ jsonb)
RETURNS void
AS $$
DECLARE err text; jsonb_parameters jsonb; jsonb_storage jsonb; json_parameters json; json_storage json;
BEGIN
  BEGIN
    jsonb_parameters := xparameters::jsonb;
    EXCEPTION WHEN OTHERS THEN
      GET STACKED DIAGNOSTICS err = MESSAGE_TEXT;
      IF err = 'unsupported Unicode escape sequence'
      THEN
         jsonb_parameters := null;
         json_parameters := xparameters::json;
      ELSE
        RAISE EXCEPTION 'I.tx: Error: %', err;
      END IF;
  END;
  BEGIN
    jsonb_storage := xstorage::jsonb;
    EXCEPTION WHEN OTHERS THEN
      GET STACKED DIAGNOSTICS err = MESSAGE_TEXT;
      IF err = 'unsupported Unicode escape sequence'
      THEN
         jsonb_storage := null;
         json_storage := xstorage::json;
      ELSE
        RAISE EXCEPTION 'I.tx: Error: %', err;
      END IF;
  END;
  INSERT INTO C.tx (operation_id, source_id, destination_id, fee, amount, originated_contracts, parameters, storage, parameters_, storage_, consumed_milligas, storage_size, status, paid_storage_size_diff, entrypoint, nonce, error_trace, strings, ticket_hash, ticket_receipt
  , uri, contracts --SEQONLY
  )
  VALUES (opaid, xsource_id, xdestination_id, xfee, xamount, xorig, jsonb_parameters, jsonb_storage, json_parameters, json_storage, xconsumed_milligas, xstorage_size, xstatus, xpaid_storage_size_diff, xentrypoint, xnonce, errors, xstrings, ticket_hash_, ticket_receipt_
  , (select extract_uris(xstrings, max_length)) --SEQONLY
  , (select extract_contracts(xstrings, opaid)) --SEQONLY
  )
  ON CONFLICT DO NOTHING --SEQONLY
  ;
  UPDATE C.tx SET strings = xstrings WHERE operation_id = opaid; --SEQONLY
  INSERT INTO C.operation_sender_and_receiver VALUES (opaid, xsource_id, xdestination_id)
  ON CONFLICT DO NOTHING --SEQONLY
  ;
END;
$$ LANGUAGE PLPGSQL;


-- create or replace function foo() returns text
-- AS $$
-- DECLARE x text;
-- BEGIN
-- select '{"error" : "\u0000"}'::jsonb;
-- EXCEPTION WHEN OTHERS THEN
--   GET STACKED DIAGNOSTICS x = PG_EXCEPTION_CONTEXT;
-- return x;
-- END $$ language plpgsql;


DROP FUNCTION IF EXISTS I.origination (opaid bigint, source bigint, k bigint, consumed_milligas numeric, storage_size numeric, paid_storage_size_diff numeric, fee bigint, nonce int, preorigination_id bigint, xscript json, delegate_id bigint, credit bigint, manager_id bigint, block_level_ int, status smallint, errors jsonb, xstrings text[], max_length smallint);
CREATE OR REPLACE FUNCTION I.origination (opaid bigint, source bigint, k bigint, consumed_milligas numeric, storage_size numeric, paid_storage_size_diff numeric, fee bigint, nonce int, preorigination_id bigint, xscript json, delegate_id bigint, credit bigint, manager_id bigint, block_level_ int, status_ smallint, errors jsonb, xstrings text[], max_length smallint)
returns void
as $$
begin
insert into C.origination
values
(opaid, source, k, consumed_milligas, storage_size, paid_storage_size_diff, fee, nonce, preorigination_id, delegate_id, credit, status_, errors)
on conflict do nothing --SEQONLY
;
-- If you don't need scripts, you might want to remove the following insertion:
if xscript is not null and k is not null and status_ = 0
then
  insert into C.contract_script (address_id, script, block_level, strings
    , uri, contracts --SEQONLY
  ) values (k, xscript, block_level_, xstrings
  , (select extract_uris(xstrings, max_length)) --SEQONLY
  , (select extract_contracts(xstrings, opaid)) --SEQONLY
  )
  on conflict (address_id) do update set script = xscript, block_level = block_level_ --SEQONLY
  ;
end if;
-- The immediate following insertion might be useless, depending on your needs:
if manager_id is not null
then
insert into C.manager values (opaid, manager_id)
on conflict do nothing--SEQONLY
;
end if;
insert into C.operation_sender_and_receiver
values (opaid, source, k)
on conflict do nothing --SEQONLY
;
end
$$ language plpgsql;


DROP FUNCTION IF EXISTS I.delegation (opaid bigint, source bigint, pkh bigint, gas numeric, f bigint, n int, status smallint, errors jsonb);
CREATE OR REPLACE FUNCTION I.delegation (opaid bigint, source bigint, pkh bigint, gas numeric, f bigint, n int, status_ smallint, errors jsonb)
returns void
as $$
insert into C.delegation values (opaid, source, pkh, gas, f, n, status_, errors)
on conflict do nothing --SEQONLY
;
insert into C.operation_sender_and_receiver values (opaid, source, pkh)
on conflict do nothing --SEQONLY
;
$$ language sql;


DROP FUNCTION IF EXISTS I.reveal (opaid bigint, source bigint, pk char, gas numeric, f bigint, n int, status smallint, errors jsonb);
DROP FUNCTION IF EXISTS I.reveal (opaid bigint, source bigint, pk char, gas numeric, f bigint, n int, status_ smallint, errors jsonb);
CREATE OR REPLACE FUNCTION I.reveal (opaid bigint, source bigint, pk text, gas numeric, f bigint, n int, status_ smallint, errors jsonb)
returns void
as $$
insert into C.reveal values (opaid, source, pk, gas, f, n, status_, errors)
on conflict do nothing --SEQONLY
;
insert into C.operation_sender_and_receiver values (opaid, source, null)
on conflict do nothing --SEQONLY
;
$$ language sql;


DROP FUNCTION IF EXISTS I.rgc (opaid bigint, source bigint, n int, f bigint, status smallint, v jsonb, gas numeric, sc int, ga char, errors jsonb);
CREATE OR REPLACE FUNCTION I.rgc (opaid bigint, source bigint, n int, f bigint, status_ smallint, v jsonb, gas numeric, sc int, ga char, errors jsonb)
RETURNS VOID
AS $$
-- FIXME: n is no longer used
DECLARE ga_id bigint := (SELECT global_address_id(ga));
BEGIN
IF ga_id IS NULL AND (status_ = 0 OR status_ = 1)
THEN
  INSERT INTO C.global_constants VALUES (ga, v, opaid, sc) ON CONFLICT DO NOTHING;
  ga_id := (SELECT global_address_id FROM C.global_constants WHERE global_address = ga);
  IF ga_id IS NULL
  THEN
    RAISE 'could not record global constant opaid=%, ga=%', opaid, ga;
  END IF;
END IF;
INSERT INTO C.register_global_constant VALUES (opaid, source, ga_id, gas, f, status_, errors)
ON CONFLICT DO NOTHING --SEQONLY
;
INSERT INTO C.operation_sender_and_receiver VALUES (opaid, source, null)
ON CONFLICT DO NOTHING --SEQONLY
;
END;
$$ LANGUAGE PLPGSQL;

DROP FUNCTION IF EXISTS I.set_deposits_limit (opaid bigint, source bigint, n int, f bigint, status smallint, v numeric, gas numeric, errors jsonb);
CREATE OR REPLACE FUNCTION I.set_deposits_limit (opaid bigint, source bigint, n int, f bigint, status_ smallint, v numeric, gas numeric, errors jsonb)
RETURNS VOID
AS $$
-- FIXME: n is no longer used
BEGIN
INSERT INTO C.set_deposits_limit VALUES (opaid, source, v, gas, f, status_, errors)
ON CONFLICT DO NOTHING --SEQONLY
;
INSERT INTO C.operation_sender_and_receiver VALUES (opaid, source, null)
ON CONFLICT DO NOTHING --SEQONLY
;
END;
$$ LANGUAGE PLPGSQL;

DROP FUNCTION IF EXISTS I.tx_rollup_origination (opaid bigint, source bigint, n int, f bigint, status smallint, ro text, gas numeric, errors jsonb);
DROP FUNCTION IF EXISTS I.tx_rollup_origination (opaid bigint, source bigint, n int, f bigint, status_ smallint, ro text, gas numeric, errors jsonb);
CREATE OR REPLACE FUNCTION I.tx_rollup_origination (opaid bigint, source bigint, n int, f bigint, status_ smallint, ro bigint, gas numeric, errors jsonb)
RETURNS VOID
AS $$
BEGIN
-- FIXME: jsonb vs json ("error_trace" vs "error_trace_")
INSERT INTO C.tx_rollup_origination (operation_id, source_id, tx_rollup_origination, consumed_milligas, fee, status, error_trace) VALUES (opaid, source, ro, gas, f, status_, errors)
ON CONFLICT DO NOTHING --SEQONLY
;
INSERT INTO C.operation_sender_and_receiver VALUES (opaid, source, ro)
ON CONFLICT DO NOTHING --SEQONLY
;
END;
$$ LANGUAGE PLPGSQL;

DROP FUNCTION IF EXISTS I.tx_rollup_submit_batch (opaid bigint, source bigint, n int, f bigint, st smallint, ro text, gas numeric, errors jsonb, c text, bl numeric);
DROP FUNCTION IF EXISTS I.tx_rollup_submit_batch (opaid bigint, source bigint, n int, f bigint, st smallint, ro text, gas numeric, errors jsonb, c bytea, bl numeric);
DROP FUNCTION IF EXISTS I.tx_rollup_submit_batch (opaid bigint, source bigint, n int, f bigint, st smallint, ro text, gas numeric, errors jsonb, c bytea, bl numeric, paid_storage_size_diff_ numeric);
CREATE OR REPLACE FUNCTION I.tx_rollup_submit_batch (opaid bigint, source bigint, n int, f bigint, st smallint, ro bigint, gas numeric, errors jsonb, c bytea, bl numeric, paid_storage_size_diff_ numeric)
RETURNS VOID
AS $$
BEGIN
-- FIXME: n is no longer used
-- FIXME: jsonb vs json ("error_trace" vs "error_trace_")
INSERT INTO C.tx_rollup_submit_batch
(operation_id,source_id,tx_rollup,consumed_milligas,fee,status,error_trace,content,burn_limit,paid_storage_size_diff)
VALUES (opaid, source, ro, gas, f, st, errors, c, bl, paid_storage_size_diff_)
ON CONFLICT DO NOTHING --SEQONLY
;
INSERT INTO C.operation_sender_and_receiver VALUES (opaid, source, ro)
ON CONFLICT DO NOTHING --SEQONLY
;
END;
$$ LANGUAGE PLPGSQL;

DROP FUNCTION IF EXISTS I.tx_rollup_commit (opaid bigint, source bigint, n int, f bigint, status smallint, ro text, gas numeric, errors jsonb, c jsonb);
DROP FUNCTION IF EXISTS I.tx_rollup_commit (opaid bigint, source bigint, n int, f bigint, status_ smallint, ro text, gas numeric, errors jsonb, c jsonb);
CREATE OR REPLACE FUNCTION I.tx_rollup_commit (opaid bigint, source bigint, n int, f bigint, status_ smallint, ro bigint, gas numeric, errors jsonb, c jsonb)
RETURNS VOID
AS $$
BEGIN
-- FIXME: n is no longer used
-- FIXME: jsonb vs json ("error_trace" vs "error_trace_")
INSERT INTO C.tx_rollup_commit
(operation_id,source_id,tx_rollup,consumed_milligas,fee,commitment,status,error_trace)
VALUES
(opaid, source, ro, gas, f, c, status_, errors)
ON CONFLICT DO NOTHING --SEQONLY
;
INSERT INTO C.operation_sender_and_receiver VALUES (opaid, source, ro)
ON CONFLICT DO NOTHING --SEQONLY
;
END;
$$ LANGUAGE PLPGSQL;

DROP FUNCTION IF EXISTS I.tx_rollup_finalize_commitment (opaid bigint, source bigint, n int, f bigint, status smallint, ro text, gas numeric, errors jsonb, l int);
DROP FUNCTION IF EXISTS I.tx_rollup_finalize_commitment (opaid bigint, source bigint, n int, f bigint, status_ smallint, ro text, gas numeric, errors jsonb, l int);
CREATE OR REPLACE FUNCTION I.tx_rollup_finalize_commitment (opaid bigint, source bigint, n int, f bigint, status_ smallint, ro bigint, gas numeric, errors jsonb, l int)
RETURNS VOID
AS $$
BEGIN
-- FIXME: n is no longer used
-- FIXME: jsonb vs json ("error_trace" vs "error_trace_")
INSERT INTO C.tx_rollup_finalize_commitment
(operation_id,source_id,tx_rollup,consumed_milligas,fee,level,status,error_trace)
VALUES (opaid, source, ro, gas, f, l, status_, errors)
ON CONFLICT DO NOTHING --SEQONLY
;
INSERT INTO C.operation_sender_and_receiver VALUES (opaid, source, ro)
ON CONFLICT DO NOTHING --SEQONLY
;
END;
$$ LANGUAGE PLPGSQL;


DROP FUNCTION IF EXISTS I.tx_rollup_rejection (opaid bigint, source bigint, n int, f bigint, status smallint, ro text, gas numeric, errors jsonb, l int);
DROP FUNCTION IF EXISTS I.tx_rollup_rejection (opaid bigint, source bigint, n int, f bigint, status smallint, ro text, gas numeric, errors jsonb, l int, message_ jsonb, message_position_ int, message_path_ jsonb, message_result_hash_ jsonb, message_result_path_ jsonb, previous_message_result_ jsonb, previous_message_result_path_ jsonb, proof_ jsonb);
DROP FUNCTION IF EXISTS I.tx_rollup_rejection (opaid bigint, source bigint, n int, f bigint, status_ smallint, ro text, gas numeric, errors jsonb, l int, message_ jsonb, message_position_ int, message_path_ jsonb, message_result_hash_ jsonb, message_result_path_ jsonb, previous_message_result_ jsonb, previous_message_result_path_ jsonb, proof_ jsonb);
CREATE OR REPLACE FUNCTION I.tx_rollup_rejection (opaid bigint, source bigint, n int, f bigint, status_ smallint, ro bigint, gas numeric, errors jsonb, l int, message_ jsonb, message_position_ int, message_path_ jsonb, message_result_hash_ jsonb, message_result_path_ jsonb, previous_message_result_ jsonb, previous_message_result_path_ jsonb, proof_ jsonb)
RETURNS VOID
AS $$
BEGIN
-- FIXME: n is no longer used
-- FIXME: jsonb vs json ("error_trace" vs "error_trace_")
INSERT INTO C.tx_rollup_rejection
(operation_id,source_id,tx_rollup,consumed_milligas,fee,level,status,error_trace,message,message_position,message_path,message_result_hash,message_result_path,previous_message_result,previous_message_result_path,proof)
VALUES (opaid, source, ro, gas, f, l, status_, errors,message_,message_position_,message_path_,message_result_hash_,message_result_path_,previous_message_result_,previous_message_result_path_,proof_)
ON CONFLICT DO NOTHING --SEQONLY
;
INSERT INTO C.operation_sender_and_receiver VALUES (opaid, source, ro)
ON CONFLICT DO NOTHING --SEQONLY
;
END;
$$ LANGUAGE PLPGSQL;

DROP FUNCTION IF EXISTS I.tx_rollup_dispatch_tickets (opaid bigint, source bigint, n int, f bigint, status smallint, ro text, gas numeric, errors jsonb, l int);
DROP FUNCTION IF EXISTS I.tx_rollup_dispatch_tickets (opaid bigint, source bigint, n int, f bigint, status smallint, ro text, gas numeric, errors jsonb, l int, context_hash_ jsonb, message_index_ jsonb, message_result_path_ jsonb, tickets_info_ jsonb, paid_storage_size_diff_ numeric);
DROP FUNCTION IF EXISTS I.tx_rollup_dispatch_tickets (opaid bigint, source bigint, n int, f bigint, status smallint, ro text, gas numeric, errors jsonb, l int, context_hash_ jsonb, message_index_ int, message_result_path_ jsonb, tickets_info_ jsonb, paid_storage_size_diff_ numeric);
CREATE OR REPLACE FUNCTION I.tx_rollup_dispatch_tickets (opaid bigint, source bigint, n int, f bigint, status_ smallint, ro bigint, gas numeric, errors jsonb, l int, context_hash_ jsonb, message_index_ int, message_result_path_ jsonb, tickets_info_ jsonb, paid_storage_size_diff_ numeric)
RETURNS VOID
AS $$
BEGIN
-- FIXME: n is no longer used
-- FIXME: jsonb vs json ("error_trace" vs "error_trace_")
INSERT INTO C.tx_rollup_dispatch_tickets
(operation_id,source_id,tx_rollup,consumed_milligas,fee,level,status,error_trace,context_hash,message_index,message_result_path,tickets_info,paid_storage_size_diff)
VALUES (opaid, source, ro, gas, f, l, status_, errors,context_hash_,message_index_,message_result_path_,tickets_info_,paid_storage_size_diff_)
ON CONFLICT DO NOTHING --SEQONLY
;
INSERT INTO C.operation_sender_and_receiver VALUES (opaid, source, ro)
ON CONFLICT DO NOTHING --SEQONLY
;
END;
$$ LANGUAGE PLPGSQL;

DROP FUNCTION IF EXISTS I.tx_rollup_remove_commitment (opaid bigint, source bigint, n int, f bigint, status smallint, ro text, gas numeric, errors jsonb, l int);
DROP FUNCTION IF EXISTS I.tx_rollup_remove_commitment (opaid bigint, source bigint, n int, f bigint, status_ smallint, ro text, gas numeric, errors jsonb, l int);
CREATE OR REPLACE FUNCTION I.tx_rollup_remove_commitment (opaid bigint, source bigint, n int, f bigint, status_ smallint, ro bigint, gas numeric, errors jsonb, l int)
RETURNS VOID
AS $$
BEGIN
-- FIXME: n is no longer used
-- FIXME: jsonb vs json ("error_trace" vs "error_trace_")
INSERT INTO C.tx_rollup_remove_commitment
(operation_id,source_id,tx_rollup,consumed_milligas,fee,level,status,error_trace)
VALUES (opaid, source, ro, gas, f, l, status_, errors)
ON CONFLICT DO NOTHING --SEQONLY
;
INSERT INTO C.operation_sender_and_receiver VALUES (opaid, source, ro)
ON CONFLICT DO NOTHING --SEQONLY
;
END;
$$ LANGUAGE PLPGSQL;


DROP FUNCTION IF EXISTS I.tx_rollup_return_bond (opaid bigint, source bigint, n int, f bigint, status smallint, ro text, gas numeric, errors jsonb);
DROP FUNCTION IF EXISTS I.tx_rollup_return_bond (opaid bigint, source bigint, n int, f bigint, status_ smallint, ro text, gas numeric, errors jsonb);
CREATE OR REPLACE FUNCTION I.tx_rollup_return_bond (opaid bigint, source bigint, n int, f bigint, status_ smallint, ro bigint, gas numeric, errors jsonb)
RETURNS VOID
AS $$
BEGIN
-- FIXME: n is no longer used
-- FIXME: jsonb vs json ("error_trace" vs "error_trace_")
INSERT INTO C.tx_rollup_return_bond
(operation_id,source_id,tx_rollup,consumed_milligas,fee,status,error_trace)
VALUES (opaid, source, ro, gas, f, status_, errors)
ON CONFLICT DO NOTHING --SEQONLY
;
INSERT INTO C.operation_sender_and_receiver VALUES (opaid, source, ro)
ON CONFLICT DO NOTHING --SEQONLY
;
END;
$$ LANGUAGE PLPGSQL;



DROP FUNCTION IF EXISTS I.transfer_ticket (operation_id_ bigint, source_id_ bigint, fee_ bigint, status smallint, consumed_milligas_ numeric, error_trace_b jsonb, contents_ text, ty_ jsonb, ticketer_ jsonb, amount_ numeric, destination_id_ bigint, entrypoint_ jsonb, paid_storage_size_diff_ numeric);
DROP FUNCTION IF EXISTS I.transfer_ticket (operation_id_ bigint, source_id_ bigint, fee_ bigint, status_ smallint, consumed_milligas_ numeric, error_trace_b jsonb, contents_ text, ty_ jsonb, ticketer_ jsonb, amount_ numeric, destination_id_ bigint, entrypoint_ jsonb, paid_storage_size_diff_ numeric);
DROP FUNCTION IF EXISTS I.transfer_ticket (operation_id_ bigint, source_id_ bigint, fee_ bigint, status_ smallint, consumed_milligas_ numeric, error_trace_b jsonb, contents_ text, ty_ jsonb, ticketer_ jsonb, amount_ numeric, destination_id_ bigint, entrypoint_ jsonb, paid_storage_size_diff_ numeric, ticket_receipt_ jsonb);
CREATE OR REPLACE FUNCTION I.transfer_ticket (operation_id_ bigint, source_id_ bigint, fee_ bigint, status_ smallint, consumed_milligas_ numeric, error_trace_b jsonb, contents_ jsonb, ty_ jsonb, ticketer_ bigint, amount_ numeric, destination_id_ bigint, entrypoint_ jsonb, paid_storage_size_diff_ numeric, ticket_receipt_ jsonb)
RETURNS VOID
AS $$
BEGIN
-- FIXME: jsonb vs json ("error_trace" vs "error_trace_")
INSERT INTO C.transfer_ticket
(operation_id, source_id, fee, status, consumed_milligas, error_trace, contents, ty, ticketer, amount, destination_id, entrypoint, paid_storage_size_diff, ticket_receipt)
VALUES
(operation_id_, source_id_, fee_, status_, consumed_milligas_, error_trace_b, contents_, ty_, ticketer_, amount_, destination_id_, entrypoint_, paid_storage_size_diff_, ticket_receipt_)
ON CONFLICT DO NOTHING --SEQONLY
;
INSERT INTO C.operation_sender_and_receiver VALUES (operation_id_, source_id_, destination_id_)
ON CONFLICT DO NOTHING --SEQONLY
;
END;
$$ LANGUAGE PLPGSQL;



DROP FUNCTION IF EXISTS I.sc_rollup_originate (opaid bigint, source bigint, n int, f bigint, status smallint, gas numeric, errors jsonb, scrk text, bs text, scra text, sz numeric);
DROP FUNCTION IF EXISTS I.sc_rollup_originate (opaid bigint, source bigint, n int, f bigint, status smallint, gas numeric, errors jsonb, scrk text, bs text, scra text, sz numeric, parameters_ty_ jsonb, origination_proof_ jsonb, genesis_commitment_hash_ jsonb);
DROP FUNCTION IF EXISTS I.sc_rollup_originate (opaid bigint, source bigint, n int, f bigint, status_ smallint, gas numeric, errors jsonb, scrk text, bs text, scra text, sz numeric, parameters_ty_ jsonb, origination_proof_ jsonb, genesis_commitment_hash_ jsonb);
DROP FUNCTION IF EXISTS I.sc_rollup_originate (opaid bigint, source bigint, n int, f bigint, status_ smallint, gas numeric, errors jsonb, scrk text, bs text, scra bigint, sz numeric, parameters_ty_ jsonb, origination_proof_ jsonb, genesis_commitment_hash_ jsonb);
DROP FUNCTION IF EXISTS I.sc_rollup_originate (opaid bigint, source bigint, n int, f bigint, status_ smallint, gas numeric, errors jsonb, scrk text, bs text, scra bigint, sz numeric, parameters_ty_ jsonb, origination_proof_ text, genesis_commitment_hash_ jsonb);
DROP FUNCTION IF EXISTS I.sc_rollup_originate (opaid bigint, source bigint, n int, f bigint, status_ smallint, gas numeric, errors jsonb, scrk text, bs text, scra bigint, sz numeric, parameters_ty_ jsonb, origination_proof_ text, genesis_commitment_hash_ text);
DROP FUNCTION IF EXISTS I.sc_rollup_originate (opaid bigint, source bigint, n int, f bigint, status_ smallint, gas numeric, errors jsonb, scrk text, bs bytea, scra bigint, sz numeric, parameters_ty_ jsonb, origination_proof_ text, genesis_commitment_hash_ text);
CREATE OR REPLACE FUNCTION I.sc_rollup_originate (opaid bigint, source bigint, n int, f bigint, status_ smallint, gas numeric, errors jsonb, scrk text, boot_sector_hash_ char, scra bigint, sz numeric, parameters_ty_ jsonb, origination_proof_ text, genesis_commitment_hash_ text)
RETURNS VOID
AS $$
BEGIN
-- FIXME: n is no longer used
INSERT INTO C.sc_rollup_originate (operation_id, source_id, address_id, size, kind, boot_sector_hash, consumed_milligas, fee, parameters_ty, origination_proof, genesis_commitment_hash, status, error_trace)
VALUES (opaid, source, scra, sz, scrk, boot_sector_hash_, gas, f, parameters_ty_, origination_proof_, genesis_commitment_hash_, status_, errors)
ON CONFLICT DO NOTHING --SEQONLY
;
INSERT INTO C.operation_sender_and_receiver VALUES (opaid, source, scra)
ON CONFLICT DO NOTHING --SEQONLY
;
END;
$$ LANGUAGE PLPGSQL;

CREATE OR REPLACE FUNCTION I.sc_rollup_boot_sector_txt (hash_ char, boot_sector text)
RETURNS char
AS $$
INSERT INTO c.sc_rollup_boot_sector (hash, txt) VALUES (hash_, boot_sector) ON CONFLICT DO NOTHING;
SELECT hash_;
$$ LANGUAGE SQL;
CREATE OR REPLACE FUNCTION I.sc_rollup_boot_sector_bin (hash_ char, boot_sector bytea)
RETURNS char
AS $$
INSERT INTO c.sc_rollup_boot_sector (hash, bin) VALUES (hash_, boot_sector) ON CONFLICT DO NOTHING;
SELECT hash_;
$$ LANGUAGE SQL;


DROP FUNCTION IF EXISTS I.sc_rollup_cement (opaid bigint, source bigint, f bigint, gas numeric, status_ smallint, errors jsonb, inbox_level_ int, rollup_ bigint, commitment_hash_ jsonb);
CREATE OR REPLACE FUNCTION I.sc_rollup_cement (opaid bigint, source bigint, f bigint, gas numeric, status_ smallint, errors jsonb, inbox_level_ int, rollup_ bigint, commitment_hash_ char) -- since v10.4.0
RETURNS VOID
AS $$
BEGIN
INSERT INTO C.sc_rollup_cement (operation_id, source_id, fee, consumed_milligas, status, error_trace, inbox_level, rollup_id, commitment_hash)
VALUES (opaid, source, f, gas, status_, errors, inbox_level_, rollup_, commitment_hash_)
ON CONFLICT DO NOTHING --SEQONLY
;
INSERT INTO C.operation_sender_and_receiver VALUES (opaid, source, rollup_)
ON CONFLICT DO NOTHING --SEQONLY
;
END;
$$ LANGUAGE PLPGSQL;

DROP FUNCTION IF EXISTS I.sc_rollup_timeout (opaid bigint, source bigint, f bigint, gas numeric, status_ smallint, errors jsonb, rollup_id_ bigint, stakers jsonb, game_status jsonb);
DROP FUNCTION IF EXISTS I.sc_rollup_timeout (opaid bigint, source bigint, f bigint, status_ smallint, rollup_id_ bigint, gas numeric,  errors jsonb, stakers jsonb, game_status jsonb);
CREATE OR REPLACE FUNCTION I.sc_rollup_timeout (opaid bigint, source bigint, f bigint, status_ smallint, rollup_id_ bigint, gas numeric,  errors jsonb, stakers_ jsonb, game_status_ jsonb) -- since v10.4.0
RETURNS VOID
AS $$
BEGIN
INSERT INTO C.sc_rollup_timeout
       (operation_id, source_id, fee, consumed_milligas, status , error_trace, rollup_id , stakers , game_status)
VALUES (opaid       , source   , f  , gas              , status_, errors     , rollup_id_, stakers_, game_status_)
ON CONFLICT DO NOTHING --SEQONLY
;
INSERT INTO C.operation_sender_and_receiver VALUES (opaid, source, rollup_id_)
ON CONFLICT DO NOTHING --SEQONLY
;
END;
$$ LANGUAGE PLPGSQL;

DROP FUNCTION IF EXISTS I.sc_rollup_refute (opaid bigint, source bigint, f bigint, gas numeric, status_ smallint, errors jsonb, rollup_id_ bigint, opponent_id_ bigint, refutation_ jsonb, game_status jsonb);
DROP FUNCTION IF EXISTS I.sc_rollup_refute (opaid bigint, source bigint, f bigint, status_ smallint, rollup_id_ bigint, gas numeric, errors jsonb, opponent_id_ bigint, refutation_ jsonb, game_status jsonb);
CREATE OR REPLACE FUNCTION I.sc_rollup_refute (opaid bigint, source bigint, f bigint, status_ smallint, rollup_id_ bigint, gas numeric, errors jsonb, opponent_id_ bigint, refutation_ jsonb, game_status_ jsonb) -- since v10.4.0
RETURNS VOID
AS $$
BEGIN
INSERT INTO C.sc_rollup_refute (operation_id, source_id, fee, consumed_milligas, status, error_trace, rollup_id, opponent_id, refutation, game_status)
VALUES (opaid, source, f, gas, status_, errors, rollup_id_, opponent_id_, refutation_, game_status_)
ON CONFLICT DO NOTHING --SEQONLY
;
INSERT INTO C.operation_sender_and_receiver VALUES (opaid, source, rollup_id_)
ON CONFLICT DO NOTHING --SEQONLY
;
END;
$$ LANGUAGE PLPGSQL;



-- since v10.3.0
DROP FUNCTION IF EXISTS I.sc_rollup_publish (opaid bigint, source bigint, n int, f bigint, status smallint, ro text, gas numeric, errors jsonb, c jsonb, staked_hash_ text, published_at_level_ int);
DROP FUNCTION IF EXISTS I.sc_rollup_publish (opaid bigint, source bigint, n int, f bigint, status_ smallint, ro text, gas numeric, errors jsonb, c jsonb, staked_hash_ text, published_at_level_ int);
DROP FUNCTION IF EXISTS I.sc_rollup_publish (opaid bigint, source bigint, n int, f bigint, status_ smallint, ro bigint, gas numeric, errors jsonb, c jsonb, staked_hash_ text, published_at_level_ int);
CREATE OR REPLACE FUNCTION I.sc_rollup_publish (opaid bigint, source bigint, n int, f bigint, status_ smallint, ro bigint, gas numeric, errors jsonb, c jsonb, staked_hash_ char, published_at_level_ int)
RETURNS VOID
AS $$
BEGIN
-- FIXME: jsonb vs json ("error_trace" vs "error_trace_")
-- FIXME: n is no longer used
INSERT INTO C.sc_rollup_publish
(operation_id, source_id, rollup_id, consumed_milligas, fee, commitment, status, error_trace, staked_hash, published_at_level)
VALUES
(opaid, source, ro, gas, f, c, status_, errors, staked_hash_, published_at_level_)
ON CONFLICT DO NOTHING --SEQONLY
;
INSERT INTO C.operation_sender_and_receiver VALUES (opaid, source, ro)
ON CONFLICT DO NOTHING --SEQONLY
;
END;
$$ LANGUAGE PLPGSQL;

-- since v10.3.0
CREATE OR REPLACE FUNCTION I.zk_rollup_publish (opaid bigint, source bigint, fee_ bigint, status_ smallint, zk_rollup bigint, gas numeric, errors jsonb, ops_ jsonb, paid_storage_size_diff_ numeric)
RETURNS VOID
AS $$
BEGIN
-- FIXME: jsonb vs json ("error_trace" vs "error_trace_")
INSERT INTO C.zk_rollup_publish
(operation_id,source_id,fee,status,zk_rollup_id,consumed_milligas,error_trace,ops,paid_storage_size_diff)
VALUES
(opaid, source, f, status_, zk_rollup, gas, errors, ops_, paid_storage_size_diff_)
ON CONFLICT DO NOTHING --SEQONLY
;
INSERT INTO C.operation_sender_and_receiver VALUES (opaid, source, zk_rollup)
ON CONFLICT DO NOTHING --SEQONLY
;
END;
$$ LANGUAGE PLPGSQL;

DROP FUNCTION IF EXISTS I.sc_rollup_add_messages (opaid bigint, source bigint, n int, f bigint, status_ smallint, gas numeric, errors jsonb, ia text, r char, ms text[]);
CREATE OR REPLACE FUNCTION I.sc_rollup_add_messages (opaid bigint, source bigint, n int, f bigint, status_ smallint, gas numeric, errors jsonb, ms text[])
RETURNS VOID
AS $$
BEGIN
-- FIXME: n is no longer used
INSERT INTO C.sc_rollup_add_messages (operation_id, source_id, messages, consumed_milligas, fee, status, error_trace)
VALUES (opaid, source, ms, gas, f, status_, errors)
ON CONFLICT DO NOTHING --SEQONLY
;
INSERT INTO C.operation_sender_and_receiver VALUES (opaid, source, null)
ON CONFLICT DO NOTHING --SEQONLY
;
END;
$$ LANGUAGE PLPGSQL;


CREATE OR REPLACE FUNCTION I.event (opaid bigint, source bigint, n int, f bigint, status_ smallint, ty_ jsonb, tag_ jsonb, payload_ jsonb, gas numeric, errors jsonb)
RETURNS VOID
AS $$
BEGIN
-- FIXME: f is no longer used
INSERT INTO C.event VALUES (opaid, source, n, status_, ty_, tag_, payload_, gas, errors)
ON CONFLICT DO NOTHING --SEQONLY
;
INSERT INTO C.operation_sender_and_receiver VALUES (opaid, source, null)
ON CONFLICT DO NOTHING --SEQONLY
;
END;
$$ LANGUAGE PLPGSQL;

CREATE OR REPLACE FUNCTION I.increase_paid_storage (opaid bigint, source bigint, n int, f bigint, status_ smallint, amount_in_bytes_ numeric, destination_id_ bigint, gas numeric, errors jsonb)
RETURNS VOID
AS $$
BEGIN
-- FIXME: n is no longer used
INSERT INTO C.increase_paid_storage (operation_id, source_id, fee, status, amount_in_bytes, destination_id, consumed_milligas, error_trace) VALUES (opaid, source, f, status_, amount_in_bytes_, destination_id_, gas, errors)
ON CONFLICT DO NOTHING --SEQONLY
;
INSERT INTO C.operation_sender_and_receiver VALUES (opaid, source, destination_id_)
ON CONFLICT DO NOTHING --SEQONLY
;
END;
$$ LANGUAGE PLPGSQL;

DROP FUNCTION IF EXISTS I.dal_publish_slot_header (opaid bigint, source bigint, n int, f bigint, status_ smallint, gas numeric, dal_slot_ jsonb, errors jsonb);
CREATE OR REPLACE FUNCTION I.dal_publish_slot_header (opaid bigint, source_id_ bigint, fee_ bigint, status_ smallint, consumed_gas_ numeric, published_level_ int, index_ jsonb, error_trace_ jsonb, commitment_ jsonb, proof_ jsonb)
RETURNS VOID
AS $$
BEGIN
INSERT INTO C.dal_publish_slot_header
  (operation_id, source_id, fee, published_level, index, commitment, proof, status, consumed_milligas, error_trace)
VALUES
  (opaid, source_id_, fee_, published_level_, index_, commitment_, proof_, status_, consumed_milligas_, error_trace_)
ON CONFLICT DO NOTHING --SEQONLY
;
INSERT INTO C.operation_sender_and_receiver VALUES (opaid, source, null)
ON CONFLICT DO NOTHING --SEQONLY
;
END;
$$ LANGUAGE PLPGSQL;


CREATE OR REPLACE FUNCTION I.dal_attestation (opaid bigint, source_id_ bigint, attestor_id_ bigint, attestation_ jsonb, level_ int, delegate_id_ bigint)
RETURNS VOID
AS $$
BEGIN
INSERT INTO C.dal_attestation
  (operation_id, attestor_id, attestation, level, delegate_id)
VALUES
  (opaid, attestor_id_, attestation_, level_, delegate_id_)
ON CONFLICT DO NOTHING --SEQONLY
;
INSERT INTO C.operation_sender_and_receiver VALUES (opaid, attestor_id_, delegate_id_)
ON CONFLICT DO NOTHING --SEQONLY
;
END;
$$ LANGUAGE PLPGSQL;

DROP FUNCTION IF EXISTS I.sc_rollup_execute_outbox_message (opaid bigint, source bigint, n int, f bigint, status_ smallint, gas numeric, errors jsonb, paid_storage_size_diff_ numeric, rollup_ text, cemented_commitment_ jsonb, output_proof_ text);
DROP FUNCTION IF EXISTS I.sc_rollup_execute_outbox_message (opaid bigint, source bigint, n int, f bigint, status_ smallint, gas numeric, errors jsonb, paid_storage_size_diff_ numeric, rollup_ bigint, cemented_commitment_ jsonb, output_proof_ text);
DROP FUNCTION IF EXISTS I.sc_rollup_execute_outbox_message (opaid bigint, source bigint, n int, f bigint, status_ smallint, gas numeric, errors jsonb, paid_storage_size_diff_ numeric, rollup_ bigint, cemented_commitment_ jsonb, output_proof_ text, ticket_receipt_ jsonb);
CREATE OR REPLACE FUNCTION I.sc_rollup_execute_outbox_message (opaid bigint, source bigint, n int, f bigint, status_ smallint, gas numeric, errors jsonb, paid_storage_size_diff_ numeric, rollup_ bigint, cemented_commitment_ char, output_proof_ text, ticket_receipt_ jsonb)
RETURNS VOID
AS $$
BEGIN
-- FIXME: n is no longer used
INSERT INTO C.sc_rollup_execute_outbox_message (operation_id, source_id, fee, status, consumed_milligas, error_trace, paid_storage_size_diff, rollup_id, cemented_commitment, output_proof, ticket_receipt)
VALUES (opaid, source, f, status_, gas, errors, paid_storage_size_diff_, rollup_, cemented_commitment_, output_proof_, ticket_receipt_)
ON CONFLICT DO NOTHING --SEQONLY
;
INSERT INTO C.operation_sender_and_receiver VALUES (opaid, source, rollup_)
ON CONFLICT DO NOTHING --SEQONLY
;
END;
$$ LANGUAGE PLPGSQL;


CREATE OR REPLACE FUNCTION I.update_consensus_key (opaid bigint, source bigint, n int, f bigint, status_ smallint, gas numeric, errors jsonb, pk char)
RETURNS VOID
AS $$
BEGIN
-- FIXME: n is no longer used
INSERT INTO C.update_consensus_key (operation_id, source_id, fee, status, consumed_milligas, error_trace, public_key) VALUES (opaid, source, f, status_, gas, errors, pk)
ON CONFLICT DO NOTHING --SEQONLY
;
INSERT INTO C.operation_sender_and_receiver VALUES (opaid, source, null)
ON CONFLICT DO NOTHING --SEQONLY
;
END;
$$ LANGUAGE PLPGSQL;


DROP FUNCTION IF EXISTS I.sc_rollup_recover_bond (opaid bigint, source bigint, n int, f bigint, status_ smallint, gas numeric, errors jsonb, rollup_ text);
CREATE OR REPLACE FUNCTION I.sc_rollup_recover_bond (opaid bigint, source bigint, n int, f bigint, status_ smallint, gas numeric, errors jsonb, rollup_ bigint)
RETURNS VOID
AS $$
BEGIN
INSERT INTO C.sc_rollup_recover_bond (operation_id, source_id, fee, status, consumed_milligas, error_trace, rollup_id)
VALUES (opaid, source, f, status_, gas, errors, rollup_)
ON CONFLICT DO NOTHING --SEQONLY
;
INSERT INTO C.operation_sender_and_receiver VALUES (opaid, source, rollup_)
ON CONFLICT DO NOTHING --SEQONLY
;
END;
$$ LANGUAGE PLPGSQL;

DROP FUNCTION IF EXISTS I.sc_rollup_dal_slot_subscribe; -- this operation no longer exists and was never used

DROP FUNCTION IF EXISTS I.balance (block_level int, opaid bigint, iorid int, bal smallint, k bigint, cy int, di bigint, id int, bpkh char);
DROP FUNCTION IF EXISTS I.balance (block_level_ int, opaid bigint, iorid int, bal smallint, k bigint, cy int, di bigint, id int, bpkh char);
CREATE OR REPLACE FUNCTION I.balance (block_level_ int, opaid bigint, iorid int, bal smallint, k bigint, cy int, di bigint, id int, bpkh char, origin_ smallint)
RETURNS VOID
AS $$
BEGIN
  INSERT INTO C.balance_updates (block_level, operation_id, implicit_operations_results_id, balance_kind, contract_address_id, cycle, diff, id, blinded_public_key_hash, origin)
  VALUES (block_level_, opaid, iorid, bal, k, cy, di, id, bpkh, origin_)
  ON CONFLICT ON CONSTRAINT balance_updates_pkey DO NOTHING --SEQONLY
  ;
  IF k IS NOT NULL THEN INSERT INTO C.contract_balance (address_id, block_level) --SEQONLY
  VALUES (k, block_level_) --SEQONLY
  ON CONFLICT DO NOTHING; --SEQONLY
  END IF; --SEQONLY
END;
$$ LANGUAGE PLPGSQL;


-- insertion for C.implicit_operations_results
DROP FUNCTION IF EXISTS I.ior (op_kind smallint, orig_ks bigint[], ss text[], sto jsonb, gas numeric, sto_size numeric, paid_sto numeric, allocated bool, iorid int, bl int, otr text, scadd text, scsize numeric, scia jsonb);
DROP FUNCTION IF EXISTS I.ior (op_kind smallint, orig_ks bigint[], ss text[], sto jsonb, gas numeric, sto_size numeric, paid_sto numeric, allocated bool, iorid int, bl int, otr text, scadd text, scsize numeric, scia jsonb, tx_rollup_level_ int);
DROP FUNCTION IF EXISTS I.ior (op_kind smallint, orig_ks bigint[], ss text[], sto jsonb, gas numeric, sto_size numeric, paid_sto numeric, allocated bool, iorid int, bl int, otr text, scadd text, scsize numeric, scia jsonb, tx_rollup_level_ int, ticket_hash_ jsonb);
DROP FUNCTION IF EXISTS I.ior (op_kind smallint, orig_ks bigint[], ss text[], sto jsonb, gas numeric, sto_size numeric, paid_sto numeric, allocated bool, iorid int, bl int, otr text, scadd text, scsize numeric, scia jsonb, tx_rollup_level_ int, ticket_hash_ jsonb, ticket_receipt_ jsonb, genesis_commitment_hash_ jsonb);
CREATE OR REPLACE FUNCTION I.ior (op_kind smallint, orig_ks bigint[], ss text[], sto jsonb, gas numeric, sto_size numeric, paid_sto numeric, allocated bool, iorid int, bl int, otr bigint, scadd bigint, scsize numeric, scia jsonb, tx_rollup_level_ int, ticket_hash_ jsonb, ticket_receipt_ jsonb, genesis_commitment_hash_ jsonb)
RETURNS VOID
AS $$
  INSERT INTO C.implicit_operations_results (block_level, operation_kind, consumed_milligas, storage, originated_contracts, storage_size, paid_storage_size_diff, allocated_destination_contract, strings, id, originated_tx_rollup_id, sc_rollup_address_id, sc_rollup_size, sc_rollup_inbox_after, tx_rollup_level, ticket_hash, ticket_receipt, genesis_commitment_hash)
  VALUES (bl, op_kind, gas, sto, orig_ks, sto_size, paid_sto, allocated, ss, iorid, otr, scadd, scsize, scia, tx_rollup_level_, ticket_hash_, ticket_receipt_, genesis_commitment_hash_)
  ON CONFLICT DO NOTHING --SEQONLY
  ;
$$ LANGUAGE SQL;

CREATE OR REPLACE FUNCTION I.drain_delegate(opaid bigint, consensus_key_ bigint, delegate_ bigint, destination_ bigint, allocated_destination_contract_ bool)
RETURNS VOID
AS $$
INSERT INTO C.drain_delegate(operation_id,consensus_key_id,delegate_id,destination_id,allocated_destination_contract)
VALUES (opaid, consensus_key_, delegate_, destination_, allocated_destination_contract_)
ON CONFLICT DO NOTHING; --SEQONLY
$$ LANGUAGE SQL;

CREATE OR REPLACE FUNCTION balance_at_level(x varchar, lev int)
RETURNS TABLE(bal bigint)
AS $$
select coalesce(
  (SELECT cb.balance
   FROM C.contract_balance cb
   WHERE address_id = address_id(x)
   AND cb.block_level <= lev
   order by cb.block_level desc limit 1
  ),
  0) as bal
$$ LANGUAGE SQL stable;
-- SELECT balance_at_level('tz2FCNBrERXtaTtNX6iimR1UJ5JSDxvdHM93', 1000000);


CREATE OR REPLACE FUNCTION delete_one_operation (xoperation_hash varchar)
returns  void
as $$
select record_log(concat('delete from C.operation where hash = ', xoperation_hash)) where xoperation_hash is not null;
delete from C.operation where hash = xoperation_hash;
$$ language sql;


CREATE OR REPLACE FUNCTION delete_one_block (x varchar)
returns varchar
as $$
select record_log(concat('delete from C.block where hash = ', x)) where x is not null;
delete from C.block where x is not null and hash = x;
select x;
$$ language SQL;


CREATE OR REPLACE FUNCTION delete_blocks_from_level (l int)
returns int
as $$
select record_log(concat('delete blocks from level ', l::text, ' where highest level is ', (select concat(level::text, concat(' ', hash)) from C.block order by level::int desc limit 1)));
delete from C.block where level >= l;
select record_log(concat('new highest block: ', (select level from C.block order by level desc limit 1)::text));
select level from C.block order by level desc limit 1;
$$ language SQL;


CREATE OR REPLACE FUNCTION delete_last_block ()
returns int
as $$
select record_log(concat('delete last block, where highest level is ', (select concat(level::text, concat(' ', hash)) from C.block order by level::int desc limit 1)));
delete from C.block where level = (select level from c.block order by level desc limit 1);
select record_log(concat('new highest block: ', (select level from C.block order by level desc limit 1)::text));
select level from C.block order by level desc limit 1;
$$ language SQL;
-- src/db-schema/bigmaps.sql
-- Open Source License
-- Copyright (c) 2019-2022 Nomadic Labs <contact@nomadic-labs.com>
-- Copyright (c) 2021 Philippe Wang <philippe.wang@gmail.com>
--
-- Permission is hereby granted, free of charge, to any person obtaining a
-- copy of this software and associated documentation files (the "Software"),
-- to deal in the Software without restriction, including without limitation
-- the rights to use, copy, modify, merge, publish, distribute, sublicense,
-- and/or sell copies of the Software, and to permit persons to whom the
-- Software is furnished to do so, subject to the following conditions:
--
-- The above copyright notice and this permission notice shall be included
-- in all copies or substantial portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
-- IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
-- FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
-- THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
-- LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
-- FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
-- DEALINGS IN THE SOFTWARE.


SELECT 'bigmaps.sql' as file;

-- Table `C.bigmap` uses an storage method similar or equal to "copy-on-write",
-- to allow reorganisations to happen seemlessly while offering fast access to any
-- big map at any block level.
-- Depending on what your queries are, you might want to create additional indexes.
CREATE TABLE IF NOT EXISTS C.bigmap (
     id bigint
   , "key" jsonb -- key can be null because of allocs
   , key_hash char(54) -- key_hash can be null because key can be null
   , "key_type" jsonb
   , "value" jsonb -- if null, then it means it was deleted, or not filled yet
   , "value_type" jsonb
   , block_level int not null
   , operation_id bigint -- nullable since v9.5.0
   , operation_hash_id bigint -- since v9.9.0
   , implicit_operations_results_id int -- since v9.5.0
   , sender_id bigint -- nullable since v9.5.0
   , receiver_id bigint -- nullable since v9.5.0
   , i bigint not null -- i is for ordering: the later it comes in the blockchain, the greater the value -- changed with v9.9.0
                       -- i is not unique, duplications are caused by copies - i can no longer be negative
   , kind smallint not null -- 0: alloc, 1: update, 2: clear, 3: result of a copy, and (since v10.1.0) -1: result of a copy from an alloc
   , annots text
   , strings text[]
   , uri int[]
   , contracts bigint[]
   , metadata jsonb
);

-- about temporary big maps:
-- val reset_temporary_big_map
-- temporary_lazy_storage_ids
-- Lazy_storage_diff.cleanup_temporaries
-- every time [apply_manager_contents_list] is successful, it cleans up the temporaries (if not successful, context isn't changed anyways)

-- BEGIN FOR COPIES =================================================================
create index IF NOT EXISTS bigmap_id_ophid_i_k ON C.bigmap using btree(id, operation_hash_id, i, kind); --SEQONLY

-- The following index seems to create conflict for the execution planner, since removing it resulted in taking 30% less execution time
-- create index IF NOT EXISTS bigmap_id_kh_block_level_i ON C.bigmap using btree(id, key_hash, block_level, i); --SEQONLY

create index IF NOT EXISTS bigmap_bl_id_i_k_kh ON C.bigmap using btree(block_level, id, i, kind, key_hash); --SEQONLY

create index IF NOT EXISTS bigmap_id_ophid_i ON C.bigmap using btree(id, operation_hash_id, i); --SEQONLY
create index if not exists bigmap_ophid_id_kind ON c.bigmap using btree(operation_hash_id, id, kind); --SEQONLY
create index if not exists bigmap_kh_i ON c.bigmap using btree(key_hash, id); --SEQONLY
create index if not exists bigmap_kind_id_i_kh ON c.bigmap using btree(kind, id, i, key_hash); --SEQONLY


--------------------------------

DROP FUNCTION IF EXISTS B.get_for_copy_aux1n;
CREATE OR REPLACE FUNCTION B.get_for_copy_aux1n (xid bigint, xblock_level int, xi bigint)
RETURNS TABLE (key_hash char, id bigint, "key" jsonb, key_type jsonb, "value" jsonb, value_type jsonb, block_level int, i bigint, kind smallint, annots text, strings text[], uri int[], contracts bigint[], metadata jsonb, operation_hash_id bigint)
AS $$
    (SELECT b.key_hash, b.id, b."key", b.key_type, b."value", b.value_type, b.block_level, b.i, b.kind, b.annots, b.strings, b.uri, b.contracts, b.metadata, b.operation_hash_id
     FROM C.bigmap b
     WHERE (b.block_level, b.id) = (xblock_level, xid) AND b.i < xi AND kind < 1
     ORDER BY i DESC limit 1)
UNION
    (SELECT DISTINCT ON (b.key_hash) key_hash, b.id, b."key", b.key_type, b."value", b.value_type, b.block_level, b.i, b.kind, b.annots, b.strings, b.uri, b.contracts, b.metadata, b.operation_hash_id
     FROM C.bigmap b
     WHERE (b.block_level, b.id) = (xblock_level, xid) AND b.i < xi AND kind > 0
     ORDER BY b.key_hash, i DESC)
$$ LANGUAGE SQL STABLE;

DROP FUNCTION IF EXISTS B.get_for_copy_aux1p;
CREATE OR REPLACE FUNCTION B.get_for_copy_aux1p (xid bigint, xblock_level int, xi bigint)
RETURNS TABLE (key_hash char, id bigint, "key" jsonb, key_type jsonb, "value" jsonb, value_type jsonb, block_level int, i bigint, kind smallint, annots text, strings text[], uri int[], contracts bigint[], metadata jsonb, operation_hash_id bigint)
AS $$
    (SELECT b.key_hash, b.id, b."key", b.key_type, b."value", b.value_type, b.block_level, b.i, b.kind, b.annots, b.strings, b.uri, b.contracts, b.metadata, b.operation_hash_id
     FROM C.bigmap b
     WHERE kind < 1 AND b.id = xid AND b.i < xi
     ORDER BY i DESC limit 1)
UNION
    (SELECT DISTINCT ON (b.key_hash) key_hash, b.id, b."key", b.key_type, b."value", b.value_type, b.block_level, b.i, b.kind, b.annots, b.strings, b.uri, b.contracts, b.metadata, b.operation_hash_id
     FROM C.bigmap b
     WHERE kind > 0 AND b.id = xid AND b.i < xi
     ORDER BY b.key_hash, i DESC)
$$ LANGUAGE SQL STABLE;

DROP FUNCTION IF EXISTS B.get_for_copy_aux2n;
CREATE OR REPLACE FUNCTION B.get_for_copy_aux2n (xid bigint, xblock_level int, ophid bigint, xi bigint)
RETURNS TABLE (key_hash char, id bigint, "key" jsonb, key_type jsonb, "value" jsonb, value_type jsonb, block_level int, i bigint, kind smallint, annots text, strings text[], uri int[], contracts bigint[], metadata jsonb, operation_hash_id bigint)
AS $$
    (SELECT b.key_hash, b.id, b."key", b.key_type, b."value", b.value_type, b.block_level, b.i, b.kind, b.annots, b.strings, b.uri, b.contracts, b.metadata, b.operation_hash_id
     FROM C.bigmap b
     WHERE (b.id, b.operation_hash_id) = (xid, ophid) AND b.i < xi AND kind < 1
     ORDER BY i DESC limit 1)
UNION
    (SELECT DISTINCT ON (b.key_hash) key_hash, b.id, b."key", b.key_type, b."value", b.value_type, b.block_level, b.i, b.kind, b.annots, b.strings, b.uri, b.contracts, b.metadata, b.operation_hash_id
     FROM C.bigmap b
     WHERE (b.id, b.operation_hash_id) = (xid, ophid) AND  b.i < xi AND kind > 0
     ORDER BY b.key_hash, i DESC)
$$ LANGUAGE SQL STABLE;

DROP FUNCTION IF EXISTS B.get_for_copy_aux2p;
CREATE OR REPLACE FUNCTION B.get_for_copy_aux2p (xid bigint, xblock_level int, ophid bigint, xi bigint)
RETURNS TABLE (key_hash char, id bigint, "key" jsonb, key_type jsonb, "value" jsonb, value_type jsonb, block_level int, i bigint, kind smallint, annots text, strings text[], uri int[], contracts bigint[], metadata jsonb, operation_hash_id bigint)
AS $$
    (SELECT b.key_hash, b.id, b."key", b.key_type, b."value", b.value_type, b.block_level, b.i, b.kind, b.annots, b.strings, b.uri, b.contracts, b.metadata, b.operation_hash_id
     FROM C.bigmap b
     WHERE kind < 1 AND b.id = xid AND b.i < xi
     ORDER BY i DESC limit 1)
UNION
    (SELECT DISTINCT ON (b.key_hash) key_hash, b.id, b."key", b.key_type, b."value", b.value_type, b.block_level, b.i, b.kind, b.annots, b.strings, b.uri, b.contracts, b.metadata, b.operation_hash_id
     FROM C.bigmap b
     WHERE kind > 0 AND b.id = xid AND b.i < xi
     ORDER BY b.key_hash, i DESC)
$$ LANGUAGE SQL STABLE;

DROP FUNCTION IF EXISTS B.get_for_copy;
CREATE OR REPLACE FUNCTION B.get_for_copy (xid bigint, xblock_level int, ophid bigint, xi bigint)
RETURNS TABLE (id bigint, "key" jsonb, key_hash char, key_type jsonb, "value" jsonb, value_type jsonb, block_level int, i bigint, kind smallint, annots text, strings text[], uri int[], contracts bigint[], metadata jsonb, operation_hash_id bigint)
AS $$
BEGIN
IF ophid IS NULL THEN
  IF xid >= 0 THEN
    RETURN QUERY
     SELECT b.id, b."key", b.key_hash, b.key_type, b."value", b.value_type, b.block_level, b.i, b.kind, b.annots, b.strings, b.uri, b.contracts, b.metadata, b.operation_hash_id
     FROM B.get_for_copy_aux1p(xid, xblock_level, xi) b;
  ELSE
    RETURN QUERY
     SELECT b.id, b."key", b.key_hash, b.key_type, b."value", b.value_type, b.block_level, b.i, b.kind, b.annots, b.strings, b.uri, b.contracts, b.metadata, b.operation_hash_id
     FROM B.get_for_copy_aux1n(xid, xblock_level, xi) b;
  END IF;
ELSE
  IF xid >= 0 THEN
    RETURN QUERY
     SELECT b.id, b."key", b.key_hash, b.key_type, b."value", b.value_type, b.block_level, b.i, b.kind, b.annots, b.strings, b.uri, b.contracts, b.metadata, b.operation_hash_id
     FROM B.get_for_copy_aux2p(xid, xblock_level, ophid, xi) b;
  ELSE
    RETURN QUERY
     SELECT b.id, b."key", b.key_hash, b.key_type, b."value", b.value_type, b.block_level, b.i, b.kind, b.annots, b.strings, b.uri, b.contracts, b.metadata, b.operation_hash_id
     FROM B.get_for_copy_aux2n(xid, xblock_level, ophid, xi) b;
  END IF;
END IF;
END;
$$ LANGUAGE PLPGSQL STABLE;

-- CREATE SEQUENCE IF NOT EXISTS C.bigmap_serial START 1;

DROP FUNCTION IF EXISTS B.copy;
CREATE OR REPLACE FUNCTION B.copy (xid bigint, yid bigint, xblock_level int, xsender bigint, xreceiver bigint, xi bigint, opaid bigint, iorid int, ophid bigint)
returns void
as $$
--     9 007 199 254 740 991 is max int53
-- 9 223 372 036 854 775 807 is max int64
BEGIN
INSERT INTO C.bigmap
 (id, "key", key_hash, "key_type", "value", value_type, block_level, sender_id, receiver_id, i,                    operation_id, implicit_operations_results_id, kind, annots, strings, uri, contracts, metadata, operation_hash_id) --
SELECT
 yid, r."key", r.key_hash, r."key_type", r."value", r.value_type, xblock_level, xsender, xreceiver,
 xi, opaid, iorid, coalesce((select -1 where kind < 1), 3), r.annots, r.strings, r.uri, r.contracts, r.metadata, ophid
FROM B.get_for_copy (xid, xblock_level, ophid, xi) r
ON CONFLICT DO NOTHING;
END;
$$ LANGUAGE PLPGSQL;
-- block_level * 1000000000 --> ([1-9] * 10^6) * 10^9 -> block_level can go up to 9*10^6 with int53, or 9*10^9 with int64 -- both are fine for now
-- Going at 10 blocks per minute (instead of 1), we'd be good with int53 up to about end of September 2022.
-- At the rythm of 1 block / min, this holds up to after year 2030.
-- i * 100000 --> i*10^5 --> the size of one bigmap is limited to 100K (0-99,999),
-- the number of bigmap diffs per block is limited to 99,999
-- If at some point it no longer fits, we'll change the numbers to fully use 64-bit integers.


-- There is no way for bigmaps to be fully "concurrently indexed by segments" because
-- we may record a bigmap copy without having access to the original bigmap.
-- Therefore we create a table that contains the copy instructions so we can apply them
-- when we convert the DB from multicore mode to default mode.


select now (); --SEQONLY
DO $$
BEGIN
  IF (SELECT count(*) FROM pg_tables WHERE tablename = 'indexer_version') > 0
     AND (SELECT count(*) FROM indexer_version WHERE conversion_in_progress) > 0
     AND (SELECT count(*) FROM pg_tables WHERE tablename = 'bigmap_delayed_copies' AND schemaname = 'c') > 0
     AND (select F.is_enabled('postmulticore_bigmap_copy'))
  THEN
    PERFORM B.copy (xid, yid, xblock_level, xsender, xreceiver, i, opaid, iorid, ophid) from C.bigmap_delayed_copies order by i asc;
    DELETE FROM C.bigmap_delayed_copies;
  END IF;
END;
$$;
select now (); --SEQONLY
-- END FOR COPIES =================================================================

-- BEGIN FOR METADATA =============================================================
create INDEX IF NOT EXISTS bigmap_uri on C.bigmap using GIN (uri); --1 --SEQONLY
create INDEX IF NOT EXISTS bigmap_contracts on C.bigmap using GIN (contracts); --1 --SEQONLY

CREATE OR REPLACE FUNCTION B.fill_metadata (xid bigint)
RETURNS VOID
AS $$
BEGIN
UPDATE C.bigmap SET metadata = (extract_jsonb_from_text_array(strings))[1] WHERE id = xid AND strings IS NOT NULL;
END;
$$ LANGUAGE PLPGSQL;

create index IF NOT EXISTS bigmap_annots on C.bigmap using btree (annots); --SEQONLY --1

DO $$
BEGIN
  IF (SELECT count(*) FROM indexer_version WHERE conversion_in_progress) > 0
  THEN
    BEGIN
      PERFORM B.fill_metadata(id) FROM C.bigmap WHERE annots = '%metadata'; -- <-- faster version
      -- UPDATE C.bigmap SET metadata = (extract_jsonb_from_text_array(strings))[1] WHERE ID in (select id from c.bigmap where annots = '%metadata') AND strings is not null; <--- slow version
    END;
  END IF;
END;
$$;

-- select 'creating bigmap_pkey';
-- DO $$
--   BEGIN
--   IF (select count(*) from indexer_version where conversion_in_progress) > 0
--   THEN
--     BEGIN
--       ALTER TABLE C.bigmap
--         ADD CONSTRAINT bigmap_pkey
--         PRIMARY KEY (block_level, i);
--     EXCEPTION
--     WHEN SQLSTATE '42P16' THEN RETURN;
--     END;
--   END IF;
-- END;
-- $$;

DO $$ --SEQONLY
BEGIN --SEQONLY
ALTER TABLE C.bigmap ADD COLUMN uid SERIAL NOT NULL; --SEQONLY
EXCEPTION WHEN OTHERS THEN RETURN; --SEQONLY
END $$; --SEQONLY

DROP FUNCTION IF EXISTS b.bigmap_pin_addresses_from_strings (s text[], block_level int, bigmapindex bigint); --SEQONLY
CREATE OR REPLACE FUNCTION b.bigmap_pin_addresses_from_strings (s text[], uid_ int) --SEQONLY
RETURNS VOID --SEQONLY
AS $$ --SEQONLY
 with x as (select unnest(s) as element) --SEQONLY
 update c.bigmap --SEQONLY
   set --SEQONLY
   uri = --SEQONLY
     (select array_agg(I.uri(element)) from x where char_length(element) < 2048 AND (element ~ '[a-zA-Z][a-zA-Z]*://..*' or element ~ 'tezos-storage:.*')), --SEQONLY
   contracts = --SEQONLY
     (select array_agg(I.address(element::char(36),0::bigint)) from x where (element like 'tz%' or element like 'KT%') and octet_length(element) >= 36 --SEQONLY
     ) --SEQONLY
   where uid = uid_ ; --SEQONLY
$$ LANGUAGE SQL; --SEQONLY
 --SEQONLY
DO $$ --SEQONLY
BEGIN --SEQONLY
  --FIXME: this is deactivated for now because it takes too much time --SEQONLY
  IF FALSE AND (select count(*) from indexer_version where conversion_in_progress) > 0 AND (select F.is_enabled('postmulticore_address_extraction')) --SEQONLY
  THEN --SEQONLY
    PERFORM b.bigmap_pin_addresses_from_strings(strings, uid) --SEQONLY
    FROM c.bigmap --SEQONLY
    WHERE strings IS NOT NULL --SEQONLY
      AND array_length(strings, 1) > 0 --SEQONLY
      AND uri IS NULL --SEQONLY
      AND contracts IS NULL; --SEQONLY
  END IF; --SEQONLY
END $$; --SEQONLY
-- END FOR PIN ADDRESSES FROM STRINGS =================================================================


--the following pkey is useless... and can no longer be created anyways
--pkey bigmap_pkey; C.bigmap; block_level, i --SEQONLY

--FKEY bigmap_block_hash_block_fkey; C.bigmap; block_level; C.block(level); CASCADE --SEQONLY
--FKEY bigmap_operation_id_fkey; C.bigmap; operation_id; C.operation_alpha(autoid); CASCADE --SEQONLY
--FKEY bigmap_sender_fkey; C.bigmap; sender_id; C.address(address_id); CASCADE --SEQONLY
--FKEY bigmap_receiver_fkey; C.bigmap; receiver_id; C.address(address_id); CASCADE --SEQONLY


-- the following index is for insertion performance



------------------------
-- metadata extraction
CREATE OR REPLACE FUNCTION jsonb_of_text (t text)
RETURNS jsonb
AS $$
DECLARE r jsonb; --err text;
BEGIN
  perform (t::int);
  RETURN NULL;
EXCEPTION WHEN OTHERS THEN
  -- GET STACKED DIAGNOSTICS err = MESSAGE_TEXT;
  -- raise '%', err;
  BEGIN
    perform (t::bool);
    RETURN NULL;
  EXCEPTION WHEN OTHERS THEN
    BEGIN
      r := (SELECT t::jsonb);
      RETURN r;
      EXCEPTION WHEN OTHERS THEN RETURN NULL;
    END;
  END;
END;
$$ LANGUAGE PLPGSQL;

CREATE OR REPLACE FUNCTION extract_jsonb_from_text_array (t text[])
RETURNS jsonb[]
AS $$
BEGIN
RETURN (WITH stuff AS (SELECT jsonb_of_text(x) AS j FROM UNNEST(t) x)
        SELECT array_agg(j) FROM stuff WHERE j IS NOT NULL);
END; $$ LANGUAGE PLPGSQL;


CREATE OR REPLACE FUNCTION B.get_by_id_with_key (xid bigint, xkey_hash char)
RETURNS TABLE (id bigint, "key" jsonb, key_hash char, key_type jsonb, "value" jsonb, value_type jsonb, block_level int, i bigint, metadata jsonb)
AS $$
 SELECT id, "key", key_hash, key_type, "value", value_type, block_level as block_level, i, metadata
 FROM C.bigmap b
 WHERE (b.id, b.key_hash) = (xid, xkey_hash)
 ORDER BY i DESC
 LIMIT 1
$$ LANGUAGE SQL STABLE;

CREATE OR REPLACE FUNCTION B.get_by_id (xid bigint)
RETURNS TABLE (id bigint, "key" jsonb, key_hash char, key_type jsonb, "value" jsonb, value_type jsonb, block_level int, i bigint, metadata jsonb)
AS $$
WITH r AS
(SELECT DISTINCT b.key_hash FROM C.bigmap b WHERE b.id = xid)
SELECT B.get_by_id_with_key(xid, key_hash) FROM r WHERE (SELECT 1 FROM C.bigmap b WHERE b.id = xid AND kind = 2) IS NULL;
$$ LANGUAGE SQL STABLE;

CREATE OR REPLACE FUNCTION B.get_by_key_hash (xkey_hash char) -- results are undefined if xkey_hash is null
returns table (id bigint, "key" jsonb, key_hash char, key_type jsonb, "value" jsonb, value_type jsonb, block_level int, i bigint, metadata jsonb)
as $$
DECLARE xid bigint := (select b.id from C.bigmap b where b.key_hash = xkey_hash order by b.block_level desc, b.i desc limit 1);
BEGIN
RETURN QUERY
with r as
(select * from B.get_by_id(xid))
select * from r where r.key_hash = xkey_hash;
END
$$ language PLPGSQL;


CREATE OR REPLACE FUNCTION B.assoc (xid bigint, xkey jsonb)
returns table ("key" jsonb, "value" jsonb, block_hash char)
as $$
select "key", "value", block_hash(block_level)
from C.bigmap
where id = xid and "key" = xkey
and block_level = (select b.block_level from C.bigmap b where xkey = b.key order by b.block_level desc limit 1);
$$ language SQL stable;


CREATE OR REPLACE FUNCTION B.update (xid bigint, xkey jsonb, xkey_hash char, xvalue jsonb, xblock_hash int, xsender bigint, xreceiver bigint, xi bigint, opaid bigint, iorid int, xstrings text[], max_length smallint, ophid bigint)
returns void
as $$
insert into C.bigmap (id, "key", key_hash, "value", block_level, sender_id, receiver_id, i, operation_id, operation_hash_id, implicit_operations_results_id, kind, strings
  , uri, contracts, metadata --SEQONLY
)
values (xid, xkey, xkey_hash, xvalue, xblock_hash, xsender, xreceiver, xi, opaid, ophid, iorid, 1, xstrings
  , (select extract_uris(xstrings, max_length)) --SEQONLY
  , (select extract_contracts(xstrings, opaid)) --SEQONLY
  , (select (extract_jsonb_from_text_array(xstrings))[0] FROM C.bigmap x WHERE xid = x.id AND annots = '%metadata' limit 1) --SEQONLY
)
on conflict do nothing; --SEQONLY
$$ language SQL;


CREATE OR REPLACE FUNCTION B.clear (xid bigint, xblock_level int, xsender bigint, xreceiver bigint, xi bigint, opaid bigint, iorid int, ophid bigint)
returns void
as $$
insert into C.bigmap (id, "key", key_hash, "value", block_level, sender_id, receiver_id, i, operation_id, operation_hash_id, implicit_operations_results_id, kind)
values (xid, null, null, null, xblock_level, xsender, xreceiver, xi, opaid, ophid, iorid, 2)
on conflict do nothing; --SEQONLY
$$ language SQL;


CREATE OR REPLACE FUNCTION B.alloc (xid bigint, xkey_type jsonb, xvalue_type jsonb, xblock_level int, xsender bigint, xreceiver bigint, xi bigint, opaid bigint, iorid int, xannots text, ophid bigint)
returns void
as $$
insert into C.bigmap (id, "key_type", value_type, block_level, sender_id, receiver_id, i, operation_id, operation_hash_id, implicit_operations_results_id, kind, annots)
values (xid, xkey_type, xvalue_type, xblock_level, xsender, xreceiver, xi, opaid, ophid, iorid, 0, xannots)
-- on conflict do nothing; --SEQONLY
$$ language SQL;
-- src/db-schema/tokens.sql
-- Open Source License
-- Copyright (c) 2020-2022 Nomadic Labs <contact@nomadic-labs.com>
--
-- Permission is hereby granted, free of charge, to any person obtaining a
-- copy of this software and associated documentation files (the "Software"),
-- to deal in the Software without restriction, including without limitation
-- the rights to use, copy, modify, merge, publish, distribute, sublicense,
-- and/or sell copies of the Software, and to permit persons to whom the
-- Software is furnished to do so, subject to the following conditions:
--
-- The above copyright notice and this permission notice shall be included
-- in all copies or substantial portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
-- IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
-- FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
-- THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
-- LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
-- FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
-- DEALINGS IN THE SOFTWARE.

-- Lines starting with --OPT may be automatically activated
-- Lines ending with --OPT may be automatically deactivated

-- DB schema for tokens operations

SELECT 'tokens.sql' as file;

DO $$
BEGIN
  IF NOT EXISTS (SELECT 1 FROM pg_type WHERE typname = 'token_contract_kind') THEN
    CREATE TYPE token_contract_kind AS ENUM ('fa1-2', 'fa2');
  END IF;
END
$$;

CREATE TABLE IF NOT EXISTS T.contract ( --NL
  address_id bigint not null
, block_level int not null
, kind token_contract_kind not null
-- , primary key(address_id)
--  , foreign key(block_level) references C.block(level) on delete cascade
);
--pkey contract_pkey; T.contract; address_id --SEQONLY
--FKEY t_contract_block_level; T.contract; block_level; C.block(level); CASCADE --SEQONLY

select now (); --SEQONLY
DO $$
BEGIN
  IF (SELECT count(*) FROM pg_tables WHERE tablename = 'indexer_version') > 0
     AND (SELECT count(*) FROM indexer_version WHERE conversion_in_progress) > 0
  THEN

-- Do not use the following "temporary table method" because T.contract is a tiny table and the naive simple query returns pretty much instantaneously
-- CREATE TABLE IF NOT EXISTS T.contract_tmp ( --NL
--   address_id bigint primary key
-- , block_level int not null
-- , kind token_contract_kind not null
-- );
-- INSERT INTO T.contract_tmp SELECT * FROM T.contract ORDER BY block_level ASC ON CONFLICT DO NOTHING;
-- DROP TABLE T.contract CASCADE;
-- ALTER TABLE T.contract_tmp RENAME TO T.contract;

-- Here follows the simple query that returns pretty much instantaneously:
    DELETE FROM
      T.contract a
        USING T.contract b
      WHERE
        a.block_level > b.block_level
      AND a.address_id = b.address_id;
  END IF;
END;
$$;
DO $$
BEGIN
  IF  NOT ((SELECT count(*) FROM indexer_version WHERE multicore) > 0)
   OR (SELECT count(*) FROM indexer_version WHERE conversion_in_progress) > 0
  THEN
    BEGIN
     ALTER TABLE T.contract ADD CONSTRAINT contract_pkey PRIMARY KEY (address_id);
      EXCEPTION WHEN SQLSTATE '42P16' THEN RETURN;
    END;
  END IF;
END
$$;
select now (); --SEQONLY


CREATE OR REPLACE FUNCTION T.I_contract (a bigint, b int, k token_contract_kind)
returns void
as $$
insert into T.contract (address_id, block_level, kind)
values (a, b, k)
on conflict do nothing; --SEQONLY
$$ language sql;




-- CREATE TABLE IF NOT EXISTS T.balance (
--   token_address_id bigint not null
-- , address_id bigint not null
-- , amount smallint
-- , primary key (token_address_id, address_id)
-- );
-- --FKEY t_balance_token_address_id_fkey; T.balance; token_address_id; T.contract(address_id); CASCADE --SEQONLY
-- --FKEY t_balance_address_id_fkey; T.balance; address_id; C.address(address_id); CASCADE --SEQONLY




-- CREATE OR REPLACE FUNCTION T.I_balance (ta bigint, a bigint, am smallint)
-- returns void
-- as $$
-- -- insert into addresses values (a) on conflict do nothing;
-- insert into T.balance (token_address_id, address_id, amount)
-- values (ta, a, am)
-- on conflict (token_address_id, address_id) do  --SEQONLY
-- update set amount = am where T.balance.token_address_id = ta and T.balance.address_id = a;  --SEQONLY
-- $$ language sql;

DO $$
BEGIN
  IF NOT EXISTS (SELECT 1 FROM pg_type WHERE typname = 'fa12_operation_kind') THEN
    CREATE TYPE fa12_operation_kind AS ENUM ('transfer', 'approve', 'getBalance', 'getAllowance', 'getTotalSupply');
  END IF;
END
$$;



CREATE TABLE IF NOT EXISTS T.fa12_operation ( --NL
  operation_id bigint not null primary key
, token_address_id bigint not null
, caller_id bigint not null
, kind fa12_operation_kind not null
, autoid SERIAL UNIQUE -- this field should always be last --SEQONLY
);
--FKEY t_fa12_operation_token_address_id_fkey; T.fa12_operation; token_address_id; T.contract(address_id); CASCADE --SEQONLY
--FKEY t_fa12_operation_operation_id_fkey; T.fa12_operation; operation_id; C.operation_alpha(autoid) ; CASCADE --SEQONLY
--FKEY t_fa12_operation_caller_id_fkey; T.fa12_operation; caller_id; C.address(address_id) ; CASCADE --SEQONLY

WITH X AS (SELECT DISTINCT(token_address_id) FROM T.fa12_operation WHERE token_address_id NOT IN (SELECT address_id FROM T.contract)) --SEQONLY
INSERT INTO T.contract (address_id, block_level) --SEQONLY
SELECT X.token_address_id, ((SELECT Y.operation_id FROM T.fa12_operation Y WHERE Y.token_address_id = X.token_address_id ORDER BY operation_id asc limit 1)/10000000::bigint)::int --SEQONLY
FROM X ; --SEQONLY


CREATE OR REPLACE FUNCTION T.I_fa12_operation (opaid bigint, ta bigint, c bigint, k fa12_operation_kind)
returns void
as $$
insert into T.fa12_operation
(operation_id, token_address_id, caller_id, kind)
values (opaid, ta, c, k)
on conflict (operation_id) do nothing;
--SEQONLY
$$ language sql;



CREATE TABLE IF NOT EXISTS T.fa12_transfer ( --L
  operation_id bigint not null primary key
, source_id bigint not null
, destination_id bigint not null
, amount numeric not null
);
--FKEY t_fa12_transfer_operation_id_fkey; T.fa12_transfer; operation_id; T.fa12_operation(operation_id) ; CASCADE --SEQONLY
--FKEY t_fa12_transfer_source_id_fkey; T.fa12_transfer; source_id; C.address(address_id) ; CASCADE --SEQONLY
--FKEY t_fa12_transfer_destination_id_fkey; T.fa12_transfer; destination_id; C.address(address_id) ; CASCADE --SEQONLY


CREATE OR REPLACE FUNCTION T.I_fa12_transfer (opaid bigint, s bigint, d bigint, a numeric)
returns void
as $$
insert into T.fa12_transfer (operation_id, source_id, destination_id, amount)
values (opaid, s, d, a)
on conflict (operation_id) do nothing; --SEQONLY
$$ language sql;



CREATE TABLE IF NOT EXISTS T.fa12_approve ( --L
  operation_id bigint not null primary key
, address_id bigint not null
, amount numeric not null
);
--FKEY t_fa12_approve_operation_id_fkey; T.fa12_approve; operation_id; T.fa12_operation(operation_id) ; CASCADE --SEQONLY
--FKEY t_fa12_approve_address_id_fkey; T.fa12_approve; address_id; C.address(address_id) ; CASCADE --SEQONLY



CREATE OR REPLACE FUNCTION T.I_fa12_approve(opaid bigint, a bigint, am numeric)
returns void
as $$
insert into T.fa12_approve (operation_id, address_id, amount)
values (opaid, a, am)
on conflict (operation_id) do nothing --SEQONLY
$$ language sql;


CREATE TABLE IF NOT EXISTS T.fa12_get_balance ( --L
  operation_id bigint not null primary key
, address_id bigint not null
, callback_id bigint not null
);
--FKEY t_fa12_get_balance_operation_id_fkey; T.fa12_get_balance; operation_id; T.fa12_operation(operation_id) ; CASCADE --SEQONLY
--FKEY t_fa12_get_balance_address_id_fkey; T.fa12_get_balance; address_id; C.address(address_id) ; CASCADE --SEQONLY
--FKEY t_fa12_get_balance_callback_id_fkey; T.fa12_get_balance; callback_id; C.address(address_id) ; CASCADE --SEQONLY


CREATE OR REPLACE FUNCTION T.I_fa12_get_balance (opaid bigint, a bigint, c bigint)
returns void
as $$
insert into T.fa12_get_balance(operation_id, address_id, callback_id)
values (opaid, a, c)
on conflict (operation_id) do nothing; --SEQONLY
$$ language sql;



CREATE TABLE IF NOT EXISTS T.fa12_get_allowance ( --L
  operation_id bigint not null primary key
, source_id bigint not null
, destination_id bigint not null
, callback_id bigint not null
);
--FKEY t_fa12_get_allowance_operation_id_fkey; T.fa12_get_allowance; operation_id; T.fa12_operation(operation_id) ; CASCADE --SEQONLY
--FKEY t_fa12_get_allowance_source_id_fkey; T.fa12_get_allowance; source_id; C.address(address_id) ; CASCADE --SEQONLY
--FKEY t_fa12_get_allowance_destination_id_fkey; T.fa12_get_allowance; destination_id; C.address(address_id) ; CASCADE --SEQONLY
--FKEY t_fa12_get_allowance_callback_id_fkey; T.fa12_get_allowance; callback_id; C.address(address_id) ; CASCADE --SEQONLY



CREATE OR REPLACE FUNCTION T.I_fa12_get_allowance (opaid bigint, s bigint, d bigint, c bigint)
returns void
as $$
insert into T.fa12_get_allowance(operation_id, source_id, destination_id, callback_id)
values (opaid, s, d, c)
on conflict (operation_id) do nothing; --SEQONLY
$$ language sql;


CREATE TABLE IF NOT EXISTS T.fa12_get_total_supply ( --L
  operation_id bigint not null primary key
, callback_id bigint not null
);
--FKEY t_fa12_get_total_supply_operation_id_fkey; T.fa12_get_total_supply; operation_id; T.fa12_operation(operation_id) ; CASCADE --SEQONLY
--FKEY t_fa12_get_total_supply_callback_id_fkey; T.fa12_get_total_supply; callback_id; C.address(address_id) ; CASCADE --SEQONLY



CREATE OR REPLACE FUNCTION T.I_fa12_get_total_supply(opaid bigint, c bigint)
returns void
as $$
insert into T.fa12_get_total_supply(operation_id, callback_id)
values (opaid, c)
on conflict (operation_id) do nothing; --SEQONLY
$$ language sql;


---- FA2


DO $$
BEGIN
  IF NOT EXISTS (SELECT 1 FROM pg_type WHERE typname = 'fa2_operation_kind') THEN
    CREATE TYPE fa2_operation_kind AS ENUM ('transfer', 'update_operators', 'balance_of');
  END IF;
END
$$;


CREATE TABLE IF NOT EXISTS T.fa2_operation ( --NL
  operation_id bigint not null primary key
, token_address_id bigint not null
, caller_id bigint not null
, kind fa2_operation_kind not null
, autoid SERIAL UNIQUE -- this field should always be last --SEQONLY
);
--FKEY t_fa2_operation_token_address_id_fkey; T.fa2_operation; token_address_id; T.contract(address_id); CASCADE --SEQONLY
--FKEY t_fa2_operation_operation_id_fkey; T.fa2_operation; operation_id; C.operation_alpha(autoid) ; CASCADE --SEQONLY
--FKEY t_fa2_operation_caller_id_fkey; T.fa2_operation; caller_id; C.address(address_id) ; CASCADE --SEQONLY

WITH X AS (SELECT DISTINCT(token_address_id) FROM T.fa2_operation WHERE token_address_id NOT IN (SELECT address_id FROM T.contract)) --SEQONLY
INSERT INTO T.contract (address_id, block_level) --SEQONLY
SELECT X.token_address_id, ((SELECT Y.operation_id FROM T.fa2_operation Y WHERE Y.token_address_id = X.token_address_id ORDER BY operation_id asc limit 1)/10000000::bigint)::int --SEQONLY
FROM X ; --SEQONLY


CREATE OR REPLACE FUNCTION T.I_fa2_operation (opaid bigint, ta bigint, c bigint, k fa2_operation_kind)
returns void
as $$
insert into T.fa2_operation
(operation_id, token_address_id, caller_id, kind)
values (opaid, ta, c, k)
on conflict (operation_id) do nothing;
--SEQONLY
$$ language sql;


CREATE TABLE IF NOT EXISTS T.fa2_transfer ( --L
  operation_id bigint not null
, internal_op_id smallint not null
, token_id numeric not null
, source_id bigint not null
, destination_id bigint not null
, amount numeric not null
, PRIMARY KEY (operation_id, internal_op_id)
);
--FKEY t_fa2_transfer_operation_id_fkey; T.fa2_transfer; operation_id; T.fa2_operation(operation_id) ; CASCADE --SEQONLY
--FKEY t_fa2_transfer_source_id_fkey; T.fa2_transfer; source_id; C.address(address_id) ; CASCADE --SEQONLY
--FKEY t_fa2_transfer_destination_id_fkey; T.fa2_transfer; destination_id; C.address(address_id) ; CASCADE --SEQONLY


CREATE OR REPLACE FUNCTION T.I_fa2_transfer (opaid bigint, iop bigint, tkid numeric, s bigint, d bigint, a numeric)
returns void
as $$
insert into T.fa2_transfer (operation_id, internal_op_id, token_id, source_id, destination_id, amount)
values (opaid, iop, tkid, s, d, a)
on conflict (operation_id, internal_op_id) do nothing; --SEQONLY
$$ language sql;


CREATE TABLE IF NOT EXISTS T.fa2_update_operators ( --L
  operation_id bigint not null
, internal_op_id smallint not null
, token_id numeric not null
, add_operator bool not null
, owner_id bigint not null
, operator_id bigint not null
, PRIMARY KEY (operation_id, internal_op_id)
);
--FKEY t_fa2_update_operators_operation_id_fkey; T.fa2_update_operators; operation_id; T.fa2_operation(operation_id) ; CASCADE --SEQONLY
--FKEY t_fa2_update_operators_owner_id_fkey; T.fa2_update_operators; owner_id; C.address(address_id) ; CASCADE --SEQONLY
--FKEY t_fa2_update_operators_operator_id_fkey; T.fa2_update_operators; operator_id; C.address(address_id) ; CASCADE --SEQONLY


CREATE OR REPLACE FUNCTION T.I_fa2_update_operators (opaid bigint, iop bigint, tkid numeric, add_op bool, ow bigint, op bigint)
returns void
as $$
insert into T.fa2_update_operators (operation_id, internal_op_id, token_id, add_operator, owner_id, operator_id)
values (opaid, iop, tkid, add_op, ow, op)
on conflict (operation_id, internal_op_id) do nothing; --SEQONLY
$$ language sql;


CREATE TABLE IF NOT EXISTS T.fa2_balance_of ( --L
  operation_id bigint not null
, internal_op_id smallint not null
, token_id numeric not null
, owner_id bigint not null
, callback_id bigint not null
, PRIMARY KEY (operation_id, internal_op_id)
);
--FKEY t_fa2_balance_of_operation_id_fkey; T.fa2_balance_of; operation_id; T.fa2_operation(operation_id) ; CASCADE --SEQONLY
--FKEY t_fa2_balance_of_owner_id_fkey; T.fa2_balance_of; owner_id; C.address(address_id) ; CASCADE --SEQONLY
--FKEY t_fa2_balance_of_callback_id_fkey; T.fa2_balance_of; callback_id; C.address(address_id) ; CASCADE --SEQONLY


CREATE OR REPLACE FUNCTION T.I_fa2_balance_of (opaid bigint, iop bigint, tkid numeric, ow bigint, cb bigint)
returns void
as $$
insert into T.fa2_balance_of (operation_id, internal_op_id, token_id, owner_id, callback_id)
values (opaid, iop, tkid, ow, cb)
on conflict (operation_id, internal_op_id) do nothing; --SEQONLY
$$ language sql;


CREATE OR REPLACE FUNCTION T.is_token (t bigint)
returns table(kind token_contract_kind)
as $$
select kind from T.contract where address_id = t limit 1
-- the "limit 1" is only useful for multicore mode
$$ language sql stable;


CREATE TABLE IF NOT EXISTS T.accounts_registry ( --L
  token_address_id bigint not null
, address_id bigint not null
, block_level int not null -- since v10 -- this field does NOT necessarily contain the first block where the token appeared.
, primary key (token_address_id, address_id)
);
DO $$
BEGIN
ALTER TABLE T.accounts_registry ADD COLUMN block_level int not null default 0;
EXCEPTION WHEN OTHERS THEN RETURN;
END $$;
--FKEY t_accounts_registry_block_fkey; T.accounts_registry; block_level; C.block(level); CASCADE --SEQONLY
--fkey t_accounts_registry_token_address_id_fkey; T.accounts_registry; token_address_id; T.contract(address_id); CASCADE --SEQONLY
-- drop t_accounts_registry_token_address_id_fkey because of an obscure race condition

--FKEY t_accounts_registry_token_address_id_fkey; T.accounts_registry; token_address_id; C.address(address_id); CASCADE --SEQONLY
--FKEY t_accounts_registry_address_id_fkey; T.accounts_registry; address_id; C.address(address_id); CASCADE --SEQONLY

CREATE OR REPLACE FUNCTION T.I_accounts_registry (ta bigint, a bigint, bl int)
returns void
as $$
-- insert into addresses values (a) on conflict do nothing;
insert into T.accounts_registry (token_address_id, address_id, block_level)
values (ta, a, bl)
on conflict (token_address_id, address_id) do nothing;
$$ language sql;

-- In multicore mode, we put the values in a temporary table instead, that will not care about duplicates, for faster insertion. Then we take the contents of the table and dump it without duplicates into the final table. This allows much faster insertions.
-- multicore mode only:
-- convert from multicore to default mode:
DO $$ --SEQONLY
BEGIN --SEQONLY
  IF EXISTS (SELECT 1 FROM pg_tables where tablename = 'accounts_registry_tmp') THEN --SEQONLY
    INSERT INTO T.accounts_registry (token_address_id, address_id) --SEQONLY
    SELECT token_address_id, address_id FROM accounts_registry_tmp --SEQONLY
    ON CONFLICT DO NOTHING; --SEQONLY
    DROP TABLE accounts_registry_tmp; --SEQONLY
  END IF; --SEQONLY
END --SEQONLY
$$; --SEQONLY
-- src/db-schema/extras.sql
-- Open Source License
-- Copyright (c) 2019 Vincent Bernardoff <vb@luminar.eu.org>
-- Copyright (c) 2019-2022 Nomadic Labs <contact@nomadic-labs.com>
--
-- Permission is hereby granted, free of charge, to any person obtaining a
-- copy of this software and associated documentation files (the "Software"),
-- to deal in the Software without restriction, including without limitation
-- the rights to use, copy, modify, merge, publish, distribute, sublicense,
-- and/or sell copies of the Software, and to permit persons to whom the
-- Software is furnished to do so, subject to the following conditions:
--
-- The above copyright notice and this permission notice shall be included
-- in all copies or substantial portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
-- IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
-- FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
-- THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
-- LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
-- FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
-- DEALINGS IN THE SOFTWARE.

-----------------------------------------------------------------------------

CREATE TABLE IF NOT EXISTS indexer_measurements (
  block_hash char(51) -- null if multicore mode
, level int not null
, "timestamp" timestamp not null
, number_of_operations int
, started timestamp not null default CURRENT_TIMESTAMP
, ended timestamp
, first_indexed timestamp default CURRENT_TIMESTAMP
, last_indexed timestamp
, primary key (level, "timestamp")
);
-- src/db-schema/conversion_from_multicore.sql
-- Open Source License
-- Copyright (c) 2019-2022 Nomadic Labs <contact@nomadic-labs.com>
-- Copyright (c) 2021 Philippe Wang <philippe.wang@gmail.com>
--
-- Permission is hereby granted, free of charge, to any person obtaining a
-- copy of this software and associated documentation files (the "Software"),
-- to deal in the Software without restriction, including without limitation
-- the rights to use, copy, modify, merge, publish, distribute, sublicense,
-- and/or sell copies of the Software, and to permit persons to whom the
-- Software is furnished to do so, subject to the following conditions:
--
-- The above copyright notice and this permission notice shall be included
-- in all copies or substantial portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
-- IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
-- FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
-- THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
-- LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
-- FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
-- DEALINGS IN THE SOFTWARE.

-- This file contains things that must be executed towards the end of the conversion from multicore mode to sequential mode


select 'creating tx_pkey';
DO $$
BEGIN
    ALTER TABLE C.tx
      ADD CONSTRAINT tx_pkey
      PRIMARY KEY (operation_id);
EXCEPTION
WHEN SQLSTATE '42P16' THEN RETURN;
END;
$$;


CREATE OR REPLACE FUNCTION C.tx_pin_addresses_from_strings (s text[], opaid bigint)
RETURNS VOID
AS $$
 with x as (select unnest(s) as element)
 update c.tx
   set
   uri =
     (select array_agg(I.uri(element)) from x where char_length(element) < 2048 AND (element ~ '[a-zA-Z][a-zA-Z]*://..*' or element ~ 'tezos-storage:.*')),
   contracts =
     (select array_agg(I.address(element::char(36),opaid)) from x where (element like 'tz%' or element like 'KT%') and octet_length(element) >= 36
     )
   where operation_id = opaid;
$$ LANGUAGE SQL;


CREATE OR REPLACE FUNCTION C.origination_pin_addresses_from_strings (s text[], k bigint)
RETURNS VOID
AS $$
 with x as (select unnest(s) as element)
 update c.contract_script
   set
   uri =
     (select array_agg(I.uri(element)) from x where char_length(element) < 2048 AND (element ~ '[a-zA-Z][a-zA-Z]*://..*' or element ~ 'tezos-storage:.*')),
   contracts =
     (select array_agg(I.address(element::char(36),0::bigint)) from x where (element like 'tz%' or element like 'KT%') and octet_length(element) >= 36
     )
   where address_id = k;
$$ LANGUAGE SQL;


-- BEGIN keep up to date with chain.sql
CREATE INDEX IF NOT EXISTS tx_uri on C.tx using GIN (uri); --1
CREATE INDEX IF NOT EXISTS tx_contracts on C.tx using GIN (contracts); --1
CREATE INDEX IF NOT EXISTS contract_script_uri on C.contract_script using GIN (uri); --1
CREATE INDEX IF NOT EXISTS contract_script_contracts on C.contract_script using GIN (contracts); --1
select 'creating contract_balance_pkey';
DO $$
BEGIN
    ALTER TABLE C.contract_balance
      ADD CONSTRAINT contract_balance_pkey
      PRIMARY KEY (address_id, block_level);
EXCEPTION
WHEN SQLSTATE '42P16' THEN RETURN;
END;
$$;
-- END keep up to date with chain.sql



-- in multicore mode, there can be race conditions that prevent this treatment, so we run it when we convert the multicore mode to sequential mode

--the following indexes do not speed up anything
--create index if not exists tx_pin on C.tx using btree ((strings is not null and array_length(strings, 1) > 0 and uri is null and contracts is null));
--create index if not exists contract_script_pin on C.contract_script using btree ((strings is not null and array_length(strings, 1) > 0 and uri is null and contracts is null));

DO $$
BEGIN
IF (select count(*) from indexer_version where multicore) > 0 AND (select F.is_enabled('postmulticore_address_extraction'))
THEN
PERFORM C.tx_pin_addresses_from_strings(strings, operation_id) FROM c.tx WHERE strings IS NOT NULL AND array_length(strings, 1) > 0 AND uri IS NULL AND contracts IS NULL;
END IF;
END $$ ;

DO $$
BEGIN
IF (select count(*) from indexer_version where multicore) > 0 AND (select F.is_enabled('postmulticore_address_extraction'))
THEN
PERFORM C.origination_pin_addresses_from_strings(strings, address_id) FROM c.contract_script WHERE strings IS NOT NULL AND array_length(strings, 1) > 0 AND uri IS NULL AND contracts IS NULL;
END IF;
END $$ ;


DO $$
BEGIN
IF (select count(*) from indexer_version where multicore) > 0 AND (select F.is_enabled('postmulticore_contract_balance'))
THEN
INSERT INTO C.contract_balance (address_id, block_level)
SELECT contract_address_id, block_level
FROM C.balance_updates
WHERE contract_address_id IS NOT NULL
ON CONFLICT DO NOTHING;
END IF;
END $$ ;
-- create indexes
CREATE INDEX IF NOT EXISTS addresses_autoid on C.address using btree (address_id); --1 --SEQONLY
CREATE INDEX IF NOT EXISTS uri_address_id on C.uri using btree (address_id); --1
-- CREATE INDEX IF NOT EXISTS indexer_log_timestamp on indexer_log using btree(timestamp); --1
CREATE UNIQUE INDEX IF NOT EXISTS block_level on C.block using btree (level); --SEQONLY --1
CREATE INDEX IF NOT EXISTS block_proto_level  on C.block using btree (proto, level); --SEQONLY --1
CREATE UNIQUE INDEX IF NOT EXISTS operation_kind_id on C.operation_kind using btree (id); --SEQONLY --1
CREATE UNIQUE INDEX IF NOT EXISTS implicit_operations_results_block_id on C.implicit_operations_results using btree (block_level, id); --SEQONLY --1
CREATE INDEX IF NOT EXISTS implicit_operations_results_block on C.implicit_operations_results using btree (block_level); --SEQONLY --1
CREATE INDEX IF NOT EXISTS implicit_operations_results_opkind on C.implicit_operations_results using btree (operation_kind); --SEQONLY --1
CREATE INDEX IF NOT EXISTS operation_block on C.operation using btree (block_level); --SEQONLY --1
CREATE INDEX IF NOT EXISTS operation_hash on C.operation using btree (hash); --SEQONLY --1
CREATE INDEX IF NOT EXISTS operation_hash_id on C.operation using btree (hash_id); --SEQONLY --1
CREATE INDEX IF NOT EXISTS operation_alpha_kind on C.operation_alpha using btree (operation_kind); --SEQONLY --1
CREATE INDEX IF NOT EXISTS operation_alpha_hash on C.operation_alpha using btree (hash_id); --SEQONLY --1
CREATE INDEX IF NOT EXISTS operation_alpha_block_hash on C.operation_alpha using btree (block_level); --SEQONLY --1
CREATE UNIQUE INDEX IF NOT EXISTS operation_alpha_autoid on C.operation_alpha using btree (autoid); --1
CREATE INDEX IF NOT EXISTS operation_sender_and_receiver_sender_opaid on C.operation_sender_and_receiver using btree (sender_id, operation_id); --SEQONLY --1
CREATE INDEX IF NOT EXISTS operation_sender_and_receiver_receiver_opaid on C.operation_sender_and_receiver using btree (receiver_id, operation_id); --SEQONLY --1
CREATE UNIQUE INDEX IF NOT EXISTS proposals_proposal_id on C.proposals using btree (proposal_id); --1
CREATE INDEX IF NOT EXISTS proposal_operation on C.proposal using btree (operation_id); --1
CREATE INDEX IF NOT EXISTS proposal_source on C.proposal using btree (source_id); --SEQONLY --1
CREATE INDEX IF NOT EXISTS proposal_proposal on C.proposal using btree (proposal_id); --1
CREATE INDEX IF NOT EXISTS proposal_period on C.proposal using btree (period); --SEQONLY --1
CREATE INDEX IF NOT EXISTS ballot_source on C.ballot using btree (source_id); --SEQONLY --1
--OPT CREATE INDEX IF NOT EXISTS manager_numbers_counter on C.manager_numbers using btree (counter); --SEQONLY
--OPT CREATE INDEX IF NOT EXISTS manager_numbers_gas_limit on C.manager_numbers using btree (gas_limit); --SEQONLY
--OPT CREATE INDEX IF NOT EXISTS manager_numbers_storage_limit on C.manager_numbers using btree (storage_limit); --SEQONLY
CREATE INDEX IF NOT EXISTS endorsement_delegate_id on C.endorsement using btree (delegate_id); --SEQONLY --1
CREATE INDEX IF NOT EXISTS endorsement_level on C.endorsement using btree (level); --SEQONLY --1
CREATE INDEX IF NOT EXISTS preendorsement_delegate_id on C.preendorsement using btree (delegate_id); --SEQONLY --1
CREATE INDEX IF NOT EXISTS preendorsement_level on C.preendorsement using btree (level); --SEQONLY --1
CREATE INDEX IF NOT EXISTS block_alpha_baker on C.block_alpha using btree (baker_id); --SEQONLY --1
CREATE INDEX IF NOT EXISTS block_alpha_level_position on C.block_alpha using btree (level_position); --SEQONLY --1
CREATE INDEX IF NOT EXISTS block_alpha_cycle on C.block_alpha using btree (cycle); --SEQONLY --1
CREATE INDEX IF NOT EXISTS block_alpha_cycle_position on C.block_alpha using btree (cycle_position); --SEQONLY --1
CREATE INDEX IF NOT EXISTS block_alpha_delegate on C.block_alpha using btree (delegate_id); --SEQONLY --1
CREATE INDEX IF NOT EXISTS deactivated_pkh on C.deactivated using btree (pkh_id); --SEQONLY --1
CREATE INDEX IF NOT EXISTS deactivated_block_hash on C.deactivated using btree (block_level); --SEQONLY --1
--CREATE INDEX IF NOT EXISTS deactivated_autoid on C.deactivated using btree (autoid); --OPT --SEQONLY --FIXME: is it useful?
CREATE INDEX IF NOT EXISTS contract_balance_address on C.contract_balance using btree (address_id); --SEQONLY --1
CREATE INDEX IF NOT EXISTS contract_balance_balance on C.contract_balance using btree (balance); --SEQONLY --1
-- CREATE INDEX IF NOT EXISTS contract_balance_address_block_level on C.contract_balance using btree (address_id, block_level desc); --SEQONLY --1 <---- do not create this index, it's already a primary key
CREATE INDEX IF NOT EXISTS contract_balance_block_hash on C.contract_balance using btree (block_level); --SEQONLY --1
CREATE INDEX IF NOT EXISTS contract_balance_balance_block_level on C.contract_balance using btree (balance, block_level); --SEQONLY --1
CREATE INDEX IF NOT EXISTS contract_script_address on C.contract_script using btree (address_id); --SEQONLY --1
CREATE INDEX IF NOT EXISTS contract_script_block_hash on C.contract_script using btree (block_level); --SEQONLY --1
CREATE INDEX IF NOT EXISTS contract_script_missing_script on C.contract_script using btree (missing_script); --SEQONLY --1
--CREATE INDEX IF NOT EXISTS contract_script_strings on C.contract_script using GIN (strings); --SEQONLY
CREATE INDEX IF NOT EXISTS contract_script_address_id_script on C.contract_script using btree (address_id, (script is null)); --SEQONLY --1
CREATE INDEX IF NOT EXISTS contract_script_address_id_script_missing_script on C.contract_script using btree (address_id, (script is null), (script_ is null), (missing_script is null)); --SEQONLY --1
CREATE INDEX IF NOT EXISTS contract_script_uri on C.contract_script using GIN (uri); --SEQONLY --1
CREATE INDEX IF NOT EXISTS contract_script_contracts on C.contract_script using GIN (contracts); --SEQONLY --1
CREATE INDEX IF NOT EXISTS contract_script_script_code ON C.contract_script USING HASH ((script -> 'code')); --SEQONLY
CREATE INDEX IF NOT EXISTS tx_source on C.tx using btree (source_id); --SEQONLY --1
CREATE INDEX IF NOT EXISTS tx_destination on C.tx using btree (destination_id); --SEQONLY --1
CREATE INDEX IF NOT EXISTS tx_destination_opaid on C.tx using btree (destination_id, operation_id desc); --SEQONLY --1
--CREATE INDEX IF NOT EXISTS tx_strings on C.tx using GIN (strings); --SEQONLY
CREATE INDEX IF NOT EXISTS tx_uri on C.tx using GIN (uri); --SEQONLY --1
CREATE INDEX IF NOT EXISTS tx_contracts on C.tx using GIN (contracts); --SEQONLY --1
CREATE INDEX IF NOT EXISTS origination_source         on C.origination using btree (source_id); --SEQONLY --1
CREATE INDEX IF NOT EXISTS origination_k              on C.origination using btree (k_id); --SEQONLY --1
CREATE INDEX IF NOT EXISTS origination_preorigination on C.origination using btree (preorigination_id); --SEQONLY --1
CREATE INDEX IF NOT EXISTS origination_delegate       on C.origination using btree (delegate_id); --SEQONLY --1
CREATE INDEX IF NOT EXISTS delegation_source         on C.delegation using btree (source_id); --SEQONLY --1
CREATE INDEX IF NOT EXISTS delegation_pkh            on C.delegation using btree (pkh_id); --SEQONLY --1
CREATE INDEX IF NOT EXISTS delegation_operation_hash on C.delegation using btree (operation_id); --SEQONLY --1
CREATE INDEX IF NOT EXISTS reveal_source         on C.reveal using btree (source_id); --SEQONLY --1
CREATE INDEX IF NOT EXISTS reveal_pk             on C.reveal using btree (pk); --SEQONLY --1
CREATE UNIQUE INDEX IF NOT EXISTS global_constants_address on C.global_constants using btree (global_address); --SEQONLY --1
-- CREATE UNIQUE INDEX IF NOT EXISTS global_constants_value on C.global_constants using btree ("value"); -- this index crashes on a value that is too large to fit
CREATE UNIQUE INDEX IF NOT EXISTS global_constants_address_id on C.global_constants using btree (global_address_id); --SEQONLY --1
CREATE INDEX IF NOT EXISTS register_global_constant_source         on C.register_global_constant using btree (source_id); --SEQONLY --1
CREATE INDEX IF NOT EXISTS register_global_constant_gaid            on C.register_global_constant using btree (global_address_id); --SEQONLY --1
CREATE INDEX IF NOT EXISTS set_deposits_limit_source on C.set_deposits_limit using btree (source_id); --SEQONLY --1
CREATE INDEX IF NOT EXISTS set_deposits_limit_value  on C.set_deposits_limit using btree (value); --SEQONLY --1
CREATE INDEX IF NOT EXISTS tx_rollup_origination_source on C.tx_rollup_origination using btree (source_id); --SEQONLY --1
--CREATE INDEX IF NOT EXISTS tx_rollup_origination_value  on C.tx_rollup_origination using btree (tx_rollup_origination); --SEQONLY --1
CREATE INDEX IF NOT EXISTS tx_rollup_submit_batch_source on C.tx_rollup_submit_batch using btree (source_id); --SEQONLY --1
--CREATE INDEX IF NOT EXISTS tx_rollup_submit_batch_value  on C.tx_rollup_submit_batch using btree (tx_rollup_submit_batch); --SEQONLY --1
CREATE INDEX IF NOT EXISTS tx_rollup_commit_source on C.tx_rollup_commit using btree (source_id); --SEQONLY --1
--CREATE INDEX IF NOT EXISTS tx_rollup_commit_value  on C.tx_rollup_commit using btree (tx_rollup_commit); --SEQONLY --1
CREATE INDEX IF NOT EXISTS tx_rollup_finalize_commitment_source on C.tx_rollup_finalize_commitment using btree (source_id); --SEQONLY --1
--CREATE INDEX IF NOT EXISTS tx_rollup_finalize_commitment_value  on C.tx_rollup_finalize_commitment using btree (tx_rollup_finalize_commitment); --SEQONLY --1
CREATE INDEX IF NOT EXISTS tx_rollup_rejection_source on C.tx_rollup_rejection using btree (source_id); --SEQONLY --1
--CREATE INDEX IF NOT EXISTS tx_rollup_rejection_value  on C.tx_rollup_rejection using btree (tx_rollup_rejection); --SEQONLY --1
CREATE INDEX IF NOT EXISTS tx_rollup_dispatch_tickets_source on C.tx_rollup_dispatch_tickets using btree (source_id); --SEQONLY --1
--CREATE INDEX IF NOT EXISTS tx_rollup_dispatch_tickets_value  on C.tx_rollup_dispatch_tickets using btree (tx_rollup_dispatch_tickets); --SEQONLY --1
CREATE INDEX IF NOT EXISTS tx_rollup_remove_commitment_source on C.tx_rollup_remove_commitment using btree (source_id); --SEQONLY --1
--CREATE INDEX IF NOT EXISTS tx_rollup_remove_commitment_value  on C.tx_rollup_remove_commitment using btree (tx_rollup_remove_commitment); --SEQONLY --1
CREATE INDEX IF NOT EXISTS tx_rollup_return_bond_source on C.tx_rollup_return_bond using btree (source_id); --SEQONLY --1
--CREATE INDEX IF NOT EXISTS tx_rollup_return_bond_value  on C.tx_rollup_return_bond using btree (tx_rollup_return_bond); --SEQONLY --1
CREATE INDEX IF NOT EXISTS transfer_ticket_source on C.transfer_ticket using btree (source_id); --SEQONLY --1
--CREATE INDEX IF NOT EXISTS transfer_ticket_value  on C.transfer_ticket using btree (transfer_ticket); --SEQONLY --1
CREATE INDEX IF NOT EXISTS sc_rollup_originate_source on C.sc_rollup_originate using btree (source_id); --SEQONLY --1
CREATE INDEX IF NOT EXISTS sc_rollup_boo_sectors_hash on C.sc_rollup_boot_sector using btree(hash);
CREATE INDEX IF NOT EXISTS sc_rollup_publish_source on C.sc_rollup_publish using btree (source_id); --SEQONLY --1
--CREATE INDEX IF NOT EXISTS sc_rollup_publish_value  on C.sc_rollup_publish using btree (sc_rollup_publish); --SEQONLY --1
CREATE INDEX IF NOT EXISTS sc_rollup_cement_source on C.sc_rollup_cement using btree (source_id); --SEQONLY --1
CREATE INDEX IF NOT EXISTS sc_rollup_cement_rollup on C.sc_rollup_cement using btree (rollup_id); --SEQONLY --1
CREATE INDEX IF NOT EXISTS sc_rollup_timeout_source on C.sc_rollup_timeout using btree (source_id); --SEQONLY --1
CREATE INDEX IF NOT EXISTS sc_rollup_timeout_rollup on C.sc_rollup_timeout using btree (rollup_id); --SEQONLY --1
CREATE INDEX IF NOT EXISTS sc_rollup_refute_source on C.sc_rollup_refute using btree (source_id); --SEQONLY --1
CREATE INDEX IF NOT EXISTS sc_rollup_refute_rollup on C.sc_rollup_refute using btree (rollup_id); --SEQONLY --1
CREATE INDEX IF NOT EXISTS sc_rollup_refute_opponent on C.sc_rollup_refute using btree (opponent_id); --SEQONLY --1
CREATE INDEX IF NOT EXISTS zk_rollup_publish_source on C.zk_rollup_publish using btree (source_id); --SEQONLY --1
CREATE INDEX IF NOT EXISTS sc_rollup_add_messages_source on C.sc_rollup_add_messages using btree (source_id); --SEQONLY --1
CREATE INDEX IF NOT EXISTS event_source on C.event using btree (source_id); --SEQONLY --1
CREATE INDEX IF NOT EXISTS increase_paid_storage_source on C.increase_paid_storage using btree (source_id); --SEQONLY --1
CREATE INDEX IF NOT EXISTS dal_publish_slot_header_source on C.dal_publish_slot_header using btree (source_id); --SEQONLY --1
CREATE INDEX IF NOT EXISTS dal_attestation_attestor on C.dal_attestation using btree (attestor_id); --SEQONLY --1
CREATE INDEX IF NOT EXISTS dal_attestation_delegate on C.dal_attestation using btree (delegate_id); --SEQONLY --1
CREATE INDEX IF NOT EXISTS sc_rollup_execute_outbox_message_source on C.sc_rollup_execute_outbox_message using btree (source_id); --SEQONLY --1
CREATE INDEX IF NOT EXISTS update_consensus_key_source on C.update_consensus_key using btree (source_id); --SEQONLY --1
CREATE INDEX IF NOT EXISTS sc_rollup_recover_bond_source on C.sc_rollup_recover_bond using btree (source_id); --SEQONLY --1
CREATE INDEX IF NOT EXISTS sc_rollup_recover_bond_rollup on C.sc_rollup_recover_bond using btree (rollup_id); --SEQONLY --1
CREATE INDEX IF NOT EXISTS balance_block  on C.balance_updates using btree (block_level); --SEQONLY --1
CREATE INDEX IF NOT EXISTS balance_cat    on C.balance_updates using btree (balance_kind); --SEQONLY --1
CREATE INDEX IF NOT EXISTS balance_k      on C.balance_updates using btree (contract_address_id); --SEQONLY --1
CREATE INDEX IF NOT EXISTS balance_cycle  on C.balance_updates using btree (cycle); --SEQONLY --1
CREATE INDEX IF NOT EXISTS balance_operation_id on C.balance_updates using btree (operation_id); --SEQONLY --1
--CREATE INDEX IF NOT EXISTS balance_blinded_public_key_hash on C.balance_updates using btree (blinded_public_key_hash); --SEQONLY --1
CREATE INDEX IF NOT EXISTS delegated_contract_cycle     on C.delegated_contract using btree (cycle); --SEQONLY --1
CREATE INDEX IF NOT EXISTS delegated_contract_level     on C.delegated_contract using btree (level); --SEQONLY --1
CREATE INDEX IF NOT EXISTS delegated_contract_delegate  on C.delegated_contract using btree (delegate_id); --SEQONLY --1
CREATE INDEX IF NOT EXISTS delegated_contract_delegator on C.delegated_contract using btree (delegator_id); --SEQONLY --1
CREATE INDEX IF NOT EXISTS bigmap_pin ON C.bigmap USING BTREE ((strings IS NOT NULL AND array_length(strings, 1) > 0 AND uri IS NULL AND contracts IS NULL)); --SEQONLY
CREATE INDEX IF NOT EXISTS bigmap_uid ON C.bigmap USING BTREE (uid); --SEQONLY
CREATE INDEX IF NOT EXISTS bigmap_block_hash on C.bigmap using btree (block_level); --SEQONLY --1
CREATE INDEX IF NOT EXISTS bigmap_operation_id on C.bigmap using btree (operation_id); --SEQONLY --1
CREATE INDEX IF NOT EXISTS bigmap_ophid_id on C.bigmap using btree (operation_hash_id); --SEQONLY --1
CREATE INDEX IF NOT EXISTS bigmap_key_hash on C.bigmap using btree (key_hash); --SEQONLY --1
--CREATE INDEX IF NOT EXISTS bigmap_key on C.bigmap using btree ("key"); --SEQONLY --1 -- drop this because some values () are too big (e.g. around block 382287 on hangzhounet)
CREATE INDEX IF NOT EXISTS bigmap_key_type on C.bigmap using btree ("key_type"); --SEQONLY --1
-- CREATE INDEX IF NOT EXISTS bigmap_value_type on C.bigmap using btree ("value_type"); --SEQONLY -- drop this index because some values (eg. from block 1778745 on mainnet) are too big to be indexed (since v9.7.7)
CREATE INDEX IF NOT EXISTS bigmap_id on C.bigmap using btree (id); --SEQONLY --1
CREATE INDEX IF NOT EXISTS bigmap_sender on C.bigmap using btree (sender_id); --SEQONLY --1
CREATE INDEX IF NOT EXISTS bigmap_receiver on C.bigmap using btree (receiver_id); --SEQONLY --1
CREATE INDEX IF NOT EXISTS bigmap_metadata on C.bigmap using GIN(metadata); --SEQONLY --1
CREATE INDEX IF NOT EXISTS bigmap_id_annots on C.bigmap using btree(id, annots); --SEQONLY --1
--OPT CREATE INDEX IF NOT EXISTS bigmap_key_type on C.bigmap using btree ("key_type"); --SEQONLY
--OPT CREATE INDEX IF NOT EXISTS bigmap_value_type on C.bigmap using btree ("value_type"); --SEQONLY
--CREATE INDEX IF NOT EXISTS bigmap_strings on C.bigmap using GIN (strings); --SEQONLY
CREATE INDEX IF NOT EXISTS bigmap_uri on C.bigmap using GIN (uri); --SEQONLY --1
CREATE INDEX IF NOT EXISTS bigmap_contracts on C.bigmap using GIN (contracts); --SEQONLY --1
-- CREATE INDEX IF NOT EXISTS token_contract_block_hash on T.contract using btree(block_level);
--CREATE INDEX IF NOT EXISTS token_contract_address on T.contract using btree(address_id); --SEQONLY --1
CREATE INDEX IF NOT EXISTS token_contract_block_hash on T.contract using btree(block_level); --SEQONLY --1
--CREATE INDEX IF NOT EXISTS token_contract_autoid on T.contract using btree(autoid); --SEQONLY
-- CREATE INDEX IF NOT EXISTS token_balance_token on T.balance using btree(token_address_id); --SEQONLY
-- CREATE INDEX IF NOT EXISTS token_balance_address on T.balance using btree(address_id); --SEQONLY
-- CREATE INDEX IF NOT EXISTS token_balance_autoid on T.balance using btree(autoid) ;--SEQONLY
CREATE INDEX IF NOT EXISTS fa12_operation_kind    on T.fa12_operation using btree (kind); --SEQONLY --1
-- CREATE INDEX IF NOT EXISTS fa12_operation_autoid  on T.fa12_operation using btree (autoid); --SEQONLY
CREATE INDEX IF NOT EXISTS fa12_operation_caller  on T.fa12_operation using btree (caller_id); --SEQONLY --1
CREATE INDEX IF NOT EXISTS fa12_operation_address on T.fa12_operation using btree (token_address_id); --SEQONLY --1
CREATE INDEX IF NOT EXISTS fa12_transfer_source         on T.fa12_transfer using btree (source_id); --SEQONLY --1
CREATE INDEX IF NOT EXISTS fa12_transfer_destination    on T.fa12_transfer using btree (destination_id); --SEQONLY --1
-- CREATE INDEX IF NOT EXISTS fa12_transfer_autoid         on T.fa12_transfer using btree (autoid); --SEQONLY
CREATE INDEX IF NOT EXISTS fa12_approve_address           on T.fa12_approve using btree (address_id); --SEQONLY --1
-- CREATE INDEX IF NOT EXISTS fa12_approve_autoid            on T.fa12_approve using btree (autoid); --SEQONLY
CREATE INDEX IF NOT EXISTS fa12_get_balance_address           on T.fa12_get_balance using btree (address_id); --SEQONLY --1
CREATE INDEX IF NOT EXISTS fa12_get_balance_callback          on T.fa12_get_balance using btree (callback_id); --SEQONLY --1
-- CREATE INDEX IF NOT EXISTS fa12_get_balance_autoid            on T.fa12_get_balance using btree (autoid); --SEQONLY
CREATE INDEX IF NOT EXISTS fa12_get_allowance_source            on T.fa12_get_allowance using btree (source_id); --SEQONLY --1
CREATE INDEX IF NOT EXISTS fa12_get_allowance_destination       on T.fa12_get_allowance using btree (destination_id); --SEQONLY --1
CREATE INDEX IF NOT EXISTS fa12_get_allowance_callback          on T.fa12_get_allowance using btree (callback_id); --SEQONLY --1
-- CREATE INDEX IF NOT EXISTS fa12_get_allowance_autoid            on T.fa12_get_balance using btree (autoid); --SEQONLY
CREATE INDEX IF NOT EXISTS fa12_get_total_supply_callback          on T.fa12_get_total_supply using btree (callback_id); --SEQONLY --1
-- CREATE INDEX IF NOT EXISTS fa12_get_total_supply_autoid            on T.fa12_get_total_supply using btree (autoid); --SEQONLY
CREATE INDEX IF NOT EXISTS fa2_operation_kind    on T.fa2_operation using btree (kind); --SEQONLY --1
-- CREATE INDEX IF NOT EXISTS fa2_operation_autoid  on T.fa2_operation using btree (autoid); --SEQONLY
CREATE INDEX IF NOT EXISTS fa2_operation_caller  on T.fa2_operation using btree (caller_id); --SEQONLY --1
CREATE INDEX IF NOT EXISTS fa2_operation_address on T.fa2_operation using btree (token_address_id); --SEQONLY --1
CREATE INDEX IF NOT EXISTS fa2_transfer_source         on T.fa2_transfer using btree (source_id); --SEQONLY --1
CREATE INDEX IF NOT EXISTS fa2_transfer_destination    on T.fa2_transfer using btree (destination_id); --SEQONLY --1
-- CREATE INDEX IF NOT EXISTS fa2_transfer_autoid         on T.fa2_transfer using btree (autoid); --SEQONLY
CREATE INDEX IF NOT EXISTS fa2_update_operators_owner    on T.fa2_update_operators using btree (owner_id); --SEQONLY --1
CREATE INDEX IF NOT EXISTS fa2_update_operators_operator on T.fa2_update_operators using btree (operator_id); --SEQONLY --1
-- CREATE INDEX IF NOT EXISTS fa2_update_operators_autoid   on T.fa2_update_operators using btree (autoid); --SEQONLY
CREATE INDEX IF NOT EXISTS fa2_balance_of_owner    on T.fa2_balance_of using btree (owner_id); --SEQONLY --1
CREATE INDEX IF NOT EXISTS fa2_balance_of_callback on T.fa2_balance_of using btree (callback_id); --SEQONLY --1
-- CREATE INDEX IF NOT EXISTS fa2_balance_of_autoid   on T.fa2_balance_of using btree (autoid); --SEQONLY
CREATE INDEX IF NOT EXISTS token_accounts_registry_token on T.accounts_registry using btree(token_address_id); --SEQONLY --1
CREATE INDEX IF NOT EXISTS token_accounts_registry_address on T.accounts_registry using btree(address_id); --SEQONLY --1
-- CREATE INDEX IF NOT EXISTS token_accounts_registry_autoid on T.accounts_registry using btree(autoid); --SEQONLY
CREATE INDEX IF NOT EXISTS indexer_measurements_block_hash on indexer_measurements using btree (block_hash);
--CREATE INDEX IF NOT EXISTS indexer_measurements_level on indexer_measurements using btree (level);
-- create primary keys
DO $$
BEGIN
 ALTER TABLE C.block ADD CONSTRAINT block_pkey PRIMARY KEY (hash);
 EXCEPTION WHEN SQLSTATE '42P16' THEN RETURN;
END; $$;
DO $$
BEGIN
 ALTER TABLE C.operation ADD CONSTRAINT operation_pkey PRIMARY KEY (hash_id);
 EXCEPTION WHEN SQLSTATE '42P16' THEN RETURN;
END; $$;
DO $$
BEGIN
 ALTER TABLE C.operation_alpha ADD CONSTRAINT operation_alpha_pkey PRIMARY KEY (hash_id, id, internal, block_level);
 EXCEPTION WHEN SQLSTATE '42P16' THEN RETURN;
END; $$;
DO $$
BEGIN
 ALTER TABLE C.operation_sender_and_receiver ADD CONSTRAINT operation_sender_and_receiver_pkey PRIMARY KEY (operation_id);
 EXCEPTION WHEN SQLSTATE '42P16' THEN RETURN;
END; $$;
DO $$
BEGIN
 ALTER TABLE C.proposals ADD CONSTRAINT proposals_pkey PRIMARY KEY (proposal);
 EXCEPTION WHEN SQLSTATE '42P16' THEN RETURN;
END; $$;
DO $$
BEGIN
 ALTER TABLE C.proposal ADD CONSTRAINT proposal_pkey PRIMARY KEY (operation_id, proposal_id);
 EXCEPTION WHEN SQLSTATE '42P16' THEN RETURN;
END; $$;
DO $$
BEGIN
 ALTER TABLE C.ballot ADD CONSTRAINT ballot_pkey PRIMARY KEY (operation_id);
 EXCEPTION WHEN SQLSTATE '42P16' THEN RETURN;
END; $$;
DO $$
BEGIN
 ALTER TABLE C.double_endorsement_evidence ADD CONSTRAINT double_endorsement_evidence_pkey PRIMARY KEY (operation_id);
 EXCEPTION WHEN SQLSTATE '42P16' THEN RETURN;
END; $$;
DO $$
BEGIN
 ALTER TABLE C.double_preendorsement_evidence ADD CONSTRAINT double_preendorsement_evidence_pkey PRIMARY KEY (operation_id);
 EXCEPTION WHEN SQLSTATE '42P16' THEN RETURN;
END; $$;
DO $$
BEGIN
 ALTER TABLE C.double_baking_evidence ADD CONSTRAINT double_baking_evidence_pkey PRIMARY KEY (operation_id);
 EXCEPTION WHEN SQLSTATE '42P16' THEN RETURN;
END; $$;
DO $$
BEGIN
 ALTER TABLE C.manager_numbers ADD CONSTRAINT manager_numbers_pkey PRIMARY KEY (operation_id);
 EXCEPTION WHEN SQLSTATE '42P16' THEN RETURN;
END; $$;
DO $$
BEGIN
 ALTER TABLE C.activation ADD CONSTRAINT activation_pkey PRIMARY KEY (operation_id);
 EXCEPTION WHEN SQLSTATE '42P16' THEN RETURN;
END; $$;
DO $$
BEGIN
 ALTER TABLE C.endorsement ADD CONSTRAINT endorsement_pkey PRIMARY KEY (operation_id);
 EXCEPTION WHEN SQLSTATE '42P16' THEN RETURN;
END; $$;
DO $$
BEGIN
 ALTER TABLE C.preendorsement ADD CONSTRAINT preendorsement_pkey PRIMARY KEY (operation_id);
 EXCEPTION WHEN SQLSTATE '42P16' THEN RETURN;
END; $$;
DO $$
BEGIN
 ALTER TABLE C.seed_nonce_revelation ADD CONSTRAINT seed_nonce_revelation_pkey PRIMARY KEY (operation_id);
 EXCEPTION WHEN SQLSTATE '42P16' THEN RETURN;
END; $$;
DO $$
BEGIN
 ALTER TABLE C.block_alpha ADD CONSTRAINT block_alpha_pkey PRIMARY KEY (level);
 EXCEPTION WHEN SQLSTATE '42P16' THEN RETURN;
END; $$;
DO $$
BEGIN
 ALTER TABLE C.deactivated ADD CONSTRAINT deactivated_pkey PRIMARY KEY (pkh_id, block_level);
 EXCEPTION WHEN SQLSTATE '42P16' THEN RETURN;
END; $$;
DO $$
BEGIN
 ALTER TABLE C.contract_balance ADD CONSTRAINT contract_balance_pkey PRIMARY KEY (address_id, block_level);
 EXCEPTION WHEN SQLSTATE '42P16' THEN RETURN;
END; $$;
DO $$
BEGIN
 ALTER TABLE C.tx ADD CONSTRAINT tx_pkey PRIMARY KEY (operation_id);
 EXCEPTION WHEN SQLSTATE '42P16' THEN RETURN;
END; $$;
DO $$
BEGIN
 ALTER TABLE C.origination ADD CONSTRAINT origination_pkey PRIMARY KEY (operation_id);
 EXCEPTION WHEN SQLSTATE '42P16' THEN RETURN;
END; $$;
DO $$
BEGIN
 ALTER TABLE C.manager ADD CONSTRAINT manager_pkey PRIMARY KEY (operation_id);
 EXCEPTION WHEN SQLSTATE '42P16' THEN RETURN;
END; $$;
DO $$
BEGIN
 ALTER TABLE C.delegation ADD CONSTRAINT delegation_pkey PRIMARY KEY (operation_id);
 EXCEPTION WHEN SQLSTATE '42P16' THEN RETURN;
END; $$;
DO $$
BEGIN
 ALTER TABLE C.reveal ADD CONSTRAINT reveal_pkey PRIMARY KEY (operation_id);
 EXCEPTION WHEN SQLSTATE '42P16' THEN RETURN;
END; $$;
DO $$
BEGIN
 ALTER TABLE C.register_global_constant ADD CONSTRAINT register_global_constant_pkey PRIMARY KEY (operation_id);
 EXCEPTION WHEN SQLSTATE '42P16' THEN RETURN;
END; $$;
DO $$
BEGIN
 ALTER TABLE C.set_deposits_limit ADD CONSTRAINT set_deposits_limit_pkey PRIMARY KEY (operation_id);
 EXCEPTION WHEN SQLSTATE '42P16' THEN RETURN;
END; $$;
DO $$
BEGIN
 ALTER TABLE C.tx_rollup_origination ADD CONSTRAINT tx_rollup_origination_pkey PRIMARY KEY (operation_id);
 EXCEPTION WHEN SQLSTATE '42P16' THEN RETURN;
END; $$;
DO $$
BEGIN
 ALTER TABLE C.tx_rollup_submit_batch ADD CONSTRAINT tx_rollup_submit_batch_pkey PRIMARY KEY (operation_id);
 EXCEPTION WHEN SQLSTATE '42P16' THEN RETURN;
END; $$;
DO $$
BEGIN
 ALTER TABLE C.tx_rollup_commit ADD CONSTRAINT tx_rollup_commit_pkey PRIMARY KEY (operation_id);
 EXCEPTION WHEN SQLSTATE '42P16' THEN RETURN;
END; $$;
DO $$
BEGIN
 ALTER TABLE C.tx_rollup_finalize_commitment ADD CONSTRAINT tx_rollup_finalize_commitment_pkey PRIMARY KEY (operation_id);
 EXCEPTION WHEN SQLSTATE '42P16' THEN RETURN;
END; $$;
DO $$
BEGIN
 ALTER TABLE C.tx_rollup_rejection ADD CONSTRAINT tx_rollup_rejection_pkey PRIMARY KEY (operation_id);
 EXCEPTION WHEN SQLSTATE '42P16' THEN RETURN;
END; $$;
DO $$
BEGIN
 ALTER TABLE C.tx_rollup_dispatch_tickets ADD CONSTRAINT tx_rollup_dispatch_tickets_pkey PRIMARY KEY (operation_id);
 EXCEPTION WHEN SQLSTATE '42P16' THEN RETURN;
END; $$;
DO $$
BEGIN
 ALTER TABLE C.tx_rollup_remove_commitment ADD CONSTRAINT tx_rollup_remove_commitment_pkey PRIMARY KEY (operation_id);
 EXCEPTION WHEN SQLSTATE '42P16' THEN RETURN;
END; $$;
DO $$
BEGIN
 ALTER TABLE C.tx_rollup_return_bond ADD CONSTRAINT tx_rollup_return_bond_pkey PRIMARY KEY (operation_id);
 EXCEPTION WHEN SQLSTATE '42P16' THEN RETURN;
END; $$;
DO $$
BEGIN
 ALTER TABLE C.transfer_ticket ADD CONSTRAINT transfer_ticket_pkey PRIMARY KEY (operation_id);
 EXCEPTION WHEN SQLSTATE '42P16' THEN RETURN;
END; $$;
DO $$
BEGIN
 ALTER TABLE C.drain_delegate ADD CONSTRAINT drain_delegate_pkey PRIMARY KEY (operation_id);
 EXCEPTION WHEN SQLSTATE '42P16' THEN RETURN;
END; $$;
DO $$
BEGIN
 ALTER TABLE C.sc_rollup_originate ADD CONSTRAINT sc_rollup_originate_pkey PRIMARY KEY (operation_id);
 EXCEPTION WHEN SQLSTATE '42P16' THEN RETURN;
END; $$;
DO $$
BEGIN
 ALTER TABLE C.sc_rollup_publish ADD CONSTRAINT sc_rollup_publish_pkey PRIMARY KEY (operation_id);
 EXCEPTION WHEN SQLSTATE '42P16' THEN RETURN;
END; $$;
DO $$
BEGIN
 ALTER TABLE C.sc_rollup_cement ADD CONSTRAINT sc_rollup_cement_pkey PRIMARY KEY (operation_id);
 EXCEPTION WHEN SQLSTATE '42P16' THEN RETURN;
END; $$;
DO $$
BEGIN
 ALTER TABLE C.sc_rollup_timeout ADD CONSTRAINT sc_rollup_timeout_pkey PRIMARY KEY (operation_id);
 EXCEPTION WHEN SQLSTATE '42P16' THEN RETURN;
END; $$;
DO $$
BEGIN
 ALTER TABLE C.sc_rollup_refute ADD CONSTRAINT sc_rollup_refute_pkey PRIMARY KEY (operation_id);
 EXCEPTION WHEN SQLSTATE '42P16' THEN RETURN;
END; $$;
DO $$
BEGIN
 ALTER TABLE C.zk_rollup_publish ADD CONSTRAINT zk_rollup_publish_pkey PRIMARY KEY (operation_id);
 EXCEPTION WHEN SQLSTATE '42P16' THEN RETURN;
END; $$;
DO $$
BEGIN
 ALTER TABLE C.sc_rollup_add_messages ADD CONSTRAINT sc_rollup_add_messages_pkey PRIMARY KEY (operation_id);
 EXCEPTION WHEN SQLSTATE '42P16' THEN RETURN;
END; $$;
DO $$
BEGIN
 ALTER TABLE C.event ADD CONSTRAINT event_pkey PRIMARY KEY (operation_id);
 EXCEPTION WHEN SQLSTATE '42P16' THEN RETURN;
END; $$;
DO $$
BEGIN
 ALTER TABLE C.increase_paid_storage ADD CONSTRAINT increase_paid_storage_pkey PRIMARY KEY (operation_id);
 EXCEPTION WHEN SQLSTATE '42P16' THEN RETURN;
END; $$;
DO $$
BEGIN
 ALTER TABLE C.dal_publish_slot_header ADD CONSTRAINT dal_publish_slot_header_pkey PRIMARY KEY (operation_id);
 EXCEPTION WHEN SQLSTATE '42P16' THEN RETURN;
END; $$;
DO $$
BEGIN
 ALTER TABLE C.dal_attestation ADD CONSTRAINT dal_attestation_pkey PRIMARY KEY (operation_id);
 EXCEPTION WHEN SQLSTATE '42P16' THEN RETURN;
END; $$;
DO $$
BEGIN
 ALTER TABLE C.sc_rollup_execute_outbox_message ADD CONSTRAINT sc_rollup_execute_outbox_message_pkey PRIMARY KEY (operation_id);
 EXCEPTION WHEN SQLSTATE '42P16' THEN RETURN;
END; $$;
DO $$
BEGIN
 ALTER TABLE C.update_consensus_key ADD CONSTRAINT update_consensus_key_pkey PRIMARY KEY (operation_id);
 EXCEPTION WHEN SQLSTATE '42P16' THEN RETURN;
END; $$;
DO $$
BEGIN
 ALTER TABLE C.sc_rollup_recover_bond ADD CONSTRAINT sc_rollup_recover_bond_pkey PRIMARY KEY (operation_id);
 EXCEPTION WHEN SQLSTATE '42P16' THEN RETURN;
END; $$;
DO $$
BEGIN
 ALTER TABLE C.vdf_revelation ADD CONSTRAINT vdf_revelation_pkey PRIMARY KEY (operation_id);
 EXCEPTION WHEN SQLSTATE '42P16' THEN RETURN;
END; $$;
DO $$
BEGIN
 ALTER TABLE C.delegated_contract ADD CONSTRAINT delegated_contract_pkey PRIMARY KEY (delegate_id, delegator_id, cycle, level);
 EXCEPTION WHEN SQLSTATE '42P16' THEN RETURN;
END; $$;
-- create foreign keys
DO $$
BEGIN
  ALTER TABLE C.block ADD CONSTRAINT block_predecessor_fkey FOREIGN KEY (predecessor) REFERENCES C.block(level) ON DELETE CASCADE ON UPDATE CASCADE;
  EXCEPTION WHEN SQLSTATE '42710' THEN RETURN;
END; $$;
DO $$
BEGIN
  ALTER TABLE C.implicit_operations_results ADD CONSTRAINT implicit_operations_results_tx_roll FOREIGN KEY (originated_tx_rollup_id) REFERENCES C.address(address_id) ON DELETE CASCADE ON UPDATE CASCADE;
  EXCEPTION WHEN SQLSTATE '42710' THEN RETURN;
END; $$;
DO $$
BEGIN
  ALTER TABLE C.implicit_operations_results ADD CONSTRAINT implicit_operations_results_op_kind FOREIGN KEY (operation_kind) REFERENCES C.operation_kind(id) ON DELETE CASCADE ON UPDATE CASCADE;
  EXCEPTION WHEN SQLSTATE '42710' THEN RETURN;
END; $$;
DO $$
BEGIN
  ALTER TABLE C.operation ADD CONSTRAINT operation_block_level_fkey FOREIGN KEY (block_level) REFERENCES C.block(level) ON DELETE CASCADE ON UPDATE CASCADE;
  EXCEPTION WHEN SQLSTATE '42710' THEN RETURN;
END; $$;
DO $$
BEGIN
  ALTER TABLE C.operation_alpha ADD CONSTRAINT operation_alpha_hash_fkey FOREIGN KEY (hash_id) REFERENCES C.operation(hash_id) ON DELETE CASCADE ON UPDATE CASCADE;
  EXCEPTION WHEN SQLSTATE '42710' THEN RETURN;
END; $$;
DO $$
BEGIN
  ALTER TABLE C.operation_alpha ADD CONSTRAINT operation_alpha_block_hash_fkey FOREIGN KEY (block_level) REFERENCES C.block(level) ON DELETE CASCADE ON UPDATE CASCADE;
  EXCEPTION WHEN SQLSTATE '42710' THEN RETURN;
END; $$;
DO $$
BEGIN
  ALTER TABLE C.operation_alpha ADD CONSTRAINT operation_alpha_op_kind FOREIGN KEY (operation_kind) REFERENCES C.operation_kind(id) ON DELETE CASCADE ON UPDATE CASCADE;
  EXCEPTION WHEN SQLSTATE '42710' THEN RETURN;
END; $$;
DO $$
BEGIN
  ALTER TABLE C.operation_sender_and_receiver ADD CONSTRAINT operation_sender_and_receiver_operation_id_fkey FOREIGN KEY (operation_id) REFERENCES C.operation_alpha(autoid) ON DELETE CASCADE ON UPDATE CASCADE;
  EXCEPTION WHEN SQLSTATE '42710' THEN RETURN;
END; $$;
DO $$
BEGIN
  ALTER TABLE C.operation_sender_and_receiver ADD CONSTRAINT operation_sender_and_receiver_sender_id_fkey FOREIGN KEY (sender_id) REFERENCES C.address(address_id) ON DELETE CASCADE ON UPDATE CASCADE;
  EXCEPTION WHEN SQLSTATE '42710' THEN RETURN;
END; $$;
DO $$
BEGIN
  ALTER TABLE C.operation_sender_and_receiver ADD CONSTRAINT operation_sender_and_receiver_receiver_id_fkey FOREIGN KEY (receiver_id) REFERENCES C.address(address_id) ON DELETE CASCADE ON UPDATE CASCADE;
  EXCEPTION WHEN SQLSTATE '42710' THEN RETURN;
END; $$;
DO $$
BEGIN
  ALTER TABLE C.proposal ADD CONSTRAINT proposal_operation_id_fkey FOREIGN KEY (operation_id) REFERENCES C.operation_alpha(autoid) ON DELETE CASCADE ON UPDATE CASCADE;
  EXCEPTION WHEN SQLSTATE '42710' THEN RETURN;
END; $$;
DO $$
BEGIN
  ALTER TABLE C.proposal ADD CONSTRAINT proposal_source_fkey FOREIGN KEY (source_id) REFERENCES C.address(address_id) ON DELETE CASCADE ON UPDATE CASCADE;
  EXCEPTION WHEN SQLSTATE '42710' THEN RETURN;
END; $$;
DO $$
BEGIN
  ALTER TABLE C.proposal ADD CONSTRAINT proposal_proposal_id_fkey FOREIGN KEY (proposal_id) REFERENCES C.proposals(proposal_id) ON DELETE CASCADE ON UPDATE CASCADE;
  EXCEPTION WHEN SQLSTATE '42710' THEN RETURN;
END; $$;
DO $$
BEGIN
  ALTER TABLE C.ballot ADD CONSTRAINT ballot_operation_id_fkey FOREIGN KEY (operation_id) REFERENCES C.operation_alpha(autoid) ON DELETE CASCADE ON UPDATE CASCADE;
  EXCEPTION WHEN SQLSTATE '42710' THEN RETURN;
END; $$;
DO $$
BEGIN
  ALTER TABLE C.ballot ADD CONSTRAINT ballot_proposal_id_fkey FOREIGN KEY (proposal_id) REFERENCES C.proposals(proposal_id) ON DELETE CASCADE ON UPDATE CASCADE;
  EXCEPTION WHEN SQLSTATE '42710' THEN RETURN;
END; $$;
DO $$
BEGIN
  ALTER TABLE C.double_endorsement_evidence ADD CONSTRAINT double_endorsement_evidence_operation_id_fkey FOREIGN KEY (operation_id) REFERENCES C.operation_alpha(autoid) ON DELETE CASCADE ON UPDATE CASCADE;
  EXCEPTION WHEN SQLSTATE '42710' THEN RETURN;
END; $$;
DO $$
BEGIN
  ALTER TABLE C.double_preendorsement_evidence ADD CONSTRAINT double_preendorsement_evidence_operation_id_fkey FOREIGN KEY (operation_id) REFERENCES C.operation_alpha(autoid) ON DELETE CASCADE ON UPDATE CASCADE;
  EXCEPTION WHEN SQLSTATE '42710' THEN RETURN;
END; $$;
DO $$
BEGIN
  ALTER TABLE C.double_baking_evidence ADD CONSTRAINT double_baking_evidence_operation_id_fkey FOREIGN KEY (operation_id) REFERENCES C.operation_alpha(autoid) ON DELETE CASCADE ON UPDATE CASCADE;
  EXCEPTION WHEN SQLSTATE '42710' THEN RETURN;
END; $$;
DO $$
BEGIN
  ALTER TABLE C.manager_numbers ADD CONSTRAINT manager_numbers_operation_id_fkey FOREIGN KEY (operation_id) REFERENCES C.operation_alpha(autoid) ON DELETE CASCADE ON UPDATE CASCADE;
  EXCEPTION WHEN SQLSTATE '42710' THEN RETURN;
END; $$;
DO $$
BEGIN
  ALTER TABLE C.activation ADD CONSTRAINT activation_operation_id_fkey FOREIGN KEY (operation_id) REFERENCES C.operation_alpha(autoid) ON DELETE CASCADE ON UPDATE CASCADE;
  EXCEPTION WHEN SQLSTATE '42710' THEN RETURN;
END; $$;
DO $$
BEGIN
  ALTER TABLE C.activation ADD CONSTRAINT activation_pkh_fkey FOREIGN KEY (pkh_id) REFERENCES C.address(address_id) ON DELETE CASCADE ON UPDATE CASCADE;
  EXCEPTION WHEN SQLSTATE '42710' THEN RETURN;
END; $$;
DO $$
BEGIN
  ALTER TABLE C.endorsement ADD CONSTRAINT endorsement_operation_id_fkey FOREIGN KEY (operation_id) REFERENCES C.operation_alpha(autoid) ON DELETE CASCADE ON UPDATE CASCADE;
  EXCEPTION WHEN SQLSTATE '42710' THEN RETURN;
END; $$;
DO $$
BEGIN
  ALTER TABLE C.endorsement ADD CONSTRAINT endorsement_delegate_fkey FOREIGN KEY (delegate_id) REFERENCES C.address(address_id) ON DELETE CASCADE ON UPDATE CASCADE;
  EXCEPTION WHEN SQLSTATE '42710' THEN RETURN;
END; $$;
DO $$
BEGIN
  ALTER TABLE C.preendorsement ADD CONSTRAINT preendorsement_operation_id_fkey FOREIGN KEY (operation_id) REFERENCES C.operation_alpha(autoid) ON DELETE CASCADE ON UPDATE CASCADE;
  EXCEPTION WHEN SQLSTATE '42710' THEN RETURN;
END; $$;
DO $$
BEGIN
  ALTER TABLE C.preendorsement ADD CONSTRAINT preendorsement_delegate_fkey FOREIGN KEY (delegate_id) REFERENCES C.address(address_id) ON DELETE CASCADE ON UPDATE CASCADE;
  EXCEPTION WHEN SQLSTATE '42710' THEN RETURN;
END; $$;
DO $$
BEGIN
  ALTER TABLE C.seed_nonce_revelation ADD CONSTRAINT seed_nonce_revelation_operation_id_fkey FOREIGN KEY (operation_id) REFERENCES C.operation_alpha(autoid) ON DELETE CASCADE ON UPDATE CASCADE;
  EXCEPTION WHEN SQLSTATE '42710' THEN RETURN;
END; $$;
DO $$
BEGIN
  ALTER TABLE C.block_alpha ADD CONSTRAINT block_alpha_hash_fkey FOREIGN KEY (level) REFERENCES C.block(level) ON DELETE CASCADE ON UPDATE CASCADE;
  EXCEPTION WHEN SQLSTATE '42710' THEN RETURN;
END; $$;
DO $$
BEGIN
  ALTER TABLE C.block_alpha ADD CONSTRAINT block_alpha_baker_fkey FOREIGN KEY (baker_id) REFERENCES C.address(address_id) ON DELETE CASCADE ON UPDATE CASCADE;
  EXCEPTION WHEN SQLSTATE '42710' THEN RETURN;
END; $$;
DO $$
BEGIN
  ALTER TABLE C.block_alpha ADD CONSTRAINT block_alpha_delegate FOREIGN KEY (delegate_id) REFERENCES C.address(address_id) ON DELETE CASCADE ON UPDATE CASCADE;
  EXCEPTION WHEN SQLSTATE '42710' THEN RETURN;
END; $$;
DO $$
BEGIN
  ALTER TABLE C.deactivated ADD CONSTRAINT deactivated_pkh_fkey FOREIGN KEY (pkh_id) REFERENCES C.address(address_id) ON DELETE CASCADE ON UPDATE CASCADE;
  EXCEPTION WHEN SQLSTATE '42710' THEN RETURN;
END; $$;
DO $$
BEGIN
  ALTER TABLE C.deactivated ADD CONSTRAINT deactivated_block_hash_fkey FOREIGN KEY (block_level) REFERENCES C.block(level) ON DELETE CASCADE ON UPDATE CASCADE;
  EXCEPTION WHEN SQLSTATE '42710' THEN RETURN;
END; $$;
DO $$
BEGIN
  ALTER TABLE C.contract_balance ADD CONSTRAINT contract_balance_address_fkey FOREIGN KEY (address_id) REFERENCES C.address(address_id) ON DELETE CASCADE ON UPDATE CASCADE;
  EXCEPTION WHEN SQLSTATE '42710' THEN RETURN;
END; $$;
DO $$
BEGIN
  ALTER TABLE C.contract_balance ADD CONSTRAINT contract_balance_block_hash_fkey FOREIGN KEY (block_level) REFERENCES C.block(level) ON DELETE CASCADE ON UPDATE CASCADE;
  EXCEPTION WHEN SQLSTATE '42710' THEN RETURN;
END; $$;
DO $$
BEGIN
  ALTER TABLE C.contract_script ADD CONSTRAINT contract_script_address_fkey FOREIGN KEY (address_id) REFERENCES C.address(address_id) ON DELETE CASCADE ON UPDATE CASCADE;
  EXCEPTION WHEN SQLSTATE '42710' THEN RETURN;
END; $$;
DO $$
BEGIN
  ALTER TABLE C.contract_script ADD CONSTRAINT contract_script_block_level_fkey FOREIGN KEY (block_level) REFERENCES C.block(level) ON DELETE SET NULL ON UPDATE CASCADE;
  EXCEPTION WHEN SQLSTATE '42710' THEN RETURN;
END; $$;
DO $$
BEGIN
  ALTER TABLE C.tx ADD CONSTRAINT tx_operation_id_fkey FOREIGN KEY (operation_id) REFERENCES C.operation_alpha(autoid) ON DELETE CASCADE ON UPDATE CASCADE;
  EXCEPTION WHEN SQLSTATE '42710' THEN RETURN;
END; $$;
DO $$
BEGIN
  ALTER TABLE C.tx ADD CONSTRAINT tx_source_fkey FOREIGN KEY (source_id) REFERENCES C.address(address_id) ON DELETE CASCADE ON UPDATE CASCADE;
  EXCEPTION WHEN SQLSTATE '42710' THEN RETURN;
END; $$;
DO $$
BEGIN
  ALTER TABLE C.tx ADD CONSTRAINT tx_destination_fkey FOREIGN KEY (destination_id) REFERENCES C.address(address_id) ON DELETE CASCADE ON UPDATE CASCADE;
  EXCEPTION WHEN SQLSTATE '42710' THEN RETURN;
END; $$;
DO $$
BEGIN
  ALTER TABLE C.origination ADD CONSTRAINT origination_operation_id_fkey FOREIGN KEY (operation_id) REFERENCES C.operation_alpha(autoid) ON DELETE CASCADE ON UPDATE CASCADE;
  EXCEPTION WHEN SQLSTATE '42710' THEN RETURN;
END; $$;
DO $$
BEGIN
  ALTER TABLE C.origination ADD CONSTRAINT origination_operation_source_fkey FOREIGN KEY (source_id) REFERENCES C.address(address_id) ON DELETE CASCADE ON UPDATE CASCADE;
  EXCEPTION WHEN SQLSTATE '42710' THEN RETURN;
END; $$;
DO $$
BEGIN
  ALTER TABLE C.origination ADD CONSTRAINT origination_k_fkey FOREIGN KEY (k_id) REFERENCES C.address(address_id) ON DELETE CASCADE ON UPDATE CASCADE;
  EXCEPTION WHEN SQLSTATE '42710' THEN RETURN;
END; $$;
DO $$
BEGIN
  ALTER TABLE C.manager ADD CONSTRAINT manager_operation_id_fkey FOREIGN KEY (operation_id) REFERENCES C.operation_alpha(autoid) ON DELETE CASCADE ON UPDATE CASCADE;
  EXCEPTION WHEN SQLSTATE '42710' THEN RETURN;
END; $$;
DO $$
BEGIN
  ALTER TABLE C.manager ADD CONSTRAINT manager_manager_id_fkey FOREIGN KEY (manager_id) REFERENCES C.address(address_id) ON DELETE CASCADE ON UPDATE CASCADE;
  EXCEPTION WHEN SQLSTATE '42710' THEN RETURN;
END; $$;
DO $$
BEGIN
  ALTER TABLE C.delegation ADD CONSTRAINT delegation_operation_id_fkey FOREIGN KEY (operation_id) REFERENCES C.operation_alpha(autoid) ON DELETE CASCADE ON UPDATE CASCADE;
  EXCEPTION WHEN SQLSTATE '42710' THEN RETURN;
END; $$;
DO $$
BEGIN
  ALTER TABLE C.delegation ADD CONSTRAINT delegation_source_fkey FOREIGN KEY (source_id) REFERENCES C.address(address_id) ON DELETE CASCADE ON UPDATE CASCADE;
  EXCEPTION WHEN SQLSTATE '42710' THEN RETURN;
END; $$;
DO $$
BEGIN
  ALTER TABLE C.delegation ADD CONSTRAINT delegation_pkh_fkey FOREIGN KEY (pkh_id) REFERENCES C.address(address_id) ON DELETE CASCADE ON UPDATE CASCADE;
  EXCEPTION WHEN SQLSTATE '42710' THEN RETURN;
END; $$;
DO $$
BEGIN
  ALTER TABLE C.reveal ADD CONSTRAINT reveal_operation_hash_op_id_fkey FOREIGN KEY (operation_id) REFERENCES C.operation_alpha(autoid) ON DELETE CASCADE ON UPDATE CASCADE;
  EXCEPTION WHEN SQLSTATE '42710' THEN RETURN;
END; $$;
DO $$
BEGIN
  ALTER TABLE C.reveal ADD CONSTRAINT reveal_source_fkey FOREIGN KEY (source_id) REFERENCES C.address(address_id) ON DELETE CASCADE ON UPDATE CASCADE;
  EXCEPTION WHEN SQLSTATE '42710' THEN RETURN;
END; $$;
DO $$
BEGIN
  ALTER TABLE C.global_constants ADD CONSTRAINT global_constants_global_address_id_fkey FOREIGN KEY (global_address_id) REFERENCES C.operation_alpha(autoid) ON DELETE CASCADE ON UPDATE CASCADE;
  EXCEPTION WHEN SQLSTATE '42710' THEN RETURN;
END; $$;
DO $$
BEGIN
  ALTER TABLE C.register_global_constant ADD CONSTRAINT register_global_constant_operation_id_fkey FOREIGN KEY (operation_id) REFERENCES C.operation_alpha(autoid) ON DELETE CASCADE ON UPDATE CASCADE;
  EXCEPTION WHEN SQLSTATE '42710' THEN RETURN;
END; $$;
DO $$
BEGIN
  ALTER TABLE C.register_global_constant ADD CONSTRAINT register_global_constant_source_fkey FOREIGN KEY (source_id) REFERENCES C.address(address_id) ON DELETE CASCADE ON UPDATE CASCADE;
  EXCEPTION WHEN SQLSTATE '42710' THEN RETURN;
END; $$;
DO $$
BEGIN
  ALTER TABLE C.set_deposits_limit ADD CONSTRAINT set_deposits_limit_operation_id_fkey FOREIGN KEY (operation_id) REFERENCES C.operation_alpha(autoid) ON DELETE CASCADE ON UPDATE CASCADE;
  EXCEPTION WHEN SQLSTATE '42710' THEN RETURN;
END; $$;
DO $$
BEGIN
  ALTER TABLE C.set_deposits_limit ADD CONSTRAINT set_deposits_limit_source_fkey FOREIGN KEY (source_id) REFERENCES C.address(address_id) ON DELETE CASCADE ON UPDATE CASCADE;
  EXCEPTION WHEN SQLSTATE '42710' THEN RETURN;
END; $$;
DO $$
BEGIN
  ALTER TABLE C.tx_rollup_origination ADD CONSTRAINT tx_rollup_origination_operation_id_fkey FOREIGN KEY (operation_id) REFERENCES C.operation_alpha(autoid) ON DELETE CASCADE ON UPDATE CASCADE;
  EXCEPTION WHEN SQLSTATE '42710' THEN RETURN;
END; $$;
DO $$
BEGIN
  ALTER TABLE C.tx_rollup_origination ADD CONSTRAINT tx_rollup_origination_source_fkey FOREIGN KEY (source_id) REFERENCES C.address(address_id) ON DELETE CASCADE ON UPDATE CASCADE;
  EXCEPTION WHEN SQLSTATE '42710' THEN RETURN;
END; $$;
DO $$
BEGIN
  ALTER TABLE C.tx_rollup_origination ADD CONSTRAINT tx_rollup_origination_tx_roll FOREIGN KEY (tx_rollup_origination) REFERENCES C.address(address_id) ON DELETE CASCADE ON UPDATE CASCADE;
  EXCEPTION WHEN SQLSTATE '42710' THEN RETURN;
END; $$;
DO $$
BEGIN
  ALTER TABLE C.tx_rollup_submit_batch ADD CONSTRAINT tx_rollup_submit_batch_operation_id_fkey FOREIGN KEY (operation_id) REFERENCES C.operation_alpha(autoid) ON DELETE CASCADE ON UPDATE CASCADE;
  EXCEPTION WHEN SQLSTATE '42710' THEN RETURN;
END; $$;
DO $$
BEGIN
  ALTER TABLE C.tx_rollup_submit_batch ADD CONSTRAINT tx_rollup_submit_batch_source_fkey FOREIGN KEY (source_id) REFERENCES C.address(address_id) ON DELETE CASCADE ON UPDATE CASCADE;
  EXCEPTION WHEN SQLSTATE '42710' THEN RETURN;
END; $$;
DO $$
BEGIN
  ALTER TABLE C.tx_rollup_submit_batch ADD CONSTRAINT tx_rollup_submit_batch_tx_roll FOREIGN KEY (tx_rollup) REFERENCES C.address(address_id) ON DELETE CASCADE ON UPDATE CASCADE;
  EXCEPTION WHEN SQLSTATE '42710' THEN RETURN;
END; $$;
DO $$
BEGIN
  ALTER TABLE C.tx_rollup_commit ADD CONSTRAINT tx_rollup_commit_operation_id_fkey FOREIGN KEY (operation_id) REFERENCES C.operation_alpha(autoid) ON DELETE CASCADE ON UPDATE CASCADE;
  EXCEPTION WHEN SQLSTATE '42710' THEN RETURN;
END; $$;
DO $$
BEGIN
  ALTER TABLE C.tx_rollup_commit ADD CONSTRAINT tx_rollup_commit_source_fkey FOREIGN KEY (source_id) REFERENCES C.address(address_id) ON DELETE CASCADE ON UPDATE CASCADE;
  EXCEPTION WHEN SQLSTATE '42710' THEN RETURN;
END; $$;
DO $$
BEGIN
  ALTER TABLE C.tx_rollup_commit ADD CONSTRAINT tx_rollup_commit_tx_roll FOREIGN KEY (tx_rollup) REFERENCES C.address(address_id) ON DELETE CASCADE ON UPDATE CASCADE;
  EXCEPTION WHEN SQLSTATE '42710' THEN RETURN;
END; $$;
DO $$
BEGIN
  ALTER TABLE C.tx_rollup_finalize_commitment ADD CONSTRAINT tx_rollup_finalize_commitment_operation_id_fkey FOREIGN KEY (operation_id) REFERENCES C.operation_alpha(autoid) ON DELETE CASCADE ON UPDATE CASCADE;
  EXCEPTION WHEN SQLSTATE '42710' THEN RETURN;
END; $$;
DO $$
BEGIN
  ALTER TABLE C.tx_rollup_finalize_commitment ADD CONSTRAINT tx_rollup_finalize_commitment_source_fkey FOREIGN KEY (source_id) REFERENCES C.address(address_id) ON DELETE CASCADE ON UPDATE CASCADE;
  EXCEPTION WHEN SQLSTATE '42710' THEN RETURN;
END; $$;
DO $$
BEGIN
  ALTER TABLE C.tx_rollup_finalize_commitment ADD CONSTRAINT tx_rollup_finalize_commitment_tx_roll FOREIGN KEY (tx_rollup) REFERENCES C.address(address_id) ON DELETE CASCADE ON UPDATE CASCADE;
  EXCEPTION WHEN SQLSTATE '42710' THEN RETURN;
END; $$;
DO $$
BEGIN
  ALTER TABLE C.tx_rollup_rejection ADD CONSTRAINT tx_rollup_rejection_operation_id_fkey FOREIGN KEY (operation_id) REFERENCES C.operation_alpha(autoid) ON DELETE CASCADE ON UPDATE CASCADE;
  EXCEPTION WHEN SQLSTATE '42710' THEN RETURN;
END; $$;
DO $$
BEGIN
  ALTER TABLE C.tx_rollup_rejection ADD CONSTRAINT tx_rollup_rejection_source_fkey FOREIGN KEY (source_id) REFERENCES C.address(address_id) ON DELETE CASCADE ON UPDATE CASCADE;
  EXCEPTION WHEN SQLSTATE '42710' THEN RETURN;
END; $$;
DO $$
BEGIN
  ALTER TABLE C.tx_rollup_rejection ADD CONSTRAINT tx_rollup_rejection_tx_roll FOREIGN KEY (tx_rollup) REFERENCES C.address(address_id) ON DELETE CASCADE ON UPDATE CASCADE;
  EXCEPTION WHEN SQLSTATE '42710' THEN RETURN;
END; $$;
DO $$
BEGIN
  ALTER TABLE C.tx_rollup_dispatch_tickets ADD CONSTRAINT tx_rollup_dispatch_tickets_operation_id_fkey FOREIGN KEY (operation_id) REFERENCES C.operation_alpha(autoid) ON DELETE CASCADE ON UPDATE CASCADE;
  EXCEPTION WHEN SQLSTATE '42710' THEN RETURN;
END; $$;
DO $$
BEGIN
  ALTER TABLE C.tx_rollup_dispatch_tickets ADD CONSTRAINT tx_rollup_dispatch_tickets_source_fkey FOREIGN KEY (source_id) REFERENCES C.address(address_id) ON DELETE CASCADE ON UPDATE CASCADE;
  EXCEPTION WHEN SQLSTATE '42710' THEN RETURN;
END; $$;
DO $$
BEGIN
  ALTER TABLE C.tx_rollup_dispatch_tickets ADD CONSTRAINT tx_rollup_dispatch_tickets_tx_roll FOREIGN KEY (tx_rollup) REFERENCES C.address(address_id) ON DELETE CASCADE ON UPDATE CASCADE;
  EXCEPTION WHEN SQLSTATE '42710' THEN RETURN;
END; $$;
DO $$
BEGIN
  ALTER TABLE C.tx_rollup_remove_commitment ADD CONSTRAINT tx_rollup_remove_commitment_operation_id_fkey FOREIGN KEY (operation_id) REFERENCES C.operation_alpha(autoid) ON DELETE CASCADE ON UPDATE CASCADE;
  EXCEPTION WHEN SQLSTATE '42710' THEN RETURN;
END; $$;
DO $$
BEGIN
  ALTER TABLE C.tx_rollup_remove_commitment ADD CONSTRAINT tx_rollup_remove_commitment_source_fkey FOREIGN KEY (source_id) REFERENCES C.address(address_id) ON DELETE CASCADE ON UPDATE CASCADE;
  EXCEPTION WHEN SQLSTATE '42710' THEN RETURN;
END; $$;
DO $$
BEGIN
  ALTER TABLE C.tx_rollup_remove_commitment ADD CONSTRAINT tx_rollup_remove_commitment_tx_roll FOREIGN KEY (tx_rollup) REFERENCES C.address(address_id) ON DELETE CASCADE ON UPDATE CASCADE;
  EXCEPTION WHEN SQLSTATE '42710' THEN RETURN;
END; $$;
DO $$
BEGIN
  ALTER TABLE C.tx_rollup_return_bond ADD CONSTRAINT tx_rollup_return_bond_operation_id_fkey FOREIGN KEY (operation_id) REFERENCES C.operation_alpha(autoid) ON DELETE CASCADE ON UPDATE CASCADE;
  EXCEPTION WHEN SQLSTATE '42710' THEN RETURN;
END; $$;
DO $$
BEGIN
  ALTER TABLE C.tx_rollup_return_bond ADD CONSTRAINT tx_rollup_return_bond_source_fkey FOREIGN KEY (source_id) REFERENCES C.address(address_id) ON DELETE CASCADE ON UPDATE CASCADE;
  EXCEPTION WHEN SQLSTATE '42710' THEN RETURN;
END; $$;
DO $$
BEGIN
  ALTER TABLE C.tx_rollup_return_bond ADD CONSTRAINT tx_rollup_return_bond_tx_roll FOREIGN KEY (tx_rollup) REFERENCES C.address(address_id) ON DELETE CASCADE ON UPDATE CASCADE;
  EXCEPTION WHEN SQLSTATE '42710' THEN RETURN;
END; $$;
DO $$
BEGIN
  ALTER TABLE C.transfer_ticket ADD CONSTRAINT transfer_ticket_operation_id_fkey FOREIGN KEY (operation_id) REFERENCES C.operation_alpha(autoid) ON DELETE CASCADE ON UPDATE CASCADE;
  EXCEPTION WHEN SQLSTATE '42710' THEN RETURN;
END; $$;
DO $$
BEGIN
  ALTER TABLE C.transfer_ticket ADD CONSTRAINT transfer_ticket_source_fkey FOREIGN KEY (source_id) REFERENCES C.address(address_id) ON DELETE CASCADE ON UPDATE CASCADE;
  EXCEPTION WHEN SQLSTATE '42710' THEN RETURN;
END; $$;
DO $$
BEGIN
  ALTER TABLE C.drain_delegate ADD CONSTRAINT drain_delegate_operation_id_fkey FOREIGN KEY (operation_id) REFERENCES C.operation_alpha(autoid) ON DELETE CASCADE ON UPDATE CASCADE;
  EXCEPTION WHEN SQLSTATE '42710' THEN RETURN;
END; $$;
DO $$
BEGIN
  ALTER TABLE C.drain_delegate ADD CONSTRAINT drain_delegate_consensus_key_id_fkey FOREIGN KEY (consensus_key_id) REFERENCES C.address(address_id) ON DELETE CASCADE ON UPDATE CASCADE;
  EXCEPTION WHEN SQLSTATE '42710' THEN RETURN;
END; $$;
DO $$
BEGIN
  ALTER TABLE C.drain_delegate ADD CONSTRAINT drain_delegate_delegate_id_fkey FOREIGN KEY (delegate_id) REFERENCES C.address(address_id) ON DELETE CASCADE ON UPDATE CASCADE;
  EXCEPTION WHEN SQLSTATE '42710' THEN RETURN;
END; $$;
DO $$
BEGIN
  ALTER TABLE C.drain_delegate ADD CONSTRAINT drain_delegate_destination_id_fkey FOREIGN KEY (destination_id) REFERENCES C.address(address_id) ON DELETE CASCADE ON UPDATE CASCADE;
  EXCEPTION WHEN SQLSTATE '42710' THEN RETURN;
END; $$;
DO $$
BEGIN
  ALTER TABLE C.sc_rollup_originate ADD CONSTRAINT sc_rollup_originate_operation_id_fkey FOREIGN KEY (operation_id) REFERENCES C.operation_alpha(autoid) ON DELETE CASCADE ON UPDATE CASCADE;
  EXCEPTION WHEN SQLSTATE '42710' THEN RETURN;
END; $$;
DO $$
BEGIN
  ALTER TABLE C.sc_rollup_originate ADD CONSTRAINT sc_rollup_originate_source_fkey FOREIGN KEY (source_id) REFERENCES C.address(address_id) ON DELETE CASCADE ON UPDATE CASCADE;
  EXCEPTION WHEN SQLSTATE '42710' THEN RETURN;
END; $$;
DO $$
BEGIN
  ALTER TABLE C.sc_rollup_originate ADD CONSTRAINT sc_rollup_originate_address_fkey FOREIGN KEY (address_id) REFERENCES C.address(address_id) ON DELETE CASCADE ON UPDATE CASCADE;
  EXCEPTION WHEN SQLSTATE '42710' THEN RETURN;
END; $$;
DO $$
BEGIN
  ALTER TABLE C.sc_rollup_publish ADD CONSTRAINT sc_rollup_publish_operation_id_fkey FOREIGN KEY (operation_id) REFERENCES C.operation_alpha(autoid) ON DELETE CASCADE ON UPDATE CASCADE;
  EXCEPTION WHEN SQLSTATE '42710' THEN RETURN;
END; $$;
DO $$
BEGIN
  ALTER TABLE C.sc_rollup_publish ADD CONSTRAINT sc_rollup_publish_source_fkey FOREIGN KEY (source_id) REFERENCES C.address(address_id) ON DELETE CASCADE ON UPDATE CASCADE;
  EXCEPTION WHEN SQLSTATE '42710' THEN RETURN;
END; $$;
DO $$
BEGIN
  ALTER TABLE C.sc_rollup_publish ADD CONSTRAINT sc_rollup_publish_sc_rollup_fkey FOREIGN KEY (rollup_id) REFERENCES C.address(address_id) ON DELETE CASCADE ON UPDATE CASCADE;
  EXCEPTION WHEN SQLSTATE '42710' THEN RETURN;
END; $$;
DO $$
BEGIN
  ALTER TABLE C.sc_rollup_cement ADD CONSTRAINT sc_rollup_cement_operation_id_fkey FOREIGN KEY (operation_id) REFERENCES C.operation_alpha(autoid) ON DELETE CASCADE ON UPDATE CASCADE;
  EXCEPTION WHEN SQLSTATE '42710' THEN RETURN;
END; $$;
DO $$
BEGIN
  ALTER TABLE C.sc_rollup_cement ADD CONSTRAINT sc_rollup_cement_source_fkey FOREIGN KEY (source_id) REFERENCES C.address(address_id) ON DELETE CASCADE ON UPDATE CASCADE;
  EXCEPTION WHEN SQLSTATE '42710' THEN RETURN;
END; $$;
DO $$
BEGIN
  ALTER TABLE C.sc_rollup_cement ADD CONSTRAINT sc_rollup_cement_rollup_fkey FOREIGN KEY (rollup_id) REFERENCES C.address(address_id) ON DELETE CASCADE ON UPDATE CASCADE;
  EXCEPTION WHEN SQLSTATE '42710' THEN RETURN;
END; $$;
DO $$
BEGIN
  ALTER TABLE C.sc_rollup_timeout ADD CONSTRAINT sc_rollup_timeout_operation_id_fkey FOREIGN KEY (operation_id) REFERENCES C.operation_alpha(autoid) ON DELETE CASCADE ON UPDATE CASCADE;
  EXCEPTION WHEN SQLSTATE '42710' THEN RETURN;
END; $$;
DO $$
BEGIN
  ALTER TABLE C.sc_rollup_timeout ADD CONSTRAINT sc_rollup_timeout_source_fkey FOREIGN KEY (source_id) REFERENCES C.address(address_id) ON DELETE CASCADE ON UPDATE CASCADE;
  EXCEPTION WHEN SQLSTATE '42710' THEN RETURN;
END; $$;
DO $$
BEGIN
  ALTER TABLE C.sc_rollup_timeout ADD CONSTRAINT sc_rollup_timeout_sc_rollup_fkey FOREIGN KEY (rollup_id) REFERENCES C.address(address_id) ON DELETE CASCADE ON UPDATE CASCADE;
  EXCEPTION WHEN SQLSTATE '42710' THEN RETURN;
END; $$;
DO $$
BEGIN
  ALTER TABLE C.sc_rollup_refute ADD CONSTRAINT sc_rollup_refute_operation_id_fkey FOREIGN KEY (operation_id) REFERENCES C.operation_alpha(autoid) ON DELETE CASCADE ON UPDATE CASCADE;
  EXCEPTION WHEN SQLSTATE '42710' THEN RETURN;
END; $$;
DO $$
BEGIN
  ALTER TABLE C.sc_rollup_refute ADD CONSTRAINT sc_rollup_refute_source_fkey FOREIGN KEY (source_id) REFERENCES C.address(address_id) ON DELETE CASCADE ON UPDATE CASCADE;
  EXCEPTION WHEN SQLSTATE '42710' THEN RETURN;
END; $$;
DO $$
BEGIN
  ALTER TABLE C.sc_rollup_refute ADD CONSTRAINT sc_rollup_refute_opponent_fkey FOREIGN KEY (opponent_id) REFERENCES C.address(address_id) ON DELETE CASCADE ON UPDATE CASCADE;
  EXCEPTION WHEN SQLSTATE '42710' THEN RETURN;
END; $$;
DO $$
BEGIN
  ALTER TABLE C.sc_rollup_refute ADD CONSTRAINT sc_rollup_refute_sc_rollup_fkey FOREIGN KEY (rollup_id) REFERENCES C.address(address_id) ON DELETE CASCADE ON UPDATE CASCADE;
  EXCEPTION WHEN SQLSTATE '42710' THEN RETURN;
END; $$;
DO $$
BEGIN
  ALTER TABLE C.zk_rollup_publish ADD CONSTRAINT zk_rollup_publish_operation_id_fkey FOREIGN KEY (operation_id) REFERENCES C.operation_alpha(autoid) ON DELETE CASCADE ON UPDATE CASCADE;
  EXCEPTION WHEN SQLSTATE '42710' THEN RETURN;
END; $$;
DO $$
BEGIN
  ALTER TABLE C.zk_rollup_publish ADD CONSTRAINT zk_rollup_publish_source_fkey FOREIGN KEY (source_id) REFERENCES C.address(address_id) ON DELETE CASCADE ON UPDATE CASCADE;
  EXCEPTION WHEN SQLSTATE '42710' THEN RETURN;
END; $$;
DO $$
BEGIN
  ALTER TABLE C.zk_rollup_publish ADD CONSTRAINT zk_rollup_publish_zk_rollup_fkey FOREIGN KEY (zk_rollup_id) REFERENCES C.address(address_id) ON DELETE CASCADE ON UPDATE CASCADE;
  EXCEPTION WHEN SQLSTATE '42710' THEN RETURN;
END; $$;
DO $$
BEGIN
  ALTER TABLE C.sc_rollup_add_messages ADD CONSTRAINT sc_rollup_add_messages_operation_id_fkey FOREIGN KEY (operation_id) REFERENCES C.operation_alpha(autoid) ON DELETE CASCADE ON UPDATE CASCADE;
  EXCEPTION WHEN SQLSTATE '42710' THEN RETURN;
END; $$;
DO $$
BEGIN
  ALTER TABLE C.sc_rollup_add_messages ADD CONSTRAINT sc_rollup_add_messages_source_fkey FOREIGN KEY (source_id) REFERENCES C.address(address_id) ON DELETE CASCADE ON UPDATE CASCADE;
  EXCEPTION WHEN SQLSTATE '42710' THEN RETURN;
END; $$;
DO $$
BEGIN
  ALTER TABLE C.event ADD CONSTRAINT event_operation_id_fkey FOREIGN KEY (operation_id) REFERENCES C.operation_alpha(autoid) ON DELETE CASCADE ON UPDATE CASCADE;
  EXCEPTION WHEN SQLSTATE '42710' THEN RETURN;
END; $$;
DO $$
BEGIN
  ALTER TABLE C.event ADD CONSTRAINT event_source_fkey FOREIGN KEY (source_id) REFERENCES C.address(address_id) ON DELETE CASCADE ON UPDATE CASCADE;
  EXCEPTION WHEN SQLSTATE '42710' THEN RETURN;
END; $$;
DO $$
BEGIN
  ALTER TABLE C.increase_paid_storage ADD CONSTRAINT increase_paid_storage_operation_id_fkey FOREIGN KEY (operation_id) REFERENCES C.operation_alpha(autoid) ON DELETE CASCADE ON UPDATE CASCADE;
  EXCEPTION WHEN SQLSTATE '42710' THEN RETURN;
END; $$;
DO $$
BEGIN
  ALTER TABLE C.increase_paid_storage ADD CONSTRAINT increase_paid_storage_source_fkey FOREIGN KEY (source_id) REFERENCES C.address(address_id) ON DELETE CASCADE ON UPDATE CASCADE;
  EXCEPTION WHEN SQLSTATE '42710' THEN RETURN;
END; $$;
DO $$
BEGIN
  ALTER TABLE C.increase_paid_storage ADD CONSTRAINT increase_paid_storage_destination_fkey FOREIGN KEY (destination_id) REFERENCES C.address(address_id) ON DELETE CASCADE ON UPDATE CASCADE;
  EXCEPTION WHEN SQLSTATE '42710' THEN RETURN;
END; $$;
DO $$
BEGIN
  ALTER TABLE C.dal_publish_slot_header ADD CONSTRAINT dal_publish_slot_header_operation_id_fkey FOREIGN KEY (operation_id) REFERENCES C.operation_alpha(autoid) ON DELETE CASCADE ON UPDATE CASCADE;
  EXCEPTION WHEN SQLSTATE '42710' THEN RETURN;
END; $$;
DO $$
BEGIN
  ALTER TABLE C.dal_publish_slot_header ADD CONSTRAINT dal_publish_slot_header_source_fkey FOREIGN KEY (source_id) REFERENCES C.address(address_id) ON DELETE CASCADE ON UPDATE CASCADE;
  EXCEPTION WHEN SQLSTATE '42710' THEN RETURN;
END; $$;
DO $$
BEGIN
  ALTER TABLE C.dal_attestation ADD CONSTRAINT dal_attestation_operation_id_fkey FOREIGN KEY (operation_id) REFERENCES C.operation_alpha(autoid) ON DELETE CASCADE ON UPDATE CASCADE;
  EXCEPTION WHEN SQLSTATE '42710' THEN RETURN;
END; $$;
DO $$
BEGIN
  ALTER TABLE C.dal_attestation ADD CONSTRAINT dal_attestation_attestor_fkey FOREIGN KEY (attestor_id) REFERENCES C.address(address_id) ON DELETE CASCADE ON UPDATE CASCADE;
  EXCEPTION WHEN SQLSTATE '42710' THEN RETURN;
END; $$;
DO $$
BEGIN
  ALTER TABLE C.dal_attestation ADD CONSTRAINT dal_attestation_delegate_fkey FOREIGN KEY (delegate_id) REFERENCES C.address(address_id) ON DELETE CASCADE ON UPDATE CASCADE;
  EXCEPTION WHEN SQLSTATE '42710' THEN RETURN;
END; $$;
DO $$
BEGIN
  ALTER TABLE C.sc_rollup_execute_outbox_message ADD CONSTRAINT sc_rollup_execute_outbox_message_operation_id_fkey FOREIGN KEY (operation_id) REFERENCES C.operation_alpha(autoid) ON DELETE CASCADE ON UPDATE CASCADE;
  EXCEPTION WHEN SQLSTATE '42710' THEN RETURN;
END; $$;
DO $$
BEGIN
  ALTER TABLE C.sc_rollup_execute_outbox_message ADD CONSTRAINT sc_rollup_execute_outbox_message_source_fkey FOREIGN KEY (source_id) REFERENCES C.address(address_id) ON DELETE CASCADE ON UPDATE CASCADE;
  EXCEPTION WHEN SQLSTATE '42710' THEN RETURN;
END; $$;
DO $$
BEGIN
  ALTER TABLE C.sc_rollup_execute_outbox_message ADD CONSTRAINT sc_rollup_execute_outbox_message_rollup_fkey FOREIGN KEY (rollup_id) REFERENCES C.address(address_id) ON DELETE CASCADE ON UPDATE CASCADE;
  EXCEPTION WHEN SQLSTATE '42710' THEN RETURN;
END; $$;
DO $$
BEGIN
  ALTER TABLE C.update_consensus_key ADD CONSTRAINT update_consensus_key_operation_id_fkey FOREIGN KEY (operation_id) REFERENCES C.operation_alpha(autoid) ON DELETE CASCADE ON UPDATE CASCADE;
  EXCEPTION WHEN SQLSTATE '42710' THEN RETURN;
END; $$;
DO $$
BEGIN
  ALTER TABLE C.update_consensus_key ADD CONSTRAINT update_consensus_key_source_fkey FOREIGN KEY (source_id) REFERENCES C.address(address_id) ON DELETE CASCADE ON UPDATE CASCADE;
  EXCEPTION WHEN SQLSTATE '42710' THEN RETURN;
END; $$;
DO $$
BEGIN
  ALTER TABLE C.sc_rollup_recover_bond ADD CONSTRAINT sc_rollup_recover_bond_operation_id_fkey FOREIGN KEY (operation_id) REFERENCES C.operation_alpha(autoid) ON DELETE CASCADE ON UPDATE CASCADE;
  EXCEPTION WHEN SQLSTATE '42710' THEN RETURN;
END; $$;
DO $$
BEGIN
  ALTER TABLE C.sc_rollup_recover_bond ADD CONSTRAINT sc_rollup_recover_bond_source_fkey FOREIGN KEY (source_id) REFERENCES C.address(address_id) ON DELETE CASCADE ON UPDATE CASCADE;
  EXCEPTION WHEN SQLSTATE '42710' THEN RETURN;
END; $$;
DO $$
BEGIN
  ALTER TABLE C.sc_rollup_recover_bond ADD CONSTRAINT sc_rollup_recover_bond_rollup_fkey FOREIGN KEY (rollup_id) REFERENCES C.address(address_id) ON DELETE CASCADE ON UPDATE CASCADE;
  EXCEPTION WHEN SQLSTATE '42710' THEN RETURN;
END; $$;
DO $$
BEGIN
  ALTER TABLE C.vdf_revelation ADD CONSTRAINT vdf_revelation_operation_id_fkey FOREIGN KEY (operation_id) REFERENCES C.operation_alpha(autoid) ON DELETE CASCADE ON UPDATE CASCADE;
  EXCEPTION WHEN SQLSTATE '42710' THEN RETURN;
END; $$;
DO $$
BEGIN
  ALTER TABLE C.balance_updates ADD CONSTRAINT balance_updates_block_hash_fkey FOREIGN KEY (block_level) REFERENCES C.block(level) ON DELETE CASCADE ON UPDATE CASCADE;
  EXCEPTION WHEN SQLSTATE '42710' THEN RETURN;
END; $$;
DO $$
BEGIN
  ALTER TABLE C.balance_updates ADD CONSTRAINT balance_updates_contract_address_fkey FOREIGN KEY (contract_address_id) REFERENCES C.address(address_id) ON DELETE CASCADE ON UPDATE CASCADE;
  EXCEPTION WHEN SQLSTATE '42710' THEN RETURN;
END; $$;
DO $$
BEGIN
  ALTER TABLE C.balance_updates ADD CONSTRAINT balance_updates_iorid_fkey FOREIGN KEY (block_level, implicit_operations_results_id) REFERENCES C.implicit_operations_results(block_level, id) ON DELETE CASCADE ON UPDATE CASCADE;
  EXCEPTION WHEN SQLSTATE '42710' THEN RETURN;
END; $$;
DO $$
BEGIN
  ALTER TABLE C.delegated_contract ADD CONSTRAINT delegated_contract_delegate_fkey FOREIGN KEY (delegate_id) REFERENCES C.address(address_id) ON DELETE CASCADE ON UPDATE CASCADE;
  EXCEPTION WHEN SQLSTATE '42710' THEN RETURN;
END; $$;
DO $$
BEGIN
  ALTER TABLE C.delegated_contract ADD CONSTRAINT delegated_contract_delegator_fkey FOREIGN KEY (delegator_id) REFERENCES C.address(address_id) ON DELETE CASCADE ON UPDATE CASCADE;
  EXCEPTION WHEN SQLSTATE '42710' THEN RETURN;
END; $$;
DO $$
BEGIN
  ALTER TABLE C.delegated_contract ADD CONSTRAINT delegated_contract_cycle_level_fkey FOREIGN KEY (cycle, level) REFERENCES C.snapshot(cycle, level) ON DELETE CASCADE ON UPDATE CASCADE;
  EXCEPTION WHEN SQLSTATE '42710' THEN RETURN;
END; $$;
DO $$
BEGIN
  ALTER TABLE C.bigmap ADD CONSTRAINT bigmap_block_hash_block_fkey FOREIGN KEY (block_level) REFERENCES C.block(level) ON DELETE CASCADE ON UPDATE CASCADE;
  EXCEPTION WHEN SQLSTATE '42710' THEN RETURN;
END; $$;
DO $$
BEGIN
  ALTER TABLE C.bigmap ADD CONSTRAINT bigmap_operation_id_fkey FOREIGN KEY (operation_id) REFERENCES C.operation_alpha(autoid) ON DELETE CASCADE ON UPDATE CASCADE;
  EXCEPTION WHEN SQLSTATE '42710' THEN RETURN;
END; $$;
DO $$
BEGIN
  ALTER TABLE C.bigmap ADD CONSTRAINT bigmap_sender_fkey FOREIGN KEY (sender_id) REFERENCES C.address(address_id) ON DELETE CASCADE ON UPDATE CASCADE;
  EXCEPTION WHEN SQLSTATE '42710' THEN RETURN;
END; $$;
DO $$
BEGIN
  ALTER TABLE C.bigmap ADD CONSTRAINT bigmap_receiver_fkey FOREIGN KEY (receiver_id) REFERENCES C.address(address_id) ON DELETE CASCADE ON UPDATE CASCADE;
  EXCEPTION WHEN SQLSTATE '42710' THEN RETURN;
END; $$;
DO $$
BEGIN
  ALTER TABLE T.contract ADD CONSTRAINT t_contract_block_level FOREIGN KEY (block_level) REFERENCES C.block(level) ON DELETE CASCADE ON UPDATE CASCADE;
  EXCEPTION WHEN SQLSTATE '42710' THEN RETURN;
END; $$;
DO $$
BEGIN
  ALTER TABLE T.fa12_operation ADD CONSTRAINT t_fa12_operation_token_address_id_fkey FOREIGN KEY (token_address_id) REFERENCES T.contract(address_id) ON DELETE CASCADE ON UPDATE CASCADE;
  EXCEPTION WHEN SQLSTATE '42710' THEN RETURN;
END; $$;
DO $$
BEGIN
  ALTER TABLE T.fa12_operation ADD CONSTRAINT t_fa12_operation_operation_id_fkey FOREIGN KEY (operation_id) REFERENCES C.operation_alpha(autoid) ON DELETE CASCADE ON UPDATE CASCADE;
  EXCEPTION WHEN SQLSTATE '42710' THEN RETURN;
END; $$;
DO $$
BEGIN
  ALTER TABLE T.fa12_operation ADD CONSTRAINT t_fa12_operation_caller_id_fkey FOREIGN KEY (caller_id) REFERENCES C.address(address_id) ON DELETE CASCADE ON UPDATE CASCADE;
  EXCEPTION WHEN SQLSTATE '42710' THEN RETURN;
END; $$;
DO $$
BEGIN
  ALTER TABLE T.fa12_transfer ADD CONSTRAINT t_fa12_transfer_operation_id_fkey FOREIGN KEY (operation_id) REFERENCES T.fa12_operation(operation_id) ON DELETE CASCADE ON UPDATE CASCADE;
  EXCEPTION WHEN SQLSTATE '42710' THEN RETURN;
END; $$;
DO $$
BEGIN
  ALTER TABLE T.fa12_transfer ADD CONSTRAINT t_fa12_transfer_source_id_fkey FOREIGN KEY (source_id) REFERENCES C.address(address_id) ON DELETE CASCADE ON UPDATE CASCADE;
  EXCEPTION WHEN SQLSTATE '42710' THEN RETURN;
END; $$;
DO $$
BEGIN
  ALTER TABLE T.fa12_transfer ADD CONSTRAINT t_fa12_transfer_destination_id_fkey FOREIGN KEY (destination_id) REFERENCES C.address(address_id) ON DELETE CASCADE ON UPDATE CASCADE;
  EXCEPTION WHEN SQLSTATE '42710' THEN RETURN;
END; $$;
DO $$
BEGIN
  ALTER TABLE T.fa12_approve ADD CONSTRAINT t_fa12_approve_operation_id_fkey FOREIGN KEY (operation_id) REFERENCES T.fa12_operation(operation_id) ON DELETE CASCADE ON UPDATE CASCADE;
  EXCEPTION WHEN SQLSTATE '42710' THEN RETURN;
END; $$;
DO $$
BEGIN
  ALTER TABLE T.fa12_approve ADD CONSTRAINT t_fa12_approve_address_id_fkey FOREIGN KEY (address_id) REFERENCES C.address(address_id) ON DELETE CASCADE ON UPDATE CASCADE;
  EXCEPTION WHEN SQLSTATE '42710' THEN RETURN;
END; $$;
DO $$
BEGIN
  ALTER TABLE T.fa12_get_balance ADD CONSTRAINT t_fa12_get_balance_operation_id_fkey FOREIGN KEY (operation_id) REFERENCES T.fa12_operation(operation_id) ON DELETE CASCADE ON UPDATE CASCADE;
  EXCEPTION WHEN SQLSTATE '42710' THEN RETURN;
END; $$;
DO $$
BEGIN
  ALTER TABLE T.fa12_get_balance ADD CONSTRAINT t_fa12_get_balance_address_id_fkey FOREIGN KEY (address_id) REFERENCES C.address(address_id) ON DELETE CASCADE ON UPDATE CASCADE;
  EXCEPTION WHEN SQLSTATE '42710' THEN RETURN;
END; $$;
DO $$
BEGIN
  ALTER TABLE T.fa12_get_balance ADD CONSTRAINT t_fa12_get_balance_callback_id_fkey FOREIGN KEY (callback_id) REFERENCES C.address(address_id) ON DELETE CASCADE ON UPDATE CASCADE;
  EXCEPTION WHEN SQLSTATE '42710' THEN RETURN;
END; $$;
DO $$
BEGIN
  ALTER TABLE T.fa12_get_allowance ADD CONSTRAINT t_fa12_get_allowance_operation_id_fkey FOREIGN KEY (operation_id) REFERENCES T.fa12_operation(operation_id) ON DELETE CASCADE ON UPDATE CASCADE;
  EXCEPTION WHEN SQLSTATE '42710' THEN RETURN;
END; $$;
DO $$
BEGIN
  ALTER TABLE T.fa12_get_allowance ADD CONSTRAINT t_fa12_get_allowance_source_id_fkey FOREIGN KEY (source_id) REFERENCES C.address(address_id) ON DELETE CASCADE ON UPDATE CASCADE;
  EXCEPTION WHEN SQLSTATE '42710' THEN RETURN;
END; $$;
DO $$
BEGIN
  ALTER TABLE T.fa12_get_allowance ADD CONSTRAINT t_fa12_get_allowance_destination_id_fkey FOREIGN KEY (destination_id) REFERENCES C.address(address_id) ON DELETE CASCADE ON UPDATE CASCADE;
  EXCEPTION WHEN SQLSTATE '42710' THEN RETURN;
END; $$;
DO $$
BEGIN
  ALTER TABLE T.fa12_get_allowance ADD CONSTRAINT t_fa12_get_allowance_callback_id_fkey FOREIGN KEY (callback_id) REFERENCES C.address(address_id) ON DELETE CASCADE ON UPDATE CASCADE;
  EXCEPTION WHEN SQLSTATE '42710' THEN RETURN;
END; $$;
DO $$
BEGIN
  ALTER TABLE T.fa12_get_total_supply ADD CONSTRAINT t_fa12_get_total_supply_operation_id_fkey FOREIGN KEY (operation_id) REFERENCES T.fa12_operation(operation_id) ON DELETE CASCADE ON UPDATE CASCADE;
  EXCEPTION WHEN SQLSTATE '42710' THEN RETURN;
END; $$;
DO $$
BEGIN
  ALTER TABLE T.fa12_get_total_supply ADD CONSTRAINT t_fa12_get_total_supply_callback_id_fkey FOREIGN KEY (callback_id) REFERENCES C.address(address_id) ON DELETE CASCADE ON UPDATE CASCADE;
  EXCEPTION WHEN SQLSTATE '42710' THEN RETURN;
END; $$;
DO $$
BEGIN
  ALTER TABLE T.fa2_operation ADD CONSTRAINT t_fa2_operation_token_address_id_fkey FOREIGN KEY (token_address_id) REFERENCES T.contract(address_id) ON DELETE CASCADE ON UPDATE CASCADE;
  EXCEPTION WHEN SQLSTATE '42710' THEN RETURN;
END; $$;
DO $$
BEGIN
  ALTER TABLE T.fa2_operation ADD CONSTRAINT t_fa2_operation_operation_id_fkey FOREIGN KEY (operation_id) REFERENCES C.operation_alpha(autoid) ON DELETE CASCADE ON UPDATE CASCADE;
  EXCEPTION WHEN SQLSTATE '42710' THEN RETURN;
END; $$;
DO $$
BEGIN
  ALTER TABLE T.fa2_operation ADD CONSTRAINT t_fa2_operation_caller_id_fkey FOREIGN KEY (caller_id) REFERENCES C.address(address_id) ON DELETE CASCADE ON UPDATE CASCADE;
  EXCEPTION WHEN SQLSTATE '42710' THEN RETURN;
END; $$;
DO $$
BEGIN
  ALTER TABLE T.fa2_transfer ADD CONSTRAINT t_fa2_transfer_operation_id_fkey FOREIGN KEY (operation_id) REFERENCES T.fa2_operation(operation_id) ON DELETE CASCADE ON UPDATE CASCADE;
  EXCEPTION WHEN SQLSTATE '42710' THEN RETURN;
END; $$;
DO $$
BEGIN
  ALTER TABLE T.fa2_transfer ADD CONSTRAINT t_fa2_transfer_source_id_fkey FOREIGN KEY (source_id) REFERENCES C.address(address_id) ON DELETE CASCADE ON UPDATE CASCADE;
  EXCEPTION WHEN SQLSTATE '42710' THEN RETURN;
END; $$;
DO $$
BEGIN
  ALTER TABLE T.fa2_transfer ADD CONSTRAINT t_fa2_transfer_destination_id_fkey FOREIGN KEY (destination_id) REFERENCES C.address(address_id) ON DELETE CASCADE ON UPDATE CASCADE;
  EXCEPTION WHEN SQLSTATE '42710' THEN RETURN;
END; $$;
DO $$
BEGIN
  ALTER TABLE T.fa2_update_operators ADD CONSTRAINT t_fa2_update_operators_operation_id_fkey FOREIGN KEY (operation_id) REFERENCES T.fa2_operation(operation_id) ON DELETE CASCADE ON UPDATE CASCADE;
  EXCEPTION WHEN SQLSTATE '42710' THEN RETURN;
END; $$;
DO $$
BEGIN
  ALTER TABLE T.fa2_update_operators ADD CONSTRAINT t_fa2_update_operators_owner_id_fkey FOREIGN KEY (owner_id) REFERENCES C.address(address_id) ON DELETE CASCADE ON UPDATE CASCADE;
  EXCEPTION WHEN SQLSTATE '42710' THEN RETURN;
END; $$;
DO $$
BEGIN
  ALTER TABLE T.fa2_update_operators ADD CONSTRAINT t_fa2_update_operators_operator_id_fkey FOREIGN KEY (operator_id) REFERENCES C.address(address_id) ON DELETE CASCADE ON UPDATE CASCADE;
  EXCEPTION WHEN SQLSTATE '42710' THEN RETURN;
END; $$;
DO $$
BEGIN
  ALTER TABLE T.fa2_balance_of ADD CONSTRAINT t_fa2_balance_of_operation_id_fkey FOREIGN KEY (operation_id) REFERENCES T.fa2_operation(operation_id) ON DELETE CASCADE ON UPDATE CASCADE;
  EXCEPTION WHEN SQLSTATE '42710' THEN RETURN;
END; $$;
DO $$
BEGIN
  ALTER TABLE T.fa2_balance_of ADD CONSTRAINT t_fa2_balance_of_owner_id_fkey FOREIGN KEY (owner_id) REFERENCES C.address(address_id) ON DELETE CASCADE ON UPDATE CASCADE;
  EXCEPTION WHEN SQLSTATE '42710' THEN RETURN;
END; $$;
DO $$
BEGIN
  ALTER TABLE T.fa2_balance_of ADD CONSTRAINT t_fa2_balance_of_callback_id_fkey FOREIGN KEY (callback_id) REFERENCES C.address(address_id) ON DELETE CASCADE ON UPDATE CASCADE;
  EXCEPTION WHEN SQLSTATE '42710' THEN RETURN;
END; $$;
DO $$
BEGIN
  ALTER TABLE T.accounts_registry ADD CONSTRAINT t_accounts_registry_block_fkey FOREIGN KEY (block_level) REFERENCES C.block(level) ON DELETE CASCADE ON UPDATE CASCADE;
  EXCEPTION WHEN SQLSTATE '42710' THEN RETURN;
END; $$;
DO $$
BEGIN
  ALTER TABLE T.accounts_registry ADD CONSTRAINT t_accounts_registry_token_address_id_fkey FOREIGN KEY (token_address_id) REFERENCES C.address(address_id) ON DELETE CASCADE ON UPDATE CASCADE;
  EXCEPTION WHEN SQLSTATE '42710' THEN RETURN;
END; $$;
DO $$
BEGIN
  ALTER TABLE T.accounts_registry ADD CONSTRAINT t_accounts_registry_address_id_fkey FOREIGN KEY (address_id) REFERENCES C.address(address_id) ON DELETE CASCADE ON UPDATE CASCADE;
  EXCEPTION WHEN SQLSTATE '42710' THEN RETURN;
END; $$;
-- src/db-schema/confirm_conversion_from_multicore.sql
-- Open Source License
-- Copyright (c) 2019-2022 Nomadic Labs <contact@nomadic-labs.com>
-- Copyright (c) 2021 Philippe Wang <philippe.wang@gmail.com>
--
-- Permission is hereby granted, free of charge, to any person obtaining a
-- copy of this software and associated documentation files (the "Software"),
-- to deal in the Software without restriction, including without limitation
-- the rights to use, copy, modify, merge, publish, distribute, sublicense,
-- and/or sell copies of the Software, and to permit persons to whom the
-- Software is furnished to do so, subject to the following conditions:
--
-- The above copyright notice and this permission notice shall be included
-- in all copies or substantial portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
-- IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
-- FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
-- THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
-- LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
-- FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
-- DEALINGS IN THE SOFTWARE.

-- This file contains things that must be executed at the very end of the conversion from multicore mode to sequential mode


SELECT update_indexer_version ();
UPDATE indexer_version SET conversion_in_progress = false;
-- the following line should be at the end of this file
UPDATE indexer_version SET MULTICORE = false;
-- src/db-schema/mempool.sql
-- Open Source License
-- Copyright (c) 2020-2022 Nomadic Labs <contact@nomadic-labs.com>
--
-- Permission is hereby granted, free of charge, to any person obtaining a
-- copy of this software and associated documentation files (the "Software"),
-- to deal in the Software without restriction, including without limitation
-- the rights to use, copy, modify, merge, publish, distribute, sublicense,
-- and/or sell copies of the Software, and to permit persons to whom the
-- Software is furnished to do so, subject to the following conditions:
--
-- The above copyright notice and this permission notice shall be included
-- in all copies or substantial portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
-- IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
-- FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
-- THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
-- LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
-- FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
-- DEALINGS IN THE SOFTWARE.

-- Lines starting with --OPT may be automatically activated
-- Lines ending with --OPT may be automatically deactivated

-- DB schema for operations in the mempool, so you may track the life of an operation

SELECT 'mempool.sql' as file;

DO $$
BEGIN
  IF NOT EXISTS (SELECT 1 FROM pg_type WHERE typname = 'mempool_op_status') THEN
    CREATE TYPE mempool_op_status AS ENUM ('applied', 'refused', 'branch_refused', 'unprocessed', 'branch_delayed');
  END IF;
END
$$;

CREATE TABLE IF NOT EXISTS M.operation_alpha (
  hash char(51) not null,
  first_seen_level int not null,
  first_seen_timestamp double precision not null,
  last_seen_level int not null,
  last_seen_timestamp double precision not null,
  status mempool_op_status not null,
  id smallint not null,
  -- index of op in contents_list
  operation_kind smallint not null,
  -- ... cf. chain.sql
  source char(36),
  -- sender
  destination char(36),
  -- receiver, if any
  operation_alpha jsonb,
  branch char(51),
  autoid SERIAL, -- this field should always be last
  PRIMARY KEY(hash, id, status)
);
CREATE INDEX IF NOT EXISTS mempool_operations_hash on M.operation_alpha(hash); --1
CREATE INDEX IF NOT EXISTS mempool_operations_status on M.operation_alpha(status); --OPT --1
CREATE INDEX IF NOT EXISTS mempool_operations_source on M.operation_alpha(source); --1
CREATE INDEX IF NOT EXISTS mempool_operations_destination on M.operation_alpha(destination); --1
CREATE INDEX IF NOT EXISTS mempool_operations_kind on M.operation_alpha(operation_kind); --1
CREATE INDEX IF NOT EXISTS mempool_operations_branch on M.operation_alpha(branch);  --1
CREATE INDEX IF NOT EXISTS mempool_operations_autoid on M.operation_alpha(autoid);  --1

CREATE TABLE IF NOT EXISTS M.operation_kinds (
  kind smallint PRIMARY KEY
);

CREATE OR REPLACE FUNCTION M.I_operation_alpha (
  xbranch char(51),
  xlevel int,
  xhash char(51),
  xstatus mempool_op_status,
  xid smallint,
  xoperation_kind smallint,
  xsource char(36),
  xdestination char(36),
  xseen_timestamp double precision,
  xoperation_alpha jsonb
)
RETURNS void
AS $$
  INSERT INTO M.operation_alpha (
      hash
    , first_seen_level
    , first_seen_timestamp
    , last_seen_level
    , last_seen_timestamp
    , status
    , id
    , operation_kind
    , source
    , destination
    , operation_alpha
    , branch
  ) SELECT
      xhash
    , xlevel
    , xseen_timestamp
    , xlevel
    , xseen_timestamp
    , xstatus
    , xid
    , xoperation_kind
    , xsource
    , xdestination
    , xoperation_alpha
    , xbranch
  WHERE (SELECT * FROM M.operation_kinds) IS NULL
     OR xoperation_kind IN (SELECT kind FROM M.operation_kinds)
  ON CONFLICT (hash, id, status)
  DO UPDATE SET
    last_seen_level = xlevel
  , last_seen_timestamp = xseen_timestamp
  WHERE M.operation_alpha.hash = xhash
    AND M.operation_alpha.id = xid
    AND M.operation_alpha.status = xstatus
$$ LANGUAGE SQL;

CREATE OR REPLACE FUNCTION M.purge (distance int)
RETURNS VOID
AS $$
DECLARE x int := (SELECT level - distance FROM C.block ORDER BY level desc limit 1);
BEGIN
DELETE FROM M.operation_alpha WHERE last_seen_level < x;
END $$ LANGUAGE PLPGSQL;
-- src/db-schema/mezos.sql
-- Open Source License
-- Copyright (c) 2018-2022 Nomadic Labs <contact@nomadic-labs.com>
--
-- Permission is hereby granted, free of charge, to any person obtaining a
-- copy of this software and associated documentation files (the "Software"),
-- to deal in the Software without restriction, including without limitation
-- the rights to use, copy, modify, merge, publish, distribute, sublicense,
-- and/or sell copies of the Software, and to permit persons to whom the
-- Software is furnished to do so, subject to the following conditions:
--
-- The above copyright notice and this permission notice shall be included
-- in all copies or substantial portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
-- IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
-- FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
-- THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
-- LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
-- FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
-- DEALINGS IN THE SOFTWARE.

-- Lines starting with --OPT may be automatically activated
-- Lines ending with --OPT may be automatically deactivated

SELECT 'mezos.sql' as file;


CREATE OR REPLACE FUNCTION identical_contracts (model_contract varchar)
RETURNS TABLE(address varchar)
AS $$
SELECT
  address(c.address_id)
FROM C.contract_script c
WHERE script->'code' = (select cs.script->'code' from C.contract_script cs where cs.address_id = address_id (model_contract))
$$ LANGUAGE SQL STABLE;


DROP FUNCTION IF EXISTS multisig_base;
CREATE OR REPLACE FUNCTION multisig_base (model_contract varchar)
RETURNS TABLE(address_id bigint, storage jsonb, strings text[])
AS $$
SELECT
  c.address_id
  , (select
      coalesce ((select storage from c.tx t where t.destination_id = cs.address_id order by t.operation_id desc limit 1)
                , script->'storage')
     from c.contract_script cs
     where cs.address_id = c.address_id
     limit 1) as storage
  , (select strings from c.tx t where t.destination_id = c.address_id order by t.operation_id desc limit 1)
FROM C.contract_script c
WHERE script->'code' = (select cs.script->'code' from C.contract_script cs where cs.address_id = address_id (model_contract))
$$ LANGUAGE SQL STABLE;


CREATE OR REPLACE FUNCTION multisig (model_contract varchar, pkh varchar)
RETURNS TABLE(contracts varchar)
AS $$
WITH r AS (select * from multisig_base(model_contract))
SELECT DISTINCT(address(r.address_id))
FROM r
WHERE r.storage::text LIKE (concat('%"', pkh, '"%'))
   OR pkh = ANY(r.strings)
$$ LANGUAGE SQL STABLE;


CREATE OR REPLACE FUNCTION contracts2 (x varchar)
RETURNS TABLE(k char, bal bigint, operation_hash char)
AS $$
SELECT x, coalesce((SELECT balance FROM C.contract_balance WHERE address_id = address_id(x) order by block_level desc limit 1), 0), null
UNION ALL
SELECT
  address(k_id)
, coalesce((SELECT balance FROM C.contract_balance WHERE address_id = k_id order by block_level desc limit 1), 0)
, operation_hash_alpha(operation_id)
FROM C.origination o
WHERE o.source_id = address_id(x)
$$ LANGUAGE SQL stable;


CREATE OR REPLACE FUNCTION latest_balance_by_id (x bigint)
RETURNS TABLE(bal bigint)
AS $$
select coalesce((
SELECT cb.balance
FROM C.contract_balance cb
WHERE cb.address_id = x
order by cb.block_level desc limit 1
), 0) as bal
$$ LANGUAGE SQL stable;


CREATE OR REPLACE FUNCTION latest_balance (x varchar)
RETURNS TABLE(bal bigint)
AS $$
select latest_balance_by_id(address_id(x)) as bal;
$$ LANGUAGE SQL stable;


-- DROP FUNCTION contracts3;

CREATE OR REPLACE FUNCTION contracts3 (x varchar)
RETURNS TABLE(k char, bal bigint, operation_hash char, delegate char, storage jsonb)
AS $$
SELECT x, (select latest_balance (x)), null, null, null
UNION ALL
SELECT
  address(k_id),
  (select latest_balance_by_id(k_id)),
  operation_hash_alpha(operation_id),
  address(delegate_id),
  (select cs.script->'storage' as storage from C.contract_script cs where cs.address_id = k_id)
FROM C.origination o
WHERE o.source_id = address_id(x)
$$ LANGUAGE SQL stable;


CREATE OR REPLACE FUNCTION manager (x varchar)
RETURNS TABLE(pkh char)
AS $$
select coalesce(
(select address(tx.source_id)
from C.tx tx
where tx.destination_id = address_id(x)
limit 1),
(select address(o.source_id)
from C.origination o
where o.k_id = address_id(x))
)
$$ LANGUAGE SQL stable;



CREATE OR REPLACE FUNCTION get_reveal (address varchar)
RETURNS TABLE(
type text,
id bigint,
level int,
"timestamp" timestamp,
block char,
hash char,
source char,
fee bigint,
counter numeric,
gas_limit numeric,
storage_limit numeric,
op_id smallint,
internal smallint,
nonce int,
public_key char,
amount bigint,
destination char,
"parameters" jsonb,
entrypoint char,
contract_address char,
delegate char,
status char,
error_trace jsonb
)
AS $$
select
       'reveal', -- type
       oa.autoid as id, -- id
       b.level, -- level
       b.timestamp, -- timestamp
       b.hash,  -- block
       op.hash, -- hash
       address, -- source
       r.fee, -- fees
       m.counter,
       m.gas_limit,
       m.storage_limit,
       oa.id,
       oa.internal, -- internal
       r.nonce, -- nonce
       r.pk, -- public_key (reveal)
       cast(null as bigint), -- amount (tx)
       null, -- destination (tx)
       null::jsonb, -- parameters (tx)
       null, -- entrypoint (tx)
       null, -- contract_address (origination)
       null  -- delegate (delegation)
       , status(status)
       , error_trace
from
   C.operation_alpha oa,
   C.block b,
   C.manager_numbers m,
   C.reveal r,
   C.operation op
where
    r.source_id = address_id(address)
AND r.operation_id = oa.autoid
AND op.hash_id = oa.hash_id
AND b.level = op.block_level
AND m.operation_id = oa.autoid
$$ LANGUAGE SQL stable;
-- select * from get_reveal('tz1NKR6nBuLPxSGnFBBTXWLtD2Dt5UAYPWXo') limit 10;
-- select * from get_reveal('tz1LbSsDSmekew3prdDGx1nS22ie6jjBN6B3') limit 10;


CREATE OR REPLACE FUNCTION get_transaction (address varchar)
RETURNS TABLE(
type text,
id bigint,
level int,
"timestamp" timestamp,
block char,
hash char,
source char,
fee bigint,
counter numeric,
gas_limit numeric,
storage_limit numeric,
op_id smallint,
internal smallint,
nonce int,
public_key char,
amount bigint,
destination char,
"parameters" jsonb,
entrypoint char,
contract_address char,
delegate char,
status char,
error_trace jsonb
)
AS $$
select 'transaction', oa.autoid, b.level, b.timestamp, b.hash, op.hash, address(t.source_id), t.fee,
       m.counter, m.gas_limit, m.storage_limit, oa.id,
       oa.internal,
       t.nonce,
       null,  -- address (reveal)
       t.amount, -- amount (tx)
       address(t.destination_id), -- destination (tx)
       t."parameters", -- parameters (tx)
       t.entrypoint, -- entrypoint (tx)
       null, -- contract_address (origination)
       null -- delegate (delegation)
       , status(status)
       , error_trace
from C.operation_alpha oa, C.tx t, C.manager_numbers m
, C.operation op
, C.block b
where
    (address_id(address) = t.destination_id or address_id(address) = t.source_id)
AND oa.autoid = t.operation_id
AND op.hash_id = oa.hash_id
AND m.operation_id = oa.autoid
AND b.level = op.block_level
$$ LANGUAGE SQL stable;
-- select * from get_transaction('tz1NKR6nBuLPxSGnFBBTXWLtD2Dt5UAYPWXo') limit 10;
-- select * from get_transaction('tz1LbSsDSmekew3prdDGx1nS22ie6jjBN6B3') limit 10;


CREATE OR REPLACE FUNCTION get_origination (address varchar)
RETURNS TABLE(
type text,
id bigint,
level int,
"timestamp" timestamp,
block char,
hash char,
source char,
fee bigint,
counter numeric,
gas_limit numeric,
storage_limit numeric,
op_id smallint,
internal smallint,
nonce int,
public_key char,
amount bigint,
destination char,
"parameters" jsonb,
entrypoint char,
contract_address char,
delegate char,
status char,
error_trace jsonb
)
AS $$
select
  'origination'
, oa.autoid
, b.level
, b.timestamp
, b.hash
, op.hash
, address(o.source_id)
, o.fee
, m.counter
, m.gas_limit
, m.storage_limit
, oa.id
, oa.internal
, o.nonce
, null
, cast(null as bigint) -- amount (tx)
, null -- destination (tx)
, null::jsonb -- parameters (tx)
, null -- entrypoint (tx)
, address(o.k_id) -- contract_address (origination)
, null -- delegate (delegation)
, status(status)
, error_trace
from C.operation_alpha oa, C.manager_numbers m, C.origination o, C.block b, C.operation op
where
    (address_id(address) = o.source_id or address_id(address) = o.k_id)
and o.operation_id = oa.autoid
AND oa.hash_id = op.hash_id
AND m.operation_id = oa.autoid
and op.block_level = b.level
$$ LANGUAGE SQL stable;
-- select * from get_origination('tz1NKR6nBuLPxSGnFBBTXWLtD2Dt5UAYPWXo') limit 10;


CREATE OR REPLACE FUNCTION get_delegation (address varchar)
RETURNS TABLE(
type text,
id bigint,
level int,
"timestamp" timestamp,
block char,
hash char,
source char,
fee bigint,
counter numeric,
gas_limit numeric,
storage_limit numeric,
op_id smallint,
internal smallint,
nonce int,
public_key char,
amount bigint,
destination char,
"parameters" jsonb,
entrypoint char,
contract_address char,
delegate char,
status char,
error_trace jsonb
)
AS $$
select
  'delegation'
, oa.autoid
, b.level
, b.timestamp
, b.hash
, op.hash
, address(d.source_id)
, d.fee
, m.counter
, m.gas_limit
, m.storage_limit
, oa.id
, oa.internal
, d.nonce
, null -- public_key (revelation)
, cast(null as bigint) --amount (tx)
, null -- destination (tx)
, null::jsonb -- parameters (tx)
, null -- entrypoint (tx)
, null -- contract_address (origination)
, address(pkh_id) -- delegate (delegation)
, status(status)
, error_trace
from C.operation_alpha oa, C.manager_numbers m, C.delegation d, C.block b, C.operation op
where
   (address_id(address) = d.pkh_id or address_id(address) = d.source_id)
AND d.operation_id = oa.autoid
AND m.operation_id = oa.autoid
AND oa.hash_id = op.hash_id
AND op.block_level = b.level
$$ LANGUAGE SQL stable;
-- select * from get_delegation('tz1hGaDz45yCG1AbZqwS653KFDcvmv6jUVqW') limit 10;;
-- select * from get_delegation('tz1LbSsDSmekew3prdDGx1nS22ie6jjBN6B3') limit 10;


CREATE OR REPLACE FUNCTION get_operations (address varchar, lastid bigint, lim integer)
RETURNS TABLE(
type text,
id bigint,
level int,
"timestamp" timestamp,
block char,
hash char,
source char,
fee bigint,
counter numeric,
gas_limit numeric,
storage_limit numeric,
op_id smallint,
internal smallint,
nonce int,
public_key char,
amount bigint,
destination char,
"parameters" jsonb,
entrypoint char,
contract_address char,
delegate char,
status char,
error_trace jsonb
)
AS $$
((select * from get_delegation(address) where id < lastid order by id desc limit lim)
union
(select * from get_origination(address) where id < lastid order by id desc limit lim)
union
(select * from get_transaction(address) where id < lastid order by id desc limit lim)
union
(select * from get_reveal(address)  where id < lastid order by id desc limit lim)
)
order by id desc limit lim
$$ language sql stable;


-- drop function get_operations ;
-- drop function get_transaction ;
-- drop function get_reveal ;
-- drop function get_origination ;
-- drop function get_delegation ;

CREATE OR REPLACE FUNCTION exists_address (addr char(36))
RETURNS bool as $$
select exists (select * from C.address where address = addr)
$$ language sql;
-- src/db-schema/operation_functions.sql
-- Open Source License
-- Copyright (c) 2021-2022 Nomadic Labs <contact@nomadic-labs.com>
-- Copyright (c) 2021 Rémy El Sibaïe <remy@nomadic-labs.com>
--
-- Permission is hereby granted, free of charge, to any person obtaining a
-- copy of this software and associated documentation files (the "Software"),
-- to deal in the Software without restriction, including without limitation
-- the rights to use, copy, modify, merge, publish, distribute, sublicense,
-- and/or sell copies of the Software, and to permit persons to whom the
-- Software is furnished to do so, subject to the following conditions:
--
-- The above copyright notice and this permission notice shall be included
-- in all copies or substantial portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
-- IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
-- FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
-- THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
-- LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
-- FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
-- DEALINGS IN THE SOFTWARE.

CREATE OR REPLACE FUNCTION get_alpha_infos (op_id bigint)
  RETURNS TABLE(
    hash varchar,
    id smallint,
    block_hash varchar,
    op_timestamp timestamp,
    level integer,
    internal smallint
  ) as
  $$
  select
    o.hash::varchar,
    oa.id,
    b.hash::varchar,
    b.timestamp,
    b.level,
    oa.internal
  from c.operation_alpha oa
  join c.operation o on oa.hash_id = o.hash_id
  join c.block b on oa.block_level = b.level
  where oa.autoid = op_id
$$ language sql;

CREATE OR REPLACE FUNCTION get_address (id bigint)
  RETURNS TABLE(hash varchar) as
$$
  select address::varchar from c.address where address_id = id
$$ language sql;

-- Such a value contains specific informations about the manager operation
-- kind should be 'transaction', 'delegation', 'reveal', 'origination'
DO $$
BEGIN
    IF (SELECT COUNT(*) FROM pg_type WHERE typname = 'manager_data') > 0 THEN
    BEGIN
      drop type manager_data cascade;
      EXCEPTION WHEN OTHERS THEN PERFORM NULL;
    END;
    BEGIN
      drop type full_operation cascade;
      EXCEPTION WHEN OTHERS THEN PERFORM NULL;
    END;
    BEGIN
      drop type very_full_operation cascade;
      EXCEPTION WHEN OTHERS THEN PERFORM NULL;
    END;
    END IF;
    CREATE TYPE manager_data AS (
        operation_id bigint
      , kind text
      , source_hash varchar
      , fee bigint
      , status smallint
      , data jsonb
    );
    CREATE TYPE very_full_operation AS (
       operation_id bigint
      , hash varchar
      , id smallint
      , block_hash varchar
      , op_timestamp timestamp
      , level integer
      , internal smallint
      , kind text
      , source_hash varchar
      , status varchar
      , fee bigint
      , data jsonb
      , counter numeric
      , gas_limit numeric
      , storage_limit numeric
    );
    CREATE TYPE full_operation AS (
        hash varchar
      , id smallint
      , block_hash varchar
      , op_timestamp timestamp
      , level integer
      , internal smallint
      , kind text
      , source_hash varchar
      , status varchar
      , fee bigint
      , data jsonb
      , counter numeric
      , gas_limit numeric
      , storage_limit numeric
    );
END $$;


-- Returns all the transfers where one address from the given set is
-- source or destination
CREATE OR REPLACE FUNCTION get_tx_no_token(address_hashes varchar[])
  RETURNS setof manager_data as $$

  select tr.operation_id, 'transaction', coalesce(src.hash, adds.address)
  , tr.fee
  , tr.status
  , jsonb_build_object
  -- the amount is returned into a text: Caqti does not have support for `jsonb`
  -- as such the value is returned as a string. Mezos then needs to parse this
  -- string into a JSON using Data_encoding. Data_encoding encodes Int64 and
  -- Zarith values as string, while Caqti encodes thoses as numerical values.
  -- Zarith values can be represented with more than 64bit, as such it cannot be
  -- interpreted by OCaml and transformed using an Obj.magic since it will
  -- probably be truncated.
  ( 'amount', tr.amount::text
  , 'token', 'tez'
  , 'destination', coalesce(dest.hash, adds.address)
  , 'parameters', tr.parameters
  , 'entrypoint', tr.entrypoint
  )
  from unnest(address_hashes) hash
  join c.address adds on adds.address = hash
  join c.tx tr on address_id = source_id or destination_id = address_id
  left join get_address(destination_id) dest on address_id = source_id
  left join get_address(source_id) src on address_id = destination_id
  -- Are filtered out from transfers the one fetched
  -- from token transfers. It allows the union all
  where not exists (select 1 from t.fa12_transfer where operation_id = tr.operation_id)

  $$ language sql;


-- Returns all the token transfers where one address from the given set is
-- source or destination
CREATE OR REPLACE FUNCTION get_tx_fa12(address_hashes varchar[])
  RETURNS setof manager_data as $$

  select tk.operation_id, 'transaction', coalesce(src.hash, adds.address)
  , tr.fee
  , tr.status
  , jsonb_build_object
  ( 'token_amount', tk.amount::text
  , 'amount', tr.amount::text
  , 'token', 'fa1-2'
  , 'destination', coalesce(dest.hash, adds.address)
  , 'contract', tk_add.hash
  )
  from unnest(address_hashes) hash
  join c.address adds on adds.address = hash
  join t.fa12_transfer tk on address_id = source_id or destination_id = address_id
  join t.fa12_operation t_op using (operation_id)
  join get_address(t_op.token_address_id) tk_add on true
  join c.tx tr using (operation_id)
  left join get_address(tk.destination_id) dest on address_id = tk.source_id
  left join get_address(tk.source_id) src on address_id = tk.destination_id
$$ language sql;

-- Returns all the token transfers where one address from the given set is
-- source or destination
CREATE OR REPLACE FUNCTION get_tx_fa2(address_hashes varchar[])
  RETURNS setof manager_data as $$

  select tk.operation_id, 'transaction', coalesce(src.hash, adds.address)
  , tr.fee
  , tr.status
  , jsonb_build_object
  ( 'token_amount', tk.amount::text
  , 'amount', tr.amount::text
  , 'token', 'fa2'
  , 'token_id', tk.token_id::text
  , 'internal_op_id', tk.internal_op_id
  , 'destination', coalesce(dest.hash, adds.address)
  , 'contract', tk_add.hash
  )
  from unnest(address_hashes) hash
  join c.address adds on adds.address = hash
  join t.fa2_transfer tk on address_id = source_id or destination_id = address_id
  join t.fa2_operation t_op using (operation_id)
  join get_address(t_op.token_address_id) tk_add on true
  join c.tx tr using (operation_id)
  left join get_address(tk.destination_id) dest on address_id = tk.source_id
  left join get_address(tk.source_id) src on address_id = tk.destination_id
$$ language sql;

-- Returns all the reveals where one address from the given set is the source
CREATE OR REPLACE FUNCTION get_all_reveals(address_hashes varchar[])
  RETURNS setof manager_data as $$

  select r.operation_id
  , 'reveal'
  , adds.address::varchar
  , r.fee
  , r.status
  , jsonb_build_object('public_key', r.pk)::jsonb
  from unnest(address_hashes) hash
  join c.address adds on adds.address = hash
  join c.reveal r on address_id = source_id

$$ language sql;

-- Returns all the delegations where one address from the given set is the source
-- or the delegate pkh
CREATE OR REPLACE FUNCTION get_all_delegations(address_hashes varchar[])
  RETURNS setof manager_data as $$

  select r.operation_id, 'delegate', coalesce(src.hash, adds.address)
  , r.fee
  , r.status
  , (select jsonb_build_object('delegate', pkh.hash) where pkh.hash is not null)
  from unnest(address_hashes) hash
  join c.address adds on adds.address = hash
  join c.delegation r on address_id = source_id or address_id = pkh_id
  left join get_address(pkh_id) pkh on address_id = source_id
  left join get_address(source_id) src on address_id = pkh_id

$$ language sql;

-- Returns all the originations where one address from the given set is the source
CREATE OR REPLACE FUNCTION get_all_originations(address_hashes varchar[])
  RETURNS setof manager_data as $$


  select r.operation_id, 'origination', adds.address::varchar, r.fee
  , r.status
  , jsonb_build_object
    ( 'contract', kt.hash
    , 'storage_size', r.storage_size::text
    , 'paid_storage_size_diff', r.paid_storage_size_diff::text
  )
  from unnest(address_hashes) hash
  join c.address adds on adds.address = hash
  join c.origination r on address_id = source_id or address_id = k_id
  left join get_address(r.k_id) kt on true

$$ language sql;


CREATE OR REPLACE FUNCTION mempool_oa_to_data(destination varchar, token_kind token_contract_kind, data jsonb)
  RETURNS jsonb as $$

  select case
         when data->>'kind' = 'transaction' and token_kind is not null then
           jsonb_build_object
           ( 'token', token_kind
           , 'amount', cast(data->>'amount' as varchar)
           , 'contract', destination
           , 'parameters', (data->'parameters'->>'value')::jsonb -- data must be parsed by the lib_indexer
           , 'entrypoint', data->'parameters'->>'entrypoint'
           )
         when data->>'kind' = 'transaction' and token_kind is null then
           jsonb_build_object
           ( 'amount', cast(data->>'amount' as varchar)
           , 'token', 'tez'
           , 'destination', destination
           , 'parameters', (data->'parameters'->>'value')::jsonb -- data must be parsed by the lib_indexer
           , 'entrypoint', data->'parameters'->>'entrypoint'
           )
         when data->>'kind' = 'delegation' then
           jsonb_build_object ('delegate', data->>'delegate')
         when data->>'kind' = 'reveal' then
           jsonb_build_object ('public_key', data->>'public_key')
         when data->>'kind' = 'origination' then
           -- these info are not in the mempool operation
           jsonb_build_object
             ( 'contract', null
             , 'storage_size', null
             , 'paid_storage_size_diff', null)
         else jsonb_build_object ()
  end

$$ language sql;

CREATE OR REPLACE FUNCTION get_manager_operations_on_mempool(address_hashes varchar[], only_kinds varchar[])
  RETURNS setof full_operation as $$

  select oa.hash::varchar, oa.id
  , null as block_hash
  , to_timestamp(first_seen_timestamp)::timestamp without time zone
  , first_seen_level
  , null::smallint as internal
  , oa.operation_alpha->>'kind'
  , oa.source::varchar
  , status::varchar
  , cast(oa.operation_alpha->>'fee' AS bigint)
  , mempool_oa_to_data(destination, tkc.kind, oa.operation_alpha)
  , cast(oa.operation_alpha->>'counter' AS numeric)
  , cast(oa.operation_alpha->>'gas_limit' AS numeric)
  , cast(oa.operation_alpha->>'storage_limit' AS numeric)
  from unnest(address_hashes) addr
  join m.operation_alpha oa on destination = addr or source = addr
  left join c.address adds on adds.address = oa.destination
  left join t.contract tkc on adds.address_id = tkc.address_id
  where cardinality(only_kinds) = 0 or oa.operation_alpha->>'kind' = any(only_kinds)

$$ language sql;

CREATE OR REPLACE FUNCTION get_manager_numbers (opaid bigint)
RETURNS TABLE (counter numeric, gas_limit numeric, storage_limit numeric)
AS $$
WITH r AS (
SELECT * FROM C.manager_numbers mn WHERE mn.operation_id = opaid
)
SELECT
   (SELECT counter FROM r)
 , (SELECT gas_limit FROM r)
 , (SELECT storage_limit FROM r)
;
$$ LANGUAGE SQL;


-- Returns every manager operation where the given set of address is either
-- source of destination. This is basically a gathering of all functions above.
-- The previous functions return disjoint sets, so union all is pretty efficient there.
-- The current function also gather common infos, like manager numbers, ids and so on.
CREATE OR REPLACE FUNCTION full_get_manager_operations(address_hashes varchar[], only_kinds varchar[], lim bigint, ofs bigint)
  RETURNS setof very_full_operation as $$

  select * from
    ((select r.operation_id, infos.*, r.kind, r.source_hash, status(r.status)::varchar, r.fee, r.data
           , manager_numbers.*
      from (
        (select * from get_tx_no_token(address_hashes) txs
           where cardinality(only_kinds) = 0 or 'transaction' = any(only_kinds)
           order by operation_id desc limit lim offset ofs)
         union all
        (select * from get_tx_fa12(address_hashes)
         where cardinality(only_kinds) = 0 or 'transaction' = any(only_kinds)
         order by operation_id desc limit lim offset ofs)
         union all
        (select * from get_tx_fa2(address_hashes)
         where cardinality(only_kinds) = 0 or 'transaction' = any(only_kinds)
         order by operation_id desc limit lim offset ofs)
         union all
        (select * from get_all_delegations(address_hashes)
         where cardinality(only_kinds) = 0 or 'delegation' = any(only_kinds)
          order by operation_id desc limit lim offset ofs)
         union all
        (select * from get_all_reveals(address_hashes)
         where cardinality(only_kinds) = 0 or 'reveal' = any(only_kinds)
         order by operation_id desc limit lim offset ofs)
         union all
        (select * from get_all_originations(address_hashes)
         where cardinality(only_kinds) = 0 or 'origination' = any(only_kinds)
         order by operation_id desc limit lim offset ofs)
      ) r
      join get_alpha_infos(r.operation_id) infos on true
      join get_manager_numbers(r.operation_id) manager_numbers on true
      order by infos.op_timestamp desc
       limit lim offset ofs)
     union all
    (select null::bigint as operation_id, * from get_manager_operations_on_mempool(address_hashes, only_kinds) mmo
     where not exists (select 1 from c.operation op where op.hash = mmo.hash)
           order by op_timestamp desc limit lim)
  ) r
  order by op_timestamp desc, operation_id desc limit lim offset ofs
$$ language sql;


CREATE OR REPLACE FUNCTION get_manager_operations(address_hashes varchar[], only_kinds varchar[], lim bigint, ofs bigint)
  RETURNS setof full_operation as $$
  select
        hash
      , id
      , block_hash
      , op_timestamp
      , level
      , internal
      , kind
      , source_hash
      , status
      , fee
      , data
      , counter
      , gas_limit
      , storage_limit
  from full_get_manager_operations(address_hashes, only_kinds, lim, ofs);
$$ language sql;

  -- select count(*)
  --   from get_manager_operations(
  --     ARRAY['tz1MirJ1tHvjTt8CjxH9nox4XSmm2Le5AQQj']
  --     , ARRAY['delegation']::text[], 100, 10
  --   );

  -- explain analyze select * from get_manager_operations_on_mempool(ARRAY['tz1MirJ1tHvjTt8CjxH9nox4XSmm2Le5AQQj'], Array[]::text[]) mmo
  --  where not exists (select 1 from c.operation op where op.hash = mmo.hash)
  --  order by op_timestamp limit 10;

  -- explain analyze select op_timestamp, source_hash, data
  --   from get_manager_operations(ARRAY['tz1UBwnNSFrvbPmtVT4vHCsba3mjMGtKhpu2', 'tz1Rrc4XieKdAqhLkYHGJUeJGGjZWMfx7qgC'
  --                                     , 'tz1MirJ1tHvjTt8CjxH9nox4XSmm2Le5AQQj']
  --                               , ARRAY[]::text[], 10, 0
  --   );

  -- tz1MirJ1tHvjTt8CjxH9nox4XSmm2Le5AQQj
-- src/db-schema/mezos_tokens.sql
-- Open Source License
-- Copyright (c) 2021-2022 Nomadic Labs <contact@nomadic-labs.com>
--
-- Permission is hereby granted, free of charge, to any person obtaining a
-- copy of this software and associated documentation files (the "Software"),
-- to deal in the Software without restriction, including without limitation
-- the rights to use, copy, modify, merge, publish, distribute, sublicense,
-- and/or sell copies of the Software, and to permit persons to whom the
-- Software is furnished to do so, subject to the following conditions:
--
-- The above copyright notice and this permission notice shall be included
-- in all copies or substantial portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
-- IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
-- FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
-- THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
-- LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
-- FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
-- DEALINGS IN THE SOFTWARE.

-- Lines starting with --OPT may be automatically activated
-- Lines ending with --OPT may be automatically deactivated

SELECT 'mezos_tokens.sql' as file;

CREATE OR REPLACE FUNCTION get_all_token_contracts ()
RETURNS TABLE(address char(36), kind token_contract_kind)
AS $$
select address(c.address_id), c.kind
from t.contract c
$$ LANGUAGE SQL;

CREATE OR REPLACE FUNCTION get_token_contracts
       (lim bigint, ofs bigint,
       kinds token_contract_kind[] default ARRAY['fa1-2', 'fa2']::token_contract_kind[])
RETURNS TABLE(address char(36), kind token_contract_kind)
AS $$
select address(c.address_id), c.kind
from unnest(kinds) k
join t.contract c on c.kind = k
order by c.address_id desc limit lim offset ofs
$$ LANGUAGE SQL;

CREATE OR REPLACE FUNCTION exists_token_contract (addr char(36))
RETURNS TABLE (kind token_contract_kind)
AS $$
select c.kind
from get_all_token_contracts() c
where c.address = addr
$$ LANGUAGE SQL;

CREATE OR REPLACE FUNCTION find_tokens_for_addresses
       (addresses varchar[], lim bigint, ofs bigint,
       kinds token_contract_kind[] default ARRAY['fa1-2', 'fa2']::token_contract_kind[])
RETURNS TABLE (address char(36), kind token_contract_kind)
AS $$
select address(r.token_address_id), c.kind
from unnest(addresses) addr
join c.address addrs on addrs.address = addr
join t.accounts_registry r on r.address_id = addrs.address_id
join t.contract c on c.address_id = r.token_address_id
join unnest(kinds) k on c.kind = k
order by c.address_id desc limit lim offset ofs
$$ LANGUAGE SQL;

CREATE OR REPLACE FUNCTION get_token_transfer (address varchar, token varchar)
RETURNS TABLE(
type text,
id bigint,
level int,
"timestamp" timestamp,
block char,
hash char,
caller char,
tz_amount bigint,
fee bigint,
counter numeric,
gas_limit numeric,
storage_limit numeric,
op_id smallint,
action_source char,
action_destination char,
action_amount numeric,
action_callback char
)
AS $$
with a as (select address_id(address) as ddress)
select 'transfer',
       oa.autoid, -- id
       b.level, -- level
       b.timestamp, -- timestamp
       b.hash,  -- block
       op.hash, -- hash
       address, -- caller
       t.amount, -- tz_amount
       t.fee, -- fees
       m.counter, m.gas_limit, m.storage_limit, oa.id,
       address(tktx.source_id), -- source (tktx)
       address(tktx.destination_id), -- destination (tktx)
       tktx.amount, -- amount (tktx)
       null -- callback (tktx)
from
       C.operation_alpha oa,
       C.operation op,
       T.fa12_operation tkop,
       T.fa12_transfer tktx,
       C.block b,
       C.manager_numbers m,
       C.tx t,
       a
where
       (tktx.source_id = a.ddress OR tktx.destination_id = a.ddress)
       and tkop.token_address_id = address_id(token)
       and tkop.operation_id = t.operation_id
       and oa.autoid = t.operation_id
       and op.hash_id = oa.hash_id
       and b.level = op.block_level
       and m.operation_id = oa.autoid
       and tkop.kind = 'transfer'
       and tktx.operation_id = tkop.operation_id
$$ LANGUAGE SQL;


CREATE OR REPLACE FUNCTION get_token_approve (address varchar, token varchar)
RETURNS TABLE(
type text,
id bigint,
level int,
"timestamp" timestamp,
block char,
hash char,
caller char,
tz_amount bigint,
fee bigint,
counter numeric,
gas_limit numeric,
storage_limit numeric,
op_id smallint,
action_source char,
action_destination char,
action_amount numeric,
action_callback char
)
AS $$
with a as (select address_id(address) as ddress)
select 'approve',
       oa.autoid, -- id
       b.level, -- level
       b.timestamp, -- timestamp
       b.hash,  -- block
       op.hash, -- hash
       address, -- caller
       t.amount, -- tz_amount
       t.fee, -- fees
       m.counter, m.gas_limit, m.storage_limit, oa.id,
       address(tkapp.address_id), -- source (tkapp)
       null, --
       tkapp.amount, -- amount (tkapp)
       null --
from
       C.operation_alpha oa,
       C.operation op,
       T.fa12_operation tkop,
       T.fa12_approve tkapp,
       C.block b,
       C.manager_numbers m,
       C.tx t,
       a
where
       (tkapp.address_id = a.ddress OR tkop.caller_id = a.ddress)
       and tkop.token_address_id = token_address_id
       and tkop.operation_id = t.operation_id
       and t.operation_id = oa.autoid
       and op.hash_id = oa.hash_id
       and b.level = op.block_level
       and m.operation_id = oa.autoid
       and tkop.kind = 'approve'
       and tkapp.operation_id = tkop.operation_id
$$ LANGUAGE SQL;


CREATE OR REPLACE FUNCTION get_token_get_balance (address varchar, token varchar)
RETURNS TABLE(
type text,
id bigint,
level int,
"timestamp" timestamp,
block char,
hash char,
caller char,
tz_amount bigint,
fee bigint,
counter numeric,
gas_limit numeric,
storage_limit numeric,
op_id smallint,
action_source char,
action_destination char,
action_amount numeric,
action_callback char
)
AS $$
with a as (select address_id(address) as ddress)
select 'getBalance',
       oa.autoid, -- id
       b.level, -- level
       b.timestamp, -- timestamp
       b.hash,  -- block
       op.hash, -- hash
       address, -- caller
       t.amount, -- tz_amount

       t.fee, -- fees
       m.counter, m.gas_limit, m.storage_limit, oa.id,
       address(tkbal.address_id), -- address (tkbal)
       null, --
       cast(null as numeric), --
       address(tkbal.callback_id) -- amount (tkbal)
from
       C.operation_alpha oa,
       C.operation op,
       T.fa12_operation tkop,
       T.fa12_get_balance tkbal,
       C.block b,
       C.manager_numbers m,
       C.tx t,
       a
where
       (tkbal.address_id = a.ddress OR tkop.caller_id = a.ddress)
       and tkop.token_address_id = address_id(token)
       and t.operation_id = oa.autoid
       and op.hash_id = oa.hash_id
       and b.level = op.block_level
       and m.operation_id = oa.autoid
       and tkop.operation_id = t.operation_id
       and tkop.kind = 'getBalance'
       and tkbal.operation_id = tkop.operation_id
$$ LANGUAGE SQL;


CREATE OR REPLACE FUNCTION get_token_get_allowance (address varchar, token varchar)
RETURNS TABLE(
type text,
id bigint,
level int,
"timestamp" timestamp,
block char,
hash char,
caller char,
tz_amount bigint,
fee bigint,
counter numeric,
gas_limit numeric,
storage_limit numeric,
op_id smallint,
action_source char,
action_destination char,
action_amount numeric,
action_callback char
)
AS $$
with a as (select address_id(address) as ddress)
select 'getAllowance',
       oa.autoid, -- id
       b.level, -- level
       b.timestamp, -- timestamp
       b.hash,  -- block
       op.hash, -- hash
       address, -- caller
       t.amount, -- tz_amount
       t.fee, -- fees
       m.counter, m.gas_limit, m.storage_limit, oa.id,
       address(tkalw.source_id), -- source (tkalw)
       address(tkalw.destination_id), -- destination (tkalw)
       cast(null as numeric), --
       address(tkalw.callback_id) -- amount (tkalw)
from
       C.operation_alpha oa,
       C.operation op,
       T.fa12_operation tkop,
       T.fa12_get_allowance tkalw,
       C.block b,
       C.manager_numbers m,
       C.tx t,
       a
where
       (tkalw.source_id = a.ddress
       OR tkalw.destination_id = a.ddress
       OR tkop.caller_id = a.ddress)
       and tkop.token_address_id = address_id(token)
       and t.operation_id = oa.autoid
       and op.hash_id = oa.hash_id
       and b.level = op.block_level
       and m.operation_id = oa.autoid
       and tkop.operation_id = t.operation_id
       and tkop.kind = 'getAllowance'
       and tkalw.operation_id = tkop.operation_id
$$ LANGUAGE SQL;


CREATE OR REPLACE FUNCTION get_token_get_total_supply (address varchar, token varchar)
RETURNS TABLE(
type text,
id bigint,
level int,
"timestamp" timestamp,
block char,
hash char,
caller char,
tz_amount bigint,
fee bigint,
counter numeric,
gas_limit numeric,
storage_limit numeric,
op_id smallint,
action_source char,
action_destination char,
action_amount numeric,
action_callback char
)
AS $$
with a as (select address_id(address) as ddress)
select 'getTotalSupply',
       oa.autoid, -- id
       b.level, -- level
       b.timestamp, -- timestamp
       b.hash,  -- block
       op.hash, -- hash
       address, -- caller
       t.amount, -- tz_amount
       t.fee, -- fees
       m.counter, m.gas_limit, m.storage_limit, oa.id,
       null, --
       null, --
       cast(null as numeric), --
       address(tkts.callback_id) -- amount (tkbal)
from
       C.operation_alpha oa,
       C.operation op,
       T.fa12_operation tkop,
       T.fa12_get_total_supply tkts,
       C.block b,
       C.manager_numbers m,
       C.tx t,
       a
where
       (tkop.caller_id = a.ddress)
       and tkop.token_address_id = address_id(token)
       and t.operation_id = oa.autoid
       and op.hash_id = oa.hash_id
       and b.level = op.block_level
       and m.operation_id = oa.autoid
       and tkop.operation_id = t.operation_id
       and tkop.kind = 'getTotalSupply'
       and tkts.operation_id = tkop.operation_id
$$ LANGUAGE SQL;
