# Open Source License
# Copyright (c) 2019-2020 Nomadic Labs <contact@nomadic-labs.com>
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
# DEALINGS IN THE SOFTWARE.


SHELL=/bin/bash

all:
	@$(MAKE) noalpha

indexer:
	@$(MAKE) tezos
	@$(MAKE) protos
	@test ! -f src/bin_indexer/dune || rm src/bin_indexer/dune
	@$(MAKE) build

alpha:
	ALPHA=x $(MAKE) indexer
noalpha:
	$(MAKE) indexer

# makefile to make a local copy of tezos
include tezos.Makefile

# makefile to install opam packages
include opam.Makefile

# makefile to generate database schema
include db.Makefile

# makefile to test docker builds
include docker.Makefile

# makefile for git-related things
include git.Makefile

utils/db-schema-all-multicore.sql: Makefile src/db-schema/* src/proto_meta/proto_misc.ml
	$(MAKE) -s --no-print-directory db-schema-all-multicore > $@
utils/db-schema-all-default.sql: Makefile src/db-schema/* src/proto_meta/proto_misc.ml
	$(MAKE) -s --no-print-directory db-schema-all-default > $@

build:src/bin_indexer/dune
	dune build --root=. ./_build/default/src/bin_indexer/main_indexer.exe
	cp -f ./_build/default/src/bin_indexer/main_indexer.exe tezos-indexer
	echo $$(date) $$(git describe --tags) size=$$(wc -c < tezos-indexer) md5=$$(md5sum < tezos-indexer) >> compilation.log

# __NEW_PROTO__ 19 add the generated folder and files to ${GENERATED_FILES}
GENERATED_FILES = src/bin_indexer/db_schema.ml
GENERATED_FILES += src/lib_indexer/version.ml
GENERATED_FILES +=src/bin_indexer/snapshot_utils_6.ml
GENERATED_FILES +=src/bin_indexer/snapshot_utils_6.mli
GENERATED_FILES +=src/bin_indexer/snapshot_utils_7.ml
GENERATED_FILES +=src/bin_indexer/snapshot_utils_7.mli
GENERATED_FILES +=src/bin_indexer/snapshot_utils_8.ml
GENERATED_FILES +=src/bin_indexer/snapshot_utils_8.mli
GENERATED_FILES +=src/bin_indexer/snapshot_utils_9.ml
GENERATED_FILES +=src/bin_indexer/snapshot_utils_9.mli
GENERATED_FILES +=src/bin_indexer/snapshot_utils_10.ml
GENERATED_FILES +=src/bin_indexer/snapshot_utils_10.mli
GENERATED_FILES +=src/bin_indexer/snapshot_utils_11.ml
GENERATED_FILES +=src/bin_indexer/snapshot_utils_11.mli
GENERATED_FILES +=src/bin_indexer/indexer_helpers.ml
GENERATED_FILES +=src/proto_001_PtCJ7pwo
GENERATED_FILES +=src/proto_002_PsYLVpVv
GENERATED_FILES +=src/proto_003_PsddFKi3
GENERATED_FILES +=src/proto_004_Pt24m4xi
GENERATED_FILES +=src/proto_005_PsBabyM1
GENERATED_FILES +=src/proto_006_PsCARTHA
GENERATED_FILES +=src/proto_007_PsDELPH1
GENERATED_FILES +=src/proto_008_PtEdo2Zk
GENERATED_FILES +=src/proto_009_PsFLoren
GENERATED_FILES +=src/proto_010_PtGRANAD
GENERATED_FILES +=src/proto_011_PtHangz2
GENERATED_FILES +=src/proto_012_Psithaca
GENERATED_FILES +=src/proto_013_PtJakart
GENERATED_FILES +=src/proto_014_PtKathma
GENERATED_FILES +=src/proto_015_PtLimaPt
GENERATED_FILES +=src/proto_016_PtMumbai
GENERATED_FILES +=src/proto_017_PtNairob
GENERATED_FILES +=src/proto_018_Proxford
# GENERATED_FILES +=src/proto_019_?
# __NEW_PROTO__ 19
# GENERATED_FILES +=src/proto_alpha
GENERATED_FILES +=src/bin_indexer/chain_db_transition_001_002.ml
GENERATED_FILES +=src/bin_indexer/chain_db_transition_001_002.mli
GENERATED_FILES +=src/bin_indexer/chain_db_transition_002_003.ml
GENERATED_FILES +=src/bin_indexer/chain_db_transition_002_003.mli
GENERATED_FILES +=src/bin_indexer/chain_db_transition_003_004.ml
GENERATED_FILES +=src/bin_indexer/chain_db_transition_003_004.mli
GENERATED_FILES +=src/bin_indexer/chain_db_transition_004_005.ml
GENERATED_FILES +=src/bin_indexer/chain_db_transition_004_005.mli
GENERATED_FILES +=src/bin_indexer/chain_db_transition_005_006.ml
GENERATED_FILES +=src/bin_indexer/chain_db_transition_005_006.mli
GENERATED_FILES +=src/bin_indexer/chain_db_transition_006_007.ml
GENERATED_FILES +=src/bin_indexer/chain_db_transition_006_007.mli
GENERATED_FILES +=src/bin_indexer/chain_db_transition_007_008.ml
GENERATED_FILES +=src/bin_indexer/chain_db_transition_007_008.mli
GENERATED_FILES +=src/bin_indexer/chain_db_transition_008_009.ml
GENERATED_FILES +=src/bin_indexer/chain_db_transition_008_009.mli
GENERATED_FILES +=src/bin_indexer/chain_db_transition_009_010.ml
GENERATED_FILES +=src/bin_indexer/chain_db_transition_009_010.mli
GENERATED_FILES +=src/bin_indexer/chain_db_transition_010_011.ml
GENERATED_FILES +=src/bin_indexer/chain_db_transition_010_011.mli
GENERATED_FILES +=src/bin_indexer/chain_db_transition_011_012.ml
GENERATED_FILES +=src/bin_indexer/chain_db_transition_011_012.mli
GENERATED_FILES +=src/bin_indexer/chain_db_transition_012_013.ml
GENERATED_FILES +=src/bin_indexer/chain_db_transition_012_013.mli
GENERATED_FILES +=src/bin_indexer/chain_db_transition_013_014.ml
GENERATED_FILES +=src/bin_indexer/chain_db_transition_013_014.mli
GENERATED_FILES +=src/bin_indexer/chain_db_transition_014_015.ml
GENERATED_FILES +=src/bin_indexer/chain_db_transition_014_015.mli
GENERATED_FILES +=src/bin_indexer/chain_db_transition_015_016.ml
GENERATED_FILES +=src/bin_indexer/chain_db_transition_015_016.mli
GENERATED_FILES +=src/bin_indexer/chain_db_transition_016_017.ml
GENERATED_FILES +=src/bin_indexer/chain_db_transition_016_017.mli
GENERATED_FILES +=src/bin_indexer/chain_db_transition_017_018.ml
GENERATED_FILES +=src/bin_indexer/chain_db_transition_017_018.mli
# __NEW_PROTO__ 19
# GENERATED_FILES +=src/bin_indexer/chain_db_transition_018_019.ml
# GENERATED_FILES +=src/bin_indexer/chain_db_transition_018_019.mli
GENERATED_FILES +=src/bin_indexer/chain_db_transition_017_A.ml
GENERATED_FILES +=src/bin_indexer/chain_db_transition_017_A.mli
GENERATED_FILES +=src/bin_indexer/chain_db_transition_018_A.ml
GENERATED_FILES +=src/bin_indexer/chain_db_transition_018_A.mli
# __NEW_PROTO__ 19
# GENERATED_FILES +=src/bin_indexer/chain_db_transition_019_A.ml
# GENERATED_FILES +=src/bin_indexer/chain_db_transition_019_A.mli



protos: $(GENERATED_FILES)

src/lib_indexer/version.ml:Makefile $(shell ls -1 src/*/* | grep -v src/lib_indexer/version.ml) .git/*
	printf 'let c = "(" ^ String.lowercase_ascii @@ String.trim "commit: %s, %s" ^ ")"' "$$(git show|head -n 1|sed -e 's/commit //' -e 's/  */ /g')" "$$(TZ=UTC git log --date=iso-local|head -n 3|tail -n 1|sed -e 's/  */ /g') " > $@
	printf 'let t = "%s"' $$(git describe --tags) >> $@
	printf 'let t = if t <> "" then t else "(dev)"' >> $@
	printf 'let b = "(branch: %s)"' "$$(git branch |grep '*'|tr -d '*() ')" >> $@
	printf 'let sql = "%s" let sql_dev = %s ' \
		"$$(grep -e '-- version' < src/db-schema/versions.sql | sed -e "s/.*'\(.*\)'.*/\1/")" \
		"$$(grep -e '-- dev' < src/db-schema/versions.sql | grep -oE '(true|false)')" \
		>> $@
	echo 'let version () = Printf.printf "tezos-indexer %s %s %s %s\n%!" sql t b c; exit 0' >> $@


src/bin_indexer/db_schema.ml:Makefile src/db-schema/*
	>&2 ${MAKE} -s --no-print-directory db-schema-all-default > tmp.sql
	echo 'really_input_string stdin (in_channel_length stdin) |> Printf.printf "let s = %S let print () = print_endline s"' > tmp.ml
	ocaml tmp.ml < tmp.sql > $@
	echo >> $@
	>&2 ${MAKE} -s --no-print-directory db-schema-all-multicore > tmp.sql
	echo 'really_input_string stdin (in_channel_length stdin) |> Printf.printf "let m = %S let print_multicore () = print_endline m"' > tmp.ml
	ocaml tmp.ml < tmp.sql >> $@
	rm -f tmp.sql tmp.ml

ifdef ALPHA
MPP = mpp -so '' -sc '' -t ocaml -son "(**\#" -scn "\#**)" -sos-noloc '' -sos '' -scs '' -soc '' -scc '' -sec ''
else
MPP = mpp -so '' -sc '' -t ocaml -son "(**\#" -scn "\#**)" -sos-noloc '' -sos '' -scs '' -soc '' -scc '' -sec '' -set __NOALPHA__
endif

FA2 = -set __FA2__
BIGMAPDIFF =  -set __BIG_MAP_DIFF__ -set __BIG_MAP__
STORAGEDIFF =  -set __STORAGE_DIFF__ -set __BIG_MAP__
MILLIGAS = -set __MILLIGAS__
META1  =  -set __META1__
META2  =  -set __META2__  ${META1} ${BIGMAPDIFF}
META3  =  -set __META3__  ${BIGMAPDIFF}
META4  =  -set __META4__  ${META3} ${FA2}
META5  =  -set __META5__  ${META4} ${MILLIGAS}

META6  =  -set __META6__  ${MILLIGAS} ${FA2} ${STORAGEDIFF}
META7  =  -set __META7__  ${META6}
META8  =  -set __META8__  ${META7}
META9  =  -set __META9__  ${META8}
META10 =  -set __META10__ ${META9}
META11 =  -set __META11__ ${META10}
META12 =  -set __META12__ ${META11}
META13 =  -set __META13__ ${META12}
META14 =  -set __META14__ ${META13}
META15 =  -set __META15__ ${META14}
META16 =  -set __META16__ ${META15}
META17 =  -set __META17__ ${META16}
# __NEW_PROTO__ 19
# META18 =  -set __META18__

MEMPOOL = -set __MEMPOOL__


MPP_NO_OP = ${MPP} -set __NO_OP__

# block 1
PROTO0  = ${MPP_NO_OP} ${META1} -set PROTO=000_Ps9mPmXa -set PROTONEXT=000_Ps9mPmXa -set PROTONAME=000-Ps9mPmXa -set __PROTO0__ -set BS=BS0_mainnet


# "normal" blocks
PROTO1  = PROTO=1  ${MPP} ${META1}  -set PROTO=001_PtCJ7pwo -set PROTONEXT=001_PtCJ7pwo -set PROTONAME=001-PtCJ7pwo -set __PROTO1__  -set BS=BS1
PROTO2  = PROTO=2  ${MPP} ${META2}  -set PROTO=002_PsYLVpVv -set PROTONEXT=002_PsYLVpVv -set PROTONAME=002-PsYLVpVv -set __PROTO2__  -set BS=BS2  -set PPBS=BS1  -set PBS=BST12
PROTO3  = PROTO=3  ${MPP} ${META2}  -set PROTO=003_PsddFKi3 -set PROTONEXT=003_PsddFKi3 -set PROTONAME=003-PsddFKi3 -set __PROTO3__  -set BS=BS3  -set PPBS=BS2  -set PBS=BST23
PROTO4  = PROTO=4  ${MPP} ${META2}  -set PROTO=004_Pt24m4xi -set PROTONEXT=004_Pt24m4xi -set PROTONAME=004-Pt24m4xi -set __PROTO4__  -set BS=BS4  -set PPBS=BS3  -set PBS=BST34
PROTO5  = PROTO=5  ${MPP} ${META3}  -set PROTO=005_PsBabyM1 -set PROTONEXT=005_PsBabyM1 -set PROTONAME=005-PsBabyM1 -set __PROTO5__  -set BS=BS5  -set PPBS=BS4  -set PBS=BST45
PROTO6  = PROTO=6  ${MPP} ${META4}  -set PROTO=006_PsCARTHA -set PROTONEXT=006_PsCARTHA -set PROTONAME=006-PsCARTHA -set __PROTO6__  -set BS=BS6  -set PPBS=BS5  -set PBS=BST56
PROTO7  = PROTO=7  ${MPP} ${META5}  -set PROTO=007_PsDELPH1 -set PROTONEXT=007_PsDELPH1 -set PROTONAME=007-PsDELPH1 -set __PROTO7__  -set BS=BS7  -set PPBS=BS6  -set PBS=BST67
PROTO8  = PROTO=8  ${MPP} ${META6}  -set PROTO=008_PtEdo2Zk -set PROTONEXT=008_PtEdo2Zk -set PROTONAME=008-PtEdo2Zk -set __PROTO8__  -set BS=BS8  -set PPBS=BS7  -set PBS=BST78
PROTO9  = PROTO=9  ${MPP} ${META7}  -set PROTO=009_PsFLoren -set PROTONEXT=009_PsFLoren -set PROTONAME=009-PsFLoren -set __PROTO9__  -set BS=BS9  -set PPBS=BS8  -set PBS=BST89
PROTO10 = PROTO=10 ${MPP} ${META8}  -set PROTO=010_PtGRANAD -set PROTONEXT=010_PtGRANAD -set PROTONAME=010-PtGRANAD -set __PROTO10__ -set BS=BS10 -set PPBS=BS9  -set PBS=BST910
PROTO11 = PROTO=11 ${MPP} ${META9}  -set PROTO=011_PtHangz2 -set PROTONEXT=011_PtHangz2 -set PROTONAME=011-PtHangz2 -set __PROTO11__ -set BS=BS11 -set PPBS=BS10 -set PBS=BST1011
PROTO12 = PROTO=12 ${MPP} ${META10} -set PROTO=012_Psithaca -set PROTONEXT=013_PtJakart -set PROTONAME=012-Psithaca -set __PROTO12__ -set BS=BS12 -set PPBS=BS11 -set PBS=BST1112
PROTO13 = PROTO=13 ${MPP} ${META11} -set PROTO=013_PtJakart -set PROTONEXT=014_PtKathma -set PROTONAME=013-PtJakart -set __PROTO13__ -set BS=BS13 -set PPBS=BS12 -set PBS=BST1213
PROTO14 = PROTO=14 ${MPP} ${META12} -set PROTO=014_PtKathma -set PROTONEXT=015_PtLimaPt -set PROTONAME=014-PtKathma -set __PROTO14__ -set BS=BS14 -set PPBS=BS13 -set PBS=BST1314
PROTO15 = PROTO=15 ${MPP} ${META13} -set PROTO=015_PtLimaPt -set PROTONEXT=016_PtMumbai -set PROTONAME=015-PtLimaPt -set __PROTO15__ -set BS=BS15 -set PPBS=BS14 -set PBS=BST1415
PROTO16 = PROTO=16 ${MPP} ${META14} -set PROTO=016_PtMumbai -set PROTONEXT=017_PtNairob -set PROTONAME=016-PtMumbai -set __PROTO16__ -set BS=BS16 -set PPBS=BS15 -set PBS=BST1516
PROTO17 = PROTO=17 ${MPP} ${META15} -set PROTO=017_PtNairob -set PROTONEXT=018_Proxford -set PROTONAME=017-PtNairob -set __PROTO17__ -set BS=BS17 -set PPBS=BS16 -set PBS=BST1617
# __NEW_PROTO__ 19 update top proto
PROTO18 = PROTO=18 ${MPP} ${META16} -set PROTO=018_Proxford -set PROTONEXT=XXXXX        -set PROTONAME=018-Proxford -set __PROTO18__ -set BS=BS18 -set PPBS=BS17 -set PBS=BST1718
# __NEW_PROTO__ 19
# PROTO19 = PROTO=19 ${MPP} ${META15} -set PROTO=019_         -set PROTONEXT=XXXXX        -set PROTONAME=019-         -set __PROTO19__ -set BS=BS19 -set PPBS=BS18 -set PBS=BST1819
# __NEW_PROTO__ 19 update proto alpha
PROTOA = PROTO=19 ${MPP}  ${META17} -set PROTO=alpha        -set PROTONEXT=alpha        -set PROTONAME=alpha        -set __PROTOA__ -set BS=BSA -set PPBS=BS18 -set PBS=BST18A -set APPBS=BS17 -set APBS=BST17A
# __NEW_PROTO__ 19 Mempool
# PROTO19 += ${MEMPOOL}
PROTO18 += ${MEMPOOL}
PROTO17 += ${MEMPOOL}
PROTOA += ${MEMPOOL}

ifdef ALPHA
# __NEW_PROTO__ 19 update top proto
PROTO18 += -set PROTONEXT=alpha
else
# keep the long MPP command so that it can still check for obvious errors
# __NEW_PROTO__ 19 update top proto
PROTO18 += -set PROTONEXT=018_Proxford
PROTOA += | cat > /dev/null | true
endif


# protocol transition blocks
TRANS = -set __TRANSITION__
TRANS_1_2   = ${PROTO1}  ${TRANS} -set __TRANSITION_1_2__   -set PROTONEXT=002_PsYLVpVv -set BS=BST12   -set PBS=BS1
TRANS_2_3   = ${PROTO2}  ${TRANS} -set __TRANSITION_2_3__   -set PROTONEXT=003_PsddFKi3 -set BS=BST23   -set PBS=BS2
TRANS_3_4   = ${PROTO3}  ${TRANS} -set __TRANSITION_3_4__   -set PROTONEXT=004_Pt24m4xi -set BS=BST34   -set PBS=BS3
TRANS_4_5   = ${PROTO4}  ${TRANS} -set __TRANSITION_4_5__   -set PROTONEXT=005_PsBabyM1 -set BS=BST45   -set PBS=BS4
TRANS_5_6   = ${PROTO5}  ${TRANS} -set __TRANSITION_5_6__   -set PROTONEXT=006_PsCARTHA -set BS=BST56   -set PBS=BS5
TRANS_6_7   = ${PROTO6}  ${TRANS} -set __TRANSITION_6_7__   -set PROTONEXT=007_PsDELPH1 -set BS=BST67   -set PBS=BS6
TRANS_7_8   = ${PROTO7}  ${TRANS} -set __TRANSITION_7_8__   -set PROTONEXT=008_PtEdo2Zk -set BS=BST78   -set PBS=BS7
TRANS_8_9   = ${PROTO8}  ${TRANS} -set __TRANSITION_8_9__   -set PROTONEXT=009_PsFLoren -set BS=BST89   -set PBS=BS8
TRANS_9_10  = ${PROTO9}  ${TRANS} -set __TRANSITION_9_10__  -set PROTONEXT=010_PtGRANAD -set BS=BST910  -set PBS=BS9
TRANS_10_11 = ${PROTO10} ${TRANS} -set __TRANSITION_10_11__ -set PROTONEXT=011_PtHangz2 -set BS=BST1011 -set PBS=BS10
TRANS_11_12 = ${PROTO11} ${TRANS} -set __TRANSITION_11_12__ -set PROTONEXT=012_Psithaca -set BS=BST1112 -set PBS=BS11
TRANS_12_13 = ${PROTO12} ${TRANS} -set __TRANSITION_12_13__ -set PROTONEXT=013_PtJakart -set BS=BST1213 -set PBS=BS12
TRANS_13_14 = ${PROTO13} ${TRANS} -set __TRANSITION_13_14__ -set PROTONEXT=014_PtKathma -set BS=BST1314 -set PBS=BS13
TRANS_14_15 = ${PROTO14} ${TRANS} -set __TRANSITION_14_15__ -set PROTONEXT=015_PtLimaPt -set BS=BST1415 -set PBS=BS14
TRANS_15_16 = ${PROTO15} ${TRANS} -set __TRANSITION_15_16__ -set PROTONEXT=016_PtMumbai -set BS=BST1516 -set PBS=BS15
TRANS_16_17 = ${PROTO16} ${TRANS} -set __TRANSITION_16_17__ -set PROTONEXT=017_PtNairob -set BS=BST1617  -set PBS=BS16
TRANS_17_18 = ${PROTO17} ${TRANS} -set __TRANSITION_17_18__ -set PROTONEXT=018_Proxford -set BS=BST1718 -set PBS=BS17
TRANS_17_A  = ${PROTO17} ${TRANS} -set __TRANSITION_17_A__  -set PROTONEXT=alpha        -set BS=BST17A  -set PBS=BS17
TRANS_18_A  = ${PROTO18} ${TRANS} -set __TRANSITION_18_A__  -set PROTONEXT=alpha        -set BS=BST18A  -set PBS=BS18
# __NEW_PROTO__ 19
# TRANS_18_19 = ${PROTO19} ${TRANS} -set __TRANSITION_18_19__ -set PROTONEXT=019_         -set BS=BST1819 -set PBS=BS18
# TRANS_19_A  = ${PROTO19} ${TRANS} -set __TRANSITION_19_A__  -set PROTONEXT=alpha        -set BS=BST19A  -set PBS=BS19

COMMENTS_HELPER = sed -e 's/CLOSE_COMMENTS/*)/' -e 's/OPEN_COMMENTS/(*/'

src/bin_indexer/indexer_helpers.ml:src/bin_indexer/indexer_helpers.ml.mpp Makefile
	${COMMENTS_HELPER} < $< | ${MPP} > $@

ifdef ALPHA
src/bin_indexer/dune:src/bin_indexer/dune.mpp Makefile src/proto_alpha
	${COMMENTS_HELPER} < $< | ${MPP} > $@
else
src/bin_indexer/dune:src/bin_indexer/dune.mpp Makefile
# __NEW_PROTO__ 19 update chain_db_transition*ml* in following line
	rm -fr src/proto_alpha src/bin_indexer/chain_db_transition_017_A.ml* src/bin_indexer/chain_db_transition_016_A.ml* src/bin_indexer/chain_db_transition_018_A.ml*
	${COMMENTS_HELPER} < $< | ${MPP} > $@
endif

src/bin_indexer/snapshot_utils_6.ml:src/bin_indexer/snapshot_utils.ml.mpp Makefile
	${PROTO6} $< > $@
src/bin_indexer/snapshot_utils_6.mli:src/bin_indexer/snapshot_utils.mli.mpp Makefile
	${PROTO6} $< > $@

src/bin_indexer/snapshot_utils_7.ml:src/bin_indexer/snapshot_utils.ml.mpp Makefile
	${PROTO7} $< > $@
src/bin_indexer/snapshot_utils_7.mli:src/bin_indexer/snapshot_utils.mli.mpp Makefile
	${PROTO7} $< > $@

src/bin_indexer/snapshot_utils_8.ml:src/bin_indexer/snapshot_utils.ml.mpp Makefile
	${PROTO8} $< > $@
src/bin_indexer/snapshot_utils_8.mli:src/bin_indexer/snapshot_utils.mli.mpp Makefile
	${PROTO8} $< > $@

src/bin_indexer/snapshot_utils_9.ml:src/bin_indexer/snapshot_utils.ml.mpp Makefile
	${PROTO9} $< > $@
src/bin_indexer/snapshot_utils_9.mli:src/bin_indexer/snapshot_utils.mli.mpp Makefile
	${PROTO9} $< > $@

src/bin_indexer/snapshot_utils_10.ml:src/bin_indexer/snapshot_utils.ml.mpp Makefile
	${PROTO10} $< > $@
src/bin_indexer/snapshot_utils_10.mli:src/bin_indexer/snapshot_utils.mli.mpp Makefile
	${PROTO10} $< > $@

src/bin_indexer/snapshot_utils_11.ml:src/bin_indexer/snapshot_utils.ml.mpp Makefile
	${PROTO11} $< > $@
src/bin_indexer/snapshot_utils_11.mli:src/bin_indexer/snapshot_utils.mli.mpp Makefile
	${PROTO11} $< > $@

src/bin_indexer/snapshot_utils_A.ml:src/bin_indexer/snapshot_utils.ml.mpp Makefile
	cat $< | ${PROTOA} > $@
src/bin_indexer/snapshot_utils_A.mli:src/bin_indexer/snapshot_utils.mli.mpp Makefile
	cat $< | ${PROTOA} > $@

define build_proto
	mkdir -p ${1} ; touch ${1}
	${COMMENTS_HELPER} < src/proto_meta/chain_db.ml       | ${2} > ${1}/chain_db.ml
	${COMMENTS_HELPER} < src/proto_meta/chain_db.mli      | ${2} > ${1}/chain_db.mli
	${COMMENTS_HELPER} < src/proto_meta/db_alpha.ml       | ${2} > ${1}/db_alpha.ml
	${COMMENTS_HELPER} < src/proto_meta/db_alpha.mli      | ${2} > ${1}/db_alpha.mli
	${COMMENTS_HELPER} < src/proto_meta/proto_misc.ml     | ${2} > ${1}/proto_misc.ml
	${COMMENTS_HELPER} < src/proto_meta/contract_utils.ml | ${2} > ${1}/contract_utils.ml
	if [[ "${1}" == "src/proto_alpha" ]] || [[ $$(sed -e 's/.*_\(...\)_.*/\1/' -e 's/^00*//' <<<"${1}") -gt 4 ]] ; then \
	${COMMENTS_HELPER} < src/proto_meta/proto_fa2.ml      | ${2} > ${1}/proto_fa2.ml ; \
	fi
	${2} src/proto_meta/dune-tpl > ${1}/dune
	cp src/proto_meta/tpl-opam ${1}/tezos-indexer-$$(sed -e 's|src/proto_||' -e 's/_/-/' <<<${1}).opam
	echo '(lang dune 1.11)' > ${1}/dune-project
endef
define build_proto_with_mempool
	$(call build_proto,${1},${2})
	${COMMENTS_HELPER} < src/proto_meta/mempool_utils.ml | ${2} > ${1}/mempool_utils.ml
endef


src/proto_001_PtCJ7pwo:src/proto_meta/* Makefile
	$(call build_proto,$@,${PROTO1})

src/proto_002_PsYLVpVv:src/proto_meta/* Makefile
	$(call build_proto,$@,${PROTO2})

src/proto_003_PsddFKi3:src/proto_meta/* Makefile
	$(call build_proto,$@,${PROTO3})

src/proto_004_Pt24m4xi:src/proto_meta/* Makefile
	$(call build_proto,$@,${PROTO4})

src/proto_005_PsBabyM1:src/proto_meta/* Makefile
	$(call build_proto,$@,${PROTO5})

src/proto_006_PsCARTHA:src/proto_meta/* Makefile
	$(call build_proto,$@,${PROTO6})

src/proto_007_PsDELPH1:src/proto_meta/* Makefile
	$(call build_proto,$@,${PROTO7})

src/proto_008_PtEdo2Zk:src/proto_meta/* Makefile
	$(call build_proto,$@,${PROTO8})

src/proto_009_PsFLoren:src/proto_meta/* Makefile
	$(call build_proto,$@,${PROTO9})

src/proto_010_PtGRANAD:src/proto_meta/* Makefile
	$(call build_proto,$@,${PROTO10})

src/proto_011_PtHangz2:src/proto_meta/* Makefile
	$(call build_proto,$@,${PROTO11})

src/proto_012_Psithaca:src/proto_meta/* Makefile
	$(call build_proto,$@,${PROTO12})

src/proto_013_PtJakart:src/proto_meta/* Makefile
	$(call build_proto,$@,${PROTO13})

src/proto_014_PtKathma:src/proto_meta/* Makefile
	$(call build_proto,$@,${PROTO14})

src/proto_015_PtLimaPt:src/proto_meta/* Makefile
	$(call build_proto,$@,${PROTO15})

src/proto_016_PtMumbai:src/proto_meta/* Makefile
	$(call build_proto,$@,${PROTO16})

src/proto_017_PtNairob:src/proto_meta/* Makefile
	$(call build_proto_with_mempool,$@,${PROTO17})

src/proto_018_Proxford:src/proto_meta/* Makefile
	$(call build_proto_with_mempool,$@,${PROTO18})

# __NEW_PROTO__ 19
# src/proto_019_P:src/proto_meta/* Makefile
# 	$(call build_proto_with_mempool,$@,${PROTO19})


ifdef ALPHA
src/proto_alpha:src/proto_meta/* Makefile
	$(call build_proto_with_mempool,$@,${PROTOA})
else
src/proto_alpha:
	rm -fr src/proto_alpha
	echo "export ALPHA if you want support for proto alpha"
endif

src/bin_indexer/chain_db_transition_001_002.ml:src/proto_meta/chain_db.ml Makefile
	${COMMENTS_HELPER} < $< | ${TRANS_1_2} > $@

src/bin_indexer/chain_db_transition_001_002.mli:src/proto_meta/chain_db.mli Makefile
	${COMMENTS_HELPER} < $< | ${TRANS_1_2} > $@

src/bin_indexer/chain_db_transition_002_003.ml:src/proto_meta/chain_db.ml Makefile
	${COMMENTS_HELPER} < $< | ${TRANS_2_3} >  $@

src/bin_indexer/chain_db_transition_002_003.mli:src/proto_meta/chain_db.mli Makefile
	${COMMENTS_HELPER} < $< | ${TRANS_2_3} >  $@

src/bin_indexer/chain_db_transition_003_004.ml:src/proto_meta/chain_db.ml Makefile
	${COMMENTS_HELPER} < $< | ${TRANS_3_4} >  $@

src/bin_indexer/chain_db_transition_003_004.mli:src/proto_meta/chain_db.mli Makefile
	${COMMENTS_HELPER} < $< | ${TRANS_3_4} >  $@

src/bin_indexer/chain_db_transition_004_005.ml:src/proto_meta/chain_db.ml Makefile
	${COMMENTS_HELPER} < $< | ${TRANS_4_5} >  $@

src/bin_indexer/chain_db_transition_004_005.mli:src/proto_meta/chain_db.mli Makefile
	${COMMENTS_HELPER} < $< | ${TRANS_4_5} >  $@

src/bin_indexer/chain_db_transition_005_006.ml:src/proto_meta/chain_db.ml Makefile
	${COMMENTS_HELPER} < $< | ${TRANS_5_6} > $@

src/bin_indexer/chain_db_transition_005_006.mli:src/proto_meta/chain_db.mli Makefile
	${COMMENTS_HELPER} < $< | ${TRANS_5_6} > $@

src/bin_indexer/chain_db_transition_006_007.ml:src/proto_meta/chain_db.ml Makefile
	${COMMENTS_HELPER} < $< | ${TRANS_6_7} > $@

src/bin_indexer/chain_db_transition_006_007.mli:src/proto_meta/chain_db.mli Makefile
	${COMMENTS_HELPER} < $< | ${TRANS_6_7} > $@

src/bin_indexer/chain_db_transition_007_008.ml:src/proto_meta/chain_db.ml Makefile
	${COMMENTS_HELPER} < $< | ${TRANS_7_8} > $@

src/bin_indexer/chain_db_transition_007_008.mli:src/proto_meta/chain_db.mli Makefile
	${COMMENTS_HELPER} < $< | ${TRANS_7_8} > $@

src/bin_indexer/chain_db_transition_008_009.ml:src/proto_meta/chain_db.ml Makefile
	${COMMENTS_HELPER} < $< | ${TRANS_8_9} > $@

src/bin_indexer/chain_db_transition_008_009.mli:src/proto_meta/chain_db.mli Makefile
	${COMMENTS_HELPER} < $< | ${TRANS_8_9} > $@

src/bin_indexer/chain_db_transition_009_010.ml:src/proto_meta/chain_db.ml Makefile
	${COMMENTS_HELPER} < $< | ${TRANS_9_10} > $@

src/bin_indexer/chain_db_transition_009_010.mli:src/proto_meta/chain_db.mli Makefile
	${COMMENTS_HELPER} < $< | ${TRANS_9_10} > $@

src/bin_indexer/chain_db_transition_010_011.ml:src/proto_meta/chain_db.ml Makefile
	${COMMENTS_HELPER} < $< | ${TRANS_10_11} > $@

src/bin_indexer/chain_db_transition_010_011.mli:src/proto_meta/chain_db.mli Makefile
	${COMMENTS_HELPER} < $< | ${TRANS_10_11} > $@

src/bin_indexer/chain_db_transition_011_012.ml:src/proto_meta/chain_db.ml Makefile
	${COMMENTS_HELPER} < $< | ${TRANS_11_12} > $@

src/bin_indexer/chain_db_transition_011_012.mli:src/proto_meta/chain_db.mli Makefile
	${COMMENTS_HELPER} < $< | ${TRANS_11_12} > $@

src/bin_indexer/chain_db_transition_012_013.ml:src/proto_meta/chain_db.ml Makefile
	${COMMENTS_HELPER} < $< | ${TRANS_12_13} > $@

src/bin_indexer/chain_db_transition_012_013.mli:src/proto_meta/chain_db.mli Makefile
	${COMMENTS_HELPER} < $< | ${TRANS_12_13} > $@

src/bin_indexer/chain_db_transition_013_014.ml:src/proto_meta/chain_db.ml Makefile
	${COMMENTS_HELPER} < $< | ${TRANS_13_14} > $@

src/bin_indexer/chain_db_transition_013_014.mli:src/proto_meta/chain_db.mli Makefile
	${COMMENTS_HELPER} < $< | ${TRANS_13_14} > $@

src/bin_indexer/chain_db_transition_014_015.ml:src/proto_meta/chain_db.ml Makefile
	${COMMENTS_HELPER} < $< | ${TRANS_14_15} > $@

src/bin_indexer/chain_db_transition_014_015.mli:src/proto_meta/chain_db.mli Makefile
	${COMMENTS_HELPER} < $< | ${TRANS_14_15} > $@

src/bin_indexer/chain_db_transition_015_016.ml:src/proto_meta/chain_db.ml Makefile
	${COMMENTS_HELPER} < $< | ${TRANS_15_16} > $@

src/bin_indexer/chain_db_transition_015_016.mli:src/proto_meta/chain_db.mli Makefile
	${COMMENTS_HELPER} < $< | ${TRANS_15_16} > $@

src/bin_indexer/chain_db_transition_016_017.ml:src/proto_meta/chain_db.ml Makefile
	${COMMENTS_HELPER} < $< | ${TRANS_16_17} > $@

src/bin_indexer/chain_db_transition_016_017.mli:src/proto_meta/chain_db.mli Makefile
	${COMMENTS_HELPER} < $< | ${TRANS_16_17} > $@

src/bin_indexer/chain_db_transition_017_018.ml:src/proto_meta/chain_db.ml Makefile
	${COMMENTS_HELPER} < $< | ${TRANS_17_18} > $@

src/bin_indexer/chain_db_transition_017_018.mli:src/proto_meta/chain_db.mli Makefile
	${COMMENTS_HELPER} < $< | ${TRANS_17_18} > $@

# __NEW_PROTO__ 19
src/bin_indexer/chain_db_transition_017_A.ml:src/proto_meta/chain_db.ml Makefile
	${COMMENTS_HELPER} < $< | ${TRANS_17_A} > $@

src/bin_indexer/chain_db_transition_017_A.mli:src/proto_meta/chain_db.mli Makefile
	${COMMENTS_HELPER} < $< | ${TRANS_17_A} > $@

src/bin_indexer/chain_db_transition_018_A.ml:src/proto_meta/chain_db.ml Makefile
	${COMMENTS_HELPER} < $< | ${TRANS_18_A} > $@

src/bin_indexer/chain_db_transition_018_A.mli:src/proto_meta/chain_db.mli Makefile
	${COMMENTS_HELPER} < $< | ${TRANS_18_A} > $@

# __NEW_PROTO__ 19
# src/bin_indexer/chain_db_transition_019_A.ml:src/proto_meta/chain_db.ml Makefile
# 	${COMMENTS_HELPER} < $< | ${TRANS_19_A} > $@

# src/bin_indexer/chain_db_transition_019_A.mli:src/proto_meta/chain_db.mli Makefile
# 	${COMMENTS_HELPER} < $< | ${TRANS_19_A} > $@


clean:
	find . -name '*~' -delete
	rm -fr tezos-indexer $(GENERATED_FILES)
	rm -fr _build src/bin_indexer/.merlin src/bin_indexer/tezos-indexer.install src/lib_indexer/.merlin src/lib_indexer/tezos-indexer-lib.install

test-psql:
	bash utils/test-psql.bash



update-README-help:
	( IFS= ; while read l ; do \
	   echo "$$l" ; \
	   if grep -q '<!-- --help -- do not manually edit this or after this line -->' <<< "$$l"; \
	   then break; \
	   fi; \
	done < README.md ; echo '```'; ./tezos-indexer --help=plain ; echo '```') | sed -e 's/ *$$//' > README.md.tmp ; mv README.md.tmp README.md

.PHONY: clean protos test-psql db-schema-all-heavy db-schema-all-light db-schema-mezos db-schema-mezos-base db-schema-mezos-extra db-schema-all-default db-schema
