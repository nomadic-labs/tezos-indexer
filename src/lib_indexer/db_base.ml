(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2018 Dynamic Ledger Solutions, Inc. <contact@tezos.com>     *)
(* Copyright (c) 2019-2022 Nomadic Labs, <contact@nomadic-labs.com>          *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

module Env_V0 = Tezos_protocol_environment.V0.Make(
  struct let name = "Tezos_indexer_lib.Db_base" end
  )()

module Original = struct
  module Lwt = Lwt
  module Lwt_list = Lwt_list
  let ( >>= ) = Lwt.bind
end


open Env_V0

open Original


open Caqti_type.Std

let caqti_or_fail ~__LOC__ =
  function
  | Error e ->
    Stdlib.Format.eprintf "Database error: %a%a\n%!"
      Caqti_error.pp e
      (fun out e -> if !Verbose.debug then Format.fprintf out " %s" e) __LOC__;
    Stdlib.exit Verbose.ExitCodes.database_error
  | Ok e -> Lwt.return e

let caqti_or_fail_msg ~msg ~__LOC__ =
  function
  | Error e ->
    Stdlib.Format.eprintf "Database error: %a%a\n%s\n%!"
      Caqti_error.pp e
      (fun out e -> if !Verbose.debug then Format.fprintf out " %s" e) __LOC__
      msg;
    Stdlib.exit Verbose.ExitCodes.database_error
  | Ok e -> Lwt.return e

let caqti_BUT_NO_FAIL ~msg ~cmd ~__LOC__ =
  function
  | Error e ->
    let em =
      Stdlib.Format.asprintf "Database error: %a%a\n%s\n"
        Caqti_error.pp e
        (fun out e -> if !Verbose.debug then Format.fprintf out " %s" e) __LOC__
        msg
    in
    begin match cmd with
      | None -> ()
      | Some cmd ->
        let e = Stdlib.Format.asprintf "echo -e %S | %s" em cmd in
        match Sys.command e with
        | 0 -> ()
        | n ->
          Stdlib.Format.eprintf "Command <<%s>> exited with error code %d. I'll exit with the same error code.\n" e n;
          Stdlib.exit n
    end;
    if !Verbose.debug then Stdlib.Format.eprintf "%s%!" em;
    Lwt.return_none
  | Ok e -> Lwt.return_some e


let connect url =
  let ( >>= ) = Lwt.bind in
  Lwt.catch (fun () ->
      Caqti_lwt.connect url >>=
      caqti_or_fail ~__LOC__
    ) (fun e ->
      Printf.eprintf "Database connection error: %s\n\
                      To get more information about specifying how to connect \
                      to the database, run:\n%s --help\n"
        (Printexc.to_string e)
        Sys.argv.(0);
      exit Verbose.ExitCodes.database_connection
    )


let to_caqti_error = function
  | Ok a -> Ok a
  | Error e ->
    let open Tezos_base__TzPervasives in
    Error (Format.asprintf "%a" pp_print_trace e)

let without_transaction conn request elts =
  let module Conn = (val conn : Caqti_lwt.CONNECTION) in
  Lwt_list.iter_s begin fun elt ->
    Conn.exec request elt >>=
    caqti_or_fail ~__LOC__
  end elts

let without_transaction_ignore conn request elts =
  let module Conn = (val conn : Caqti_lwt.CONNECTION) in
  Lwt_list.iter_s begin fun elt ->
    Conn.find_opt request elt >>=
    caqti_or_fail ~__LOC__ >>= fun _ ->
    Lwt.return_unit
  end elts

(* let without_transaction_pool pool request elts =
 *   Caqti_lwt.Pool.use
 *     (fun dbh ->
 *        let module Conn = (val dbh : Caqti_lwt.CONNECTION) in
 *        let rec iter f = function
 *          | [] -> return ()
 *          | e::tl ->
 *            Lwt.bind
 *              (f e)
 *              (fun () -> iter f tl)
 *        in
 *        iter
 *          (fun elt -> Conn.exec request elt >>= caqti_or_fail ~__LOC__ >>= fun _ -> Lwt.return_unit)
 *          elts
 *        >>= fun _ ->
 *        Lwt.return (Ok ())
 *     ) pool >>= fun _ -> Lwt.return_unit *)

let without_transaction_pool_ignore pool request elts =
  Caqti_lwt.Pool.use
    (fun dbh ->
       let module Conn = (val dbh : Caqti_lwt.CONNECTION) in
       let rec iter f = function
         | [] -> Lwt.return_ok ()
         | e::tl ->
           Lwt.bind
             (f e)
             (fun () -> iter f tl)
       in
       iter
         (fun elt -> Conn.find_opt request elt >>= caqti_or_fail ~__LOC__ >>= fun _ -> Lwt.return_unit)
         elts
       >>= fun _ ->
       Lwt.return (Ok ())
    ) pool >>= fun _ -> Lwt.return_unit


let find_opt pool f x =
  Caqti_lwt.Pool.use
    (fun dbh ->
      let module Conn = (val dbh : Caqti_lwt.CONNECTION) in
      Conn.find_opt f x
    )
    pool

let bh =
  custom
    ~encode:(fun a -> Ok (Block_hash.to_b58check a))
    ~decode:(fun a -> to_caqti_error (Tezos_crypto.Hashed.Block_hash.of_b58check a))
    string

let chain_id =
  custom
    ~encode:(fun a -> Ok (Chain_id.to_b58check a))
    ~decode:(fun a -> to_caqti_error (Tezos_crypto.Hashed.Chain_id.of_b58check a))
    string

let time =
  (* let open Tezos_base__TzPervasives in
   * let open Tezos_stdlib_unix in *)
  let open Env_V0.Time in
  (* let open Env_V0.Time.Protocol in *)
  custom
    ~encode:begin fun a ->
      match Ptime.of_float_s (Int64.to_float (to_seconds a)) with
      | None -> Error "time"
      | Some v -> Ok v
    end
    ~decode:(fun a -> Ok (of_seconds (Int64.of_float (Ptime.to_float_s a))))
    ptime

let fitness =
  custom
    ~encode:(fun a -> let `Hex a = Env_V0.MBytes.to_hex (Fitness.to_bytes a) in Ok a)
    ~decode:(fun a -> match Fitness.of_bytes (Env_V0.MBytes.of_string a) with
        | Some a -> Ok a
        | None -> Error "fitness")
    string

let pkh =
  custom
    ~encode:(fun a -> Ok (Tezos_crypto.Signature.V0.Public_key_hash.to_b58check a))
    ~decode:(fun a -> to_caqti_error (Tezos_crypto.Signature.V0.Public_key_hash.of_b58check a))
    string

let pk =
  custom
    ~encode:(fun a -> Ok (Tezos_crypto.Signature.V0.Public_key.to_b58check a))
    ~decode:(fun a -> to_caqti_error (Tezos_crypto.Signature.V0.Public_key.of_b58check a))
    string


let operation_list_list_hash =
  custom
    ~encode:(fun a -> Ok (Operation_list_list_hash.to_b58check a))
    ~decode:(fun a -> to_caqti_error (Tezos_crypto.Hashed.Operation_list_list_hash.of_b58check a))
    string

let context_hash =
  custom
    ~encode:(fun a -> Ok (Context_hash.to_b58check a))
    ~decode:(fun a -> to_caqti_error (Tezos_crypto.Hashed.Context_hash.of_b58check a))
    string

let shell_header =
  let open Block_header in
  custom
    ~encode:begin fun {
      level ; proto_level ; predecessor ; timestamp ;
      validation_passes ; operations_hash ; fitness ; context } ->
      Ok ((level, proto_level, predecessor, timestamp),
          (validation_passes, operations_hash, fitness, context))
    end
    ~decode:begin fun
      ((level, proto_level, predecessor, timestamp),
       (validation_passes, operations_hash, fitness, context)) ->
      Ok { level ; proto_level ; predecessor ; timestamp ;
           validation_passes ; operations_hash ; fitness ; context }
    end
    (tup2
       (tup4 int32 int16 bh time)
       (tup4 int16 operation_list_list_hash fitness context_hash))

let json =
  custom
    ~encode:(fun json -> Ok (Tezos_base__TzPervasives.Data_encoding.Json.to_string json))
    ~decode:Tezos_base__TzPervasives.Data_encoding.Json.from_string
    string

let integer =
  custom
    ~encode:(fun a -> Ok(Int32.of_int a))
    ~decode:(fun a -> Ok(Int32.to_int a))
    int32

let bigint = int

(* let z =
 *   custom
 *     ~encode:(fun a -> Ok (let `Hex a = Hex.of_string (Z.to_bits a) in a))
 *     ~decode:(function a -> Ok (Z.of_bits (Hex.to_string (`Hex a))))
 *     octets
 * let milligas =
 *   custom
 *     ~encode:(fun a -> Ok (let `Hex a = Hex.of_string (Z.to_bits (Fpgas.unsafe_z_of_t a)) in a))
 *     ~decode:(function a -> Ok (Z.of_bits (Hex.to_string (`Hex a)) |> Fpgas.unsafe_t_of_z))
 *     octets *)

let z =
  custom
    ~encode:(fun a -> Ok (Z.to_string a))
    ~decode:(function a -> Ok (Z.of_string a))
    string

let milligas =
  custom
    ~encode:(fun a -> Ok (Z.to_string (Fpgas.unsafe_z_of_t a)))
    ~decode:(function a -> Ok (Z.of_string a |> Fpgas.unsafe_t_of_z))
    string

let oph =
  custom
    ~encode:(fun a -> Ok (Operation_hash.to_b58check a))
    ~decode:(fun a -> to_caqti_error (Tezos_crypto.Hashed.Operation_hash.of_b58check a))
    string

type block_level = int32
type kid = int64
type ophid = int64

let kid = int64
let block_level = int32
let ophid = int64

open Caqti_request

let query_zero input output q =
  (* do not use this if you're using a `select` *)
  create input output Caqti_mult.zero (fun _ -> Caqti_query.of_string_exn q)

let query_zero_or_one input output q =
  create input output Caqti_mult.zero_or_one (fun _ -> Caqti_query.of_string_exn q)

let query_zero_or_more input output q =
  create input output Caqti_mult.zero_or_more (fun _ -> Caqti_query.of_string_exn q)

let query_one input output q =
  create input output Caqti_mult.one (fun _ -> Caqti_query.of_string_exn q)


module Indexer_log = struct
  let record =
    query_zero (tup2 string string) unit
      "insert into indexer_log(version,argv)values($1,$2) on conflict do nothing"
  let record_action =
    query_zero string unit
      "insert into indexer_log(action)values($1) on conflict do nothing"
  let block_indexed =
    query_zero bh unit
      "update indexer_measurements set ended = CURRENT_TIMESTAMP where block_hash = $1"
  let notify_block_indexed =
    query_zero unit unit
      "NOTIFY block_indexed"
  let notify_block_deleted =
    query_zero unit unit
      "NOTIFY block_deleted"
  let notify_failure =
    query_zero unit unit
      "NOTIFY indexer_failed"
end

module Chain_id_table = struct
  let insert =
    query_zero_or_one chain_id unit
      "select I.chain(?)"
end

module Block_table = struct
  let insert0 =
    query_one
      (tup2 bh shell_header)
      (option block_level)
      "select I.block0($1,$2,$3,$4,$5,$6,$7,$8,$9)"

  let insert =
    query_one
      (tup2 bh shell_header)
      (option block_level)
      "select I.block($1,$2,$3,$4,$5,$6,$7,$8,$9)"

  let booted =
    query_one
      unit
      bool
      "select count(*) = 2 from c.block where level in (0, 1)"

  let select_max_level_with_bh =
    query_zero_or_one
      unit
      (tup2 block_level bh)
      "select * from max_level_bh()"

  let select_max_level =
    query_zero_or_one
      unit
      block_level
      "select max_level()"

  let get_block_level =
    query_zero_or_one
      bh
      block_level
      "select block_level($1)"

  let delete_last_block =
    query_zero_or_one
      unit
      block_level
      "select delete_last_block()"
end

module Operation_table = struct
  (* let insert =
   *   query_zero_or_one
   *     (tup4 oph chain_id block_level ophid)
   *     ophid
   *     "select I.operation(?,?,?,?)" *)
  let insert =
    query_zero_or_one
      (tup3 oph block_level ophid)
      ophid
      "select I.operation(?,?,?)"
end

module Deactivated_delegate_table = struct
  let insert =
    query_zero_or_one
      (tup2 kid block_level)
      unit
      "select I.deactivated(?,?)"
end


module Addresses = struct
  type t = string
  let make (x: string) : t = x
  let address = string
  let insert =
    query_zero_or_one
      (tup2 address kid)
      kid
      "select I.address(?,?)"
end


module Snapshot_table = struct
  let update_snapshot =
    query_zero_or_one
      (tup2 integer block_level)
      unit
      "select I.snapshot(?,?)"

  let store_snapshot_levels pool levels =
    without_transaction_pool_ignore pool update_snapshot levels

end


module Version = struct
  let select =
    query_zero_or_one
      unit
      (tup3 string bool bool)
      "select version, dev, multicore from indexer_version order by autoid desc limit 1"
end
