(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2021-2022 Nomadic Labs, <contact@nomadic-labs.com>          *)
(* Copyright (c) 2021 Philippe Wang <philippe.wang@gmail.com>                *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

let no_big_maps = ref false

let multicore_mode = ref false

let extracted_address_length_limit = ref 1024

let no_smart_contract_extraction = ref false

let dev_mode = ref false

let print_opspeed = ref false

let auto_recovery_delay = ref 0.
let max_auto_recovery_delay = ref 60.
let increase_auto_recovery_delay () =
  auto_recovery_delay :=
    min !max_auto_recovery_delay (max 1. (!auto_recovery_delay *. 1.5))

let auto_recovery_credit = ref 0

let node_timeout = ref 0.

let notify = ref false

let benchmarking =
  match Sys.getenv "INDEXER_BENCHMARKING" with
  | "false" | "0" -> false
  | _ -> true
  | exception _ -> false

let alert_cmd : string option ref = ref None
