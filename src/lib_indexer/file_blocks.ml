(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2020-2022 Nomadic Labs, <contact@nomadic-labs.com>          *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

module Make (Unit: sig end) = struct

  let blocks_per_file = 1000

  let last_block_level = ref 0
  let current_stream_end = ref 0

  let line_stream_of_file file =
    let ic = open_in_bin file in
    object
      method get =
        match input_line ic with
        | l -> incr last_block_level; Some l
        | exception End_of_file -> close_in ic; None
        | exception _ -> close_in ic; None
      method close =
        close_in ic
    end


  let current_stream = ref (object method get = None method close = () end)

  let file_name ~dir ~first =
    (* with %07d, we should be good until after 2030
         at 1 block per minute on mainnet *)
    Printf.sprintf "%s/%07d.tzblocks" dir first

  let first ~n =
    (n-1) / blocks_per_file * blocks_per_file + 1

  let load_file ?(dir=".") n =
    let first = first ~n in
    let filename = file_name ~dir ~first in
    last_block_level := first - 1;
    current_stream_end := first + blocks_per_file -1;
    (!current_stream)#close;
    current_stream := line_stream_of_file filename

  let rec get_block ?dir m =
    let n = Int32.to_int m in
    if n = !last_block_level + 1 then
      if n <= !current_stream_end then
        (!current_stream)#get
      else
        begin
          load_file ?dir n;
          get_block ?dir m
        end
    else
      begin
        load_file ?dir n;
        for _ = first ~n to n - 1 do
          ignore @@ (!current_stream)#get
        done;
        (!current_stream)#get
      end


  let current_write_file = ref None

  let rec mkdir d =
    if Sys.file_exists d then
      if Sys.is_directory d then
        ()
      else
        Verbose.eprintf "Given directory (%s) exists but is not a directory" d
    else
      begin
        mkdir (Filename.dirname d);
        Unix.mkdir d 0o755
      end

  let _write_file ?(dir=".") n ~block =
    let write_new_file filename block =
      mkdir dir;
      let chan = open_out_bin filename in
      current_write_file := Some (filename, chan);
      output_string chan block
    in
    let first = first ~n in
    let filename = file_name ~dir ~first in
    match !current_write_file with
    | Some (name, chan) when filename = name ->
      output_string chan block
    | Some (_, chan) ->
      close_out chan;
      write_new_file filename block
    | None ->
      write_new_file filename block

end

include Make(struct end)
