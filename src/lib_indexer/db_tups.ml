(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2022 Nomadic Labs, <contact@nomadic-labs.com>          *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

open Caqti_type.Std

type ('a, 'b) tup2 = 'a * 'b
type ('a, 'b, 'c) tup3 = 'a * 'b * 'c
type ('a, 'b, 'c, 'd) tup4 = 'a * 'b * 'c * 'd
type ('a, 'b, 'c, 'd, 'e) tup5 = ('a * 'b * 'c * 'd) * 'e
type ('a, 'b, 'c, 'd, 'e, 'f) tup6 = ('a * 'b * 'c * 'd) * 'e * 'f
type ('a, 'b, 'c, 'd, 'e, 'f, 'g) tup7 = ('a * 'b * 'c * 'd) * 'e * 'f * 'g
type ('a, 'b, 'c, 'd, 'e, 'f, 'g, 'h) tup8 = ('a * 'b * 'c * 'd) * ('e * 'f * 'g * 'h)
type ('a, 'b, 'c, 'd, 'e, 'f, 'g, 'h, 'i) tup9 = ('a * 'b * 'c * 'd) * ('e * 'f * 'g * 'h) * 'i
type ('a, 'b, 'c, 'd, 'e, 'f, 'g, 'h, 'i, 'j) tup10 = ('a * 'b * 'c * 'd) * ('e * 'f * 'g * 'h) * 'i * 'j
type ('a, 'b, 'c, 'd, 'e, 'f, 'g, 'h, 'i, 'j, 'k) tup11 = ('a * 'b * 'c * 'd) * ('e * 'f * 'g * 'h) * ('i * 'j * 'k)
type ('a, 'b, 'c, 'd, 'e, 'f, 'g, 'h, 'i, 'j, 'k, 'l) tup12 = ('a * 'b * 'c * 'd) * ('e * 'f * 'g * 'h) * ('i * 'j * 'k * 'l)
type ('a, 'b, 'c, 'd, 'e, 'f, 'g, 'h, 'i, 'j, 'k, 'l, 'm) tup13 = ('a * 'b * 'c * 'd) * ('e * 'f * 'g * 'h) * ('i * 'j * 'k * 'l) * 'm
type ('a, 'b, 'c, 'd, 'e, 'f, 'g, 'h, 'i, 'j, 'k, 'l, 'm, 'n) tup14 = ('a * 'b * 'c * 'd) * ('e * 'f * 'g * 'h) * ('i * 'j * 'k * 'l) * ('m * 'n)
type ('a, 'b, 'c, 'd, 'e, 'f, 'g, 'h, 'i, 'j, 'k, 'l, 'm, 'n, 'o) tup15 = ('a * 'b * 'c * 'd) * ('e * 'f * 'g * 'h) * ('i * 'j * 'k * 'l) * ('m * 'n * 'o)
type ('a, 'b, 'c, 'd, 'e, 'f, 'g, 'h, 'i, 'j, 'k, 'l, 'm, 'n, 'o, 'p) tup16 = ('a * 'b * 'c * 'd) * ('e * 'f * 'g * 'h) * ('i * 'j * 'k * 'l) * ('m * 'n * 'o * 'p)
type ('a, 'b, 'c, 'd, 'e, 'f, 'g, 'h, 'i, 'j, 'k, 'l, 'm, 'n, 'o, 'p, 'q) tup17 = (('a * 'b * 'c * 'd) * ('e * 'f * 'g * 'h) * ('i * 'j * 'k * 'l) * ('m * 'n * 'o * 'p)) * 'q
type ('a, 'b, 'c, 'd, 'e, 'f, 'g, 'h, 'i, 'j, 'k, 'l, 'm, 'n, 'o, 'p, 'q, 'r) tup18 = (('a * 'b * 'c * 'd) * ('e * 'f * 'g * 'h) * ('i * 'j * 'k * 'l) * ('m * 'n * 'o * 'p)) * 'q * 'r
type ('a, 'b, 'c, 'd, 'e, 'f, 'g, 'h, 'i, 'j, 'k, 'l, 'm, 'n, 'o, 'p, 'q, 'r, 's) tup19 = (('a * 'b * 'c * 'd) * ('e * 'f * 'g * 'h) * ('i * 'j * 'k * 'l) * ('m * 'n * 'o * 'p)) * 'q * 'r * 's

let tup5 a b c d e = tup2 (tup4 a b c d) e
let tup6 a b c d e f = tup3 (tup4 a b c d) e f
let tup7 a b c d e f g = tup4 (tup4 a b c d) e f g
let tup8 a b c d e f g h = tup2 (tup4 a b c d) (tup4 e f g h)
let tup9 a b c d e f g h i = tup3 (tup4 a b c d) (tup4 e f g h) i
let tup10 a b c d e f g h i j = tup4 (tup4 a b c d) (tup4 e f g h) i j
let tup11 a b c d e f g h i j k = tup3 (tup4 a b c d) (tup4 e f g h) (tup3 i j k)
let tup12 a b c d e f g h i j k l = tup3 (tup4 a b c d) (tup4 e f g h) (tup4 i j k l)
let tup13 a b c d e f g h i j k l m = tup4 (tup4 a b c d) (tup4 e f g h) (tup4 i j k l) m
let tup14 a b c d e f g h i j k l m n = tup4 (tup4 a b c d) (tup4 e f g h) (tup4 i j k l) (tup2 m n)
let tup15 a b c d e f g h i j k l m n o = tup4 (tup4 a b c d) (tup4 e f g h) (tup4 i j k l) (tup3 m n o)
let tup16 a b c d e f g h i j k l m n o p = tup4 (tup4 a b c d) (tup4 e f g h) (tup4 i j k l) (tup4 m n o p)
let tup17 a b c d e f g h i j k l m n o p q = tup2 (tup4 (tup4 a b c d) (tup4 e f g h) (tup4 i j k l) (tup4 m n o p)) q
let tup18 a b c d e f g h i j k l m n o p q r = tup3 (tup4 (tup4 a b c d) (tup4 e f g h) (tup4 i j k l) (tup4 m n o p)) q r
let tup19 a b c d e f g h i j k l m n o p q r s = tup4 (tup4 (tup4 a b c d) (tup4 e f g h) (tup4 i j k l) (tup4 m n o p)) q r s

let mtup2 a b = a, b
let mtup3 a b c = a, b, c
let mtup4 a b c d = a, b, c, d
let mtup5 a b c d e = mtup2 (mtup4 a b c d) e
let mtup6 a b c d e f = mtup3 (mtup4 a b c d) e f
let mtup7 a b c d e f g = mtup4 (mtup4 a b c d) e f g
let mtup8 a b c d e f g h = mtup2 (mtup4 a b c d) (mtup4 e f g h)
let mtup9 a b c d e f g h i = mtup3 (mtup4 a b c d) (mtup4 e f g h) i
let mtup10 a b c d e f g h i j = mtup4 (mtup4 a b c d) (mtup4 e f g h) i j
let mtup11 a b c d e f g h i j k = mtup3 (mtup4 a b c d) (mtup4 e f g h) (mtup3 i j k)
let mtup12 a b c d e f g h i j k l = mtup3 (mtup4 a b c d) (mtup4 e f g h) (mtup4 i j k l)
let mtup13 a b c d e f g h i j k l m = mtup4 (mtup4 a b c d) (mtup4 e f g h) (mtup4 i j k l) m
let mtup14 a b c d e f g h i j k l m n = mtup4 (mtup4 a b c d) (mtup4 e f g h) (mtup4 i j k l) (mtup2 m n)
let mtup15 a b c d e f g h i j k l m n o = mtup4 (mtup4 a b c d) (mtup4 e f g h) (mtup4 i j k l) (mtup3 m n o)
let mtup16 a b c d e f g h i j k l m n o p = mtup4 (mtup4 a b c d) (mtup4 e f g h) (mtup4 i j k l) (mtup4 m n o p)
let mtup17 a b c d e f g h i j k l m n o p q = mtup2 (mtup4 (mtup4 a b c d) (mtup4 e f g h) (mtup4 i j k l) (mtup4 m n o p)) q
let mtup18 a b c d e f g h i j k l m n o p q r = mtup3 (mtup4 (mtup4 a b c d) (mtup4 e f g h) (mtup4 i j k l) (mtup4 m n o p)) q r
let mtup19 a b c d e f g h i j k l m n o p q r s = mtup4 (mtup4 (mtup4 a b c d) (mtup4 e f g h) (mtup4 i j k l) (mtup4 m n o p)) q r s
