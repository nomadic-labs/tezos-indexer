(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2021-2022 Nomadic Labs, <contact@nomadic-labs.com>          *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

open Cmdliner

module Convs : sig val uri : Uri.t Arg.conv end

module Terms :
sig
  val tezos_client_dir : string Term.t
  val uri :
    default:Uri.t ->
    doc:string -> args:string list -> Uri.t Term.t
  val uri_option :
    ?default:Uri.t ->
    doc:string -> args:string list -> unit -> Uri.t option Term.t
  val db : default:Uri.t -> Uri.t Term.t
end

module Options_cmd :
  sig
    val force_from_cmd : int32 Term.t
    val use_disk_cache_cmd : bool Term.t
    val first_block_level_cmd : int32 Term.t
    val uri_option_cmd : Uri.t option Term.t
    val tezos_client_dir_cmd : string Term.t
    val db_cmd : Uri.t Term.t
    val heads_only_cmd : bool Term.t
    val no_snapshot_blocks_cmd : bool Term.t
    val snapshot_blocks_only_cmd : bool Term.t
    val verbose_mode_cmd : bool Term.t
    val debug_mode_cmd : bool Term.t
    val notify_cmd : bool Term.t
    val print_db_schema_cmd : bool Term.t
    val print_db_schema_multicore_cmd : bool Term.t
    val make_conn2_cmd : bool Term.t
    val watch_hook_cmd : string Term.t
    val version_cmd : bool Term.t
    val mempool_indexing_cmd : bool Term.t
    val verbosity_cmd : int Term.t
    val balance_updates_only_cmd : bool Term.t
    val no_balance_updates_cmd : bool Term.t
    val no_smart_contract_extraction : bool Term.t
    val cache_blocks_dir_cmd : string Term.t
    val ignore_db_version_cmd : bool Term.t
    val tokens_support_cmd : bool Term.t
    val skip_babylon_contracts_cmd : bool Term.t
    val do_not_get_missing_scripts_cmd : bool Term.t
    val no_big_maps_cmd : bool Term.t
    val up_to_cmd : int32 Term.t
    val no_watch_cmd : bool Term.t
    val auto_recovery_credit_cmd : int Term.t
    val print_opspeed_cmd : bool Term.t
    val binary_cmd : bool Term.t
    val textual_cmd : bool Term.t
    val extracted_address_length_limit : int Term.t
    val timeout_cmd : string Term.t
    val alert_cmd : string Term.t
    val node_timeout_cmd : float Term.t
  end
