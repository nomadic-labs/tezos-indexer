(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2021-2022 Nomadic Labs, <contact@nomadic-labs.com>          *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

open Legacy_monad_globals
let ( >>= ) = Lwt.bind

(** This module is an excerpt of the FA2 support in octez' lib_client, currently
   in development. *)

open Protocol_client_context
open Protocol
open Alpha_context
open Tezos_micheline

(**# ifdef __META12__
let is_implicit = function Alpha_context.Contract.Implicit _ -> true | _ -> false
#**)(**# else
let is_implicit k = match Contract.is_implicit k with Some _ -> true | _ -> false
#**)

type error += Contract_has_no_script of Contract.t

type error += Contract_has_no_storage of Contract.t

type error += Entrypoint_mismatch of string * (Script.expr * Script.expr) option

type error += Unexpected_error of Script.location * Script.expr

type error += Action_unwrapping_error of string * Script.expr

let entrypoint_mismatch_explanation ppf (name, ty) =
  match ty with
  | None -> Format.fprintf ppf "Entrypoint %s is missing" name
  | Some (ty, expected) ->
      Format.fprintf
        ppf
        "Entrypoint \"%s\" has type @[%a@], but should have type @[%a@]"
        name
        Michelson_v1_printer.print_expr
        ty
        Michelson_v1_printer.print_expr
        expected

let () =
  register_error_kind
    `Permanent
    ~id:"fa2ContractHasNoScript"
    ~title:"The given contract is not a smart contract"
    ~description:"An FA2 command has referenced a scriptless contract."
    ~pp:(fun ppf contract ->
      Format.fprintf
        ppf
        "Contract %a is not a smart contract, it has no script."
        Contract.pp
        contract)
    Data_encoding.(obj1 (req "contract" Contract.encoding))
    (function Contract_has_no_script c -> Some c | _ -> None)
    (fun c -> Contract_has_no_script c) ;
  register_error_kind
    `Permanent
    ~id:"fa2ContractHasNoStorage"
    ~title:"The given contract has no storage"
    ~description:"An FA2 command made a call on a contract that has no storage."
    ~pp:(fun ppf contract ->
      Format.fprintf ppf "Contract %a has no storage." Contract.pp contract)
    Data_encoding.(obj1 (req "contract" Contract.encoding))
    (function Contract_has_no_storage c -> Some c | _ -> None)
    (fun c -> Contract_has_no_storage c) ;
  register_error_kind
    `Permanent
    ~id:"fa2entrypointMismatch"
    ~title:"The given contract does not implement the FA2 interface"
    ~description:
      "An FA2 command has referenced a smart contract whose script does not \
       implement at least one FA2 entrypoint, or with an incompatible type. \n\
       See \
       https://gitlab.com/tzip/tzip/-/blob/master/proposals/tzip-12/tzip-12.md \
       for documentation on FA2."
    ~pp:(fun ppf (name, ty) ->
      Format.fprintf
        ppf
        "Not a supported FA2 contract.@\n%a."
        entrypoint_mismatch_explanation
        (name, ty))
    Data_encoding.(
      obj2
        (req "name" string)
        (req "type" (option (tup2 Script.expr_encoding Script.expr_encoding))))
    (function Entrypoint_mismatch (n, t) -> Some (n, t) | _ -> None)
    (fun (n, t) -> Entrypoint_mismatch (n, t)) ;
  register_error_kind
    `Permanent
    ~id:"fa2UnexpectedError"
    ~title:"Unexpected error during FA2 contract interpretation"
    ~description:
      "An unexpected Michelson error was reached during the interpretation of \
       an FA2 contract."
    ~pp:(fun ppf (loc, expr) ->
      Format.fprintf
        ppf
        "An unexpected error was reached at location %d with: %a."
        loc
        Michelson_v1_printer.print_expr
        expr)
    Data_encoding.(
      obj2
        (req "location" Tezos_micheline.Micheline_encoding.canonical_location_encoding)
        (req "value" Script.expr_encoding))
    (function Unexpected_error (loc, expr) -> Some (loc, expr) | _ -> None)
    (fun (loc, expr) -> Unexpected_error (loc, expr))

module Michelson_v1_helpers = struct

  open Tezos_micheline.Micheline
  open Script

  type type_eq_combinator = node * (node -> bool)

  let unit_t ~loc = Prim (loc, T_unit, [], [])

  (** [t_pair ~loc l] takes a list of types and respective equivalence
      check functions, and returns a type of n-ary pair of such types and
      a function checking syntactical equivalence with another node. *)
  let t_pair ?(loc = 0) l : type_eq_combinator =
    let (values, are_ty) = List.split l in
    let is_pair p =
      match p with
      | Prim (_, T_pair, l, _) -> (
          let res =
            List.for_all2
              ~when_different_lengths:()
              (fun is_ty v -> is_ty v)
              are_ty
              l
          in
          match res with Ok b -> b | Error () -> false)
      | _ -> false
    in
    (Prim (loc, T_pair, values, []), is_pair)

  (** [t_unit ~loc ()] returns a Micheline node for the `unit` type, and
      a function checking another node is syntactically equivalent. *)
  let t_unit ?(loc = 0) () : type_eq_combinator =
    let is_unit p = match p with Prim (_, T_unit, [], _) -> true | _ -> false in
    (unit_t ~loc, is_unit)

  (** [t_nat ~loc ()] returns a Micheline node for the `nat` type, and
      a function checking another node is syntactically equivalent. *)
  let t_nat ?(loc = 0) () : type_eq_combinator =
    let is_nat p = match p with Prim (_, T_nat, [], _) -> true | _ -> false in
    (Prim (loc, T_nat, [], []), is_nat)

  (** [t_address ~loc ()] returns a Micheline node for the `address`
      type, and a function checking another node is syntactically
      equivalent. *)
  let t_address ?(loc = 0) () : type_eq_combinator =
    let is_address p =
      match p with Prim (_, T_address, [], _) -> true | _ -> false
    in
    (Prim (loc, T_address, [], []), is_address)

  (** [t_contract ~loc (c, is_c)] takes a node representing a Michelson
      type and its own syntactical equivalence checker, and returns a
      Micheline node for the type `contract c`, and a function checking
      another node is syntactically equivalent. *)
  let t_contract ?(loc = 0) (a, is_a) : type_eq_combinator =
    let is_contract c =
      match c with Prim (_, T_contract, [a], _) -> is_a a | _ -> false
    in
    (Prim (loc, T_contract, [a], []), is_contract)

  (** [t_pair ~loc t] takes a node representing a Michelson
      type and its own syntactical equivalence checker, and returns a
      Micheline node for the type `list t`, and a function checking
      another node is syntactically equivalent. *)
  let t_list ?(loc = 0) (a, is_a) : type_eq_combinator =
    let is_list p =
      match p with Prim (_, T_list, [a], _) -> is_a a | _ -> false
    in
    (Prim (loc, T_list, [a], []), is_list)

  (** [t_or ~loc l r] takes two nodes representing a Michelson
      type and their own syntactical equivalence checker, and returns a
      Micheline node for the type `or l r`, and a function checking
      another node is syntactically equivalent. *)
  let t_or ?(loc = 0) (l, is_l) (r, is_r) : type_eq_combinator =
    let is_or p =
      match p with Prim (_, T_or, [l; r], _) -> is_l l && is_r r | _ -> false
    in
    (Prim (loc, T_or, [l; r], []), is_or)

  (** [t_view ~loc a b] takes two node [a] and [b] and their syntactical
      equivalence checking functions, and returns a Micheline node for
      the `view a b` type, and a function checking another node is
      syntactically equivalent. The view type is defined by
      [TZIP4](https://gitlab.com/tzip/tzip/-/blob/master/proposals/tzip-4/tzip-4.md).
  *)
  let t_view ?loc a b : type_eq_combinator = t_pair ?loc [a; t_contract ?loc b]

end
open Michelson_v1_helpers

(** * Actions *)

(** A callback from a view can be on a specific entrypoint of the
   contract, or the default one if not specified. *)
type callback_contract = Contract.t * string option

type token_id = Z.t

type token_amount = Z.t

type operator_update_action = Add_operator | Remove_operator

type action =
  | Transfer of (Contract.t * (Contract.t * token_id * token_amount) list) list
  | Update_operators of
      (operator_update_action * Contract.t * Contract.t * token_id) list
  | Balance_of of (Contract.t * token_id) list * callback_contract

let transfer_type =
  t_list
    (t_pair [t_address (); t_list (t_pair [t_address (); t_nat (); t_nat ()])])

let update_operators_type =
  let args = t_pair [t_address (); t_address (); t_nat ()] in
  t_list (t_or args args)

let balance_of_type =
  t_view
    (t_list (t_pair [t_address (); t_nat ()]))
    (t_list (t_pair [t_pair [t_address (); t_nat ()]; t_nat ()]))

let standard_entrypoints =
  [
    ("transfer", transfer_type);
    ("update_operators", update_operators_type);
    ("balance_of", balance_of_type);
  ]

let check_entrypoint entrypoints (name, (expected_ty, check)) =
  match List.assoc_opt ~equal:String.equal name entrypoints with
  | None -> error (Entrypoint_mismatch (name, None))
  | Some ty ->
      if not (check (Tezos_micheline.Micheline.root ty)) then
        error
          (Entrypoint_mismatch
             ( name,
               Some (ty, Tezos_micheline.Micheline.strip_locations expected_ty)
             ))
      else Ok ()

let contract_has_fa2_interface :
    #Protocol_client_context.rpc_context ->
    chain:Shell_services.chain ->
    block:Shell_services.block ->
    contract:Alpha_context.Contract.t ->
    unit ->
    unit tzresult Lwt.t =
 fun cctxt ~chain ~block ~contract () ->
  match is_implicit contract with
  | true -> fail (Contract_has_no_script contract)
  | false ->
      (**# ifdef __META12__ let contract = match contract with Contract.Originated contract -> contract | _ -> assert false in #**)
      Michelson_v1_entrypoints.list_contract_entrypoints
        cctxt
        ~chain
        ~block
        ~contract
      (**# ifdef __META11__ ~normalize_types:true #**)
      >>=? fun entrypoints ->
      List.iter_e (check_entrypoint entrypoints) standard_entrypoints
      |> Lwt.return

let parse_address error = function
  | Micheline.Bytes (_, b) ->
      ok @@ Data_encoding.Binary.of_bytes_exn Contract.encoding b
  | String (_, s) -> (
      match Contract.of_b58check s with Ok c -> ok c | Error _ -> error ())
  | _ -> error ()

let callback_encoding =
  Data_encoding.(
    conv
      (fun (c, e) -> (c, Option.value ~default:"" e))
      (fun (c, e) -> (c, if String.equal e "" then None else Some e))
      (tup2 Contract.encoding Variable.string))

let parse_callback error expr =
  let of_b58_check (c, entrypoint) =
    match Contract.of_b58check c with
    | Ok c -> ok (c, entrypoint)
    | Error _ -> error ()
  in
  match expr with
  | Micheline.Bytes (_, b) -> (
      match Data_encoding.Binary.of_bytes callback_encoding b with
      | Ok (c, entrypoint) -> ok (c, entrypoint)
      | Error _ -> error ())
  | String (_, s) -> (
      match String.index_opt s '%' with
      | None -> of_b58_check (s, None)
      | Some pos -> (
          let len = String.length s - pos - 1 in
          let name = String.sub s (pos + 1) len in
          match (String.sub s 0 pos, name) with
          | (addr, "default") -> of_b58_check (addr, None)
          | (addr, name) -> of_b58_check (addr, Some name)))
  | _ -> error ()

let is_transfer_destination expr =
  let open Micheline in
  let error () =
    error
      (Action_unwrapping_error
         ("transfer.is_transfer_destination", Micheline.strip_locations expr))
  in
  match expr with
  | Prim
      ( _,
        Script.D_Pair,
        [
          ((Bytes (_, _) | String (_, _)) as destination);
          Prim (_, Script.D_Pair, [Int (_, token_id); Int (_, amount)], _);
        ],
        _ )
  | Prim
      ( _,
        Script.D_Pair,
        [
          ((Bytes (_, _) | String (_, _)) as destination);
          Int (_, token_id);
          Int (_, amount);
        ],
        _ ) ->
      parse_address error destination >>? fun destination ->
      ok (destination, token_id, amount)
  | _ -> error ()

let is_transfer expr =
  let open Micheline in
  let error () =
    error
      (Action_unwrapping_error
         ("transfer.is_transfer", Micheline.strip_locations expr))
  in
  match expr with
  | Prim
      ( _,
        Script.D_Pair,
        [((Bytes (_, _) | String (_, _)) as source); Seq (_, transfers)],
        _ ) ->
      parse_address error source >>? fun source ->
      List.map_e is_transfer_destination transfers >>? fun transfers ->
      ok (source, transfers)
  | _ -> error ()

let is_balance_of_request expr =
  let open Micheline in
  let error () =
    error
      (Action_unwrapping_error
         ("balance_of.is_balance_of_request", Micheline.strip_locations expr))
  in
  match expr with
  | Prim
      ( _,
        Script.D_Pair,
        [((Bytes (_, _) | String (_, _)) as owner); Int (_, token_id)],
        _ ) ->
      parse_address error owner >>? fun owner -> ok (owner, token_id)
  | _ -> error ()

let is_balance_of expr =
  let open Micheline in
  let error () =
    error
      (Action_unwrapping_error
         ("balance_of.is_balance_of", Micheline.strip_locations expr))
  in
  match expr with
  | Prim
      ( _,
        Script.D_Pair,
        [Seq (_, requests); ((Bytes (_, _) | String (_, _)) as callback)],
        _ ) ->
      List.map_e is_balance_of_request requests >>? fun requests ->
      parse_callback error callback >>? fun callback -> ok (requests, callback)
  | Prim
      ( _,
        Script.D_Pair,
        [request; ((Bytes (_, _) | String (_, _)) as callback)],
        _ ) ->
      is_balance_of_request request >>? fun request ->
      parse_callback error callback >>? fun callback -> ok ([request], callback)
  | _ -> error ()

let is_update_operators_request expr =
  let open Micheline in
  let error () =
    error
      (Action_unwrapping_error
         ("transfer.is_transfer_destination", Micheline.strip_locations expr))
  in
  match expr with
  | Prim
      ( _,
        Script.D_Pair,
        [
          ((Bytes (_, _) | String (_, _)) as owner);
          Prim
            ( _,
              Script.D_Pair,
              [((Bytes (_, _) | String (_, _)) as operator); Int (_, token_id)],
              _ );
        ],
        _ )
  | Prim
      ( _,
        Script.D_Pair,
        [
          ((Bytes (_, _) | String (_, _)) as owner);
          ((Bytes (_, _) | String (_, _)) as operator);
          Int (_, token_id);
        ],
        _ ) ->
      parse_address error owner >>? fun owner ->
      parse_address error operator >>? fun operator ->
      ok (owner, operator, token_id)
  | _ -> error ()

let is_update_operators expr =
  let open Micheline in
  let error () =
    error
      (Action_unwrapping_error
         ("update_operator.is_update_operator", Micheline.strip_locations expr))
  in
  let to_action = function
    | Script.D_Left -> ok Add_operator
    | Script.D_Right -> ok Remove_operator
    | _ -> error ()
  in
  match expr with
  | Prim (_, ((Script.D_Left | Script.D_Right) as prim), [node], _) ->
      to_action prim >>? fun action ->
      is_update_operators_request node >>? fun (owner, operator, token_id) ->
      ok (action, owner, operator, token_id)
  | _ -> error ()

let action_of_expr ~entrypoint expr =
  let error () =
    error (Action_unwrapping_error (entrypoint, Micheline.strip_locations expr))
  in
  match (entrypoint, expr) with
  | ("transfer", Seq (_, nodes)) ->
      List.map_e is_transfer nodes >>? fun transfers -> ok (Transfer transfers)
  | ("transfer", node) ->
      is_transfer node >>? fun transfers -> ok (Transfer [transfers])
  | ("balance_of", node) ->
      is_balance_of node >>? fun (requests, callback) ->
      ok (Balance_of (requests, callback))
  | ("update_operators", Seq (_, nodes)) ->
      List.map_e is_update_operators nodes >>? fun requests ->
      ok (Update_operators requests)
  | ("update_operators", node) ->
      is_update_operators node >>? fun request ->
      ok (Update_operators [request])
  | _ -> error ()
