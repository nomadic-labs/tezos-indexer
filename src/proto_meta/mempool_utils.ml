(*************************ver****************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2019-2020 Nomadic Labs, <contact@nomadic-labs.com>          *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

(**# ifndef __META14__ OPEN_COMMENTS #**)
module Public_key_hash = Tezos_crypto.Signature.V1.Public_key_hash
module Public_key = Tezos_crypto.Signature.V1.Public_key
module Protocol_hash = Protocol_hash
(**# ifndef __META14__ CLOSE_COMMENTS #**)
(**# ifdef __META14__ OPEN_COMMENTS #**)
module Public_key_hash = Tezos_crypto.Signature.V0.Public_key_hash
module Public_key = Tezos_crypto.Signature.V0.Public_key
module Protocol_hash = Protocol_hash
(**# ifdef __META14__ CLOSE_COMMENTS #**)


(**#X ifdef __TRANSITION__ module Db_alpha = Tezos_indexer_(**# get PROTONEXT#**).Db_alpha X#**)

let ( >>= ) = Lwt.bind

open Tezos_raw_protocol_(**# get PROTO#**)
(* open Tezos_protocol_(**# get PROTO#**) *)
open Tezos_client_(**# get PROTO#**)

(**#X ifdef __TRANSITION__
module BS = Block_services.Make(Tezos_protocol_(**# get PROTO#**)_lifted.Lifted_protocol)(Tezos_protocol_(**# get PROTONEXT#**)_lifted.Lifted_protocol)
X#**)(**#X else
module BS = Block_services.Make(Tezos_protocol_(**# get PROTO#**)_lifted.Lifted_protocol)(Tezos_protocol_(**# get PROTO#**)_lifted.Lifted_protocol)
 X#**)

module Verbose = Tezos_indexer_lib.Verbose
module Debug = Verbose.Debug

(**# ifdef __META12__
let implicit_contract pkh = Alpha_context.Contract.Implicit pkh
#**)(**#X else
let implicit_contract =
  Alpha_context.Contract.implicit_contract
X#**)


let lazy_expr_pp out e = match Data_encoding.force_decode e with
  | None -> ()
  | Some p ->
    Format.fprintf out "%a"
      Michelson_v1_printer.print_expr p

type status = Db_alpha.Mempool_operations.status = | Applied | Refused | Branch_refused | Unprocessed | Branch_delayed

module Tez = Alpha_context.Tez

type result = {
  age: int;
  branch: Block_hash.t;
  fee: Tez.t;
  (* operation: mop; *)
  counter: Z.t;
  gas_limit: Z.t;
  storage_limit: Z.t;
  status: status;
  source: Public_key_hash.t;
  op_hash: Operation_hash.t;
  seen: float;
}
let string_of_status = function
  | Applied -> "applied"
  | Refused -> "refused"
  | Branch_refused -> "branch_refused"
  | Unprocessed -> "unprocessed"
  | Branch_delayed -> "branch_delayed"
let status_of_string = function
  | "applied" -> Some Applied
  | "refused" -> Some Refused
  | "branch_refused" -> Some Branch_refused
  | "unprocessed" -> Some Unprocessed
  | "branch_delayed" -> Some Branch_delayed
  | _ -> None


module MempoolOperations = struct
  type t = Db_alpha.Mempool_operations.t = {
    branch: Block_hash.t;
    op_hash: Operation_hash.t;
    status: status;
    id: int;
    operation_kind: int;
    source: Public_key_hash.t option;
    destination: Db_base.Addresses.t option;
    seen: float;
    json_op: string Lazy.t;
    context_block_level : int32;
  }

  module S = Set.Make(struct
      type t = Db_alpha.Mempool_operations.t
      let compare a b =
        compare
          (a.op_hash, a.id, a.status)
          (b.op_hash, b.id, b.status)
    end)
  let s = ref S.empty
  let new_block () = s := S.empty
  let register ~pool ~bl
      ~json_op
      ~branch ?source ~id ~operation_kind ?destination ~status op_hash =
    let seen = Unix.gettimeofday () in
    let destination = Option.map (fun d ->
        Db_base.Addresses.make @@
        Format.asprintf "%a"
          (**# ifndef __PROTO13__ Alpha_context.Contract.pp #**)(**# else Alpha_context.Destination.pp #**)
          d
      ) destination in
    let open Db_alpha.Mempool_operations in
    let d = { source ; branch ; operation_kind ; id ; destination ;
              status ; op_hash ; seen ; json_op ; context_block_level = !bl }
    in
    if S.mem d !s then
      Lwt.return (Ok ()) (* already added *)
    else
      begin
        (* register operation into map to avoid duplicates *)
        s:= S.add d !s;
        (* register operation into db *)
        Db_base.find_opt pool
          Db_alpha.Mempool_operations.insert d
        >>= Db_base.caqti_or_fail ~__LOC__ >>= fun _ ->
        Lwt.return (Ok ())
      end

end

let int_of_contents : type a. a Alpha_context.contents -> int = Proto_misc.int_of_contents

let rec process_contents :
  type a. ?id:int -> bl:int32 ref -> pool:_ -> branch:_ -> status:status -> op_hash:_ -> a Alpha_context.contents_list -> unit =
  fun ?(id=0) ~bl ~pool ~branch ~status ~op_hash cl ->
  match cl with
  | Alpha_context.Single e ->
    begin
      let json_op =
        (* we put this into a lazy value because we process each
           operation multiple times, therefore most of the time, we
           won't need this value *)
        lazy
          (Format.asprintf "%a"
             Data_encoding.Json.pp
             (Data_encoding.Json.construct Alpha_context.Operation.contents_encoding (Contents e)))
      in
      match e with
      | Manager_operation { source ; fee = _ ; operation = Reveal _ ; counter = _ ;
                            gas_limit = _ ; storage_limit = _ } ->
        Lwt.async (fun () ->
            MempoolOperations.register ~pool ~bl ~json_op ~branch ~source  ~status ~id
              ~operation_kind:(int_of_contents e) op_hash >>= fun _ -> Lwt.return_unit
          )
      | Manager_operation { source ; operation = Transaction { destination ; _ } ;
                            fee = _ ; counter = _ ;
                            gas_limit = _ ; storage_limit = _ } ->
        Lwt.async (fun () ->
            MempoolOperations.register ~pool ~bl ~json_op ~branch ~source  ~status ~id ~destination
              ~operation_kind:(int_of_contents e) op_hash >>= fun _ -> Lwt.return_unit
          )
      | Manager_operation { source ; fee = _ ; operation = Origination { delegate ; script = _ ; credit = _ ; (**# ifndef __META11__ preorigination = _ ; #**) } ; counter = _ ;
                            gas_limit = _ ; storage_limit = _ } ->
        Lwt.async (fun () ->
            MempoolOperations.register ~pool ~bl ~json_op ~branch ~source  ~status ~id
              ?destination:(match delegate with
                  | None -> None
                  | Some d ->
                    Some ((**# ifdef __PROTO13__ Alpha_context.Destination.Contract #**)
                      (implicit_contract d)))
              ~operation_kind:(int_of_contents e) op_hash >>= fun _ -> Lwt.return_unit
          )
      | Manager_operation { source ; fee = _ ; operation = Delegation pkh_opt ; counter = _ ;
                            gas_limit = _ ; storage_limit = _ } ->
        Lwt.async (fun () ->
            MempoolOperations.register ~pool ~bl ~json_op ~branch ~source  ~status ~id
              ?destination:(match pkh_opt with
                  | None -> None
                  | Some d ->
                    Some ((**# ifdef __PROTO13__ Alpha_context.Destination.Contract #**)
                      (implicit_contract d)))
              ~operation_kind:(int_of_contents e) op_hash >>= fun _ -> Lwt.return_unit
          )
(**# ifdef __META9__
      | Manager_operation { operation = Register_global_constant _ ; source ;  fee = _ ;  counter = _ ;  gas_limit = _ ;  storage_limit = _ } ->
        Lwt.async (fun () ->
            MempoolOperations.register ~pool ~bl ~json_op ~branch ~source ~status ~id
              ~operation_kind:(int_of_contents e) op_hash >>= fun _ -> Lwt.return_unit
          )
#**)
(**# ifdef __META13__
      | Manager_operation { operation = Zk_rollup_origination _ ; source ;  fee = _ ;  counter = _ ;  gas_limit = _ ;  storage_limit = _ } ->
        Lwt.async (fun () ->
            MempoolOperations.register ~pool ~bl ~json_op ~branch ~source ~status ~id
              ~operation_kind:(int_of_contents e) op_hash >>= fun _ -> Lwt.return_unit
          )
      | Manager_operation { operation = Zk_rollup_publish _ ; source ;  fee = _ ;  counter = _ ;  gas_limit = _ ;  storage_limit = _ } ->
        Lwt.async (fun () ->
            MempoolOperations.register ~pool ~bl ~json_op ~branch ~source ~status ~id
              ~operation_kind:(int_of_contents e) op_hash >>= fun _ -> Lwt.return_unit
          )
      | Manager_operation { operation = Update_consensus_key _ ; source ;  fee = _ ;  counter = _ ;  gas_limit = _ ;  storage_limit = _ } ->
        Lwt.async (fun () ->
            MempoolOperations.register ~pool ~bl ~json_op ~branch ~source ~status ~id
              ~operation_kind:(int_of_contents e) op_hash >>= fun _ -> Lwt.return_unit
          )
#**)
(**# ifdef __META10__
      | Manager_operation { operation = Set_deposits_limit _ ; source ;  fee = _ ;  counter = _ ;  gas_limit = _ ;  storage_limit = _ } ->
        Lwt.async (fun () ->
            MempoolOperations.register ~pool ~bl ~json_op ~branch ~source ~status ~id
              ~operation_kind:(int_of_contents e) op_hash >>= fun _ -> Lwt.return_unit
          )
#**)
(**# ifndef __META11__ OPEN_COMMENTS #**)
      | Manager_operation { operation = Sc_rollup_originate _ ; source ;  fee = _ ;  counter = _ ;  gas_limit = _ ;  storage_limit = _ } ->
        Lwt.async (fun () ->
            MempoolOperations.register ~pool ~bl ~json_op ~branch ~source ~status ~id
              ~operation_kind:(int_of_contents e) op_hash >>= fun _ -> Lwt.return_unit
          )
      | Manager_operation { operation = Sc_rollup_add_messages _ ; source ;  fee = _ ;  counter = _ ;  gas_limit = _ ;  storage_limit = _ } ->
        Lwt.async (fun () ->
            MempoolOperations.register ~pool ~bl ~json_op ~branch ~source ~status ~id
              ~operation_kind:(int_of_contents e) op_hash >>= fun _ -> Lwt.return_unit
          )

(**# ifdef __META15__ OPEN_COMMENTS #**)
      | Manager_operation { operation = Tx_rollup_origination ; source ;  fee = _ ;  counter = _ ;  gas_limit = _ ;  storage_limit = _ } ->
        Lwt.async (fun () ->
            MempoolOperations.register ~pool ~bl ~json_op ~branch ~source ~status ~id
              ~operation_kind:(int_of_contents e) op_hash >>= fun _ -> Lwt.return_unit
          )
      | Manager_operation { operation = Tx_rollup_submit_batch _ ; source ;  fee = _ ;  counter = _ ;  gas_limit = _ ;  storage_limit = _ } ->
        Lwt.async (fun () ->
            MempoolOperations.register ~pool ~bl ~json_op ~branch ~source ~status ~id
              ~operation_kind:(int_of_contents e) op_hash >>= fun _ -> Lwt.return_unit
          )
      | Manager_operation { operation = Tx_rollup_commit _ ; source ;  fee = _ ;  counter = _ ;  gas_limit = _ ;  storage_limit = _ } ->
        Lwt.async (fun () ->
            MempoolOperations.register ~pool ~bl ~json_op ~branch ~source ~status ~id
              ~operation_kind:(int_of_contents e) op_hash >>= fun _ -> Lwt.return_unit
          )
      | Manager_operation { operation = Tx_rollup_return_bond _ ; source ;  fee = _ ;  counter = _ ;  gas_limit = _ ;  storage_limit = _ } ->
        Lwt.async (fun () ->
            MempoolOperations.register ~pool ~bl ~json_op ~branch ~source ~status ~id
              ~operation_kind:(int_of_contents e) op_hash >>= fun _ -> Lwt.return_unit
          )
      | Manager_operation { operation = Tx_rollup_finalize_commitment _ ; source ;  fee = _ ;  counter = _ ;  gas_limit = _ ;  storage_limit = _ } ->
        Lwt.async (fun () ->
            MempoolOperations.register ~pool ~bl ~json_op ~branch ~source ~status ~id
              ~operation_kind:(int_of_contents e) op_hash >>= fun _ -> Lwt.return_unit
          )
      | Manager_operation { operation = Tx_rollup_remove_commitment _ ; source ;  fee = _ ;  counter = _ ;  gas_limit = _ ;  storage_limit = _ } ->
        Lwt.async (fun () ->
            MempoolOperations.register ~pool ~bl ~json_op ~branch ~source ~status ~id
              ~operation_kind:(int_of_contents e) op_hash >>= fun _ -> Lwt.return_unit
          )
      | Manager_operation { operation = Tx_rollup_rejection _ ; source ;  fee = _ ;  counter = _ ;  gas_limit = _ ;  storage_limit = _ } ->
        Lwt.async (fun () ->
            MempoolOperations.register ~pool ~bl ~json_op ~branch ~source ~status ~id
              ~operation_kind:(int_of_contents e) op_hash >>= fun _ -> Lwt.return_unit
          )
      | Manager_operation { operation = Tx_rollup_dispatch_tickets _ ; source ;  fee = _ ;  counter = _ ;  gas_limit = _ ;  storage_limit = _ } ->
        Lwt.async (fun () ->
            MempoolOperations.register ~pool ~bl ~json_op ~branch ~source ~status ~id
              ~operation_kind:(int_of_contents e) op_hash >>= fun _ -> Lwt.return_unit
          )
(**# ifdef __META15__ CLOSE_COMMENTS #**)

      | Manager_operation { operation = Transfer_ticket _ ; source ;  fee = _ ;  counter = _ ;  gas_limit = _ ;  storage_limit = _ } ->
        Lwt.async (fun () ->
            MempoolOperations.register ~pool ~bl ~json_op ~branch ~source ~status ~id
              ~operation_kind:(int_of_contents e) op_hash >>= fun _ -> Lwt.return_unit
          )
      | Manager_operation { operation = Sc_rollup_cement _ ; source ;  fee = _ ;  counter = _ ;  gas_limit = _ ;  storage_limit = _ } ->
        Lwt.async (fun () ->
            MempoolOperations.register ~pool ~bl ~json_op ~branch ~source ~status ~id
              ~operation_kind:(int_of_contents e) op_hash >>= fun _ -> Lwt.return_unit
          )
      | Manager_operation { operation = Sc_rollup_publish _ ; source ;  fee = _ ;  counter = _ ;  gas_limit = _ ;  storage_limit = _ } ->
        Lwt.async (fun () ->
            MempoolOperations.register ~pool ~bl ~json_op ~branch ~source ~status ~id
              ~operation_kind:(int_of_contents e) op_hash >>= fun _ -> Lwt.return_unit
          )
(**# ifndef __META11__ CLOSE_COMMENTS #**)
(**# ifndef __META12__ OPEN_COMMENTS #**)
      | Manager_operation { operation = Sc_rollup_refute _ ; source ;  fee = _ ;  counter = _ ;  gas_limit = _ ;  storage_limit = _ } ->
        Lwt.async (fun () ->
            MempoolOperations.register ~pool ~bl ~json_op ~branch ~source ~status ~id
              ~operation_kind:(int_of_contents e) op_hash >>= fun _ -> Lwt.return_unit
          )
      | Manager_operation { operation = Sc_rollup_timeout _ ; source ;  fee = _ ;  counter = _ ;  gas_limit = _ ;  storage_limit = _ } ->
        Lwt.async (fun () ->
            MempoolOperations.register ~pool ~bl ~json_op ~branch ~source ~status ~id
              ~operation_kind:(int_of_contents e) op_hash >>= fun _ -> Lwt.return_unit
          )
      | Manager_operation { operation = Dal_publish_slot_header _ ; source ;  fee = _ ;  counter = _ ;  gas_limit = _ ;  storage_limit = _ } ->
        Lwt.async (fun () ->
            MempoolOperations.register ~pool ~bl ~json_op ~branch ~source ~status ~id
              ~operation_kind:(int_of_contents e) op_hash >>= fun _ -> Lwt.return_unit
          )
      | Manager_operation { operation = Sc_rollup_execute_outbox_message _ ; source ;  fee = _ ;  counter = _ ;  gas_limit = _ ;  storage_limit = _ } ->
        Lwt.async (fun () ->
            MempoolOperations.register ~pool ~bl ~json_op ~branch ~source ~status ~id
              ~operation_kind:(int_of_contents e) op_hash >>= fun _ -> Lwt.return_unit
          )
      | Manager_operation { operation = Sc_rollup_recover_bond _ ; source ;  fee = _ ;  counter = _ ;  gas_limit = _ ;  storage_limit = _ } ->
        Lwt.async (fun () ->
            MempoolOperations.register ~pool ~bl ~json_op ~branch ~source ~status ~id
              ~operation_kind:(int_of_contents e) op_hash >>= fun _ -> Lwt.return_unit
          )
(**# ifdef __META14__ OPEN_COMMENTS #**)
      | Manager_operation { operation = Sc_rollup_dal_slot_subscribe _ ; source ;  fee = _ ;  counter = _ ;  gas_limit = _ ;  storage_limit = _ } ->
        Lwt.async (fun () ->
            MempoolOperations.register ~pool ~bl ~json_op ~branch ~source ~status ~id
              ~operation_kind:(int_of_contents e) op_hash >>= fun _ -> Lwt.return_unit
          )
      | Dal_slot_availability (pkh, _dal_endorsement) ->
        Lwt.async (fun () ->
            MempoolOperations.register ~pool ~bl ~json_op ~branch ~source:pkh ~status ~id
              ~operation_kind:(int_of_contents e) op_hash >>= fun _ -> Lwt.return_unit
          )
(**# ifdef __META14__ CLOSE_COMMENTS #**)
(**# ifndef __META14__ OPEN_COMMENTS #**)
      (* | Dal_slot_availability { endorser = source ; _ } ->
       *   Lwt.async (fun () ->
       *       MempoolOperations.register ~pool ~bl ~json_op ~branch ~source ~status ~id
       *         ~operation_kind:(int_of_contents e) op_hash >>= fun _ -> Lwt.return_unit
       *     ) *)
(**# ifdef __META16__ OPEN_COMMENTS #**)
      | Dal_attestation { attestor (* : public_key_hash *);
                          attestation = _ (* : t *);
                          level = _  (* : Raw_level.t *) } ->
        Lwt.async (fun () ->
            MempoolOperations.register ~pool ~bl ~json_op ~branch ~source:attestor ~status ~id
              ~operation_kind:(int_of_contents e) op_hash >>= fun _ -> Lwt.return_unit
          )
(**# ifdef __META16__ CLOSE_COMMENTS #**)
(**# ifndef __META16__ OPEN_COMMENTS #**)
      | Dal_attestation { attestation = _ (* : t *);
                          level = _  (* : Raw_level.t *);
                          slot = _ } ->
        Lwt.async (fun () ->
            MempoolOperations.register ~pool ~bl ~json_op ~branch ~status ~id
              ~operation_kind:(int_of_contents e) op_hash >>= fun _ -> Lwt.return_unit
          )
(**# ifndef __META16__ CLOSE_COMMENTS #**)
      | Manager_operation { operation = Zk_rollup_update _ ; source ;  fee = _ ;  counter = _ ;  gas_limit = _ ;  storage_limit = _ } ->
        Lwt.async (fun () ->
            MempoolOperations.register ~pool ~bl ~json_op ~branch ~source ~status ~id
              ~operation_kind:(int_of_contents e) op_hash >>= fun _ -> Lwt.return_unit
          )
(**# ifndef __META14__ CLOSE_COMMENTS #**)
      | Vdf_revelation _solution ->
        Lwt.async (fun () ->
            MempoolOperations.register ~pool ~bl ~json_op ~branch ~status ~id
              ~operation_kind:(int_of_contents e) op_hash >>= fun _ -> Lwt.return_unit
          )
      | Manager_operation { source ; fee = _ ; operation = Increase_paid_storage { amount_in_bytes = _ ; destination }  ; counter = _ ;
                            gas_limit = _ ; storage_limit = _ } ->
        Lwt.async (fun () ->
            MempoolOperations.register ~pool ~bl ~json_op ~branch ~source  ~status ~id
            ~destination:(Originated destination)
              ~operation_kind:(int_of_contents e) op_hash >>= fun _ -> Lwt.return_unit
          )
(**# ifndef __META12__ CLOSE_COMMENTS #**)
(**# ifndef __META13__ OPEN_COMMENTS #**)
      | Drain_delegate { delegate (* : Signature.Public_key_hash.t *)
                       ; destination (* : Signature.Public_key_hash.t; *)
                       ; consensus_key = _ } ->
        Lwt.async (fun () ->
            MempoolOperations.register ~pool ~bl ~json_op ~branch ~source:(delegate) ~status ~id
              ~destination:(Implicit destination)
              ~operation_kind:(int_of_contents e) op_hash >>= fun _ -> Lwt.return_unit
          )
(**# ifndef __META13__ CLOSE_COMMENTS #**)
      | Activate_account { id = pkh ; activation_code = _ ; } ->
        Lwt.async (fun () ->
            MempoolOperations.register ~pool ~bl ~json_op ~branch ~source:(Ed25519 pkh) ~status ~id
              ~operation_kind:(int_of_contents e) op_hash >>= fun _ -> Lwt.return_unit
          )
      | Proposals { source ; period = _ ; proposals = _ ; } ->
        Lwt.async (fun () ->
            MempoolOperations.register ~pool ~bl ~json_op ~branch ~source ~status ~id
              ~operation_kind:(int_of_contents e) op_hash >>= fun _ -> Lwt.return_unit
          )
      | Ballot { source ; period = _ ; proposal = _ ; ballot = _ ; } ->
        Lwt.async (fun () ->
            MempoolOperations.register ~pool ~bl ~json_op ~branch ~source ~status ~id
              ~operation_kind:(int_of_contents e) op_hash >>= fun _ -> Lwt.return_unit
          )
(**# ifdef __META16__ OPEN_COMMENTS #**)
      (**#X ifdef __META7__
         (**# ifndef __META10__
      | Endorsement_with_slot _
         #**)
      | Failing_noop _ X#**)
      (**# ifndef __META10__
      | Endorsement { level = _ ; }
       #**)(**# else
      | Endorsement _
      | Preendorsement _
      | Double_preendorsement_evidence _
       #**)
      | Double_endorsement_evidence
          { op1 = { shell = { branch = _ } ;
                    protocol_data = { contents = _ ; signature = _ } }  ;
            op2 = { shell = { branch = _ } ;
                    protocol_data = { contents = _ ; signature = _ } }  ;
            (**#X ifdef __META7__ (**# ifndef __META10__ slot = _ ; #**) X#**)
          }
(**# ifdef __META16__ CLOSE_COMMENTS #**)
(**# ifndef __META16__ OPEN_COMMENTS #**)
      | Preattestation _
      | Attestation _
      | Double_preattestation_evidence _
      | Double_attestation_evidence _
      | Failing_noop _
(**# ifndef __META16__ CLOSE_COMMENTS #**)
      | Seed_nonce_revelation { level = _ ; nonce = _ ; }
      | Double_baking_evidence
          { bh1 = { shell = { level = _ ; proto_level = _ ; predecessor = _ ;
                              timestamp = _ ; validation_passes = _ ;
                              operations_hash = _ ; fitness = _ ; context = _ ;
                            } ;
                    protocol_data = { contents = _ ; signature = _ }
                  } ;
            bh2 = { shell = { level = _ ; proto_level = _ ; predecessor = _ ;
                              timestamp = _ ; validation_passes = _ ;
                              operations_hash = _ ; fitness = _ ; context = _ ;
                            } ;
                    protocol_data = { contents = _ ; signature = _ }
                  } } ->
        Lwt.async (fun () ->
            MempoolOperations.register ~pool ~bl ~json_op ~branch ~status ~id
              ~operation_kind:(int_of_contents e) op_hash >>= fun _ -> Lwt.return_unit
          )
    end
  | Cons (hd, tl) ->
    begin
      process_contents ~pool ~bl ~id ~status ~branch ~op_hash (Single hd);
      process_contents ~pool ~bl ~id:(id+1) ~status ~branch ~op_hash tl
    end

(**#x ifndef __META10__
let hack_packed_operation_list : (Operation_hash.t * Tezos_protocol_(**# get PROTO#**).Protocol.operation) list -> (Operation_hash.t * Tezos_protocol_(**# get PROTO#**).Protocol.operation) list = fun x -> x
x#**)
(**#x else
let hack_packed_operation_list : (Operation_hash.t * Tezos_raw_protocol_(**# get PROTO#**).Alpha_context.packed_operation) list -> (Operation_hash.t * Tezos_protocol_(**# get PROTO#**).Protocol.operation) list = Obj.magic
x#**)

(**#x ifndef __META10__
let hack_packed_operation_map : (Tezos_protocol_(**# get PROTO#**).Protocol.operation * error list) Operation_hash.Map.t -> (Tezos_protocol_(**# get PROTO#**).Protocol.operation * error list) Operation_hash.Map.t = fun x -> x
let hack_packed_operation_umap : (Tezos_protocol_(**# get PROTO#**).Protocol.operation) Operation_hash.Map.t -> (Tezos_protocol_(**# get PROTO#**).Protocol.operation) Operation_hash.Map.t = fun x -> x
x#**)
(**#x else
let hack_packed_operation_map : (Tezos_raw_protocol_(**# get PROTO#**).Alpha_context.packed_operation * error list) Operation_hash.Map.t -> (Tezos_protocol_(**# get PROTO#**).Protocol.operation * error list) Operation_hash.Map.t = Obj.magic
let hack_packed_operation_umap : Tezos_raw_protocol_(**# get PROTO#**).Alpha_context.packed_operation Operation_hash.Map.t -> (Tezos_protocol_(**# get PROTO#**).Protocol.operation) Operation_hash.Map.t = Obj.magic
x#**)

(**# ifdef __META14__ let signature_pp out x = Tezos_crypto.Signature.V1.pp out x #**)
(**# else let signature_pp out x = Tezos_crypto.Signature.V0.pp out x #**)

let watch_mempool cctxt ~pool newproto bl =
  let open Tezos_protocol_(**# get PROTO#**).Protocol in
  assert !newproto;
  newproto := false;
  let monitor_pendings () =
    begin
      BS.Mempool.pending_operations cctxt () >>=
      function
      | (Ok BS.Mempool.{ validated = (applied(* : (Operation_hash.t * operation) list *)); refused; branch_refused; unprocessed; branch_delayed; outdated = _ (* FIXME *) }) ->
        begin
          let treat_one_op i (op_hash,
                              ({ shell = ({ branch ; } : Operation.shell_header) ;
                                 protocol_data = ((Operation_data { contents ; signature ; })
                                                  : Alpha_context.packed_protocol_data) }
                               : Alpha_context.packed_operation)) =
            Debug.printf ~vl:2 "%c (%d) branch=%a op_hash=%a%a" '@' i Block_hash.pp branch Operation_hash.pp op_hash
              (fun out ->
                 function
                 | None -> ()
                 | Some s -> Format.fprintf out " signature=%a" signature_pp s) signature;
            process_contents ~bl ~pool ~status:Applied ~branch ~op_hash contents
          in
          List.iteri treat_one_op (hack_packed_operation_list applied);
        end;
        begin
          List.iter (fun (map, status) ->
              Operation_hash.Map.iter (fun op_hash (o, _error_list) ->
                  let treat_one_op op_hash = function (
                    { shell = ({ branch ; } : Operation.shell_header) ;
                      protocol_data = ((Operation_data { contents ; signature ; })
                                       : Alpha_context.packed_protocol_data) }
                    : Alpha_context.packed_operation) ->
                    Debug.printf ~vl:2 "error=%s op=%a branch=%a%a"
                      (string_of_status status)
                      Operation_hash.pp op_hash Block_hash.pp branch
                      (fun out ->
                         function
                         | None -> ()
                         | Some s -> Format.fprintf out " signature=%a" signature_pp s) signature;
                    process_contents ~bl ~pool ~status ~branch ~op_hash contents;
                    ()
                  in
                  treat_one_op op_hash o
                )
                map
            )
            [ hack_packed_operation_map refused, Refused;
              hack_packed_operation_map branch_refused, Branch_refused;
              hack_packed_operation_map branch_delayed, Branch_delayed ];
        end;
        begin
          let status = Unprocessed in
          Operation_hash.Map.iter (fun op_hash o ->
              let treat_one_op op_hash = function (
                { shell = ({ branch ; } : Operation.shell_header) ;
                  protocol_data = ((Operation_data { contents ; signature ; })
                                   : Alpha_context.packed_protocol_data) }
                : Alpha_context.packed_operation) ->
                Debug.printf ~vl:2 "error=%s op=%a branch=%a%a"
                  (string_of_status status)
                  Operation_hash.pp op_hash Block_hash.pp branch
                  (fun out ->
                     function
                     | None -> ()
                     | Some s -> Format.fprintf out " signature=%a" signature_pp s) signature;
                process_contents ~bl ~pool ~status ~branch ~op_hash contents;
                ()
              in
              treat_one_op op_hash o
            )
            (hack_packed_operation_umap unprocessed)
        end;
        Lwt.return (Ok ())
      | Error e ->
        Lwt.return (Error e)
    end
  in
  let rec monitor_ops () =
    begin
      BS.Mempool.monitor_operations ~refused:true ~branch_refused:true cctxt () >>=
      function
      | Error e -> Lwt.return (Error e)
      | Ok (stream, stopper) ->
        MempoolOperations.new_block ();
        Debug.printf ~vl:0 "# BEGIN monitor_operations cycle";
        let rec loop () =
          Lwt_stream.get stream >>= function
          | None ->
            stopper ();
            Debug.printf ~vl:0 "# END monitor_operations cycle";
            Lwt.return (Ok ())
          | Some (_op_list:((Operation_hash.t * _(* Alpha_context.packed_operation *)) * error list option) list) ->
            (* We ignore the contents of [op_list]. What matters with
               this stream is to get a signal from the node to know
               that there's new operations; and we're using
               [BS.Mempool.pending_operations] to get full information.

               [_op_list] does not contain operations status. *)
            monitor_pendings () >>= function
            | Ok ()   -> loop ()
            | Error e -> Lwt.return (Error e)
        in
        loop ()
    end
    >>= function
    | Ok () ->
      if !newproto then
        Lwt.return (Ok ())
      else
        monitor_ops ()
    | Error e ->
      Lwt.return (Error e)
  in
  monitor_ops ()
  >>= function
  | Ok () ->
    Lwt.return (Ok ())
  | Error e ->
    Verbose.Debug.eprintf ~vl:1 "Error at %s" __LOC__;
    Verbose.error "Error while indexing mempool: %a" pp_print_trace e;
    exit 1
