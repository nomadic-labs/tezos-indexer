(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2018 Dynamic Ledger Solutions, Inc. <contact@tezos.com>     *)
(* Copyright (c) 2019-2022 Nomadic Labs, <contact@nomadic-labs.com>          *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)


(**# ifndef __META14__ OPEN_COMMENTS #**)
open Tezos_crypto.Signature.V1
(**# ifndef __META14__ CLOSE_COMMENTS #**)
(**# ifdef __META14__ OPEN_COMMENTS #**)
open Tezos_crypto.Signature.V0
(**# ifdef __META14__ CLOSE_COMMENTS #**)

(** {1 Encoders from Tezos types to Caqti (SQL) types.}  *)

(**#X ifdef __META1__
open Tezos_raw_protocol_(**# get PROTO#**)
open Alpha_context
X#**)(**#X else
open Protocol
open Alpha_context
        X#**)

(**#X ifdef __META3__ open Tezos_protocol_(**# get PROTO #**) open Protocol X#**)
(**#X ifdef __META6__ open Tezos_protocol_(**# get PROTO #**) open Protocol X#**)

open Caqti_type

(**# ifdef __PROTO9__ module Delegate = Receipt #**)
(**# ifdef __META8__ module Delegate = Receipt #**)

val k : Contract.t t
val tez : Tez.t t
val lazy_expr : Script.lazy_expr t
val script : Script.t t
val balance : (**# ifdef __META16__ Tez.t Receipt.balance t #**)(**# elseifdef __PROTO9__ Receipt.balance t #**)(**# elseifdef __META8__ Receipt.balance t #**)(**# else Delegate.balance t #**)
val balance_update : (**# ifdef __META16__ Tez.t Receipt.balance_update t #**)(**# elseifdef __PROTO9__ Receipt.balance_update t #**)(**# elseifdef __META8__ Receipt.balance_update t #**)(**# else Delegate.balance_update t #**)
val cycle : Cycle.t t
val voting_period : Voting_period.t t
val voting_period_kind : Voting_period.kind t

open Db_tups
open Db_base

type opaid = int64

(** {1 Typed requests for Indexer's SQL DB.} *)

module Block_alpha_table : sig
  val insert :
    ((block_level
     , kid (* baker *)
     , int32 (* level position *)
     , Cycle.t
     , int32 (* cycle position *)
     , Voting_period.t
     , int32 (* voting period position *)
     , Voting_period.kind
     , Fpgas.t (* consumed milligas *)
       (**# ifdef __META13__ , kid (* consensus_pkh *) #**)
     ) (**# ifdef __META13__ tup10 #**)
    (**# else tup9 #**)
    , int32
    , Caqti_mult.zero_or_one) Caqti_request.t
end

module Operation_alpha_table : sig
  val insert :
    ((ophid, int, int, block_level, int, int64) tup6, unit, Caqti_mult.zero_or_one) Caqti_request.t
end

(* module Operation_sender_receiver_table : sig
 *   type t = {
 *     block_level : block_level;
 *     ophid : ophid;
 *     op_id : int;
 *     operation_kind : int;
 *     internal: int;
 *     sender: kid;
 *     receiver: kid option;
 *   }
 *   val insert :
 *     (t, unit, Caqti_mult.zero_or_one) Caqti_request.t
 * end *)

module Proposals_table : sig
  type t = {
    opaid    : opaid;
    i        : int64;
    source   : kid;
    period   : int32;
    proposal : Protocol_hash.t;
  }
  val insert : (t, unit, Caqti_mult.zero_or_one) Caqti_request.t
  val insert2 : (t, unit, Caqti_mult.zero_or_one) Caqti_request.t
end

module Ballot_table : sig
  type t = {
    opaid    : opaid;
    i        : int64;
    source   : kid;
    period   : int32;
    proposal : Protocol_hash.t;
    ballot   : Alpha_context.Vote.ballot;
  }
  val insert : (t, unit, Caqti_mult.zero_or_one) Caqti_request.t
end

module Double_endorsement_evidence_table : sig
  type t = {
    opaid : opaid;
    baker_id : kid;
    offender_id : kid option;
    op1 : Alpha_context.Operation.packed;
    op2 : Alpha_context.Operation.packed;
  }
  val insert : (t, unit, Caqti_mult.zero_or_one) Caqti_request.t
end

module Double_preendorsement_evidence_table : sig
  type t = {
    opaid : opaid;
    baker_id : kid;
    offender_id : kid option;
    op1 : Alpha_context.Operation.packed;
    op2 : Alpha_context.Operation.packed;
  }
  val insert : (t, unit, Caqti_mult.zero_or_one) Caqti_request.t
end


module Double_baking_evidence_table : sig
  type t = {
    opaid: opaid;
    bh1 : Block_header.t;
    bh2 : Block_header.t;
    baker_id : kid;
    offender_id : kid option;
  }
  val insert : (t, unit, Caqti_mult.zero_or_one) Caqti_request.t
end


module Manager_numbers : sig
  val insert :
    ((opaid, (**# ifdef __META14__ Manager_counter.t #**)(**# else Z.t #**), Z.t, Z.t) tup4,
     unit, Caqti_mult.zero_or_one) Caqti_request.t
end

module Contract_table : sig
  val get_scriptless_contracts : (kid, Contract.t option * kid, Caqti_mult.zero_or_more) Caqti_request.t
  val update_script : ((kid, Script.t, block_level, string list option, int) tup5, unit, Caqti_mult.zero_or_one) Caqti_request.t
end

module Contract_balance_table : sig
  val update : (kid * block_level * Tez.t * Script.t option, unit, Caqti_mult.zero_or_one) Caqti_request.t
  val update_via_bh : (kid * Block_hash.t * Tez.t * Script.t option, unit, Caqti_mult.zero_or_one) Caqti_request.t
  val insert_balance_full : ((kid, Tez.t, block_level, Script.t option, string list option, int) tup6, unit, Caqti_mult.zero_or_one) Caqti_request.t
  val pre_insert_balance : (kid * block_level, unit, Caqti_mult.zero_or_one) Caqti_request.t
  val get_balanceless_contracts :
    (int
    , Contract.t option * kid * Block_hash.t * block_level
    , Caqti_mult.zero_or_more
    ) Caqti_request.t
end

module Balance_table : sig
(**# ifndef __META7__ OPEN_COMMENTS #**)

(**# ifdef __META16__ OPEN_COMMENTS #**)
  type t =
    (Delegate.balance * Delegate.balance_update * Receipt.update_origin) list
(**# ifdef __META16__ CLOSE_COMMENTS #**)

(**# ifndef __META16__ OPEN_COMMENTS #**)
  type t = Delegate.balance_update_item list
(**# ifndef __META16__ CLOSE_COMMENTS #**)

(**# ifndef __META7__ CLOSE_COMMENTS #**)

(**# ifdef __META7__ OPEN_COMMENTS #**)
  type t = (Delegate.balance * Delegate.balance_update) list
(**# ifdef __META7__ CLOSE_COMMENTS #**)

  val update :
    get_kid:(Contract.t -> kid Lwt.t) ->
    ?opaid:opaid ->
    ?iorid:int ->
    (module Caqti_lwt.CONNECTION) ->
    block_level:block_level ->
    t ->
    unit Lwt.t
end

type operation_status = int

type error_list = string

module Origination_table : sig
  type t = {
    opaid : opaid;
    src: kid ;
    k: kid option;
    consumed_gas : Fpgas.t option;
    storage_size : Z.t option;
    paid_storage_size_diff : Z.t option;
    fee : Tez.t option ;
    nonce : int option;
    preorigination_id : kid option;
    script : Script.t option;
    delegate_id : kid option;
    credit : Tez.t;
    manager_id : kid option;
    block_level : block_level;
    status : operation_status;
    error_list : error_list option;
    strings : string list option;
  }
  val insert : (t, unit, Caqti_mult.zero_or_one) Caqti_request.t

end

module Tx_table : sig
  type t = {
    opaid : opaid;
    source : kid ;
    destination : kid ;
    fee : Tez.t option ;
    amount : Tez.t ;
    originated_contracts : kid list ;
    parameters : Script.lazy_expr option ;
    storage : Script.lazy_expr option ;
    consumed_gas : Fpgas.t option ;
    storage_size : Z.t option ;
    paid_storage_size_diff : Z.t option ;
    entrypoint : string option ;
    nonce : int option ;
    status : operation_status ;
    error_list : error_list option;
    strings : string list option;
    (**# ifdef __META11__ ticket_hash : Ticket_hash.t option; #**)
    (**# ifdef __META13__ ticket_receipt : Ticket_receipt.t option; #**)
  }
  val insert : (t, unit, Caqti_mult.zero_or_one) Caqti_request.t
end

module Delegation_table : sig
  val insert :
    ((opaid, kid, kid option, Fpgas.t option
     , Tez.t option, int option, operation_status, error_list option) tup8
    , unit, Caqti_mult.zero_or_one) Caqti_request.t
end

module Reveal_table : sig
  val insert :
    (( opaid, kid, Public_key.t, Fpgas.t option
      , Tez.t option, int option, operation_status, error_list option) tup8
    , unit, Caqti_mult.zero_or_one) Caqti_request.t
end

module Endorsement_or_preendorsement_tables : sig
  type t = {
    opaid : opaid ;
    level : block_level ;
    delegate_id : kid ;
    slots : int list ;
    endorsement_or_preendorsement_power : int option ;
    round : int32 option ;
    block_payload_hash : string option ;
  }

  val insert : pre:bool -> (t, unit, Caqti_mult.zero_or_one) Caqti_request.t

end

module Mempool_operations : sig
  type status =
    | Applied | Refused | Branch_refused | Unprocessed | Branch_delayed

  type t = {
    branch: Block_hash.t;
    op_hash : Operation_hash.t;
    status: status;
    id: int;
    operation_kind: int;
    source: Public_key_hash.t option;
    destination: Addresses.t option;
    seen: float;
    json_op: string Lazy.t;
    context_block_level: block_level;
  }

  val string_of_status : status -> string
  val status_of_string : string -> status option
  val sql_encoding : t Caqti_type.t
  val insert : (t, unit, Caqti_mult.zero_or_one) Caqti_request.t
end

module Seed_nonce_revelation_table : sig
  type t = {
    opaid : opaid;
    sender_id : kid;
    baker_id : kid;
    level : block_level;
    nonce : Nonce.t
  }
  val insert : (t, unit, Caqti_mult.zero_or_one) Caqti_request.t
end


module Bigmap : sig

  val old_insert :
    ((Tezos_raw_protocol_(**# get PROTO#**).Alpha_context.Script.expr
     , Script_expr_hash.t
     , Tezos_raw_protocol_(**# get PROTO#**).Alpha_context.Script.expr option
     , block_level
     , kid option, kid option, int64, opaid option
     , int option (* iorid *), string list option, int, int64 option) tup12
    , unit
    , Caqti_mult.zero_or_one) Caqti_request.t

  val update :
    ((Z.t
      , Tezos_raw_protocol_(**# get PROTO#**).Alpha_context.Script.expr
      , Script_expr_hash.t
      , Tezos_raw_protocol_(**# get PROTO#**).Alpha_context.Script.expr option
      , block_level, kid option, kid option, int64
      , opaid option, int option, string list option, int
      , int64 option) tup13
     , unit
     , Caqti_mult.zero_or_one) Caqti_request.t

  val alloc :
    ((Z.t
     , Tezos_raw_protocol_(**# get PROTO#**).Alpha_context.Script.expr
     , Tezos_raw_protocol_(**# get PROTO#**).Alpha_context.Script.expr
     , block_level
     , kid option, kid option, int64, opaid option
     , int option (* iorid *)
     , string option
     , int64 option) tup11
    , unit
    , Caqti_mult.zero_or_one) Caqti_request.t

  val clear :
    ((Z.t, block_level, kid option, kid option
     , int64
     , opaid option
     , int option (* iorid *)
     , int64 option) tup8
    , unit
    , Caqti_mult.zero_or_one) Caqti_request.t

  val copy :
    ((Z.t, Z.t, block_level, kid option
    , kid option
    , int64
    , opaid option
    , int option (* iorid *)
    , int64 option) tup9
    , unit
    , Caqti_mult.zero_or_one) Caqti_request.t

end

module Tokens : sig

  module Contract_table : sig
    type kind = Contract_utils.Tokens.kind
    type t = {
      address : kid ;
      block_level : block_level;
      kind: kind;
    }

    val insert : (t, unit, Caqti_mult.zero_or_one) Caqti_request.t
    val is_token : (kid, kind, Caqti_mult.zero_or_one) Caqti_request.t
  end

  module Balance_table : sig
    type t = {
      token_address : kid;
      address : kid;
      amount : int;
    }

    val upsert : (t, unit, Caqti_mult.zero_or_one) Caqti_request.t
  end

  module Accounts_registry_table : sig
    type t = {
      token_address : kid ;
      account : kid ;
      block_level : int32 ;
    }

    val insert : (t, unit, Caqti_mult.zero_or_one) Caqti_request.t
  end

  module FA12 : sig

    module Operation_table : sig
      type kind = Transfer | Approve | GetBalance | GetAllowance | GetTotalSupply

      type t = {
        opaid : opaid ;
        token_address : kid ;
        caller : kid ;
        kind : kind ;
      }

      val insert : (t, unit, Caqti_mult.zero_or_one) Caqti_request.t
    end

    module Transfer_table : sig
      type t = {
        opaid : opaid ;
        source : kid ;
        destination : kid ;
        amount : Z.t ;
      }

      val insert : (t, unit, Caqti_mult.zero_or_one) Caqti_request.t
    end

    module Approve_table : sig
      type t = {
        opaid : opaid ;
        address : kid ;
        amount : Z.t ;
      }

      val insert : (t, unit, Caqti_mult.zero_or_one) Caqti_request.t
    end

    module Get_balance_table : sig
      type t = {
        opaid : opaid ;
        address : kid ;
        callback : kid ;
      }

      val insert : (t, unit, Caqti_mult.zero_or_one) Caqti_request.t
    end

    module Get_allowance_table : sig
      type t = {
        opaid : opaid ;
        source : kid ;
        destination : kid ;
        callback : kid ;
      }

      val insert : (t, unit, Caqti_mult.zero_or_one) Caqti_request.t
    end

    module Get_total_supply_table : sig
      type t = {
        opaid : opaid ;
        callback : kid ;
      }

      val insert : (t, unit, Caqti_mult.zero_or_one) Caqti_request.t
    end

  end

  module FA2 : sig

    module Operation_table : sig
      type kind = Transfer | Update_operators | Balance_of

      type t = {
        opaid : opaid ;
        token_address : kid ;
        caller : kid ;
        kind : kind ;
      }

      val insert : (t, unit, Caqti_mult.zero_or_one) Caqti_request.t
    end

    module Transfer_table : sig
      type t = {
        opaid : opaid ;
        internal_op_id: int ;
        token_id : Z.t ;
        source : kid ;
        destination : kid ;
        amount : Z.t ;
      }

      val insert : (t, unit, Caqti_mult.zero_or_one) Caqti_request.t
    end

    module Update_operators_table : sig
      type kind = Contract_utils.FA2.operator_update_action

      type t = {
        opaid : opaid ;
        internal_op_id: int ;
        token_id : Z.t ;
        kind : kind ;
        owner : kid ;
        operator : kid ;
      }

      val insert : (t, unit, Caqti_mult.zero_or_one) Caqti_request.t
    end

    module Balance_of_table : sig
      type t = {
        opaid : opaid ;
        internal_op_id: int ;
        token_id : Z.t ;
        owner : kid ;
        callback : kid ;
      }

      val insert : (t, unit, Caqti_mult.zero_or_one) Caqti_request.t
    end

  end

end

module Activation_table : sig
  val insert :
    ((opaid * kid * string)
    , unit
    , Caqti_mult.zero_or_one) Caqti_request.t
end


module Utils : sig
  val extract_strings_and_bytes : Alpha_context.Script.t -> string list option
  val extract_strings_and_bytes_from_expr : Alpha_context.Script.expr -> string list option
  val extract_strings_and_bytes_from_lazy_expr : Alpha_context.Script.lazy_expr -> string list option
end


(**# ifndef __META8__ OPEN_COMMENTS #**)
module Implicit_operations_results_table : sig
  type t =
    { block_level : int32
    ; operation_kind : int
    ; originated_contracts : kid list option
    ; strings : string list option
    ; storage : Script.expr option
    ; consumed_gas : Fpgas.t
    ; storage_size : Z.t option
    ; paid_storage_size_diff : Z.t option
    ; allocated_destination_contract : bool option
    ; iorid : int
    (**# ifdef __META11__ ; originated_tx_rollup : kid option
       ; ticket_hash : Ticket_hash.t option
       ; tx_rollup_level : int32 option
       ; sc_address : kid option
       ; sc_size : Z.t option
       ; sc_inbox_after : Sc_rollup.Inbox.t option #**)
    (**# ifdef __META13__ ; ticket_receipt : Ticket_receipt.t option
       ; genesis_commitment_hash : Sc_rollup.Commitment.Hash.t option #**)
    }
  val insert : (t, unit, Caqti_mult.zero_or_one) Caqti_request.t
end
(**# ifndef __META8__ CLOSE_COMMENTS #**)


(**# ifndef __META9__ OPEN_COMMENTS #**)
module Register_global_constant_table : sig
  type t = {
    opaid : opaid
  ; source_id : kid
  ; fee : Tez.t option
  ; consumed_gas : Fpgas.t option
  ; value : Script_repr.lazy_expr
  ; size_of_constant : Z.t option
  ; global_address : Script_expr_hash.t option
  ; nonce : int option
  ; status : operation_status
  ; error_list : error_list option
  }
  val insert : (t, unit, Caqti_mult.zero_or_one) Caqti_request.t
end
(**# ifndef __META9__ CLOSE_COMMENTS #**)

(**# ifndef __META10__ OPEN_COMMENTS #**)
module Set_deposits_limit_table : sig
  type t = {
    opaid : opaid
  ; source_id : kid
  ; fee : Tez.t option
  ; consumed_gas : Fpgas.t option
  ; value : Tez.t option
  ; nonce : int option
  ; status : operation_status
  ; error_list : error_list option
  }
  val insert : (t, unit, Caqti_mult.zero_or_one) Caqti_request.t
end
(**# ifndef __META10__ CLOSE_COMMENTS #**)

(**# ifndef __META11__ OPEN_COMMENTS #**)
(**# ifdef __META16__ OPEN_COMMENTS #**)
module Tx_rollup_origination_table : sig
  type t = {
    opaid : opaid
  ; source_id : kid
  ; fee : Tez.t option
  ; consumed_gas : Fpgas.t option
  ; nonce : int option
  ; status : operation_status
  ; error_list : error_list option
  ; originated_tx_rollup : kid option
  }
  val insert : (t, unit, Caqti_mult.zero_or_one) Caqti_request.t
end

module Tx_rollup_submit_batch_table : sig
  type t = {
    opaid : opaid
  ; source_id : kid
  ; fee : Tez.t option
  ; consumed_gas : Fpgas.t option
  ; nonce : int option
  ; status : operation_status
  ; error_list : error_list option
  ; tx_rollup : kid
  ; content : string
  ; burn_limit : Tez.t option
  ; paid_storage_size_diff : Z.t option
  }
  val insert : (t, unit, Caqti_mult.zero_or_one) Caqti_request.t
end

module Tx_rollup_commit_table : sig
  type t = {
    opaid : opaid
  ; source_id : kid
  ; fee : Tez.t option
  ; consumed_gas : Fpgas.t option
  ; nonce : int option
  ; status : operation_status
  ; error_list : error_list option
  ; tx_rollup : kid
  ; commitment : Tx_rollup_commitment.Full.t
  }
  val insert : (t, unit, Caqti_mult.zero_or_one) Caqti_request.t
end

module Tx_rollup_return_bond_table : sig
  type t = {
    opaid : opaid
  ; source_id : kid
  ; fee : Tez.t option
  ; consumed_gas : Fpgas.t option
  ; nonce : int option
  ; status : operation_status
  ; error_list : error_list option
  ; tx_rollup : kid
  }
  val insert : (t, unit, Caqti_mult.zero_or_one) Caqti_request.t
end

module Tx_rollup_remove_commitment_table : sig
  type t = {
    opaid : opaid
  ; source_id : kid
  ; fee : Tez.t option
  ; consumed_gas : Fpgas.t option
  ; nonce : int option
  ; status : operation_status
  ; error_list : error_list option
  ; tx_rollup : kid
  ; level : Tx_rollup_level.t option
  }
  val insert : (t, unit, Caqti_mult.zero_or_one) Caqti_request.t
end

module Tx_rollup_finalize_commitment_table : sig
  type t = {
    opaid : opaid
  ; source_id : kid
  ; fee : Tez.t option
  ; consumed_gas : Fpgas.t option
  ; nonce : int option
  ; status : operation_status
  ; error_list : error_list option
  ; tx_rollup : kid
  ; level : Tx_rollup_level.t option
  }
  val insert : (t, unit, Caqti_mult.zero_or_one) Caqti_request.t
end

module Tx_rollup_rejection_table : sig
  type t = {
    opaid : opaid
  ; source_id : kid
  ; fee : Tez.t option
  ; consumed_gas : Fpgas.t option
  ; nonce : int option
  ; status : operation_status
  ; error_list : error_list option
  ; tx_rollup : kid
  ; level : Tx_rollup_level.t
  ; message : Tx_rollup_message.t
  ; message_position : int
  ; message_path : Tx_rollup_inbox.Merkle.path
  ; message_result_hash : Tx_rollup_message_result_hash.t
  ; message_result_path : Tx_rollup_commitment.Merkle.path
  ; previous_message_result : Tx_rollup_message_result.t
  ; previous_message_result_path : Tx_rollup_commitment.Merkle.path
  ; proof : Tx_rollup_l2_proof.(**# ifdef __META13__ serialized #**)(**# else t #**)
  }
  val insert : (t, unit, Caqti_mult.zero_or_one) Caqti_request.t
end

module Tx_rollup_dispatch_tickets_table : sig
  type t = {
    opaid : opaid
  ; source_id : kid
  ; fee : Tez.t option
  ; consumed_gas : Fpgas.t option
  ; nonce : int option
  ; status : operation_status
  ; error_list : error_list option
  ; tx_rollup : kid
  ; level : Tx_rollup_level.t
  ; context_hash : Context_hash.t
  ; message_index : int
  ; message_result_path : Tx_rollup_commitment.Merkle.path
  ; tickets_info : Tx_rollup_reveal.t list
  ; paid_storage_size_diff : Z.t option
  }
  val insert : (t, unit, Caqti_mult.zero_or_one) Caqti_request.t
end
(**# ifdef __META16__ CLOSE_COMMENTS #**)
module Transfer_ticket_table : sig
  type t = {
    opaid : opaid
  ; source_id : kid
  ; fee : Tez.t option
  ; consumed_gas : Fpgas.t option
  ; status : operation_status
  ; error_list : error_list option
  ; contents : Script_repr.lazy_expr
  ; ty : Script_repr.lazy_expr
  ; ticketer : kid
  ; amount : Z.t
  ; destination : kid
  ; entrypoint : Entrypoint_repr.t
  ; paid_storage_size_diff : Z.t option
  (**# ifdef __META14__ ; ticket_receipt : Ticket_receipt.t option #**)
  }
  val insert : (t, unit, Caqti_mult.zero_or_one) Caqti_request.t
end
(**# ifndef __META11__ CLOSE_COMMENTS #**)

(**# ifndef __META12__ OPEN_COMMENTS #**)
module Vdf_revelation_table : sig
  val insert : ((opaid, Seed.vdf_solution) tup2, unit, Caqti_mult.zero_or_one) Caqti_request.t
end

module Event_table : sig
  val insert : ((opaid, opaid, int, Tez.tez option,
                 operation_status, Protocol.Alpha_context.Script.expr, Protocol.Entrypoint_repr.t, Protocol.Alpha_context.Script.expr,
                 Fpgas.t option, error_list option) tup10, unit, Caqti_mult.zero_or_one) Caqti_request.t
end
module Increase_paid_storage_table : sig
  val insert : ((opaid, opaid, int option, Tez.tez option,
                 operation_status, Z.t, kid, Fpgas.t option,
                 error_list option) tup9, unit, Caqti_mult.zero_or_one) Caqti_request.t
end

module Sc_rollup_recover_bond_table : sig
  val insert : ((opaid, opaid, int option, Tez.tez option
                , operation_status, Fpgas.t option, error_list option, kid) tup8
               , unit, Caqti_mult.zero_or_one) Caqti_request.t
end
module Sc_rollup_dal_slot_subscribe_table : sig
  val insert : ((opaid, opaid, int option, Tez.tez option
                , operation_status, Fpgas.t option, error_list option, kid
                , Raw_level.t option, Dal.Slot_index.t) tup10
               , unit, Caqti_mult.zero_or_one) Caqti_request.t
end

(**# ifndef __META12__ CLOSE_COMMENTS #**)

(**# ifndef __META13__ OPEN_COMMENTS #**)
module Sc_rollup_publish_table : sig
  type t = {
    opaid : opaid
  ; source_id : kid
  ; fee : Tez.t option
  ; consumed_gas : Fpgas.t option
  ; nonce : int option
  ; status : operation_status
  ; error_list : error_list option
  ; rollup : kid
  ; commitment : Sc_rollup.Commitment.t
  ; staked_hash : Sc_rollup.Commitment.Hash.t option
  ; published_at_level : Raw_level.t option
  }
  val insert : (t, unit, Caqti_mult.zero_or_one) Caqti_request.t
end
module Sc_rollup_execute_outbox_message_table : sig
  val insert : ((opaid, opaid, int option, Tez.tez option
                , operation_status, Fpgas.t option, error_list option, Z.t option
                , kid, Sc_rollup.Commitment.Hash.t, string, Ticket_receipt.t option) tup12
               , unit, Caqti_mult.zero_or_one) Caqti_request.t
end
module Sc_rollup_originate_table : sig
  type t = {
    opaid : opaid
  ; source_id : kid
  ; fee : Tez.t option
  ; consumed_gas : Fpgas.t option
  ; nonce : int option
  ; status : operation_status
  ; error_list : error_list option
  ; kind : Sc_rollup.Kind.t
  ; boot_sector_hash : string
  ; address : kid option
  ; size : Z.t option
  ; parameters_ty : Script.lazy_expr
  ; origination_proof : string option
  ; genesis_commitment_hash : Sc_rollup.Commitment.Hash.t option
  }
  val insert : (t, unit, Caqti_mult.zero_or_one) Caqti_request.t
end

module Sc_rollup_boot_sector_table : sig
  type t = {
    boot_sector_hash : string
  ; boot_sector : string
  }
  val insert_txt : (t, string, Caqti_mult.zero_or_one) Caqti_request.t
  val insert_bin : (t, string, Caqti_mult.zero_or_one) Caqti_request.t
end

module Update_consensus_key_table : sig
  val insert : ((opaid, opaid, int option, Tez.tez option
                , operation_status, Fpgas.t option, error_list option, Public_key.t) tup8
               , unit, Caqti_mult.zero_or_one) Caqti_request.t
end

module Zk_rollup_origination_table : sig
  type 'circuits_info t = {
    opaid : opaid;
    source_id: kid ;
    originated_zk_rollup: kid option;
    consumed_gas : Fpgas.t option;
    storage_size : Z.t option;
    fee : Tez.t option ;
    public_parameters : Tezos_protocol_environment_(**# get PROTO #**).Plonk.public_parameters;
    circuits_info : 'circuits_info;
    init_state : Zk_rollup.State.t;
    nb_ops : int;
    status : operation_status;
    error_list : error_list option;
  }
  val insert : 'circuits_info Data_encoding.t -> ('circuits_info t, unit, Caqti_mult.zero_or_one) Caqti_request.t

end
module Zk_rollup_publish_table : sig
  type t = {
    opaid : opaid
  ; source_id : kid
  ; fee : Tez.t option
  ; consumed_gas : Fpgas.t option
  ; status : operation_status
  ; error_list : error_list option
  ; zk_rollup : kid
  ; ops : (Zk_rollup.Operation.t * Zk_rollup.Ticket.t option) list
  ; paid_storage_size_diff : Z.t option
  }
  val insert : (t, unit, Caqti_mult.zero_or_one) Caqti_request.t
end
module Drain_delegate_table : sig
  type t = {
    opaid: opaid;
    consensus_key : kid;
    delegate : kid;
    destination : kid;
    allocated_destination_contract: bool;
  }
  val insert : (t, unit, Caqti_mult.zero_or_one) Caqti_request.t
end

(**# ifndef __META13__ CLOSE_COMMENTS #**)
(**# ifndef __META14__ OPEN_COMMENTS #**)
module Sc_rollup_cement_table : sig
  type t = {
    opaid : opaid
  ; source_id : kid
  ; fee : Tez.t option
  ; consumed_gas : Fpgas.t option
  ; status : operation_status
  ; error_list : error_list option
  ; inbox_level : int32 option
  ; rollup : kid
  ; commitment : Sc_rollup.Commitment.Hash.t option
  }
  val insert : (t, unit, Caqti_mult.zero_or_one) Caqti_request.t
end

module Sc_rollup_add_messages_table : sig
  type t = {
    opaid : opaid
  ; source_id : kid
  ; fee : Tez.t option
  ; consumed_gas : Fpgas.t option
  ; nonce : int option
  ; status : operation_status
  ; error_list : error_list option
  ; messages : string list
  }
  val insert : (t, unit, Caqti_mult.zero_or_one) Caqti_request.t
end

module Dal_publish_slot_header_table : sig
  val insert : ((opaid, opaid, Tez.tez option, operation_status,
                 Fpgas.t option, int32 option, error_list option, Dal.Slot_index.t
                , Dal.Slot.Commitment.t, Dal.Slot.Commitment_proof.t) tup10
               , unit, Caqti_mult.zero_or_one) Caqti_request.t
end

module Dal_attestation_table : sig
  val insert :
    ((opaid, (**# ifndef __META16__ kid, #**) Dal.Attestation.t, int32, kid) (**# ifdef __META16__ tup4 #**)(**# else tup5 #**)
    , unit, Caqti_mult.zero_or_one) Caqti_request.t
end

module Sc_rollup_refute_table : sig
  type t = {
    opaid : opaid
  ; source_id : kid
  ; fee : Tez.t option
  ; consumed_gas : Fpgas.t option
  ; status : operation_status
  ; error_list : error_list option
  ; rollup : kid
  ; opponent : kid
  ; refutation : Sc_rollup.Game.refutation
  ; game_status : Sc_rollup.Game.status option
  }
  val insert : (t, unit, Caqti_mult.zero_or_one) Caqti_request.t
end

module Sc_rollup_timeout_table : sig
  type t = {
    opaid : opaid
  ; source_id : kid
  ; fee : Tez.t option
  ; consumed_gas : Fpgas.t option
  ; status : operation_status
  ; error_list : error_list option
  ; rollup : kid
  ; stakers : Sc_rollup.Game.Index.t
  ; game_status : Sc_rollup.Game.status option
  }
  val insert : (t, unit, Caqti_mult.zero_or_one) Caqti_request.t
end





(* module Dal_slot_availability_table : sig
 *   val insert :
 *     ((opaid, opaid, operation_status option,
 *       Tez.tez,
 *       operation_status, Fpgas.t option,
 *       Protocol.Alpha_context.Dal.Slot.Header.t, error_list option,
 *       Signature.public_key_hash,
 *       Protocol.Alpha_context.Dal.Endorsement.t, int32) tup11
 *     , unit, Caqti_mult.zero_or_one) Caqti_request.t
 * end *)
(**# ifndef __META14__ CLOSE_COMMENTS #**)
