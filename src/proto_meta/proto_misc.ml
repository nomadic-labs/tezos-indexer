(* open Legacy_monad_globals *)
(**#X ifdef __META1__ open Tezos_raw_protocol_(**# get PROTO#**)X#**)
(**#X else open Tezos_protocol_(**# get PROTO #**) open Protocol X#**)
(* module Environment = Tezos_protocol_environment_(\**# get PROTO #**\) *)
(* open Tezos_indexer_lib.Std *)

(**# ifndef __META12__ OPEN_COMMENTS #**)
(**# ifdef __PROTO14__
type 'a internal_operation_contents = 'a Apply_internal_results.internal_manager_operation
type 'a internal_operation_result = 'a Apply_internal_results.internal_manager_operation_result
 #**)
(**# else type 'a internal_operation_contents = 'a Apply_internal_results.internal_operation_contents
type 'a internal_operation_result = 'a Apply_internal_results.internal_operation_result
 #**)
(**# ifndef __META12__ CLOSE_COMMENTS #**)

(**# cmd opam exec ocaml -- -stdin
let proto = int_of_string (Sys.getenv "PROTO")

type proto_filter =
 | F of int (* from *)
 | O of int list (* only *)
 | B of int (* before *)

let proto_filter l = List.filter (fun (limit, _, _) ->
  (proto > 99) ||
   (* sorry for this "hack" -- we consider that if the protocol filter takes
      only protocols >99, then it actually means all protocols and not a real
      protocol. This would have to change if it stops working, of course, like
      if we reach protocol number 100 one day. But we have a lot of time before
      that happens...  *)
  (
    match limit with
    | B p -> proto < p
    | O p -> List.mem proto p
    | F p -> proto >= p
    )
  ) l |> List.map (fun (_, a, b) -> a, b)

let internalable_manager_operations = proto_filter [
  (F 1, "Transaction", 8);
  (F 1, "Origination", 9);
  (F 1, "Delegation", 10);
]

let rec seq a b = a :: (if a = b then [] else seq (succ a) b)

let internal_only_manager_operations =
  proto_filter [
    (F 14, "Event", 43)
  ]

let internal_manager_operations =
  internal_only_manager_operations
@ internalable_manager_operations

let manager_operations =
 internalable_manager_operations @
 proto_filter [
  (F 1, "Reveal", 7);
  (O [13; 14; 15; 16], "Tx_rollup_origination", 17);
  (O [13; 14; 15; 16], "Tx_rollup_submit_batch", 18);
  (O [13; 14; 15; 16], "Tx_rollup_commit", 19);
  (O [13; 14; 15; 16], "Tx_rollup_return_bond", 20);
  (O [13; 14; 15; 16], "Tx_rollup_finalize_commitment", 21);
  (O [13; 14; 15; 16], "Tx_rollup_remove_commitment", 22);
  (O [13; 14; 15; 16], "Tx_rollup_rejection", 23);
  (O [13; 14; 15; 16], "Tx_rollup_dispatch_tickets", 24);
  (F 13, "Transfer_ticket", 25);
  (F 13, "Sc_rollup_originate", 26);
  (F 13, "Sc_rollup_add_messages", 27);
  (F 13, "Sc_rollup_cement", 28);
  (F 13, "Sc_rollup_publish", 29);
  (F 14, "Sc_rollup_refute", 30);
  (F 14, "Sc_rollup_timeout", 31);
  (F 14, "Dal_publish_slot_header", 32);
  (F 14, "Sc_rollup_execute_outbox_message", 33);
  (F 14, "Sc_rollup_recover_bond", 34);
  (O [14;15], "Sc_rollup_dal_slot_subscribe", 35);
  (F 14, "Increase_paid_storage", 38);
  (F 11, "Register_global_constant", 13);
  (O [12; 13; 14; 15; 16; 17; 18], "Set_deposits_limit", 14);
  (F 15, "Zk_rollup_origination", 39);
  (F 15, "Update_consensus_key", 40);
  (F 15, "Zk_rollup_publish", 42);
  (F 16, "Zk_rollup_update", 44);
]

let seq_1_17 = seq 1 17
let other_operations = proto_filter [
  (O seq_1_17, "Endorsement", 0);
  (F 18, "Attestation", 0);
  (F 1, "Seed_nonce_revelation", 1);
  (O seq_1_17, "Double_endorsement_evidence", 2);
  (F 18, "Double_attestation_evidence", 2);
  (F 1, "Double_baking_evidence", 3);
  (F 1, "Activate_account", 4);
  (F 1, "Proposals", 5);
  (F 1, "Ballot", 6);
  (O [9;10;11], "Endorsement_with_slot", 11);
  (F 100, "Endorsement_with_slot", 11);
  (O (seq 12 17), "Preendorsement", 15);
  (F 18, "Preattestation", 15);
  (O (seq 12 17), "Double_preendorsement_evidence", 16);
  (F 18, "Double_preattestation_evidence", 16);
  (F 9, "Failing_noop", 12);
  (O [14;15], "Dal_slot_availability", 36);
  (F 14, "Vdf_revelation", 37);
  (F 15, "Drain_delegate", 41);
  (F 16, "Dal_attestation", 45);
]

let regular_operations = manager_operations @ other_operations

let all_operations = internal_only_manager_operations @ manager_operations @ other_operations

let () = if proto >= 14 then
  begin
    print_endline "let [@warning \"-28\"] int_of_internal_manager_operation : type a. a internal_operation_contents -> _ = function";
    internal_manager_operations |> List.iter (
      fun (op, n) ->
       Printf.printf "| %s _ -> %d\n" op n
    )
  end

let () =
  begin
    print_endline "let [@warning \"-28\"] int_of_manager_operation : type a. a Alpha_context.manager_operation -> _ = function";
    manager_operations |> List.iter (
      fun (op, n) ->
       Printf.printf "| %s _ -> %d\n" op n
    )
  end

let () =
  print_endline "let [@warning \"-28\"] int_of_contents : type a. a Alpha_context.contents -> int = function";
  other_operations |> List.iter (
    fun (op, n) ->
     Printf.printf "| %s _ -> %d\n" op n
  );
  manager_operations |> List.iter (
    fun (op, n) ->
     Printf.printf "| Manager_operation { operation = %s _ ; _ } -> %d\n" op n
  )

let () =
  print_endline "let [@warning \"-28\"] string_of_contents : type a. a Alpha_context.contents -> string = function";
  other_operations |> List.iter (
    fun (op, _n) ->
     Printf.printf "| %s _ -> \"%s\"\n" op op
  );
  manager_operations |> List.iter (
    fun (op, _n) ->
     Printf.printf "| Manager_operation { operation = %s _ ; _ } -> \"%s\"\n" op op
  )

let () = if proto >= 10 then
  begin
    print_endline "let [@warning \"-28\"] int_of_manager_operation_result : type a.a Apply_results.successful_manager_operation_result -> int = function";
    manager_operations |> List.iter (
      fun (op, n) ->
        Printf.printf "| %s_result _ -> %d\n" op n
    )
  end

let () = if proto >= 100 then
  begin
    all_operations |> List.iter (
      fun (op, n) ->
        Printf.printf "select declare_operation('%s', %d);\n" op n
    )
  end
#**)
