-- Open Source License
-- Copyright (c) 2020-2022 Nomadic Labs <contact@nomadic-labs.com>
--
-- Permission is hereby granted, free of charge, to any person obtaining a
-- copy of this software and associated documentation files (the "Software"),
-- to deal in the Software without restriction, including without limitation
-- the rights to use, copy, modify, merge, publish, distribute, sublicense,
-- and/or sell copies of the Software, and to permit persons to whom the
-- Software is furnished to do so, subject to the following conditions:
--
-- The above copyright notice and this permission notice shall be included
-- in all copies or substantial portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
-- IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
-- FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
-- THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
-- LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
-- FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
-- DEALINGS IN THE SOFTWARE.

-- Lines starting with --OPT may be automatically activated
-- Lines ending with --OPT may be automatically deactivated

-- DB schema for operations in the mempool, so you may track the life of an operation

SELECT 'mempool.sql' as file;

DO $$
BEGIN
  IF NOT EXISTS (SELECT 1 FROM pg_type WHERE typname = 'mempool_op_status') THEN
    CREATE TYPE mempool_op_status AS ENUM ('applied', 'refused', 'branch_refused', 'unprocessed', 'branch_delayed');
  END IF;
END
$$;

CREATE TABLE IF NOT EXISTS M.operation_alpha (
  hash char(51) not null,
  first_seen_level int not null,
  first_seen_timestamp double precision not null,
  last_seen_level int not null,
  last_seen_timestamp double precision not null,
  status mempool_op_status not null,
  id smallint not null,
  -- index of op in contents_list
  operation_kind smallint not null,
  -- ... cf. chain.sql
  source char(36),
  -- sender
  destination char(36),
  -- receiver, if any
  operation_alpha jsonb,
  branch char(51),
  autoid SERIAL, -- this field should always be last
  PRIMARY KEY(hash, id, status)
);
CREATE INDEX IF NOT EXISTS mempool_operations_hash on M.operation_alpha(hash); --1
CREATE INDEX IF NOT EXISTS mempool_operations_status on M.operation_alpha(status); --OPT --1
CREATE INDEX IF NOT EXISTS mempool_operations_source on M.operation_alpha(source); --1
CREATE INDEX IF NOT EXISTS mempool_operations_destination on M.operation_alpha(destination); --1
CREATE INDEX IF NOT EXISTS mempool_operations_kind on M.operation_alpha(operation_kind); --1
CREATE INDEX IF NOT EXISTS mempool_operations_branch on M.operation_alpha(branch);  --1
CREATE INDEX IF NOT EXISTS mempool_operations_autoid on M.operation_alpha(autoid);  --1

CREATE TABLE IF NOT EXISTS M.operation_kinds (
  kind smallint PRIMARY KEY
);

CREATE OR REPLACE FUNCTION M.I_operation_alpha (
  xbranch char(51),
  xlevel int,
  xhash char(51),
  xstatus mempool_op_status,
  xid smallint,
  xoperation_kind smallint,
  xsource char(36),
  xdestination char(36),
  xseen_timestamp double precision,
  xoperation_alpha jsonb
)
RETURNS void
AS $$
  INSERT INTO M.operation_alpha (
      hash
    , first_seen_level
    , first_seen_timestamp
    , last_seen_level
    , last_seen_timestamp
    , status
    , id
    , operation_kind
    , source
    , destination
    , operation_alpha
    , branch
  ) SELECT
      xhash
    , xlevel
    , xseen_timestamp
    , xlevel
    , xseen_timestamp
    , xstatus
    , xid
    , xoperation_kind
    , xsource
    , xdestination
    , xoperation_alpha
    , xbranch
  WHERE (SELECT * FROM M.operation_kinds) IS NULL
     OR xoperation_kind IN (SELECT kind FROM M.operation_kinds)
  ON CONFLICT (hash, id, status)
  DO UPDATE SET
    last_seen_level = xlevel
  , last_seen_timestamp = xseen_timestamp
  WHERE M.operation_alpha.hash = xhash
    AND M.operation_alpha.id = xid
    AND M.operation_alpha.status = xstatus
$$ LANGUAGE SQL;

CREATE OR REPLACE FUNCTION M.purge (distance int)
RETURNS VOID
AS $$
DECLARE x int := (SELECT level - distance FROM C.block ORDER BY level desc limit 1);
BEGIN
DELETE FROM M.operation_alpha WHERE last_seen_level < x;
END $$ LANGUAGE PLPGSQL;
