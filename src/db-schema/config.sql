-- Open Source License
-- Copyright (c) 2021 Nomadic Labs <contact@nomadic-labs.com>
--
-- Permission is hereby granted, free of charge, to any person obtaining a
-- copy of this software and associated documentation files (the "Software"),
-- to deal in the Software without restriction, including without limitation
-- the rights to use, copy, modify, merge, publish, distribute, sublicense,
-- and/or sell copies of the Software, and to permit persons to whom the
-- Software is furnished to do so, subject to the following conditions:
--
-- The above copyright notice and this permission notice shall be included
-- in all copies or substantial portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
-- IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
-- FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
-- THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
-- LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
-- FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
-- DEALINGS IN THE SOFTWARE.

CREATE TABLE IF NOT EXISTS F.config (
  feature varchar(80) primary key,
  inactive bool not null
);

-- default values:
INSERT INTO F.config (feature, inactive) VALUES ('postmulticore_address_extraction', true) ON CONFLICT DO NOTHING;
INSERT INTO F.config (feature, inactive) VALUES ('postmulticore_contract_balance', true) ON CONFLICT DO NOTHING;
INSERT INTO F.config (feature, inactive) VALUES ('postmulticore_bigmap_copy', true) ON CONFLICT DO NOTHING;


CREATE OR REPLACE FUNCTION F.enable (feature_ varchar)
RETURNS VOID
AS $$
  INSERT INTO F.config VALUES (feature_, false) ON CONFLICT (feature) DO UPDATE SET inactive = false;
$$ LANGUAGE SQL;


CREATE OR REPLACE FUNCTION F.disable (feature_ varchar)
RETURNS VOID
AS $$
  INSERT INTO F.config VALUES (feature_, true) ON CONFLICT (feature) DO UPDATE SET inactive = true;
$$ LANGUAGE SQL;


CREATE OR REPLACE FUNCTION F.is_enabled (feature_ varchar)
RETURNS BOOL
AS $$
   SELECT COALESCE((select NOT inactive FROM F.config WHERE feature = feature_), true);
$$ LANGUAGE SQL STABLE;
