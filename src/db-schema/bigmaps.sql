-- Open Source License
-- Copyright (c) 2019-2022 Nomadic Labs <contact@nomadic-labs.com>
-- Copyright (c) 2021 Philippe Wang <philippe.wang@gmail.com>
--
-- Permission is hereby granted, free of charge, to any person obtaining a
-- copy of this software and associated documentation files (the "Software"),
-- to deal in the Software without restriction, including without limitation
-- the rights to use, copy, modify, merge, publish, distribute, sublicense,
-- and/or sell copies of the Software, and to permit persons to whom the
-- Software is furnished to do so, subject to the following conditions:
--
-- The above copyright notice and this permission notice shall be included
-- in all copies or substantial portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
-- IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
-- FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
-- THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
-- LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
-- FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
-- DEALINGS IN THE SOFTWARE.


SELECT 'bigmaps.sql' as file;

-- Table `C.bigmap` uses an storage method similar or equal to "copy-on-write",
-- to allow reorganisations to happen seemlessly while offering fast access to any
-- big map at any block level.
-- Depending on what your queries are, you might want to create additional indexes.
CREATE TABLE IF NOT EXISTS C.bigmap (
     id bigint
   , "key" jsonb -- key can be null because of allocs
   , key_hash char(54) -- key_hash can be null because key can be null
   , "key_type" jsonb
   , "value" jsonb -- if null, then it means it was deleted, or not filled yet
   , "value_type" jsonb
   , block_level int not null
   , operation_id bigint -- nullable since v9.5.0
   , operation_hash_id bigint -- since v9.9.0
   , implicit_operations_results_id int -- since v9.5.0
   , sender_id bigint -- nullable since v9.5.0
   , receiver_id bigint -- nullable since v9.5.0
   , i bigint not null -- i is for ordering: the later it comes in the blockchain, the greater the value -- changed with v9.9.0
                       -- i is not unique, duplications are caused by copies - i can no longer be negative
   , kind smallint not null -- 0: alloc, 1: update, 2: clear, 3: result of a copy, and (since v10.1.0) -1: result of a copy from an alloc
   , annots text
   , strings text[]
   , uri int[]
   , contracts bigint[]
   , metadata jsonb
);

-- about temporary big maps:
-- val reset_temporary_big_map
-- temporary_lazy_storage_ids
-- Lazy_storage_diff.cleanup_temporaries
-- every time [apply_manager_contents_list] is successful, it cleans up the temporaries (if not successful, context isn't changed anyways)

-- BEGIN FOR COPIES =================================================================
create index IF NOT EXISTS bigmap_id_ophid_i_k ON C.bigmap using btree(id, operation_hash_id, i, kind); --SEQONLY

-- The following index seems to create conflict for the execution planner, since removing it resulted in taking 30% less execution time
-- create index IF NOT EXISTS bigmap_id_kh_block_level_i ON C.bigmap using btree(id, key_hash, block_level, i); --SEQONLY

create index IF NOT EXISTS bigmap_bl_id_i_k_kh ON C.bigmap using btree(block_level, id, i, kind, key_hash); --SEQONLY

create index IF NOT EXISTS bigmap_id_ophid_i ON C.bigmap using btree(id, operation_hash_id, i); --SEQONLY
create index if not exists bigmap_ophid_id_kind ON c.bigmap using btree(operation_hash_id, id, kind); --SEQONLY
create index if not exists bigmap_kh_i ON c.bigmap using btree(key_hash, id); --SEQONLY
create index if not exists bigmap_kind_id_i_kh ON c.bigmap using btree(kind, id, i, key_hash); --SEQONLY


--------------------------------

DROP FUNCTION IF EXISTS B.get_for_copy_aux1n;
CREATE OR REPLACE FUNCTION B.get_for_copy_aux1n (xid bigint, xblock_level int, xi bigint)
RETURNS TABLE (key_hash char, id bigint, "key" jsonb, key_type jsonb, "value" jsonb, value_type jsonb, block_level int, i bigint, kind smallint, annots text, strings text[], uri int[], contracts bigint[], metadata jsonb, operation_hash_id bigint)
AS $$
    (SELECT b.key_hash, b.id, b."key", b.key_type, b."value", b.value_type, b.block_level, b.i, b.kind, b.annots, b.strings, b.uri, b.contracts, b.metadata, b.operation_hash_id
     FROM C.bigmap b
     WHERE (b.block_level, b.id) = (xblock_level, xid) AND b.i < xi AND kind < 1
     ORDER BY i DESC limit 1)
UNION
    (SELECT DISTINCT ON (b.key_hash) key_hash, b.id, b."key", b.key_type, b."value", b.value_type, b.block_level, b.i, b.kind, b.annots, b.strings, b.uri, b.contracts, b.metadata, b.operation_hash_id
     FROM C.bigmap b
     WHERE (b.block_level, b.id) = (xblock_level, xid) AND b.i < xi AND kind > 0
     ORDER BY b.key_hash, i DESC)
$$ LANGUAGE SQL STABLE;

DROP FUNCTION IF EXISTS B.get_for_copy_aux1p;
CREATE OR REPLACE FUNCTION B.get_for_copy_aux1p (xid bigint, xblock_level int, xi bigint)
RETURNS TABLE (key_hash char, id bigint, "key" jsonb, key_type jsonb, "value" jsonb, value_type jsonb, block_level int, i bigint, kind smallint, annots text, strings text[], uri int[], contracts bigint[], metadata jsonb, operation_hash_id bigint)
AS $$
    (SELECT b.key_hash, b.id, b."key", b.key_type, b."value", b.value_type, b.block_level, b.i, b.kind, b.annots, b.strings, b.uri, b.contracts, b.metadata, b.operation_hash_id
     FROM C.bigmap b
     WHERE kind < 1 AND b.id = xid AND b.i < xi
     ORDER BY i DESC limit 1)
UNION
    (SELECT DISTINCT ON (b.key_hash) key_hash, b.id, b."key", b.key_type, b."value", b.value_type, b.block_level, b.i, b.kind, b.annots, b.strings, b.uri, b.contracts, b.metadata, b.operation_hash_id
     FROM C.bigmap b
     WHERE kind > 0 AND b.id = xid AND b.i < xi
     ORDER BY b.key_hash, i DESC)
$$ LANGUAGE SQL STABLE;

DROP FUNCTION IF EXISTS B.get_for_copy_aux2n;
CREATE OR REPLACE FUNCTION B.get_for_copy_aux2n (xid bigint, xblock_level int, ophid bigint, xi bigint)
RETURNS TABLE (key_hash char, id bigint, "key" jsonb, key_type jsonb, "value" jsonb, value_type jsonb, block_level int, i bigint, kind smallint, annots text, strings text[], uri int[], contracts bigint[], metadata jsonb, operation_hash_id bigint)
AS $$
    (SELECT b.key_hash, b.id, b."key", b.key_type, b."value", b.value_type, b.block_level, b.i, b.kind, b.annots, b.strings, b.uri, b.contracts, b.metadata, b.operation_hash_id
     FROM C.bigmap b
     WHERE (b.id, b.operation_hash_id) = (xid, ophid) AND b.i < xi AND kind < 1
     ORDER BY i DESC limit 1)
UNION
    (SELECT DISTINCT ON (b.key_hash) key_hash, b.id, b."key", b.key_type, b."value", b.value_type, b.block_level, b.i, b.kind, b.annots, b.strings, b.uri, b.contracts, b.metadata, b.operation_hash_id
     FROM C.bigmap b
     WHERE (b.id, b.operation_hash_id) = (xid, ophid) AND  b.i < xi AND kind > 0
     ORDER BY b.key_hash, i DESC)
$$ LANGUAGE SQL STABLE;

DROP FUNCTION IF EXISTS B.get_for_copy_aux2p;
CREATE OR REPLACE FUNCTION B.get_for_copy_aux2p (xid bigint, xblock_level int, ophid bigint, xi bigint)
RETURNS TABLE (key_hash char, id bigint, "key" jsonb, key_type jsonb, "value" jsonb, value_type jsonb, block_level int, i bigint, kind smallint, annots text, strings text[], uri int[], contracts bigint[], metadata jsonb, operation_hash_id bigint)
AS $$
    (SELECT b.key_hash, b.id, b."key", b.key_type, b."value", b.value_type, b.block_level, b.i, b.kind, b.annots, b.strings, b.uri, b.contracts, b.metadata, b.operation_hash_id
     FROM C.bigmap b
     WHERE kind < 1 AND b.id = xid AND b.i < xi
     ORDER BY i DESC limit 1)
UNION
    (SELECT DISTINCT ON (b.key_hash) key_hash, b.id, b."key", b.key_type, b."value", b.value_type, b.block_level, b.i, b.kind, b.annots, b.strings, b.uri, b.contracts, b.metadata, b.operation_hash_id
     FROM C.bigmap b
     WHERE kind > 0 AND b.id = xid AND b.i < xi
     ORDER BY b.key_hash, i DESC)
$$ LANGUAGE SQL STABLE;

DROP FUNCTION IF EXISTS B.get_for_copy;
CREATE OR REPLACE FUNCTION B.get_for_copy (xid bigint, xblock_level int, ophid bigint, xi bigint)
RETURNS TABLE (id bigint, "key" jsonb, key_hash char, key_type jsonb, "value" jsonb, value_type jsonb, block_level int, i bigint, kind smallint, annots text, strings text[], uri int[], contracts bigint[], metadata jsonb, operation_hash_id bigint)
AS $$
BEGIN
IF ophid IS NULL THEN
  IF xid >= 0 THEN
    RETURN QUERY
     SELECT b.id, b."key", b.key_hash, b.key_type, b."value", b.value_type, b.block_level, b.i, b.kind, b.annots, b.strings, b.uri, b.contracts, b.metadata, b.operation_hash_id
     FROM B.get_for_copy_aux1p(xid, xblock_level, xi) b;
  ELSE
    RETURN QUERY
     SELECT b.id, b."key", b.key_hash, b.key_type, b."value", b.value_type, b.block_level, b.i, b.kind, b.annots, b.strings, b.uri, b.contracts, b.metadata, b.operation_hash_id
     FROM B.get_for_copy_aux1n(xid, xblock_level, xi) b;
  END IF;
ELSE
  IF xid >= 0 THEN
    RETURN QUERY
     SELECT b.id, b."key", b.key_hash, b.key_type, b."value", b.value_type, b.block_level, b.i, b.kind, b.annots, b.strings, b.uri, b.contracts, b.metadata, b.operation_hash_id
     FROM B.get_for_copy_aux2p(xid, xblock_level, ophid, xi) b;
  ELSE
    RETURN QUERY
     SELECT b.id, b."key", b.key_hash, b.key_type, b."value", b.value_type, b.block_level, b.i, b.kind, b.annots, b.strings, b.uri, b.contracts, b.metadata, b.operation_hash_id
     FROM B.get_for_copy_aux2n(xid, xblock_level, ophid, xi) b;
  END IF;
END IF;
END;
$$ LANGUAGE PLPGSQL STABLE;

-- CREATE SEQUENCE IF NOT EXISTS C.bigmap_serial START 1;

DROP FUNCTION IF EXISTS B.copy;
CREATE OR REPLACE FUNCTION B.copy (xid bigint, yid bigint, xblock_level int, xsender bigint, xreceiver bigint, xi bigint, opaid bigint, iorid int, ophid bigint)
returns void
as $$
--     9 007 199 254 740 991 is max int53
-- 9 223 372 036 854 775 807 is max int64
BEGIN
INSERT INTO C.bigmap
 (id, "key", key_hash, "key_type", "value", value_type, block_level, sender_id, receiver_id, i,                    operation_id, implicit_operations_results_id, kind, annots, strings, uri, contracts, metadata, operation_hash_id) --
SELECT
 yid, r."key", r.key_hash, r."key_type", r."value", r.value_type, xblock_level, xsender, xreceiver,
 xi, opaid, iorid, coalesce((select -1 where kind < 1), 3), r.annots, r.strings, r.uri, r.contracts, r.metadata, ophid
FROM B.get_for_copy (xid, xblock_level, ophid, xi) r
ON CONFLICT DO NOTHING;
END;
$$ LANGUAGE PLPGSQL;
-- block_level * 1000000000 --> ([1-9] * 10^6) * 10^9 -> block_level can go up to 9*10^6 with int53, or 9*10^9 with int64 -- both are fine for now
-- Going at 10 blocks per minute (instead of 1), we'd be good with int53 up to about end of September 2022.
-- At the rythm of 1 block / min, this holds up to after year 2030.
-- i * 100000 --> i*10^5 --> the size of one bigmap is limited to 100K (0-99,999),
-- the number of bigmap diffs per block is limited to 99,999
-- If at some point it no longer fits, we'll change the numbers to fully use 64-bit integers.


-- There is no way for bigmaps to be fully "concurrently indexed by segments" because
-- we may record a bigmap copy without having access to the original bigmap.
-- Therefore we create a table that contains the copy instructions so we can apply them
-- when we convert the DB from multicore mode to default mode.
CREATE TABLE IF NOT EXISTS C.bigmap_delayed_copies ( --MULTICORE
  xid bigint not null, yid bigint not null, xblock_level int not null, xsender bigint not null, xreceiver bigint not null, i bigint not null, opaid bigint, iorid int, ophid bigint --MULTICORE
); --MULTICORE
CREATE INDEX IF NOT EXISTS bigmap_delayed_copies_i on C.bigmap_delayed_copies using btree(i); --MULTICORE

-- OVERRIDE B.copy in multicore mode only --MULTICORE
CREATE OR REPLACE FUNCTION B.copy (xid bigint, yid bigint, xblock_level int, xsender bigint, xreceiver bigint, xi bigint, opaid bigint, iorid int, ophid bigint) --MULTICORE
returns void --MULTICORE
as $$ --MULTICORE
insert into C.bigmap_delayed_copies values (xid, yid, xblock_level, xsender, xreceiver, xi, opaid, iorid, ophid); --MULTICORE
$$ language SQL; --MULTICORE

select now (); --SEQONLY
DO $$
BEGIN
  IF (SELECT count(*) FROM pg_tables WHERE tablename = 'indexer_version') > 0
     AND (SELECT count(*) FROM indexer_version WHERE conversion_in_progress) > 0
     AND (SELECT count(*) FROM pg_tables WHERE tablename = 'bigmap_delayed_copies' AND schemaname = 'c') > 0
     AND (select F.is_enabled('postmulticore_bigmap_copy'))
  THEN
    PERFORM B.copy (xid, yid, xblock_level, xsender, xreceiver, i, opaid, iorid, ophid) from C.bigmap_delayed_copies order by i asc;
    DELETE FROM C.bigmap_delayed_copies;
  END IF;
END;
$$;
select now (); --SEQONLY
-- END FOR COPIES =================================================================

-- BEGIN FOR METADATA =============================================================
create INDEX IF NOT EXISTS bigmap_uri on C.bigmap using GIN (uri); --1 --SEQONLY
create INDEX IF NOT EXISTS bigmap_contracts on C.bigmap using GIN (contracts); --1 --SEQONLY

CREATE OR REPLACE FUNCTION B.fill_metadata (xid bigint)
RETURNS VOID
AS $$
BEGIN
UPDATE C.bigmap SET metadata = (extract_jsonb_from_text_array(strings))[1] WHERE id = xid AND strings IS NOT NULL;
END;
$$ LANGUAGE PLPGSQL;

create index IF NOT EXISTS bigmap_annots on C.bigmap using btree (annots); --SEQONLY --1

DO $$
BEGIN
  IF (SELECT count(*) FROM indexer_version WHERE conversion_in_progress) > 0
  THEN
    BEGIN
      PERFORM B.fill_metadata(id) FROM C.bigmap WHERE annots = '%metadata'; -- <-- faster version
      -- UPDATE C.bigmap SET metadata = (extract_jsonb_from_text_array(strings))[1] WHERE ID in (select id from c.bigmap where annots = '%metadata') AND strings is not null; <--- slow version
    END;
  END IF;
END;
$$;

-- select 'creating bigmap_pkey';
-- DO $$
--   BEGIN
--   IF (select count(*) from indexer_version where conversion_in_progress) > 0
--   THEN
--     BEGIN
--       ALTER TABLE C.bigmap
--         ADD CONSTRAINT bigmap_pkey
--         PRIMARY KEY (block_level, i);
--     EXCEPTION
--     WHEN SQLSTATE '42P16' THEN RETURN;
--     END;
--   END IF;
-- END;
-- $$;

CREATE INDEX IF NOT EXISTS bigmap_pin ON C.bigmap USING BTREE ((strings IS NOT NULL AND array_length(strings, 1) > 0 AND uri IS NULL AND contracts IS NULL)); --SEQONLY
DO $$ --SEQONLY
BEGIN --SEQONLY
ALTER TABLE C.bigmap ADD COLUMN uid SERIAL NOT NULL; --SEQONLY
EXCEPTION WHEN OTHERS THEN RETURN; --SEQONLY
END $$; --SEQONLY
CREATE INDEX IF NOT EXISTS bigmap_uid ON C.bigmap USING BTREE (uid); --SEQONLY

DROP FUNCTION IF EXISTS b.bigmap_pin_addresses_from_strings (s text[], block_level int, bigmapindex bigint); --SEQONLY
CREATE OR REPLACE FUNCTION b.bigmap_pin_addresses_from_strings (s text[], uid_ int) --SEQONLY
RETURNS VOID --SEQONLY
AS $$ --SEQONLY
 with x as (select unnest(s) as element) --SEQONLY
 update c.bigmap --SEQONLY
   set --SEQONLY
   uri = --SEQONLY
     (select array_agg(I.uri(element)) from x where char_length(element) < 2048 AND (element ~ '[a-zA-Z][a-zA-Z]*://..*' or element ~ 'tezos-storage:.*')), --SEQONLY
   contracts = --SEQONLY
     (select array_agg(I.address(element::char(36),0::bigint)) from x where (element like 'tz%' or element like 'KT%') and octet_length(element) >= 36 --SEQONLY
     ) --SEQONLY
   where uid = uid_ ; --SEQONLY
$$ LANGUAGE SQL; --SEQONLY
 --SEQONLY
DO $$ --SEQONLY
BEGIN --SEQONLY
  --FIXME: this is deactivated for now because it takes too much time --SEQONLY
  IF FALSE AND (select count(*) from indexer_version where conversion_in_progress) > 0 AND (select F.is_enabled('postmulticore_address_extraction')) --SEQONLY
  THEN --SEQONLY
    PERFORM b.bigmap_pin_addresses_from_strings(strings, uid) --SEQONLY
    FROM c.bigmap --SEQONLY
    WHERE strings IS NOT NULL --SEQONLY
      AND array_length(strings, 1) > 0 --SEQONLY
      AND uri IS NULL --SEQONLY
      AND contracts IS NULL; --SEQONLY
  END IF; --SEQONLY
END $$; --SEQONLY
-- END FOR PIN ADDRESSES FROM STRINGS =================================================================


--the following pkey is useless... and can no longer be created anyways
--pkey bigmap_pkey; C.bigmap; block_level, i --SEQONLY

--FKEY bigmap_block_hash_block_fkey; C.bigmap; block_level; C.block(level); CASCADE --SEQONLY
--FKEY bigmap_operation_id_fkey; C.bigmap; operation_id; C.operation_alpha(autoid); CASCADE --SEQONLY
--FKEY bigmap_sender_fkey; C.bigmap; sender_id; C.address(address_id); CASCADE --SEQONLY
--FKEY bigmap_receiver_fkey; C.bigmap; receiver_id; C.address(address_id); CASCADE --SEQONLY

CREATE INDEX IF NOT EXISTS bigmap_block_hash on C.bigmap using btree (block_level); --SEQONLY --1
CREATE INDEX IF NOT EXISTS bigmap_operation_id on C.bigmap using btree (operation_id); --SEQONLY --1
CREATE INDEX IF NOT EXISTS bigmap_ophid_id on C.bigmap using btree (operation_hash_id); --SEQONLY --1
CREATE INDEX IF NOT EXISTS bigmap_key_hash on C.bigmap using btree (key_hash); --SEQONLY --1
--CREATE INDEX IF NOT EXISTS bigmap_key on C.bigmap using btree ("key"); --SEQONLY --1 -- drop this because some values () are too big (e.g. around block 382287 on hangzhounet)
CREATE INDEX IF NOT EXISTS bigmap_key_type on C.bigmap using btree ("key_type"); --SEQONLY --1
-- CREATE INDEX IF NOT EXISTS bigmap_value_type on C.bigmap using btree ("value_type"); --SEQONLY -- drop this index because some values (eg. from block 1778745 on mainnet) are too big to be indexed (since v9.7.7)
CREATE INDEX IF NOT EXISTS bigmap_id on C.bigmap using btree (id); --SEQONLY --1
CREATE INDEX IF NOT EXISTS bigmap_sender on C.bigmap using btree (sender_id); --SEQONLY --1
CREATE INDEX IF NOT EXISTS bigmap_receiver on C.bigmap using btree (receiver_id); --SEQONLY --1
CREATE INDEX IF NOT EXISTS bigmap_metadata on C.bigmap using GIN(metadata); --SEQONLY --1

-- the following index is for insertion performance
CREATE INDEX IF NOT EXISTS bigmap_id_annots on C.bigmap using btree(id, annots); --SEQONLY --1

--OPT CREATE INDEX IF NOT EXISTS bigmap_key_type on C.bigmap using btree ("key_type"); --SEQONLY
--OPT CREATE INDEX IF NOT EXISTS bigmap_value_type on C.bigmap using btree ("value_type"); --SEQONLY
--CREATE INDEX IF NOT EXISTS bigmap_strings on C.bigmap using GIN (strings); --SEQONLY

CREATE INDEX IF NOT EXISTS bigmap_uri on C.bigmap using GIN (uri); --SEQONLY --1
CREATE INDEX IF NOT EXISTS bigmap_contracts on C.bigmap using GIN (contracts); --SEQONLY --1

------------------------
-- metadata extraction
CREATE OR REPLACE FUNCTION jsonb_of_text (t text)
RETURNS jsonb
AS $$
DECLARE r jsonb; --err text;
BEGIN
  perform (t::int);
  RETURN NULL;
EXCEPTION WHEN OTHERS THEN
  -- GET STACKED DIAGNOSTICS err = MESSAGE_TEXT;
  -- raise '%', err;
  BEGIN
    perform (t::bool);
    RETURN NULL;
  EXCEPTION WHEN OTHERS THEN
    BEGIN
      r := (SELECT t::jsonb);
      RETURN r;
      EXCEPTION WHEN OTHERS THEN RETURN NULL;
    END;
  END;
END;
$$ LANGUAGE PLPGSQL;

CREATE OR REPLACE FUNCTION extract_jsonb_from_text_array (t text[])
RETURNS jsonb[]
AS $$
BEGIN
RETURN (WITH stuff AS (SELECT jsonb_of_text(x) AS j FROM UNNEST(t) x)
        SELECT array_agg(j) FROM stuff WHERE j IS NOT NULL);
END; $$ LANGUAGE PLPGSQL;


CREATE OR REPLACE FUNCTION B.get_by_id_with_key (xid bigint, xkey_hash char)
RETURNS TABLE (id bigint, "key" jsonb, key_hash char, key_type jsonb, "value" jsonb, value_type jsonb, block_level int, i bigint, metadata jsonb)
AS $$
 SELECT id, "key", key_hash, key_type, "value", value_type, block_level as block_level, i, metadata
 FROM C.bigmap b
 WHERE (b.id, b.key_hash) = (xid, xkey_hash)
 ORDER BY i DESC
 LIMIT 1
$$ LANGUAGE SQL STABLE;

CREATE OR REPLACE FUNCTION B.get_by_id (xid bigint)
RETURNS TABLE (id bigint, "key" jsonb, key_hash char, key_type jsonb, "value" jsonb, value_type jsonb, block_level int, i bigint, metadata jsonb)
AS $$
WITH r AS
(SELECT DISTINCT b.key_hash FROM C.bigmap b WHERE b.id = xid)
SELECT B.get_by_id_with_key(xid, key_hash) FROM r WHERE (SELECT 1 FROM C.bigmap b WHERE b.id = xid AND kind = 2) IS NULL;
$$ LANGUAGE SQL STABLE;

CREATE OR REPLACE FUNCTION B.get_by_key_hash (xkey_hash char) -- results are undefined if xkey_hash is null
returns table (id bigint, "key" jsonb, key_hash char, key_type jsonb, "value" jsonb, value_type jsonb, block_level int, i bigint, metadata jsonb)
as $$
DECLARE xid bigint := (select b.id from C.bigmap b where b.key_hash = xkey_hash order by b.block_level desc, b.i desc limit 1);
BEGIN
RETURN QUERY
with r as
(select * from B.get_by_id(xid))
select * from r where r.key_hash = xkey_hash;
END
$$ language PLPGSQL;


CREATE OR REPLACE FUNCTION B.assoc (xid bigint, xkey jsonb)
returns table ("key" jsonb, "value" jsonb, block_hash char)
as $$
select "key", "value", block_hash(block_level)
from C.bigmap
where id = xid and "key" = xkey
and block_level = (select b.block_level from C.bigmap b where xkey = b.key order by b.block_level desc limit 1);
$$ language SQL stable;


CREATE OR REPLACE FUNCTION B.update (xid bigint, xkey jsonb, xkey_hash char, xvalue jsonb, xblock_hash int, xsender bigint, xreceiver bigint, xi bigint, opaid bigint, iorid int, xstrings text[], max_length smallint, ophid bigint)
returns void
as $$
insert into C.bigmap (id, "key", key_hash, "value", block_level, sender_id, receiver_id, i, operation_id, operation_hash_id, implicit_operations_results_id, kind, strings
  , uri, contracts, metadata --SEQONLY
)
values (xid, xkey, xkey_hash, xvalue, xblock_hash, xsender, xreceiver, xi, opaid, ophid, iorid, 1, xstrings
  , (select extract_uris(xstrings, max_length)) --SEQONLY
  , (select extract_contracts(xstrings, opaid)) --SEQONLY
  , (select (extract_jsonb_from_text_array(xstrings))[0] FROM C.bigmap x WHERE xid = x.id AND annots = '%metadata' limit 1) --SEQONLY
)
on conflict do nothing; --SEQONLY
$$ language SQL;


CREATE OR REPLACE FUNCTION B.clear (xid bigint, xblock_level int, xsender bigint, xreceiver bigint, xi bigint, opaid bigint, iorid int, ophid bigint)
returns void
as $$
insert into C.bigmap (id, "key", key_hash, "value", block_level, sender_id, receiver_id, i, operation_id, operation_hash_id, implicit_operations_results_id, kind)
values (xid, null, null, null, xblock_level, xsender, xreceiver, xi, opaid, ophid, iorid, 2)
on conflict do nothing; --SEQONLY
$$ language SQL;


CREATE OR REPLACE FUNCTION B.alloc (xid bigint, xkey_type jsonb, xvalue_type jsonb, xblock_level int, xsender bigint, xreceiver bigint, xi bigint, opaid bigint, iorid int, xannots text, ophid bigint)
returns void
as $$
insert into C.bigmap (id, "key_type", value_type, block_level, sender_id, receiver_id, i, operation_id, operation_hash_id, implicit_operations_results_id, kind, annots)
values (xid, xkey_type, xvalue_type, xblock_level, xsender, xreceiver, xi, opaid, ophid, iorid, 0, xannots)
-- on conflict do nothing; --SEQONLY
$$ language SQL;
